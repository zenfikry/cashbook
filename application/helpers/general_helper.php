<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function check_access($menu_id = '', $action = '')
{
    $CI = &get_instance();

    if (!$CI->session->userdata('login')) {
        show_404();
        die();
    }

    $CI->db->select("a." . $action . "_flag AS action");
    $CI->db->from("app_menu_per_role a");
    $CI->db->join("app_menu_lists b", "a.menu_id = b.menu_id AND b.active=1");
    $arrWHere = array(
        'a.role_id' => $CI->session->userdata('role_id'),
        'b.menu_id' => $menu_id,
        'a.read_flag' => 1,
    );
    $CI->db->where($arrWHere);
    $rs = $CI->db->get();

    if ($rs->row() != null) {
        return array('result' => true, 'data' => $rs->row_array());
    } else {
        return array('result' => false, 'data' => null);
    }
}
function uploadDataImg($file, $filename, $directory)
{
    $CI = &get_instance();

    $config['upload_path'] = $directory;
    $config['file_name'] = $filename;
    $config['allowed_types'] = 'gif|jpeg|jpg|png|pdf|xls|xlsx';
    $config['max_size'] = 204800;
    $config['max_width'] = 8000;
    $config['max_height'] = 8000;

    $CI->load->library('upload', $config);

    $CI->upload->initialize($config);
    if (!$CI->upload->do_upload($file)) {
        $error = array('upload_status' => false, 'error' => $CI->upload->display_errors());
        return $error;
    } else {
        $data = array('upload_status' => true, 'upload_data' => $CI->upload->data());
        return $data;
    }
}

function uploadDataExcel($file, $filename, $directory)
{
    $CI = &get_instance();

    $config['upload_path'] = $directory;
    $config['file_name'] = $filename;
    $config['allowed_types'] = 'xls|xlsx';
    $config['max_size'] = 204800000000;

    $CI->load->library('upload', $config);

    $CI->upload->initialize($config);
    if (!$CI->upload->do_upload($file)) {
        $error = array('upload_status' => false, 'error' => $CI->upload->display_errors());
        return $error;
    } else {
        $data = array('upload_status' => true, 'upload_data' => $CI->upload->data());
        return $data;
    }
}

function uploadDataPdf($file, $filename, $directory)
{
    $CI = &get_instance();

    $config['upload_path'] = $directory;
    $config['file_name'] = $filename;
    $config['allowed_types'] = 'pdf';
    $config['max_size'] = 204800000000;

    $CI->load->library('upload', $config);

    $CI->upload->initialize($config);
    if (!$CI->upload->do_upload($file)) {
        $error = array('upload_status' => false, 'error' => $CI->upload->display_errors());
        return $error;
    } else {
        $data = array('upload_status' => true, 'upload_data' => $CI->upload->data());
        return $data;
    }
}

function unformat_numeric($value)
{
    $locale = "id-ID|id_ID";
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $locale = explode("|", $locale);
        $locale = $locale[1];
    }

    $formatter = new NumberFormatter($locale, NumberFormatter::DECIMAL);
    $formatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 42);
    $formatter->setSymbol(NumberFormatter::GROUPING_SEPARATOR_SYMBOL, ".");
    $formatter->setSymbol(NumberFormatter::DECIMAL_SEPARATOR_SYMBOL, ",");
    return str_replace(",", ".", $formatter->parse($value));
}

function date_convert_format($value, $fromFromat = 'd/m/Y', $toFormat = 'Y-m-d')
{  
    
    return DateTime::createFromFormat($fromFromat, $value)->format($toFormat);
}

function dd($data, $useExit = true)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    echo "<br/>";
    if ($useExit) {
        exit;
    }
}

function array_format_data($data, $arrColumns = [], $arrType = [], $skipLastRow = false)
{
    if ((empty($data) && !$skipLastRow) || ((count($data) - 1) == 0 && $skipLastRow)) {
        return $data;
    }

    $length = count($data);

    foreach ($data as $idx => $row) {
        if ($skipLastRow && $idx >= ($length - 1)) {
            $data[$idx] = $row;
            continue;
        }
        foreach ($arrColumns as $key => $value) {
            if ($arrType[$key] == 'number') {
                $data[$idx][$value] = number_format($row[$value], 0, ",", ".");
            } else if ($arrType[$key] == 'date') {
                $data[$idx][$value] = date('d/m/Y', strtotime($row[$value]));
            }
        }
    }

    return $data;
}

function array_format_data_item($data, $arrColumns = [], $arrType = [], $skipLastRow = false)
{
    if ((empty($data) && !$skipLastRow) || ((count($data) - 1) == 0 && $skipLastRow)) {
        return $data;
    }

    $length = count($data);

    foreach ($data as $idx => $row) {
        if ($skipLastRow && $idx >= ($length - 1)) {
            $data[$idx] = $row;
            continue;
        }
        foreach ($arrColumns as $key => $value) {
            if ($arrType[$key] == 'number') {
                $data[$idx][$value] = number_format($row[$value], 0, ",", ".");
            } else if ($arrType[$key] == 'date') {
                if (check_arr_empty_or_null($data[$idx][$value]) || date("Y-m-d", strtotime($data[$idx][$value])) == "1900-01-01" || $data[$idx]['use_expire'] == 0) {
                    $data[$idx][$value] = "";
                } else {
                    $data[$idx][$value] = date('d/m/Y', strtotime($row[$value]));

                }
            }
        }
    }

    return $data;
}

function get_row_values($select_str = '', $table = '', $param, $value_param, $return_as_array = true, $use_where_in = false)
{
    $CI = &get_instance();

    if (is_array($param) && is_array($value_param)) {
        $CI->db->select($select_str)->from($table);
        foreach ($param as $key => $value) {
            $CI->db->where($value, $value_param[$key]);
        }
        $get = $CI->db->get();
    } else {
        $CI->db->select($select_str);
        $CI->db->from($table);
        $use_where_in ? $CI->db->where_in($param, $value_param) : $CI->db->where($param, $value_param);
        $get = $CI->db->get();
    }

    if ($return_as_array) {
        return $get->row_array();
    } else {
        return $get->row();
    }
}

function get_rows_values($select_str = '', $table = '', $param, $value_param, $return_as_array = true, $use_where_in = false)
{
    $CI = &get_instance();

    if (is_array($param) && is_array($value_param)) {
        $CI->db->select($select_str)->from($table);
        foreach ($param as $key => $value) {
            $CI->db->where($value, $value_param[$key]);
        }
        $get = $CI->db->get();
    } else {
        $CI->db->select($select_str);
        $CI->db->from($table);
        $use_where_in ? $CI->db->where_in($param, $value_param) : $CI->db->where($param, $value_param);
        $get = $CI->db->get();
    }

    if ($return_as_array) {
        return $get->result_array();
    } else {
        return $get->result();
    }
}

function check_arr_empty_or_null($param)
{
    if ($param != null || trim($param) != '' || !empty($param)) {
        return false;
    } else {
        return true;
    }
}

function check_arr_empty_or_null_and_type($input = [], $params = [], $type = [], $required = [])
{
    # Cek jika data yang diparsing kosong
    if (check_arr_empty_or_null($input)) {
        return [
            'result' => true,
            'data' => null,
            'message' => "Data not found",
        ];
    }
    # Inisialisasi variabel pengecekan
    $hasil = 'clear';
    $check_tipe_data = true;
    # Looping pengecekan input
    foreach ($params as $key => $value) {
        # Restore $check_tipe_data
        $check_tipe_data = true;
        # Cek array exist
        if ($required[$key] && !array_key_exists($value, $input)) {
            $hasil = $value . " not found";
            break;
        } else {
            $hasil = 'clear';
            # buat index baru jika key array tidak ada dengan nilai NULL
            if (!array_key_exists($value, $input)) {
                $input[$value] = null;
                $check_tipe_data = false;
            }
        }
        # Cek jika value tidak ada
        if ($required[$key] && ($input[$value] == null || trim($input[$value]) == '' || empty($input[$value]))) {
            $hasil = $value . " is required";
            break;
        } else {
            $hasil = 'clear';
        }
        # Cek jika tipe data benar
        if ($check_tipe_data) {
            $check_data_type = check_data_type($value, $input[$value], $type[$key]);
            if (!is_bool($check_data_type)) {
                $hasil = $check_data_type;
                break;
            } else {
                $hasil = 'clear';
            }
        }
    }

    if ($hasil == 'clear') {
        $data2return = [
            'result' => false,
            'data' => $input,
            'message' => 'Check done',
        ];
    } else {
        $data2return = [
            'result' => true,
            'data' => null,
            'message' => $hasil,
        ];
    }

    return $data2return;
}

function check_data_type($key_name, $value, $type)
{
    $result = true;
    switch ($type) {
        case 'numeric':
            $cek = is_numeric(unformat_numeric($value));
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'integer':
            $cek = is_integer((int) unformat_numeric($value));
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'float':
            $cek = is_float((float) unformat_numeric($value));
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'string':
            $cek = is_string($value);
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'boolean':
            $cek = is_bool($value);
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'date':
            $cek = check_date($value, 'Y-m-d');
            $result = $cek ? true : $key_name . " must be type of " . $type . " ('yyyy-mm-dd')";
            break;

        case 'datetime':
            $cek = check_date($value, 'Y-m-d H:i:s');
            $result = $cek ? true : $key_name . " must be type of " . $type . " ('yyyy-mm-dd hh:mm:ss')";
            break;

        case 'mac_address':
            $cek = filter_var($value, FILTER_VALIDATE_MAC);
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'email':
            $cek = filter_var($value, FILTER_VALIDATE_EMAIL);
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        case 'ip_address':
            $cek = filter_var($value, FILTER_VALIDATE_IP);
            $result = $cek ? true : $key_name . " must be type of " . $type;
            break;

        default:
            $result = false;
            break;
    }

    return $result;
}

function check_date($value, $format = 'Y-m-d H:i:s')
{
    if (DateTime::createFromFormat($format, $value) !== false) {
        return true;
    } else {
        return false;
    }
}

function pdfKopSurat($pdf, $store_id = null)
{
    $CI = &get_instance();

    // Ambil alamat
    $alamat = '';
    if (!check_arr_empty_or_null($store_id)) {
        $rec = get_row_values("alamat, no_hp, no_telp", "m_store", "store_id", $store_id);
        if ($rec != null) {
            $alamat = $rec['alamat'];
        }
    } else {
        $rec = get_row_values("alamat, no_hp, no_telp", "m_store", "store_id", $CI->session->userdata('store_id'));
        $alamat = $rec['alamat'];
    }

    $pdf->SetFont('helvetica', 'B', 16);
    // $pdf->Cell(25, 5, $pdf->Image('./assets/images/logo_nabawi.png', $pdf->GetX(), $pdf->GetY(), 20, 20), 0, 0, 'L');
    $pdf->Image('./assets/images/logo_nabawi.png', $pdf->GetX(), $pdf->GetY(), 20, 20);
    $pdf->Cell(25, 5, "", 0, 0, 'L');
    $pdf->Cell(175, 5, "NABAWI HERBAL", 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(25, 5, "", 0, 0, 'L');
    $pdf->MultiCell(175, 5, $alamat, 0, 'L', 0, 1, '', '', false, 0, false, true, 10, 'T');
    $pdf->Cell(25, 5, "", 0, 0, 'L');
    $pdf->Cell(175, 5, "Phone : " . $rec['no_hp'] . (check_arr_empty_or_null($rec['no_telp']) ? "" : ", " . $rec['no_telp']), 0, 0, 'L');
    $pdf->Ln(10);
}

function pdfCellWrap($pdf, $arrWidth, $height = 5, $values, $border = 1, $arrAlign, $fillCell = 0, $lineBreak = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false)
{
    // Variabel $cellcount untuk menampung nilai height pada setiap MultiCell
    $cellcount = [];
    // Ambil posisi X = Horizontal, Y = Vertikal
    $startX = $pdf->GetX();
    $startY = $pdf->GetY();

    // Set font color white (CMYK)
    $pdf->SetTextColor(0, 0, 0, 0);

    // Print MultiCell tanpa border + tanpa text dan ambil berapa banyak row yang digunakan pada MultiCell tersebut, simpan ke variabel $cellcount
    $key = 0;
    foreach ($values as $value):
        $cellcount[] = $pdf->MultiCell($arrWidth[$key], $height, $value, 0, $arrAlign[$key], $fillCell, $lineBreak, $x, $y, $reseth, $stretch, $ishtml, $autopadding, $maxh, $valign, $fitcell);
        $key++;
    endforeach;

    // Set font color black (RGB)
    $pdf->SetTextColor(0, 0, 0);

    // Terapkan posisi X dan Y
    $pdf->SetXY($startX, $startY);

    // Ambil nilai tertinggi pada $maxnocells
    $maxnocells = max($cellcount);

    // Print MultiCell tanpa border dan ambil berapa banyak row yang digunakan pada MultiCell tersebut, simpan ke variabel $cellcount
    $key = 0;
    foreach ($values as $value):
        $newHeight = $maxnocells > 1 ? ($maxnocells * ($height / 1.35)) : $height;
        $break = $lineBreak == 1 || $lineBreak ? 1 : (($key == count($values) - 1) ? 1 : 0);
        $pdf->MultiCell($arrWidth[$key], $newHeight, $value, $border, $arrAlign[$key], $fillCell, $break, $x, $y, $reseth, $stretch, $ishtml, $autopadding, $newHeight, $valign, $fitcell);
        $key++;
    endforeach;
}

function get_hpp($store_id, $item_id, $hpp_type = 'bulan', $tahun = null, $bulan = null)
{
    /**
     * $hpp_type :
     * bulan = Mengambil nilai hpp pada tabel m_items_hpp
     * fluktuatif = Mengambil nilai hpp pada tabel m_items_hpp
     *
     */

    $CI = &get_instance();

    $tahun = $tahun == null ? date('Y') : $tahun;
    $bulan = $bulan == null ? date('m') : $bulan;

    if ($hpp_type == 'bulan') {
        $CI->db->select("IFNULL(hpp, 0) AS hpp");
        $CI->db->from("m_items_hpp");
        $CI->db->where([
            "store_id" => $store_id,
            "tahun" => $tahun,
            "bulan" => $bulan,
            "item_id" => $item_id,
        ]);
        $result = $CI->db->get()->row();

        return $result != null ? $result->hpp : 0;
    } else if ($hpp_type == 'fluktuatif') {
        $CI->db->select("IFNULL(b.hpp, 0) AS hpp");
        $CI->db->from("m_store a");
        $CI->db->join("m_items_price b", "b.wil_id=a.wil_id", "left");
        $CI->db->where("a.store_id", $store_id);
        $CI->db->where("b.item_id", $item_id);
        $result = $CI->db->get()->row();

        return $result != null ? $result->hpp : 0;
    }
}

function get_hpp_per_tanggal($store_id, $item_id, $tanggal_expire)
{
    $CI = &get_instance();

    $CI->db->select("IFNULL(hpp, 0) AS hpp");
    $CI->db->from("inv_on_hand");
    $CI->db->where([
        "store_id" => $store_id,
        "item_id" => $item_id,
        "DATE(expired_date)" => $tanggal_expire,
    ]);
    $result = $CI->db->get()->row();

    return $result != null ? $result->hpp : 0;
}

function kalkulasi_hpp($store_id, $tanggal, $item_id, $item_qty, $item_value, $item_total, $tanggal_expire)
{
    /**
     * Proses kalkulasi HPP average
     * Tabel yang terlibat :
     * inv_on_hand = Tabel yang digunakan untuk menyimpanan stok on hand dan HPP per expired date
     * m_items_hpp = Tabel yang digunakan untuk menyimpan data HPP per bulan
     * m_items_price (Kolom hpp) = Digunakan untuk menyimpan nilai HPP yang sedang terjadi (Fluktuatif)
     *
     */

    $CI = &get_instance();

    # Pisahkan tahun dan bulan
    $tahun = date("Y", strtotime($tanggal));
    $bulan = date("m", strtotime($tanggal));

    # Ambil wil_id
    $wil_id = get_row_values("wil_id", "m_store", "store_id", $store_id)['wil_id'];

    /* START Hitung HPP fluktuatif */
    # Get saldo fluktuatif
    $persediaan_fluktuatif_hpp = get_hpp($store_id, $item_id, 'fluktuatif');
    $persediaan_fluktuatif_qty_saldo = $CI->db->select("IFNULL(SUM(tot_qty), 0) AS tot_qty")->from("inv_on_hand")->where(["store_id" => $store_id, "item_id" => $item_id])->get()->row_array();
    $persediaan_fluktuatif_value_saldo = $persediaan_fluktuatif_qty_saldo['tot_qty'] * $persediaan_fluktuatif_hpp;
    $persediaan_fluktuatif_hpp_baru = ($persediaan_fluktuatif_value_saldo + $item_total) / ($persediaan_fluktuatif_qty_saldo['tot_qty'] + $item_qty);
    # Cek jika HPP item pada wilayah tersebut
    $rec = get_row_values("hpp", "m_items_price", ["wil_id", "item_id"], [$wil_id, $item_id]);
    # Update tabel m_items_price jika hpp tersedia
    if ($rec != null) {
        $CI->db->update(
            "m_items_price",
            [
                'hpp' => $persediaan_fluktuatif_hpp_baru,
                'modified_by' => $CI->session->userdata('user_id'),
                'modification_date' => date('Y-m-d H:i:s'),
            ],
            [
                'wil_id' => $wil_id,
                'item_id' => $item_id,
            ]
        );
    } else {
        $CI->db->insert("m_items_price", [
            "wil_id" => $wil_id,
            "item_id" => $item_id,
            "hbeli" => $item_value,
            "hpp" => $persediaan_fluktuatif_hpp_baru,
            "created_by" => $CI->session->userdata('user_id'),
            "creation_date" => date('Y-m-d H:i:s'),
        ]);
    }
    /* END Hitung HPP fluktuatif */

    /* START Hitung HPP per periode/bulan */
    # Cek HPP bulanan, jika tidak ada insert. Jika ada maka update
    $persediaan_periode_hpp = get_row_values("hpp", "m_items_hpp", ["store_id", "tahun", "bulan", "item_id"], [$store_id, $tahun, $bulan, $item_id]);
    if ($persediaan_periode_hpp == null) {
        $CI->db->insert("m_items_hpp", [
            "store_id" => $store_id,
            "tahun" => $tahun,
            "bulan" => $bulan,
            "item_id" => $item_id,
            "hpp" => $persediaan_fluktuatif_hpp_baru,
            'created_by' => $CI->session->userdata('user_id'),
            'creation_date' => date('Y-m-d H:i:s'),
        ]);
    } else {
        $CI->db->update("m_items_hpp", [
            "hpp" => $persediaan_fluktuatif_hpp_baru,
            'modified_by' => $CI->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        ], [
            "store_id" => $store_id,
            "tahun" => $tahun,
            "bulan" => $bulan,
            "item_id" => $item_id,
        ]);
    }
    /* END Hitung HPP per periode/bulan */

    /* START Hitung HPP expired date */
    # Get saldo per tanggal
    $persediaan_per_tanggal_hpp = get_row_values("IFNULL(hpp, 0) AS hpp", "inv_on_hand", ["store_id", "item_id", "DATE(expired_date)"], [$store_id, $item_id, $tanggal_expire]);
    $persediaan_per_tanggal_qty_saldo = $CI->db->select("IFNULL(SUM(tot_qty), 0) AS tot_qty")->from("inv_on_hand")->where(["store_id" => $store_id, "item_id" => $item_id, "DATE(expired_date)" => $tanggal_expire])->get()->row_array();
    $persediaan_per_tanggal_value_saldo = $persediaan_per_tanggal_qty_saldo['tot_qty'] * $persediaan_per_tanggal_hpp['hpp'];
    $persediaan_per_tanggal_hpp_baru = ($persediaan_per_tanggal_value_saldo + $item_total) / ($persediaan_per_tanggal_qty_saldo['tot_qty'] + $item_qty);
    # Cek HPP per tanggal, jika tidak ada insert. Jika ada maka update
    if ($persediaan_per_tanggal_hpp == null) {
        $CI->db->insert("inv_on_hand", [
            'store_id' => $store_id,
            'item_id' => $item_id,
            'hpp' => $persediaan_per_tanggal_hpp_baru,
            'expired_date' => $tanggal_expire,
            'modified_by' => $CI->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        ]);
    } else {
        $CI->db->update("inv_on_hand", [
            "hpp" => $persediaan_per_tanggal_hpp_baru,
            'modified_by' => $CI->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        ], [
            'store_id' => $store_id,
            'item_id' => $item_id,
            'expired_date' => $tanggal_expire,
        ]);
    }
    /* END Hitung HPP expired date */
}

function searchIndexFromArrayWithKey($arrData, $field, $fieldValue, $returnIndexKey = true, $returnField = null)
{
    foreach ($arrData as $key => $value) {
        if ($value[$field] == $fieldValue) {
            if ($returnIndexKey) {
                return $key;
            } else {
                return $value[$returnField];
            }
        }

    }
    return null;
}

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

function get_wil_by_store_id($store_id)
{
    $CI = &get_instance();

    $rs = $CI->db->select('wil_id')->from("m_store")->where("store_id", $store_id)->get()->row();
    if ($rs != null) {
        return $rs->wil_id;
    } else {
        return null;
    }
}

function thousandsCurrencyFormat($num)
{

    if ($num > 1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

    }

    return $num;
}

function shortNumber($num)
{
    $units = ['', 'K', 'M', 'B', 'T'];
    for ($i = 0; $num >= 1000; $i++) {
        $num /= 1000;
    }
    return round($num, 1) . $units[$i];
}

function number_format_convert($number){
   
    $b = str_replace( '.', '', $number );

    if( is_numeric( $b ) ) {
        $a = $b;
    }
    return $b;
}


function date_tomysql($mydate){ 
    $date = new DateTime($mydate);
    return $date->format('Y-m-d'); 
}
function datesql_to_datefilter($mydate,$format="d/m/Y"){
    $date = new DateTime($mydate);
    return $date->format($format); // 31.07.2012
}

function datesql_to_datefilterstrip($mydate){
    $date = DateTime::createFromFormat('d/m/Y', $mydate);    
    return $date->format('d-m-Y'); // 31.07.2012
}

function datetobridge($mydate,$mydate2){
    $date = new DateTime($mydate);
    $date2 = new DateTime($mydate2);
    $rs1 =$date->format('d M y'); // 31.07.2012
    $rs2 = $date2->format('d M y'); // 31.07.2012
    return $rs1." - ".$rs2;
}
function datedocbridge($mydate){
    $date = new DateTime($mydate);
    $date->add(new DateInterval('P1D'));
    $rs1 =$date->format('Y-m-d'); // 31.07.2012
    return $rs1;
}

function datePIbridge($mydate){
    $date = new DateTime($mydate);
    // $date->add(new DateInterval('P1D'));
    $rs1 =$date->format('Y-m-d'); // 31.07.2012
    return $rs1;
}




function date_beetween_periode($strDateFrom,$strDateTo){
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange = [];

    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

    if ($iDateTo >= $iDateFrom) {
        array_push($aryRange, date('d/m/Y', $iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo) {
            $iDateFrom += 86400; // add 24 hours
            array_push($aryRange, date('d/m/Y', $iDateFrom));
        }
    }
    return $aryRange;
}