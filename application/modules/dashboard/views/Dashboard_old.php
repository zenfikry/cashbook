<style type="text/css">
img {
    display: block;
}

.thumbnail {
    position: relative;
    display: inline-block;
}

.caption {
    position: absolute;
    top: 60%;
    left: 55%;
    transform: translate( -50%, -50% );
    color: black;
    font-size: 15px;
    /*font-weight: bold;*/
}
.caption_image {
    position: absolute;
    top: 59%;
    left: 21%;
    height: 150px;
    transform: translate( -50%, -50% );
    color: black;
    font-size: 15px;
    /*font-weight: bold;*/
}
</style>
<div class="row">
    <div class="col-md-6">
        <div class="page-title-box">
            <h4>Dashboard</h4>
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <!-- <li class="breadcrumb-item active">Selamat Datang</li> -->
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="page-title-box">
            </div>
        </div>
    </div>
</div>
<div class="row">
<?php if ($this->session->userdata('role_id') == 'SISWA') { ?>
    <?php 
    $get_info_siswa = $this->db->get_where('m_rombel_upload',array("nis"=>$this->session->userdata('user_id')))->row();

    // print_r($get_info_siswa);
     ?>
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <div class="page-title-box">
                    <?php if (file_exists('./assets/data_kp/'.$this->session->userdata('user_id').'.jpg')): ?>
                        <img src="<?php echo base_url('assets/data_kp/'.$this->session->userdata('user_id').'.jpg') ?>" class="img-fluid">
                    <?php else: ?>
                        <div class="thumbnail">
                          <img src="<?php echo base_url('assets/data_kp/nis.png') ?>" alt="" class="img-fluid">
                          <div class="caption_image">
                              <img src="<?php echo base_url('assets/data_upload/ppdb/'.$get_info_siswa->foto_siswa) ?>" alt="" class="img-fluid" style="width:80%;">
                              <br>  
                          </div>
                          <div class="caption">
                              <span>Nama : <br><b><?php echo $get_info_siswa->nama_lengkap; ?></b></span>
                              <br>  
                              <span>NIS : <b><?php echo $get_info_siswa->nis; ?></b></span>
                              <br>
                              <span>TTL : <b><?php echo $get_info_siswa->tempat_lahir.', '.date("d F Y", strtotime($get_info_siswa->tanggal_lahir)); ?></b></span>
                              <br>
                              <span>Jenis Kelamin : <b><?php echo ($get_info_siswa->jenis_kelamin == 1) ? 'Laki-laki' : 'Perempuan' ; ?></b></span>
                              <br>
                              <span>NPSN : <b></b></span>
                          </div>
                      </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Informasi Terbaru</h4>
                <ol class="activity-feed mb-0">
                <?php foreach ($this->db->get('m_berita',0,5)->result() as $key => $value): ?>
                    <li class="feed-item">
                        <div class="feed-item-list">
                            <span class="date"><?php echo $value->creation_date ?></span>
                            <span class="activity-text"><?php echo $value->judul ?><br><?php echo $value->isi_berita ?></span>
                        </div>
                    </li>
                <?php endforeach ?>
                </ol>
            </div>
        </div>

    </div>
<?php } else { ?>
    <div class="col-xl-12 col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Informasi Terbaru</h4>

                <ol class="activity-feed mb-0">
                <?php foreach ($this->db->get('m_berita',0,5)->result() as $key => $value): ?>
                    <li class="feed-item">
                        <div class="feed-item-list">
                            <span class="date"><?php echo $value->creation_date ?></span>
                            <span class="activity-text"><?php echo $value->judul ?><br><?php echo $value->isi_berita ?></span>
                        </div>
                    </li>
                <?php endforeach ?>
                </ol>
            </div>
        </div>

    </div>
<?php } ?>
</div>
<script src="assets/libs/morris.js/morris.min.js"></script>
<script src="assets/libs/raphael/raphael.min.js"></script>
<script src="assets/js/pages/dashboard.init.js"></script>