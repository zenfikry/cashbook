<style type="text/css">
   .disable {
      background: #DDD !Important;
   }

   #po-form-selected {
      display: none;
   }
</style>

<div class="main">
   <div class="row mt-3">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <form id="form_filter" name="form_filter">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="card" style="border:solid 1px #3EB7BA;">
                           <div class="card-header bg-primary text-white">
                              <div class="row">
                                 <div class="col-lg-12">
                                    Filter Laporan Buku Besar
                                 </div>
                              </div>
                           </div>
                           <div class="card-body">
                            <div class="row mb-2">
                                <label for="horizontal-firstname-input" class="col-2 col-form-label">Outlet</label>
                                <div class="col-4">
                                    <select class="form-control" name="store_id" id="store_id">
                                       <option value="">Semua</option>
                                       <?php foreach ($this->db->get('m_store')->result() as $key => $value): ?>
                                          <option value="<?php echo $value->store_id ?>"><?php echo $value->store_name ?></option>
                                       <?php endforeach?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label class="col-2 col-form-label">Periode</label>
                                <div class="col-3">
                                    <div class="input-group date">
                                       <div class="input-daterange input-group" data-date-format="dd M, yyyy"  data-date-autoclose="true"  data-provide="datepicker">
                                          <input type="text" class="form-control" id="filter_start_date" name="filter_start_date" readonly />
                                          <input type="text" class="form-control" id="filter_end_date" name="filter_end_date" readonly />
                                       </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label class="col-2 col-form-label">COA</label>
                                <div class="col-3">
                                    <div class="input-group">
                                       <input type="hidden" name="coa_no" id="coa_no" value="" readonly="readonly" />
                                       <input type="text" class="form-control" placeholder="Pilih COA" name="coa_name" id="coa_name" value="" readonly="readonly" />
                                       <div class="input-group-append">
                                          <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCOA();"><i class="fas fa-search"></i></button>
                                          <button class="btn btn-outline-warning btn-sm waves-effect waves-light" type="button" onclick="LOVCOAClear();"><i class="mdi mdi-close"></i></button>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-10 offset-2">
                                    <button type="button" class="btn btn-outline-danger waves-effect waves-light mr-2" id="btnRepPdf" name="btnRepPdf"><span class="btn-label"><i class="fas fa-file-pdf"></i></span> PDF</button>
                                    <button type="button" class="btn btn-outline-success waves-effect waves-light" id="btnRepXls" name="btnRepXls"><span class="btn-label"><i class="fas fa-file-excel"></i></span> XLS/CSV</button>
                                </div>
                            </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>

   </div>
</div>

<script type="text/javascript">
   // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
   var data2Send = null;
   var action = '<?=$action?>';
   var _id = "<?=$action == 'edit' ? $id : 'null'?>";
   // END VARIABEL WAJIB

   $.getScript('<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.js?v=<?=date('YmdHis') . rand()?>', function( data, textStatus, jqxhr ) {
      initPage();
   });
</script>