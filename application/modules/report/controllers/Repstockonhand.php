<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Repstockonhand extends Core_Controller
{
    private $total_qty = 0;

    public function __construct()
    {
        parent::__construct("repstockonhand"); # parsing menu_id
        $this->load->model("repstockonhand_model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->load->view('Repstockonhand', $this->data);
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Stock On Hand";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "KopSurat";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'item_id_start' => $this->input->post("item_id_start", true),
            'item_id_end' => $this->input->post("item_id_end", true),
            'tampilkan_expire' => $this->input->post("tampilkan_expire", true),
        );

        $data = $this->m_app->get_lists($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(0, 5, $JudulLaporan, 0, 1, 'L');
        $pdf->SetFont('helvetica', '', 8);
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $pdf->Cell(0, 5, "Outlet : " . get_row_values("store_name", "m_store", "store_id", $filter['store_id'])['store_name'], 0, 1, 'L');
        } else {
            $pdf->Cell(0, 5, "Outlet : Semua", 0, 1, 'L');
        }
        if (!check_arr_empty_or_null($filter['item_id_start']) || !check_arr_empty_or_null($filter['item_id_end'])) {
            $pdf->Cell(0, 5, "Item : " . $filter['item_id_start'] . ' s/d ' . $filter['item_id_end'], 0, 1, 'L');
        } else {
            $pdf->Cell(0, 5, "Item : Semua", 0, 1, 'L');
        }
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', '', 8);
        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
					<th width="50">Store ID</th>
               <th width="70">Store Name</th>
               <th width="80">Kode Item</th>
               <th width="200">Nama Item</th>
               ' . ($filter['tampilkan_expire'] == 1 ? '<th align="center" width="50">Tanggal Expire</th>' : '') . '
					<th align="center" width="120">Total Qty</th>
				</tr>
			</thead>
			<tbody>
				' . $this->pdf_render_detail($data->result_array(), $filter) . '
			</tbody>
         <tfoot>
				<tr>
					<th align="center" colspan="' . ($filter['tampilkan_expire'] == 1 ? 5 : 4) . '">TOTAL</th>
					<th align="center" width="120">' . number_format($this->total_qty, 0, ",", ".") . '</th>
				</tr>
			</tfoot>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail($data, $filter)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			   <tr nobr="true">
               <td width="50">' . $value['store_id'] . '</td>
               <td width="70">' . $value['store_name'] . '</td>
				   <td width="80">' . $value['item_id'] . '</td>
               <td width="200">' . $value['item_name'] . '</td>
               ' . ($filter['tampilkan_expire'] == 1 ? '<td align="center" width="50">' . ($value['use_expire'] == 1 ? date('d/m/Y', strtotime($value['expired_date'])) : '') . '</td>' : '') . '
               <td align="center" width="120">' . number_format($value['total_qty'], 0, ",", ".") . '</td>
			</tr>';

            $this->total_qty += $value['total_qty'];
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Stock On Hand";
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'item_id_start' => $this->input->post("item_id_start", true),
            'item_id_end' => $this->input->post("item_id_end", true),
            'tampilkan_expire' => $this->input->post("tampilkan_expire", true),
        );
        $rs = $this->m_app->get_lists_xls($filter);
        # Deklarasi kolom yang akan ditampilkan
        if ($filter['tampilkan_expire'] == 1) {
            $Col['header'] = array('Store ID', 'Store Name', 'Kode Item', 'Nama Item', 'Tanggal Expire', 'Total Qty');
            $Col['type'] = array('string', 'string', 'string', 'string', 'date', 'money');
            $Col['align'] = array('left', 'left', 'left', 'left', 'center', 'left');
        } else {
            $Col['header'] = array('Store ID', 'Store Name', 'Kode Item', 'Nama Item', 'Total Qty');
            $Col['type'] = array('string', 'string', 'string', 'string', 'money');
            $Col['align'] = array('left', 'left', 'left', 'left', 'left');
        }
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
 *                              END DEFAULT FUNCTION                            *
 *******************************************************************************/

}
