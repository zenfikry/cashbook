<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reppembayaran_piutang extends Core_Controller
{
    private $total_potongan = 0;
    private $total_jumlah_bayar = 0;

    public function __construct()
    {
        parent::__construct("reppembayaran_piutan"); # parsing menu_id
        $this->load->model("reppembayaran_piutang_model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->load->view('Reppembayaran_piutang', $this->data);
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Pembayaran Piutang";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'judul_laporan' => $JudulLaporan,
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => $this->input->post("filter_start_date", true),
            'filter_end_date' => $this->input->post("filter_end_date", true),
            'employee' => $this->input->post("employee_name", true),
            'member' => $this->input->post("cust_name", true),
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "PembayaranPiutang";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 28, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => date_convert_format($this->input->post("filter_start_date", true)),
            'filter_end_date' => date_convert_format($this->input->post("filter_end_date", true)),
            'filter_employee_id' => $this->input->post("employee_id", true),
            'filter_cust_id' => $this->input->post("cust_id", true),
        );

        $data = $this->m_app->get_lists($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 8);

        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				' . $this->pdf_render_detail($data->result_array(), $filter) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(142, 6, "TOTAL : ", 'T', 0, 'R');
        $pdf->Cell(29, 6, number_format($this->total_potongan, 0, ",", "."), 'T', 0, 'R');
        $pdf->Cell(29, 6, number_format($this->total_jumlah_bayar, 0, ",", "."), 'T', 0, 'R');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail($data, $filter)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
                <tr nobr="true">
                    <td width="71">' . $value['no_doc'] . '</td>
                    <td width="57" align="center">' . date('d/m/Y', strtotime($value['tgl_trans'])) . '</td>
                    <td width="127">' . $value['member'] . '</td>
                    <td width="71">' . $value['coa_no'] . '</td>
                    <td width="76">' . $value['no_doc_ref'] . '</td>
                    <td width="85" align="right">' . number_format($value['potongan'], 0, ",", ".") . '</td>
                    <td width="85" align="right">' . number_format($value['pembayaran'], 0, ",", ".") . '</td>
                </tr>';

            $this->total_potongan += $value['potongan'];
            $this->total_jumlah_bayar += $value['pembayaran'];
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Pembayaran Piutang";
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => date_convert_format($this->input->post("filter_start_date", true)),
            'filter_end_date' => date_convert_format($this->input->post("filter_end_date", true)),
            'filter_employee_id' => $this->input->post("employee_id", true),
            'filter_cust_id' => $this->input->post("cust_id", true),
        );
        $rs = $this->m_app->get_lists($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('No. Pembayaran', 'Tanggal', 'Member', 'Cara Bayar', 'No. Transaksi', 'Potongan', 'Jumlah Pembayaran');
        $Col['type'] = array('string', 'date', 'string', 'string', 'string', 'money', 'money');
        $Col['align'] = array('left', 'center', 'left', 'center', 'left', 'left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
 *                              END DEFAULT FUNCTION                            *
 *******************************************************************************/

}
