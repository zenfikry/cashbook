<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reppiutang_edar extends Core_Controller
{
    public function __construct()
    {
        parent::__construct("reppiutang_edar"); # parsing menu_id
        $this->load->model("reppiutang_edar_model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->load->view('Reppiutang_edar', $this->data);
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Piutang Beredar";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'judul_laporan' => $JudulLaporan,
            'store_id' => $this->input->post("store_id", true),
            'tanggal_periode' => $this->input->post("tanggal_periode", true),
            'tipe_pembayaran' => check_arr_empty_or_null($this->input->post("tipe_pembayaran", true)) ? "Semua" : $this->input->post("tipe_pembayaran", true),
            'employee' => $this->input->post("employee_name", true),
            'member' => $this->input->post("cust_name", true),
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "PiutangBeredar";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 28, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'tanggal_periode' => date_convert_format($this->input->post("tanggal_periode", true)),
            'tipe_pembayaran' => $this->input->post("tipe_pembayaran", true),
            'filter_employee_id' => $this->input->post("employee_id", true),
            'filter_cust_id' => $this->input->post("cust_id", true),
        );

        $data = $this->m_app->get_lists($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 8);

        $html_customer_list = '';

        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				' . $this->pdf_render_detail($data->result_array(), $filter) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail($data, $filter)
    {
        $html = '';
        $cust_id = null;
        $sub_total_nilai_kredit = 0;
        $sub_total_sisa_kredit = 0;
        foreach ($data as $key => $value) {
            if ($cust_id == null || $cust_id != $value['cust_id']) {
                if ($cust_id != null && $cust_id != $value['cust_id']) {
                    $html .= '
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="3" align="right" style="height:5px">' . str_repeat("-", 105) . '</td>
                            </tr>
                            <tr nobr="true">
                                <td colspan="4" align="right"><b>Sub Total :</b></td>
                                <td width="115" align="right"><b>' . number_format($sub_total_nilai_kredit, 0, ",", ".") . '</b></td>
                                <td width="115" align="right"><b>' . number_format($sub_total_sisa_kredit, 0, ",", ".") . '</b></td>
                            </tr>';
                    $sub_total_nilai_kredit = 0;
                    $sub_total_sisa_kredit = 0;
                }
                $cust_id = $value['cust_id'];
                $html .= '
                    <tr nobr="true">
                        <td width="99"><b>' . $value['cust_id'] . '</b></td>
                        <td colspan="5"><b>' . $value['cust_name'] . '</b></td>
                    </tr>';
            }
            $html .= '
                <tr nobr="true">
                    <td width="99">' . $value['no_doc'] . '</td>
                    <td width="57" align="center">' . date('d/m/Y', strtotime($value['tgl_trans'])) . '</td>
                    <td width="57" align="center">' . date('d/m/Y', strtotime($value['tgl_jatuh_tempo'])) . '</td>
                    <td width="70" align="center">' . $value['type_payment'] . '</td>
                    <td width="115" align="right">' . number_format($value['nilai_kredit'], 0, ",", ".") . '</td>
                    <td width="115" align="right">' . number_format($value['sisa_kredit'], 0, ",", ".") . '</td>
                    <td width="58" align="center">' . $value['umur_dari_jt'] . '</td>
                </tr>';

            $sub_total_nilai_kredit += $value['nilai_kredit'];
            $sub_total_sisa_kredit += $value['sisa_kredit'];
        }

        if (count($data) > 0) {
            $html .= '
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="3" style="height:5px">' . str_repeat("-", 105) . '</td>
                    </tr>
                    <tr nobr="true">
                        <td colspan="4" align="right"><b>Sub Total :</b></td>
                        <td width="115" align="right"><b>' . number_format($sub_total_nilai_kredit, 0, ",", ".") . '</b></td>
                        <td width="115" align="right"><b>' . number_format($sub_total_sisa_kredit, 0, ",", ".") . '</b></td>
                    </tr>';
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Piutang Beredar";
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'tanggal_periode' => date_convert_format($this->input->post("tanggal_periode", true)),
            'tipe_pembayaran' => $this->input->post("tipe_pembayaran", true),
            'filter_employee_id' => $this->input->post("employee_id", true),
            'filter_cust_id' => $this->input->post("cust_id", true),
        );
        $rs = $this->m_app->get_lists($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('Member ID', 'Member Name', 'No. Transaksi', 'Tanggal', 'Tanggal JT', 'Type Payment', 'Nilai Kredit', 'Sisa Kredit', 'Umur Dari JT');
        $Col['type'] = array('string', 'string', 'string', 'date', 'date', 'string', 'money', 'money', 'general');
        $Col['align'] = array('left', 'left', 'left', 'center', 'center', 'left', 'left', 'left', 'center');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
 *                              END DEFAULT FUNCTION                            *
 *******************************************************************************/

}
