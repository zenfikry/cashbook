<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Bukubesar extends Core_Controller
{

    public function __construct()
    {
        parent::__construct("bukubesar"); # parsing menu_id
        $this->load->model("bukubesar_model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->load->view('Bukubesar', $this->data);
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Buku Besar";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'judul_laporan' => $JudulLaporan,
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => $this->input->post("filter_start_date", true),
            'filter_end_date' => $this->input->post("filter_end_date", true),
            'coa_no' => $this->input->post("coa_no", true),
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "BukuBesar";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => date_convert_format($this->input->post("filter_start_date", true)),
            'filter_end_date' => date_convert_format($this->input->post("filter_end_date", true)),
            'coa_no' => $this->input->post("coa_no", true),
        );

        $data = $this->m_app->get_lists($filter);

        $pdf->AddPage();

        $html_table = '
        <style>
            table, th {
                font-weight: bold;
            }

            table, td {
                border-collapse: collapse;
            }
        </style>
        <table cellspacing="0" cellpadding="3" border="0" width="100%">
            <tbody>';

        $html_content = '';
        $coa_code = null;
        $total_debet = 0;
        $total_kredit = 0;
        $saldo = 0;
        $no = 1;

        $pdf->SetFont('helvetica', 'B', 8);
        if (count($data['jurnal']) <= 0) {
            $pdf->Cell(0, 5, "Data tidak ditemukan.", 0, 0, 'C');
        } else {
            foreach ($data['jurnal'] as $key => $value) {
                if ($coa_code == null || $coa_code != $value['coa_code']) {
                    if ($coa_code != null && $coa_code != $value['coa_code']) {
                        $html2print = $html_table . $html_content . '</tbody></table>';
                        $pdf->writeHTML($html2print, false, false, false, false, '');
                        $pdf->Ln(1);

                        $pdf->SetFont('helvetica', 'B', 8);
                        $pdf->Cell(127, 5, "Total :", 'T', 0, 'R');
                        $pdf->Cell(25, 5, number_format($total_debet), 'T', 0, 'R');
                        $pdf->Cell(25, 5, number_format($total_kredit), 'T', 0, 'R');
                        $pdf->Cell(25, 5, "", 'T', 0, 'R');
                        $pdf->Ln(5);
                    }

                    // Reset for detail
                    $html_content = '';
                    $coa_code = $value['coa_code'];
                    $total_debet = 0;
                    $total_kredit = 0;
                    $saldo = 0;
                    $no = 1;

                    $pdf->Cell(20, 5, $value['coa_code'], 0, 0, 'L');
                    $pdf->Cell(180, 5, $value['coa_name'], 0, 1, 'L');

                    $this->pdf_render_header($pdf);
                    $pdf->Ln(8);
                }

                $pdf->SetFont('helvetica', '', 8);
                if (array_key_exists($value['coa_code'], $data['saldo_awal'])) {
                    $html_content .= '<tr nobr="true">
                    <td width="43" align="center">0</td>
                    <td width="79"></td>
                    <td width="57" align="center"></td>
                    <td width="184"></td>
                    <td width="71" align="right">0</td>
                    <td width="71" align="right">0</td>
                    <td width="71" align="right">' . number_format($data['saldo_awal'][$value['coa_code']], 0, ",", ".") . '</td>
                    </tr>';
                    // Set saldo jika ditemkan saldo sebelumnya
                    $saldo = $data['saldo_awal'][$value['coa_code']];
                    // Hapus key dari saldo_awal jika sudah ditampilkan
                    unset($data['saldo_awal'][$value['coa_code']]);
                }

                // Sst saldo
                if ($value['group_code'] < 3) {
                    $saldo = $saldo + $value['nilai_debet'] - $value['nilai_credit'];
                } else {
                    $saldo = $saldo + $value['nilai_debet'] + $value['nilai_credit'];
                }

                $html_content .= '<tr nobr="true">
                                <td width="43" align="center">' . $no++ . '</td>
                                <td width="79">' . $value['no_doc'] . '</td>
                                <td width="57" align="center">' . date('d/m/Y', strtotime($value['tgl_trans'])) . '</td>
                                <td width="184">' . $value['keterangan'] . '</td>
                                <td width="71" align="right">' . number_format($value['nilai_debet'], 0, ",", ".") . '</td>
                                <td width="71" align="right">' . number_format($value['nilai_credit'], 0, ",", ".") . '</td>
                                <td width="71" align="right">' . number_format($saldo, 0, ",", ".") . '</td>
                              </tr>';

                $total_debet += $value['nilai_debet'];
                $total_kredit += $value['nilai_credit'];

            }

            $html2print = $html_table . $html_content . '</tbody></table>';
            $pdf->writeHTML($html2print, false, false, false, false, '');
            $pdf->Ln(1);

            $pdf->SetFont('helvetica', 'B', 8);
            $pdf->Cell(127, 5, "Total :", 'T', 0, 'R');
            $pdf->Cell(25, 5, number_format($total_debet), 'T', 0, 'R');
            $pdf->Cell(25, 5, number_format($total_kredit), 'T', 0, 'R');
            $pdf->Cell(25, 5, "", 'T', 0, 'R');
            $pdf->Ln(5);
        }

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_header($pdf)
    {
        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(15, 8, "No", 'TB', 0, 'C');
        $pdf->Cell(28, 8, " No. Transaksi", 'TB', 0, 'L');
        $pdf->Cell(20, 8, "Tanggal", 'TB', 0, 'C');
        $pdf->Cell(65, 8, " Keterangan", 'TB', 0, 'L');
        $pdf->Cell(25, 8, "Debet  ", 'TB', 0, 'R');
        $pdf->Cell(25, 8, "Kredit ", 'TB', 0, 'R');
        $pdf->Cell(25, 8, "Saldo ", 'TB', 0, 'R');
    }

    private function pdf_render_detail($data, $filter)
    {
        $html = '';
        $no_doc = null;
        $total_debet = 0;
        $total_kredit = 0;
        foreach ($data as $key => $value) {
            if ($no_doc == null || $no_doc != $value['no_doc']) {
                if ($no_doc != null && $no_doc != $value['no_doc']) {
                    $html .= '
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="2" align="right" style="height:5px">' . str_repeat("-", 84) . '</td>
                            </tr>
                            <tr nobr="true">
                                <td colspan="3" align="right"></td>
                                <td width="115" align="right"><b>' . number_format($sub_total_nilai_debet, 0, ",", ".") . '</b></td>
                                <td width="115" align="right"><b>' . number_format($sub_total_nilai_kredit, 0, ",", ".") . '</b></td>
                            </tr>';
                    $sub_total_nilai_debet = 0;
                    $sub_total_nilai_kredit = 0;
                }
                $no_doc = $value['no_doc'];
                $html .= '
                    <tr nobr="true">
                        <td width="71"><b>' . $value['no_doc'] . '</b></td>
                        <td colspan="4"><b>' . date('d/m/Y', strtotime($value['tgl_trans'])) . '</b></td>
                    </tr>';
            }
            $html .= '
                <tr nobr="true">
                    <td width="71">' . $value['coa_code'] . '</td>
                    <td width="113">' . $value['coa_name'] . '</td>
                    <td width="156">' . $value['keterangan'] . '</td>
                    <td width="115" align="right">' . number_format($value['nilai_debet'], 0, ",", ".") . '</td>
                    <td width="115" align="right">' . number_format($value['nilai_credit'], 0, ",", ".") . '</td>
                </tr>';

            $sub_total_nilai_debet += $value['nilai_debet'];
            $sub_total_nilai_kredit += $value['nilai_credit'];
        }

        if (count($data) > 0) {
            $html .= '
                    <tr>
                        <td colspan="3"></td>
                        <td colspan="2" align="right" style="height:5px">' . str_repeat("-", 84) . '</td>
                    </tr>
                    <tr nobr="true">
                        <td colspan="3" align="right"></td>
                        <td width="115" align="right"><b>' . number_format($sub_total_nilai_debet, 0, ",", ".") . '</b></td>
                        <td width="115" align="right"><b>' . number_format($sub_total_nilai_kredit, 0, ",", ".") . '</b></td>
                    </tr>';
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Jurnal Umum";
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => date_convert_format($this->input->post("filter_start_date", true)),
            'filter_end_date' => date_convert_format($this->input->post("filter_end_date", true)),
            'coa_no' => $this->input->post("coa_no", true),
        );

        $rs = $this->m_app->get_lists($filter);

        $coa_code = null;
        $total_debet = 0;
        $total_kredit = 0;
        $saldo = 0;
        $no = 1;
        $idx = 0;
        $data2print = [];

        foreach ($rs['jurnal'] as $value) {
            if ($coa_code == null || $coa_code != $value['coa_code']) {
                if ($coa_code != null && $coa_code != $value['coa_code']) {
                    $data2print[$idx] = [
                        'group_code' => '',
                        'coa_code' => '',
                        'coa_name' => '',
                        'no_doc' => '',
                        'tgl_trans' => '',
                        'keterangan' => 'Total',
                        'nilai_debet' => $total_debet,
                        'nilai_credit' => $total_kredit,
                    ];
                    $data2print[$idx] = ['no' => ''] + $data2print[$idx] + ['saldo' => ''];
                    $no++;
                    $idx++;
                }

                // Reset for detail
                $coa_code = $value['coa_code'];
                $total_debet = 0;
                $total_kredit = 0;
                $saldo = 0;
                $no = 1;
            }

            if (array_key_exists($value['coa_code'], $rs['saldo_awal'])) {
                // Set saldo jika ditemkan saldo sebelumnya
                $saldo = $rs['saldo_awal'][$value['coa_code']];
                $data2print[$idx] = [
                    'group_code' => $value['group_code'],
                    'coa_code' => $value['coa_code'],
                    'coa_name' => $value['coa_name'],
                    'no_doc' => '',
                    'tgl_trans' => '',
                    'keterangan' => 'Open Balance',
                    'nilai_debet' => 0,
                    'nilai_credit' => 0,
                ];
                $data2print[$idx] = ['no' => 0] + $data2print[$idx] + ['saldo' => $saldo];
                $idx++;
            }

            // Sst saldo
            if ($value['group_code'] < 3) {
                $saldo = $saldo + $value['nilai_debet'] - $value['nilai_credit'];
            } else {
                $saldo = $saldo + $value['nilai_debet'] + $value['nilai_credit'];
            }

            $data2print[$idx] = $value;
            $data2print[$idx] = ['no' => $no] + $data2print[$idx] + ['saldo' => $saldo];
            $idx++;
            $no++;

            $total_debet += $value['nilai_debet'];
            $total_kredit += $value['nilai_credit'];
        }
        $data2print[$idx] = [
            'group_code' => '',
            'coa_code' => '',
            'coa_name' => '',
            'no_doc' => '',
            'tgl_trans' => '',
            'keterangan' => 'Total',
            'nilai_debet' => $total_debet,
            'nilai_credit' => $total_kredit,
        ];
        $data2print[$idx] = ['no' => ''] + $data2print[$idx] + ['saldo' => ''];

        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('No', 'Grup Kode', 'Kode', 'Nama', 'No. Transaksi', 'Tanggal', 'Keterangan', 'Debet', 'Kredit', 'Saldo');
        $Col['type'] = array('numeric', 'string', 'string', 'string', 'string', 'date', 'string', 'money', 'money', 'money');
        $Col['align'] = array('left', 'left', 'left', 'left', 'left', 'center', 'left', 'left', 'left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        // if (count($data['jurnal']) <= 0) {
        // dd($rs);
        if (count($rs['jurnal']) >= 1000000) {
            $xls->generateCSVByArray($data2print, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByArray($data2print, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
 *                              END DEFAULT FUNCTION                            *
 *******************************************************************************/

}
