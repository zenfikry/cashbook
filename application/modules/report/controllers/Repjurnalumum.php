<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Repjurnalumum extends Core_Controller
{
    private $total_nilai_debet = 0;
    private $total_nilai_kredit = 0;

    public function __construct()
    {
        parent::__construct("repjurnalumum"); # parsing menu_id
        $this->load->model("repjurnalumum_model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->load->view('Repjurnalumum', $this->data);
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Jurnal Umum";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'judul_laporan' => $JudulLaporan,
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => $this->input->post("filter_start_date", true),
            'filter_end_date' => $this->input->post("filter_end_date", true),
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "JurnalUmum";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 18, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => date_convert_format($this->input->post("filter_start_date", true)),
            'filter_end_date' => date_convert_format($this->input->post("filter_end_date", true)),
        );

        $data = $this->m_app->get_lists($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 8);

        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				' . $this->pdf_render_detail($data->result_array(), $filter) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');

        $pdf->Ln(5);
        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(120, 8, "Total : ", 'BT', 0, 'R');
        $pdf->Cell(40, 8, number_format($this->total_nilai_debet, 0, ",", "."), 'BT', 0, 'R');
        $pdf->Cell(40, 8, number_format($this->total_nilai_kredit, 0, ",", "."), 'BT', 0, 'R');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail($data, $filter)
    {
        $html = '';
        $no_doc = null;
        $sub_total_nilai_debet = 0;
        $sub_total_nilai_kredit = 0;
        foreach ($data as $key => $value) {
            if ($no_doc == null || $no_doc != $value['no_doc']) {
                if ($no_doc != null && $no_doc != $value['no_doc']) {
                    $html .= '
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="2" align="right" style="height:5px">' . str_repeat("-", 84) . '</td>
                            </tr>
                            <tr nobr="true">
                                <td colspan="3" align="right"></td>
                                <td width="115" align="right"><b>' . number_format($sub_total_nilai_debet, 0, ",", ".") . '</b></td>
                                <td width="115" align="right"><b>' . number_format($sub_total_nilai_kredit, 0, ",", ".") . '</b></td>
                            </tr>';
                    $sub_total_nilai_debet = 0;
                    $sub_total_nilai_kredit = 0;
                }
                $no_doc = $value['no_doc'];
                $html .= '
                    <tr nobr="true">
                        <td width="71"><b>' . $value['no_doc'] . '</b></td>
                        <td colspan="4"><b>' . date('d/m/Y', strtotime($value['tgl_trans'])) . '</b></td>
                    </tr>';
            }
            $html .= '
                <tr nobr="true">
                    <td width="71">' . $value['coa_code'] . '</td>
                    <td width="113">' . $value['coa_name'] . '</td>
                    <td width="156">' . $value['keterangan'] . '</td>
                    <td width="115" align="right">' . number_format($value['nilai_debet'], 0, ",", ".") . '</td>
                    <td width="115" align="right">' . number_format($value['nilai_credit'], 0, ",", ".") . '</td>
                </tr>';

            $sub_total_nilai_debet += $value['nilai_debet'];
            $sub_total_nilai_kredit += $value['nilai_credit'];
            $this->total_nilai_debet += $value['nilai_debet'];
            $this->total_nilai_kredit += $value['nilai_credit'];
        }

        if (count($data) > 0) {
            $html .= '
                    <tr>
                        <td colspan="3"></td>
                        <td colspan="2" align="right" style="height:5px">' . str_repeat("-", 84) . '</td>
                    </tr>
                    <tr nobr="true">
                        <td colspan="3" align="right"></td>
                        <td width="115" align="right"><b>' . number_format($sub_total_nilai_debet, 0, ",", ".") . '</b></td>
                        <td width="115" align="right"><b>' . number_format($sub_total_nilai_kredit, 0, ",", ".") . '</b></td>
                    </tr>';
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Laporan Jurnal Umum";
        # Ambil data
        $filter = array(
            'store_id' => $this->input->post("store_id", true),
            'filter_start_date' => date_convert_format($this->input->post("filter_start_date", true)),
            'filter_end_date' => date_convert_format($this->input->post("filter_end_date", true)),
        );
        $rs = $this->m_app->get_lists($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('No. Transaksi', 'Tanggal', 'Kode', 'Nama', 'Keterangan', 'Nilai Kredit', 'Sisa Kredit');
        $Col['type'] = array('string', 'date', 'string', 'string', 'string', 'money', 'money');
        $Col['align'] = array('left', 'center', 'left', 'left', 'left', 'left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
 *                              END DEFAULT FUNCTION                            *
 *******************************************************************************/

}
