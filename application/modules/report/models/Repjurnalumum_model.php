<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Repjurnalumum_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_lists($filter)
    {
        $this->db->select("a.no_doc, a.tgl_trans, a.coa_code, b.name AS coa_name, a.keterangan, a.nilai_debet, a.nilai_credit");
        $this->db->from("t_jurnal_umum a");
        $this->db->join("m_coa b", "b.code=a.coa_code", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        $this->db->where("a.status", 1);
        $this->db->group_start();
        $this->db->where([
            "CAST(a.tgl_trans AS DATE) >=" => $filter['filter_start_date'],
            "CAST(a.tgl_trans AS DATE) <=" => $filter['filter_end_date'],
        ]);
        $this->db->group_end();
        $this->db->order_by("a.tgl_trans, a.no_doc, a.id");

        return $this->db->get();
    }

}
