<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Bukubesar_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_lists($filter)
    {
        // Variabel penampung
        $jurnal = null;
        $saldo_awal = [];

        // Ambil data jurnal
        $this->db->select("b.group_code, a.coa_code, b.name AS coa_name, a.no_doc, a.tgl_trans, a.keterangan, a.nilai_debet, a.nilai_credit");
        $this->db->from("t_jurnal_umum a");
        $this->db->join("m_coa b", "b.code=a.coa_code", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // Filter coa_no
        if (!check_arr_empty_or_null($filter['coa_no'])) {
            $this->db->where("a.coa_code", $filter['coa_no']);
        }
        $this->db->where("a.status", 1);
        $this->db->group_start();
        $this->db->where([
            "CAST(a.tgl_trans AS DATE) >=" => $filter['filter_start_date'],
            "CAST(a.tgl_trans AS DATE) <=" => $filter['filter_end_date'],
        ]);
        $this->db->group_end();
        $this->db->order_by("b.group_code, a.coa_code, a.tgl_trans, a.no_doc, a.id");
        $jurnal = $this->db->get()->result_array();

        // Ambil nomor COA dari $jurnal dan hapus duplikat
        $coa_no_list = array_unique(array_column($jurnal, "coa_code"));

        // Ambil data saldo awal dari jurnal
        $saldo_awal_jurnal = [];
        $this->db->select("a.coa_code, (COALESCE(SUM(a.nilai_debet), 0) - COALESCE(SUM(a.nilai_credit), 0)) AS saldo_awal");
        $this->db->from("t_jurnal_umum a");
        $this->db->where("CAST(a.tgl_trans AS DATE) <", $filter['filter_start_date']);
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // Check jika $coa_no_list lebih dari 0
        if (count($coa_no_list) > 0) {
            $this->db->where_in("a.coa_code", $coa_no_list);
        }
        $this->db->group_by("a.coa_code");
        $saldo_awal_jurnal = $this->db->get()->result_array();

        // Ambil saldo awal
        $saldo_awal_init = [];
        $this->db->select("coa_no, jumlah");
        $this->db->from("t_coa_saldo_awal");
        $this->db->where("tanggal <", $filter['filter_start_date']);
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("store_id", $filter['store_id']);
        }
        // Check jika $coa_no_list lebih dari 0
        if (count($coa_no_list) > 0) {
            $this->db->where_in("coa_no", $coa_no_list);
        }
        $saldo_awal_init = $this->db->get()->result_array();

        // Looping $coa_no_list dan untuk ambil saldo awal dari variabel $saldo_awal_jurnal dan $saldo_awal_init
        foreach ($coa_no_list as $key => $value) {
            // Dari $saldo_awal_init
            if (in_array($value, array_column($saldo_awal_init, 'coa_no'))) {
                $saldo_awal[$value] = $this->get_value($value, $saldo_awal_init, 'coa_no', 'jumlah');
            }

            // Dari $saldo_awal_jurnal
            if (in_array($value, array_column($saldo_awal_jurnal, 'coa_code'))) {
                if (array_key_exists($value, $saldo_awal)) {
                    $saldo_awal[$value] += $this->get_value($value, $saldo_awal_jurnal, 'coa_code', 'saldo_awal');
                } else {
                    $saldo_awal[$value] = $this->get_value($value, $saldo_awal_jurnal, 'coa_code', 'saldo_awal');
                }
            }
        }

        return [
            'jurnal' => $jurnal,
            'saldo_awal' => $saldo_awal,
        ];
    }

    private function get_value($val, $arr, $key, $keyVal)
    {
        foreach ($arr as $value) {
            if ($value[$key] == $val) {
                return $value[$keyVal];
            }
        }
    }

}
