<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reppembayaran_hutang_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_lists($filter)
    {
        $this->db->select("a.no_doc, a.tgl_trans, CONCAT(a.supp_id, ' - ', c.supp_name) AS supplier, a.coa_bayar, b.no_doc_ref, b.potongan, b.bayar");
        $this->db->from("t_finance_pembayaran_hutang_hdr a");
        $this->db->join("t_finance_pembayaran_hutang_dtl b", "b.no_doc=a.no_doc", "left");
        $this->db->join("m_supplier c", "c.supp_id=a.supp_id", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // $this->db->where("a.status", 1);
        $this->db->group_start();
        $this->db->where("a.tgl_trans >=", $filter['filter_start_date']);
        $this->db->where("a.tgl_trans <=", $filter['filter_end_date']);
        $this->db->group_end();

        return $this->db->get();
    }

}
