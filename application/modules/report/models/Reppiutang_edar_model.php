<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reppiutang_edar_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_lists($filter)
    {
        $this->db->select("a.cust_id, b.cust_name, a.no_doc, a.tgl_trans, a.tgl_jatuh_tempo, a.type_payment,
                            a.grand_total AS nilai_kredit, (a.grand_total - COALESCE(SUM(d.potongan + d.pembayaran), 0)) AS sisa_kredit,
                            DATEDIFF('" . $filter['tanggal_periode'] . "', a.tgl_jatuh_tempo) AS umur_dari_jt");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_customer b", "b.cust_id=a.cust_id", "left");
        $this->db->join("t_finance_pembayaran_piutang_hdr c", "c.cust_id=a.cust_id AND c.status=1", "left");
        $this->db->join("t_finance_pembayaran_piutang_dtl d", "d.no_doc_ref=a.no_doc AND d.no_doc=c.no_doc", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // Filter tipe_pembayaran
        if (!check_arr_empty_or_null($filter['tipe_pembayaran'])) {
            $this->db->where("a.type_payment", $filter['tipe_pembayaran']);
        }
        // Filter customer/member
        if (!check_arr_empty_or_null($filter['filter_cust_id'])) {
            $this->db->where("a.cust_id", $filter['filter_cust_id']);
        }
        // Filter employee/sales
        if (!check_arr_empty_or_null($filter['filter_employee_id'])) {
            $this->db->where("d.employee_id", $filter['filter_employee_id']);
        }

        $this->db->where_not_in("a.type_payment", "TUNAI");
        $this->db->group_by("a.cust_id, b.cust_name, a.no_doc, a.tgl_trans, a.tgl_jatuh_tempo, a.type_payment, a.grand_total");
        $this->db->having("(a.grand_total - COALESCE(SUM(d.potongan + d.pembayaran), 0)) > 0", null, false);
        $this->db->order_by("a.cust_id");

        return $this->db->get();
    }

}
