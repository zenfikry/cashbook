<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Repstockonhand_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_lists($filter)
    {
        if ($filter['tampilkan_expire'] == 1) {
            $this->db->select("a.store_id, b.store_name, a.item_id, c.item_name, d.use_expire, a.expired_date, SUM(a.tot_qty) AS total_qty");
        } else {
            $this->db->select("a.store_id, b.store_name, a.item_id, c.item_name, d.use_expire, SUM(a.tot_qty) AS total_qty");
        }
        $this->db->from("inv_on_hand a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_items c", "c.item_id=a.item_id", "left");
        $this->db->join("m_jenis d", "d.jenis_id=c.jenis_id", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // Filter item
        if (!check_arr_empty_or_null($filter['item_id_start']) || !check_arr_empty_or_null($filter['item_id_end'])) {
            $this->db->where("a.item_id BETWEEN '" . $filter['item_id_start'] . "' AND '" . $filter['item_id_end'] . "'", null, false);
        }
        // Filter expired_date
        if ($filter['tampilkan_expire'] == 1) {
            $this->db->group_by("a.store_id, b.store_name, a.item_id, c.item_name, d.use_expire, a.expired_date");
        } else {
            $this->db->group_by("a.store_id, b.store_name, a.item_id, c.item_name, d.use_expire");
        }

        return $this->db->get();
    }

    public function get_lists_xls($filter)
    {
        if ($filter['tampilkan_expire'] == 1) {
            $this->db->select("a.store_id, b.store_name, a.item_id, c.item_name, a.expired_date, SUM(a.tot_qty) AS total_qty");
        } else {
            $this->db->select("a.store_id, b.store_name, a.item_id, c.item_name, SUM(a.tot_qty) AS total_qty");
        }
        $this->db->from("inv_on_hand a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_items c", "c.item_id=a.item_id", "left");
        $this->db->join("m_jenis d", "d.jenis_id=c.jenis_id", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // Filter item
        if (!check_arr_empty_or_null($filter['item_id_start']) || !check_arr_empty_or_null($filter['item_id_end'])) {
            $this->db->where("a.item_id BETWEEN '" . $filter['item_id_start'] . "' AND '" . $filter['item_id_end'] . "'", null, false);
        }
        // Filter expired_date
        if ($filter['tampilkan_expire'] == 1) {
            $this->db->group_by("a.store_id, b.store_name, a.item_id, c.item_name, a.expired_date");
        } else {
            $this->db->group_by("a.store_id, b.store_name, a.item_id, c.item_name");
        }

        return $this->db->get();
    }

}
