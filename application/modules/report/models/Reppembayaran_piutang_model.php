<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reppembayaran_piutang_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_lists($filter)
    {
        $this->db->select("a.no_doc, a.tgl_trans, CONCAT(a.cust_id, ' - ', c.cust_name) AS member, a.coa_no, b.no_doc_ref, b.potongan, b.pembayaran");
        $this->db->from("t_finance_pembayaran_piutang_hdr a");
        $this->db->join("t_finance_pembayaran_piutang_dtl b", "b.no_doc=a.no_doc", "left");
        $this->db->join("m_customer c", "c.cust_id=a.cust_id", "left");
        $this->db->join("t_sales_hdr d", "d.no_doc=b.no_doc_ref", "left");
        // Filter store
        if (!check_arr_empty_or_null($filter['store_id'])) {
            $this->db->where("a.store_id", $filter['store_id']);
        }
        // Filter customer/member
        if (!check_arr_empty_or_null($filter['filter_cust_id'])) {
            $this->db->where("a.cust_id", $filter['filter_cust_id']);
        }
        // Filter employee/sales
        if (!check_arr_empty_or_null($filter['filter_employee_id'])) {
            $this->db->where("d.employee_id", $filter['filter_employee_id']);
        }

        $this->db->where("a.status", 1);
        $this->db->group_start();
        $this->db->where("a.tgl_trans >=", $filter['filter_start_date']);
        $this->db->where("a.tgl_trans <=", $filter['filter_end_date']);
        $this->db->group_end();
        $result = $this->db->get();

        return $result;
    }

}
