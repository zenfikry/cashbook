<?php

require_once APPPATH . '/third_party/tcpdf/tcpdf.php';

class PDFReport extends TCPDF
{

    public $RepFuncHeader;
    public $dataHeader;
    public $RepFuncFooter;
    public $dataFooter;

    public function __construct()
    {
        // parent::__construct();
    }

    public function _construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }

    /*******************************************************************************
     *                               START HEADER                                   *
     *******************************************************************************/

    public function Header()
    {
        $this->SetY(3); // agar header tidak terlalu rapat dengan ujung kertas

        // To be implemented in your own inherited class
        if ($this->RepFuncHeader != "") {
            call_user_func(array($this, $this->RepFuncHeader), $this->dataHeader); # Call function with parameters
        } else {
            call_user_func(array($this, 'DefaultHeader'), $this->dataHeader); # Call function with parameters
        }
    }

    public function DefaultHeader($data)
    {
        // x
    }

    public function KopSurat()
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);
    }

    public function PiutangBeredar($data)
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);

        $this->SetFont('helvetica', 'B', 10);
        $this->Cell(0, 5, $data['judul_laporan'], 0, 1, 'L');
        $this->SetFont('helvetica', '', 8);
        if (!check_arr_empty_or_null($data['store_id'])) {
            $this->Cell(0, 5, "Outlet : " . get_row_values("store_name", "m_store", "store_id", $data['store_id'])['store_name'], 0, 1, 'L');
        } else {
            $this->Cell(0, 5, "Outlet : Semua", 0, 1, 'L');
        }
        $this->Cell(0, 5, "Periode Sampai : " . $data["tanggal_periode"], 0, 1, 'L');
        $sales = !check_arr_empty_or_null($data['tanggal_periode']) ? $data["tanggal_periode"] : 'Semua';
        $this->Cell(0, 5, "Sales : " . $sales, 0, 1, 'L');
        $member = !check_arr_empty_or_null($data['member']) ? $data["member"] : 'Semua';
        $this->Cell(0, 5, "Member : " . $member, 0, 1, 'L');
        $this->Ln(5);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(35, 8, " No. Transaksi", 'BT', 0, 'L');
        $this->Cell(20, 8, "Tanggal", 'BT', 0, 'C');
        $this->Cell(20, 8, "Tanggal JT", 'BT', 0, 'C');
        $this->Cell(25, 8, "Tipe Pembayaran", 'BT', 0, 'C');
        $this->Cell(40, 8, "Nilai Kredit ", 'BT', 0, 'R');
        $this->Cell(40, 8, "Sisa Kredit ", 'BT', 0, 'R');
        $this->Cell(21, 8, "Umur Dari JT", 'BT', 0, 'C');
    }

    public function PembayaranPiutang($data)
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);

        $this->SetFont('helvetica', 'B', 10);
        $this->Cell(0, 5, $data['judul_laporan'], 0, 1, 'L');
        $this->SetFont('helvetica', '', 8);
        if (!check_arr_empty_or_null($data['store_id'])) {
            $this->Cell(0, 5, "Outlet : " . get_row_values("store_name", "m_store", "store_id", $data['store_id'])['store_name'], 0, 1, 'L');
        } else {
            $this->Cell(0, 5, "Outlet : Semua", 0, 1, 'L');
        }
        $this->Cell(0, 5, "Periode : " . $data["filter_start_date"] . ' s/d ' . $data["filter_end_date"], 0, 1, 'L');
        $sales = !check_arr_empty_or_null($data['employee']) ? $data['employee'] : 'Semua';
        $this->Cell(0, 5, "Sales : " . $sales, 0, 1, 'L');
        $member = !check_arr_empty_or_null($data['member']) ? $data['member'] : 'Semua';
        $this->Cell(0, 5, "Member : " . $member, 0, 1, 'L');
        $this->Ln(5);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(25, 8, " No. Pembayaran", 'BT', 0, 'L');
        $this->Cell(20, 8, "Tanggal", 'BT', 0, 'C');
        $this->Cell(45, 8, " Member", 'BT', 0, 'L');
        $this->Cell(25, 8, " Cara Bayar", 'BT', 0, 'L');
        $this->Cell(27, 8, " No. Transaksi ", 'BT', 0, 'L');
        $this->Cell(30, 8, "Potongan ", 'BT', 0, 'R');
        $this->Cell(30, 8, "Jumlah Pembayaran ", 'BT', 0, 'R');
    }

    public function PembayaranHutang($data)
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);

        $this->SetFont('helvetica', 'B', 10);
        $this->Cell(0, 5, $data['judul_laporan'], 0, 1, 'L');
        $this->SetFont('helvetica', '', 8);
        if (!check_arr_empty_or_null($data['store_id'])) {
            $this->Cell(0, 5, "Outlet : " . get_row_values("store_name", "m_store", "store_id", $data['store_id'])['store_name'], 0, 1, 'L');
        } else {
            $this->Cell(0, 5, "Outlet : Semua", 0, 1, 'L');
        }
        $this->Cell(0, 5, "Periode : " . $data["filter_start_date"] . ' s/d ' . $data["filter_end_date"], 0, 1, 'L');
        $this->Ln(5);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(25, 8, " No. Bayar", 'BT', 0, 'L');
        $this->Cell(20, 8, "Tanggal", 'BT', 0, 'C');
        $this->Cell(45, 8, " Supplier", 'BT', 0, 'L');
        $this->Cell(25, 8, " Cara Bayar", 'BT', 0, 'L');
        $this->Cell(27, 8, " No. Transaksi ", 'BT', 0, 'L');
        $this->Cell(30, 8, "Potongan ", 'BT', 0, 'R');
        $this->Cell(30, 8, "Jumlah Bayar ", 'BT', 0, 'R');
    }

    public function JurnalUmum($data)
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);

        $this->SetFont('helvetica', 'B', 10);
        $this->Cell(0, 5, $data['judul_laporan'], 0, 1, 'L');
        $this->SetFont('helvetica', '', 8);
        if (!check_arr_empty_or_null($data['store_id'])) {
            $this->Cell(0, 5, "Outlet : " . get_row_values("store_name", "m_store", "store_id", $data['store_id'])['store_name'], 0, 1, 'L');
        } else {
            $this->Cell(0, 5, "Outlet : Semua", 0, 1, 'L');
        }
        $this->Cell(0, 5, "Periode : " . $data["filter_start_date"] . ' s/d ' . $data["filter_end_date"], 0, 1, 'L');
        $this->Ln(5);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(25, 8, " Kode", 'BT', 0, 'L');
        $this->Cell(40, 8, " Nama", 'BT', 0, 'L');
        $this->Cell(55, 8, " Keterangan", 'BT', 0, 'L');
        $this->Cell(40, 8, "Nilai Kredit ", 'BT', 0, 'R');
        $this->Cell(40, 8, "Sisa Kredit ", 'BT', 0, 'R');
    }

    public function BukuBesar($data)
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);

        $this->SetFont('helvetica', 'B', 10);
        $this->Cell(0, 5, $data['judul_laporan'], 0, 1, 'L');
        $this->SetFont('helvetica', '', 8);
        if (!check_arr_empty_or_null($data['store_id'])) {
            $this->Cell(0, 5, "Outlet : " . get_row_values("store_name", "m_store", "store_id", $data['store_id'])['store_name'], 0, 1, 'L');
        } else {
            $this->Cell(0, 5, "Outlet : Semua", 0, 1, 'L');
        }
        $this->Cell(0, 5, "Periode : " . $data["filter_start_date"] . ' s/d ' . $data["filter_end_date"], 0, 1, 'L');
        if (!check_arr_empty_or_null($data['store_id'])) {
            $this->Cell(0, 5, "COA : " . get_row_values("name", "m_coa", "coa", $data['coa_no'])['name'], 0, 1, 'L');
        } else {
            $this->Cell(0, 5, "COA : Semua", 0, 1, 'L');
        }
        $this->Ln(5);
    }

    /*******************************************************************************
     *                               END HEADER                                     *
     *******************************************************************************/

    /*******************************************************************************
     *                               START FOOTER                                   *
     *******************************************************************************/

    public function Footer()
    {
        // To be implemented in your own inherited class
        if ($this->RepFuncFooter != "") {
            call_user_func(array($this, $this->RepFuncFooter), $this->dataFooter); # Call function with parameters
        } else {
            call_user_func(array($this, 'DefaultFooter'), $this->dataFooter); # Call function with parameters
        }
    }

    public function DefaultFooter($data)
    {
        $this->SetY(-15);
        // helvetica italic 8
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $width = $this->w / 3;
        #$this->Cell($width,10,'Waktu Cetak : '.date("d M Y H:i:s"),0,0,'L');
        $this->Cell($width, 10, 'Waktu Cetak : ' . date("d M Y H:i:s") . ', Cetak Oleh : ' . $data['printed_by'], 0, 0, 'L');
        $this->Cell(0, 10, 'Halaman ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'R');
    }

    /*******************************************************************************
 *                               END FOOTER                                     *
 *******************************************************************************/
}
