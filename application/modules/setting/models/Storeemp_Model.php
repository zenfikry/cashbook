<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Storeemp_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	
	/* function save($input) {
		$cek = $this->db->query("SELECT * FROM app_modules WHERE module_id=?", array($input['module_id']));
		if ($cek->row() != NULL) {
			return array('result' => false, 'data' => NULL, 'msg' => 'Module ID sudah ada.');
		}
		
		$data = array(
			'module_id' => $input['module_id'],
			'module_name' => $input['module_name'],
			'active' => $input['active'],
			'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s')
		);
		
		$NonQry = $this->db->insert('app_modules', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil disimpan.');
		}
	} */
	
	function update($input) {

		# Mulai begin trans
		$this->db->trans_begin();
		foreach($input['menu_arr'] as $key => $val) {
			# Siapkan inputan
			$data = $val;
			$data['modified_by'] = $this->session->userdata('user_id');
			$data['modification_date'] = date('Y-m-d H:i:s');
			# Insert
			$where = array(
				'nik' => $input['nik'],
                'm_code' => $key,
			);
			$this->db->where($where);
			$msg = $this->db->update('m_employee_store', $data);
			# Jika terdapat masalah pada proses insert
			if ($this->db->trans_status() === FALSE) {
				# Tangkap errornya
				$err_db = $this->db->error();
				# Membatalkan semua perubahan
				$this->db->trans_rollback();
				break;
			}
		}
		# Jika tidak ada masalah dengan proses insert
		if ($this->db->trans_status() === TRUE) { 
			# Commit/Simpan semua perubahan
			$this->db->trans_commit(); 
		}
		
		if (!empty($err_db)) {
			$FilterErrDb = str_replace(str_split('\"'), '', $err_db['message']);
			$msg = explode(PHP_EOL,$FilterErrDb);
			return array('result' => false, 'data' => '', 'msg' => "Gagal simpan data \n\n".$msg[0]."\n\n".$msg[1]);
		} else {
			return array('result' => true, 'data' => '', 'msg' => 'Data berhasil disimpan.');
		}
	}
	
	function delete($input) {
		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_employee_store");
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		}
	}
	
	/* function getData2Edit($id) {
		$Qry = $this->db->query("SELECT a.id, a.module_id, a.module_name, a.active
										FROM app_modules a 
										WHERE a.id=?", array($id));
													
		if ($Qry->result() != NULL){
			return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
		} else {
			return array('result' => false, 'msg' => 'Data tidak ditmeukan.', 'data' => NULL);
		}
	} */
	
	function getList($filter) {
		$this->datatables->select('a.id, a.nik, c.name_employee, a.m_code, b.m_shortdesc, b.m_odesc, a.status');
		$this->datatables->from('m_employee_store a');
        $this->datatables->join('m_store b', 'b.m_code = a.m_code', 'left');
		$this->datatables->join('m_employee c','c.nik=a.nik', 'left');
		foreach($filter as $key => $val) {
			$this->datatables->where($key, $val);
		}
		return $this->datatables->generate();
	}
	
	function getDataList($filter) {
		$this->db->select("a.nik, a.status");
		$this->db->from('m_employee_store a');
		// $this->db->join('m_dept b', 'b.code_dept=a.code_dept', 'left');
		foreach($filter as $key => $val) {
			$this->db->where($key, $val);
		}
		return $this->db->get();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	function getStoreList() {
		$this->db->select('a.m_code, a.m_shortdesc, a.m_odesc');
		$this->db->from('m_store a');
		$this->db->where('a.status=1');
		return $this->db->get();
	}

	function getNikList() {
		$this->db->select('a.nik, a.name_employee');
		$this->db->from('m_employee a');
		$this->db->where('a.status=1');
		return $this->db->get();
	}

  	function AddMenuToRole($input) {
		# Deklarasi return menu
		$menuExist = 0;
		$menuBaru = 0;
		$menuTidakAda = 0;
		# Mulai begin trans
		$this->db->trans_begin();
		foreach($input['m_code'] as $val) {
			# Cek jika menu ada
			// $cek = $this->db->query("SELECT * FROM app_coa WHERE code_coa=?", array($val));
			// if ($cek->row() == NULL) {
			// 	$menuTidakAda++;
			// 	continue;
			// }
			# Cek jika menu berdasarkan role sudah ada
			$cek = $this->db->query("SELECT * FROM m_employee_store WHERE nik=? AND m_code=?", array($input['nik'], $val));
			if ($cek->row() != NULL) {
				$menuExist++;
				continue;
			}
			# Siapkan inputan
			$data = array(
				'nik' => $input['nik'],
				'm_code' => $val,
				'status' => 1,
				'created_by' => $this->session->userdata('user_id'),
				'creation_date' => date('Y-m-d H:i:s')
			);
			# Insert
			$msg = $this->db->insert('m_employee_store', $data);
			$menuBaru++;
			# Jika terdapat masalah pada proses insert
			if ($this->db->trans_status() === FALSE) {
				# Tangkap errornya
				$err_db = $this->db->error();
				# Membatalkan semua perubahan
				$this->db->trans_rollback();
				break;
			}
		}
		# Jika tidak ada masalah dengan proses insert
		if ($this->db->trans_status() === TRUE) { 
			# Commit/Simpan semua perubahan
			$this->db->trans_commit(); 
		}
		
		if (!empty($err_db)) {
			$FilterErrDb = str_replace(str_split('\"'), '', $err_db['message']);
			$msg = explode(PHP_EOL,$FilterErrDb);
			return array('result' => false, 'data' => '', 'msg' => "Gagal simpan data \n\n".$msg[0]."\n\n".$msg[1]);
		} else {
			return array('result' => true, 'data' => '', 'msg' => 'Store sudah ada : '.$menuExist.', Store ditambahkan : '.$menuBaru.', Store tidak ada : '.$menuTidakAda.'.');
		}
		
	}

}