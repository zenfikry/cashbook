<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Saldo_awal_inventory_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function save($input)
    {
        # Mulai begin trans
        $this->db->trans_begin();

        $tanggal = date_convert_format($input['tanggal']);
        $store_id = $this->session->userdata('store_id');

        $detail = json_decode($input['detail'], true);
        if (count($detail) > 0) {
            $list_arr_id = array_column($detail, 'detail_id');
            # Hapus dari tabel detail yang tidak memiliki id pada array dan list_arr_id
            $this->db->where_not_in('id', $list_arr_id);
            $this->db->where('store_id', $store_id);
            $this->db->delete('inv_on_hand_saldo_awal');
            $err_db = $this->db->error(); # Tangkap error
            if (empty($err_db['message'])):
                # Looping update/insert ke tabel detail
                foreach ($detail as $key => $value) {
                    if (check_arr_empty_or_null($value['item_id']) || $value['jumlah'] <= 0 || $value['locked'] == 0) {
                        continue;
                    }

                    if (empty($err_db['message'])):
                        $tanggal_expired = date('Y-m-d', strtotime($value['tanggal_expired']));
                        $cek = get_row_values("item_id, jumlah", "inv_on_hand_saldo_awal", "id", $value['detail_id']);
                        if ($cek != null) {
                            $this->db->update(
                                'inv_on_hand_saldo_awal',
                                [
                                    'tanggal' => $tanggal,
                                    'tanggal_expired' => $tanggal_expired,
                                    'jumlah' => $value['jumlah'],
                                    'uom_id' => $value['uom_id'],
                                    'harga' => $value['harga'],
                                    'total' => $value['total'],
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ],
                                [
                                    'id' => $value['detail_id'],
                                ]
                            );
                        } else {
                            $this->db->insert(
                                'inv_on_hand_saldo_awal',
                                [
                                    'store_id' => $store_id,
                                    'item_id' => $value['item_id'],
                                    'tanggal' => $tanggal,
                                    'tanggal_expired' => $tanggal_expired,
                                    'jumlah' => $value['jumlah'],
                                    'uom_id' => $value['uom_id'],
                                    'harga' => $value['harga'],
                                    'total' => $value['total'],
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ]
                            );
                        }
                        $err_db = $this->db->error(); # Tangkap error
                    else:
                        break;
                    endif;
                }
            endif;
        } else {
            // Jika tidak ada record maka hapus detail
            $this->db->where('store_id', $store_id);
            $this->db->delete('inv_on_hand_saldo_awal');
            $err_db = $this->db->error(); # Tangkap error
        }

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal update. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil disimpan.');
        }
    }

    public function update($input)
    {
        // update
    }

    public function delete($input)
    {
        # Mulai begin trans
        $this->db->trans_begin();

        # Transaction step 1
        $this->db->where('store_id', $this->session->userdata('store_id'));
        $this->db->where('id', $input['id']);
        $this->db->delete("inv_on_hand_saldo_awal");
        $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal hapus. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil dihapus.');
        }
    }

    public function getData2Edit()
    {
        $tanggal = get_row_values("tanggal", "inv_on_hand_saldo_awal", "store_id", $this->session->userdata('store_id'))['tanggal'];

        $this->db->select("a.id AS detail_id, a.item_id, b.item_name, a.tanggal_expired, d.use_expire, a.jumlah, a.uom_id, c.uom_name, a.harga, a.total, 1 AS locked");
        $this->db->from("inv_on_hand_saldo_awal a");
        $this->db->join("m_items b", "b.item_id=a.item_id", "left");
        $this->db->join("m_uom c", "c.uom_id=a.uom_id", "left");
        $this->db->join("m_jenis d", "d.jenis_id=b.jenis_id", "left");
        $this->db->where("a.store_id", $this->session->userdata('store_id'));
        $this->db->order_by("a.id");
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->result() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->result_array(), 'tanggal' => $tanggal);
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function getList($filter)
    {
        // getList
    }

    public function getDataList($filter)
    {
        // getDataList
    }

    public function confirm($input)
    {
        // confirm
    }

    public function cancel($input)
    {
        // cancel
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    private function is_use_expire($item_id)
    {
        $this->db->select("b.use_expire");
        $this->db->from("m_items a");
        $this->db->join("m_jenis b", "b.jenis_id=a.jenis_id");
        $this->db->where("a.item_id", $item_id);
        $rs = $this->db->get()->row();
        if ($rs == null) {
            return false;
        } else {
            if ($rs->use_expire == 1) {
                return true;
            } else {
                return false;
            }
        }

    }

    public function import($data2input, $flag_update)
    {
        $message = "";

        # Mulai begin trans
        $this->db->trans_begin();

        foreach ($data2input as $key => $value) {
            # Cek jika item memiliki expire
            $use_expire = $this->is_use_expire($value['item_id']);
            if ($use_expire) {
                $cek = get_row_values("id", "inv_on_hand_saldo_awal", ["store_id", "item_id", "tanggal_expired"], [$value['store_id'], $value['item_id'], $value['tanggal_expired']]);
            } else {
                $cek = get_row_values("id", "inv_on_hand_saldo_awal", ["store_id", "item_id"], [$value['store_id'], $value['item_id']]);
            }
            # Jika data kosong maka lakukan insert
            if ($cek == null) {
                $data = [
                    'store_id' => $value['store_id'],
                    'item_id' => $value['item_id'],
                    'tanggal' => $value['tanggal'],
                    'tanggal_expired' => $use_expire ? $value['tanggal_expired'] : '1900-01-01',
                    'jumlah' => $value['jumlah'],
                    'uom_id' => $value['uom_id'],
                    'harga' => $value['harga'],
                    'total' => $value['jumlah'] * $value['harga'],
                ];
                $this->db->insert('inv_on_hand_saldo_awal', $data);
                $err_db = $this->db->error(); # Tangkap error
                # Set message
                if ($use_expire) {
                    $message .= "Simpan data : " . $value['item_id'] . " expired " . date("d/m/Y", strtotime($value['tanggal_expired'])) . "." . PHP_EOL;
                } else {
                    $message .= "Simpan data : " . $value['item_id'] . "." . PHP_EOL;
                }
            } else {
                # Jika flag_update == 1 maka update data yang sudah ada
                if ($flag_update == 1) {
                    # Update kondisi
                    if ($use_expire) {
                        $arrWhere = ['store_id' => $value['store_id'], 'item_id' => $value['item_id']];
                    } else {
                        $arrWhere = ['store_id' => $value['store_id'], 'item_id' => $value['item_id'], 'tanggal_expired' => $value['tanggal_expired']];
                    }
                    # data 2 update
                    $data = [
                        'jumlah' => $value['jumlah'],
                        'uom_id' => $value['uom_id'],
                        'harga' => $value['harga'],
                        'total' => $value['jumlah'] * $value['harga'],
                    ];
                    $this->db->update('inv_on_hand_saldo_awal', $data, $arrWhere);
                    $err_db = $this->db->error(); # Tangkap error
                    # Set message
                    if ($use_expire) {
                        $message .= "Update data : " . $value['item_id'] . " expired " . date("d/m/Y", strtotime($value['tanggal_expired'])) . "." . PHP_EOL;
                    } else {
                        $message .= "Update data : " . $value['item_id'] . "." . PHP_EOL;
                    }
                }
            }

            if (!empty($err_db['message'])) {
                break;
            }
        }

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'message' => $message);
        }
    }
}
