<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Users_Model extends Core_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function save($input)
    {
        $cek = $this->db->query("SELECT * FROM app_users WHERE user_id=?", array($input['user_id']));
        if ($cek->row() != null) {
            return array('result' => false, 'data' => null, 'msg' => 'User ID sudah ada.');
        }

        $data = array(
            'user_id' => $input['user_id'],
            'user_pass' => password_hash($input['user_pass'], PASSWORD_DEFAULT),
            'name' => $input['name'],
            'nik' => $input['nik'],
            'role_id' => $input['role_id'],
            // 'def_store_id' => $input['def_store_id'],
            'active' => $input['active'],
            'created_by' => $this->session->userdata('user_id'),
            'creation_date' => date('Y-m-d H:i:s'),
        );

        $NonQry = $this->db->insert('app_users', $data);

        if (!$NonQry && !empty($this->db->error())) {
            $msg_err = $this->db->error();
            $msg = explode(':', $msg_err['message']);
            return array('result' => false, 'data' => null, 'msg' => 'Gagal input. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            $id_kar = $this->db->query("SELECT id FROM app_users WHERE user_id=?", array($input['user_id']))->row();
            return array('result' => true, 'data' => (array('id' => $id_kar->id)), 'msg' => 'Data berhasil disimpan.');
        }
    }

    public function update($input)
    {
        if (trim($input['user_pass']) == "" || empty($input['user_pass']) || $input['user_pass'] == null) {
            $data = array(
                'name' => $input['name'],
                'id' => $input['id'],
                'role_id' => $input['role_id'],
                // 'def_store_id' => $input['def_store_id'],
                'active' => $input['active'],
                'modified_by' => $this->session->userdata('user_id'),
                'modification_date' => date('Y-m-d H:i:s'),
            );
        } else {
            $data = array(
                'user_pass' => password_hash($input['user_pass'], PASSWORD_DEFAULT),
                'name' => $input['name'],
                'nik' => $input['nik'],
                'role_id' => $input['role_id'],
                // 'def_store_id' => $input['def_store_id'],
                'active' => $input['active'],
                'modified_by' => $this->session->userdata('user_id'),
                'modification_date' => date('Y-m-d H:i:s'),
            );
        }

        $this->db->where('user_id', $input['user_id']);
        $NonQry = $this->db->update('app_users', $data);

        if (!$NonQry && !empty($this->db->error())) {
            $msg_err = $this->db->error();
            $msg = explode(':', $msg_err['message']);
            return array('result' => false, 'data' => null, 'msg' => 'Gagal input. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'msg' => 'Data berhasil diupdate.');
        }
    }

    public function delete($input)
    {
        $this->db->where_in('id', $input['id']);
        $NonQry = $this->db->delete("app_users");

        if (!$NonQry && !empty($this->db->error())) {
            $msg_err = $this->db->error();
            $msg = explode(':', $msg_err['message']);
            return array('result' => false, 'data' => null, 'msg' => 'Gagal input. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'msg' => 'Data berhasil dihapus.');
        }
    }

    public function getData2Edit($id)
    {
        $Qry = $this->db->query("SELECT a.user_id, a.name, a.role_id, b.role_name, a.nik, a.active
										FROM app_users a
											LEFT JOIN app_roles b ON b.role_id=a.role_id
										WHERE a.id=?", array($id));

        if ($Qry->result() != null) {
            return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
        } else {
            return array('result' => false, 'msg' => 'Data tidak ditmeukan.', 'data' => null);
        }
    }

    public function getList($filter)
    {
        $role = $this->session->userdata('role_id');
        $user = $this->session->userdata('user_id');
        $this->datatables->select("a.id, a.user_id, a.name, a.role_id, a.active");
        $this->datatables->from("app_users a");   
        if ($role != 'ROOT') {
            $this->datatables->where('user_id', $user);
        }     
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        return $this->datatables->generate();
    }

    public function getDataList($filter)
    {
        $this->db->select("a.user_id, a.name, a.role_id, b.role_name, a.active");
        $this->db->from('app_users a');
        $this->db->join('app_roles b', 'b.role_id=a.role_id', 'left');
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        return $this->db->get();
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/
    public function getRoleList()
    {
        $this->datatables->select('a.role_id, a.role_name, a.desc');
        $this->datatables->from('app_roles a');
        return $this->datatables->generate();
    }

}
