<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Coa_Model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function save($input)
    {
        $cek = get_row_values("code", "m_coa_group", "code", $input['code']);
        if ($cek != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code sudah ada.');
        }

        $data = array(
            'code' => $input['code'],
            'name' => $input['name'],
            'created_by' => $this->session->userdata('user_id'),
            'creation_date' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('m_coa_group', $data);
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil disimpan.');
        }
    }

    public function save_coa($input)
    {
        $cek = get_row_values("code", "m_coa", "code", $input['code_coa']);
        if ($cek != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code sudah ada.');
        }

        $data = array(
            'group_code' => $input['group_code'],
            'parent_code' => check_arr_empty_or_null($input['parent_code']) ? $input['code_coa'] : $input['parent_code'],
            'code' => $input['code_coa'],
            'name' => $input['name_coa'],
            'type' => $input['tipe_coa'],
            'is_cash_bank' => $input['kas_bank_coa'],
            'created_by' => $this->session->userdata('user_id'),
            'creation_date' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('m_coa', $data);
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil disimpan.');
        }
    }

    public function save_setting($input)
    {
        $cek = get_row_values("coa_setting", "m_coa_setting", ["store_id", "coa_setting"], [$this->session->userdata('store_id'), $input['setting_code']]);
        if ($cek != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code sudah ada.');
        }

        $data = array(
            'store_id' => $this->session->userdata('store_id'),
            'coa_setting' => $input['setting_code'],
            'coa_code' => $input['setting_coa_code'],
            'updated_at' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('m_coa_setting', $data);
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil disimpan.');
        }
    }

    public function update($input)
    {
        $data = array(
            'name' => $input['name'],
            'modified_by' => $this->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        );

        $this->db->where('code', $input['id']);
        $this->db->update('m_coa_group', $data);
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal update. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil diupdate.');
        }
    }

    public function update_coa($input)
    {
        $data = array(
            'group_code' => $input['group_code'],
            'parent_code' => check_arr_empty_or_null($input['parent_code']) ? $input['code_coa'] : $input['parent_code'],
            'name' => $input['name_coa'],
            'type' => $input['tipe_coa'],
            'is_cash_bank' => $input['kas_bank_coa'],
            'modified_by' => $this->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        );

        $this->db->where('code', $input['id']);
        $this->db->update('m_coa', $data);
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal update. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil diupdate.');
        }
    }

    public function update_setting($input)
    {
        $data = array(
            'coa_code' => $input['setting_coa_code'],
            'updated_at' => date('Y-m-d H:i:s'),
        );

        $this->db->where('coa_setting', $input['id']);
        $this->db->update('m_coa_setting', $data);
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal update. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil diupdate.');
        }
    }

    public function delete($input)
    {
        // delete
    }

    public function getData2Edit($id)
    {
        $this->db->select("code, name");
        $this->db->from("m_coa_group");
        $this->db->where("code", $id);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row_array());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function getData2Edit_coa($id)
    {
        $this->db->select("a.group_code, b.name AS group_name, a.parent_code, c.name AS parent_name, a.code, a.name, a.type, a.is_cash_bank");
        $this->db->from("m_coa a");
        $this->db->join("m_coa_group b", "b.code=a.group_code", "left");
        $this->db->join("m_coa c", "c.code=a.parent_code AND c.type='H'", "left");
        $this->db->where("a.code", $id);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row_array());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function getData2Edit_setting($id)
    {
        $this->db->select("a.coa_setting, a.coa_code, b.name AS coa_name");
        $this->db->from("m_coa_setting a");
        $this->db->join("m_coa b", "b.code=a.coa_code", "left");
        $this->db->where("a.coa_setting", $id);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row_array());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function getList($filter)
    {
        $this->datatables->select("code, name");
        $this->datatables->from('m_coa_group');
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        return $this->datatables->generate();
    }

    public function list_coa($filter)
    {
        $this->datatables->select("a.group_code, b.name AS group_name, a.parent_code, c.name AS parent_name, a.code, a.name, a.type, a.is_cash_bank");
        $this->datatables->from("m_coa a");
        $this->datatables->join("m_coa_group b", "b.code=a.group_code", "left");
        $this->datatables->join("m_coa c", "c.code=a.parent_code", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        $this->datatables->order_by("a.group_code, a.code");
        return $this->datatables->generate();
    }

    public function list_coa_setting($filter)
    {
        $this->datatables->select("a.coa_setting, a.coa_code, b.name AS coa_name");
        $this->datatables->from("m_coa_setting a");
        $this->datatables->join("m_coa b", "b.code=a.coa_code", "left");
        $this->datatables->where("a.store_id", $this->session->userdata('store_id'));
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        return $this->datatables->generate();
    }

    public function getDataList($filter)
    {
        $this->db->select("code, name");
        $this->db->from('m_coa_group');
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        return $this->db->get();
    }

    public function get_data_list_coa($filter)
    {
        $this->db->select("a.group_code, b.name AS group_name, a.parent_code, c.name AS parent_name, a.code, a.name, a.type, CASE WHEN a.is_cash_bank = 1 THEN 'Ya' ELSE 'Tidak' END AS is_cash_bank_label");
        $this->db->from("m_coa a");
        $this->db->join("m_coa_group b", "b.code=a.group_code", "left");
        $this->db->join("m_coa c", "c.code=a.parent_code", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $this->db->order_by("a.group_code, a.code");

        return $this->db->get();
    }

    public function get_data_list_setting($filter)
    {
        $this->db->select("a.coa_setting, a.coa_code, b.name AS coa_name");
        $this->db->from("m_coa_setting a");
        $this->db->join("m_coa b", "b.code=a.coa_code", "left");
        $this->db->where("a.store_id", $this->session->userdata('store_id'));
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }

        return $this->db->get();
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function get_grup_coa_lov()
    {
        $this->datatables->select("a.code, a.name");
        $this->datatables->from("m_coa_group a");
        $result = $this->datatables->generate();

        return $result;
    }

    public function get_coa_lov()
    {
        $this->datatables->select("a.group_code, b.name AS group_name, CONCAT(a.group_code, ' - ', b.name) AS group_label,
									a.parent_code, c.name AS parent_name, CONCAT(a.parent_code, ' - ', c.name) AS parent_label,
									a.code, a.name, a.type,
									a.is_cash_bank, CASE WHEN a.is_cash_bank = 1 THEN 'Ya' ELSE 'Tidak' END AS is_cash_bank_label");
        $this->datatables->from("m_coa a");
        $this->datatables->join("m_coa_group b", "b.code=a.group_code", "left");
        $this->datatables->join("m_coa c", "c.code=a.parent_code", "left");
        $result = $this->datatables->generate();

        return $result;
    }

    public function get_coa_detail_lov()
    {
        $this->datatables->select("a.code, a.name, a.type");
        $this->datatables->from("m_coa a");
        // $this->datatables->where("a.is_cash_bank", 1);
        $this->datatables->where("a.type", "D");
        $result = $this->datatables->generate();

        return $result;
    }

    public function get_coa($keyword)
    {
        $this->db->select("a.code, a.name");
        $this->db->from("m_coa a");
        $this->db->where("a.type", "D");
        $this->db->like("a.code", $keyword);

        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

}
