<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Saldo_awal_coa_model extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function save($input)
    {
        # Mulai begin trans
        $this->db->trans_begin();

        $tanggal = date_convert_format($input['tanggal']);
        $store_id = $this->session->userdata('store_id');

        $detail = json_decode($input['detail'], true);
        if (count($detail) > 0) {
            $list_arr_id = array_column($detail, 'detail_id');
            # Hapus dari tabel detail yang tidak memiliki id pada array dan list_arr_id
            $this->db->where_not_in('id', $list_arr_id);
            $this->db->where('store_id', $store_id);
            $this->db->delete('t_coa_saldo_awal');
            $err_db = $this->db->error(); # Tangkap error
            if (empty($err_db['message'])):
                # Looping update/insert ke tabel detail
                foreach ($detail as $key => $value) {
                    if (check_arr_empty_or_null($value['coa_no']) || $value['jumlah'] <= 0 || $value['locked'] == 0) {
                        continue;
                    }

                    if (empty($err_db['message'])):
                        $cek = get_row_values("id", "t_coa_saldo_awal", "id", $value['detail_id']);
                        if ($cek != null) {
                            $this->db->update(
                                't_coa_saldo_awal',
                                [
                                    'coa_no' => $value['coa_no'],
                                    'tanggal' => $tanggal,
                                    'jumlah' => $value['jumlah'],
                                    'modified_by' => $this->session->userdata('user_id'),
                                    'modification_date' => date('Y-m-d H:i:s'),
                                ],
                                [
                                    'id' => $value['detail_id'],
                                ]
                            );
                        } else {
                            $this->db->insert(
                                't_coa_saldo_awal',
                                [
                                    'store_id' => $store_id,
                                    'coa_no' => $value['coa_no'],
                                    'tanggal' => $tanggal,
                                    'jumlah' => $value['jumlah'],
                                    'created_by' => $this->session->userdata('user_id'),
                                    'creation_date' => date('Y-m-d H:i:s'),
                                ]
                            );
                        }
                        $err_db = $this->db->error(); # Tangkap error
                    else:
                        break;
                    endif;
                }
            endif;
        } else {
            // Jika tidak ada record maka hapus detail
            $this->db->where('store_id', $store_id);
            $this->db->delete('t_coa_saldo_awal');
            $err_db = $this->db->error(); # Tangkap error
        }

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal update. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil disimpan.');
        }
    }

    public function update($input)
    {
        // update
    }

    public function delete($input)
    {
        # Mulai begin trans
        $this->db->trans_begin();

        # Transaction step 1
        $this->db->where('store_id', $this->session->userdata('store_id'));
        $this->db->where('id', $input['id']);
        $this->db->delete("t_coa_saldo_awal");
        $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal hapus. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'message' => 'Data berhasil dihapus.');
        }
    }

    public function getData2Edit()
    {
        $tanggal = get_row_values("tanggal", "t_coa_saldo_awal", "store_id", $this->session->userdata('store_id'))['tanggal'];

        $this->db->select("a.id AS detail_id, a.store_id, b.store_name, a.coa_no, c.name AS coa_name, a.jumlah, 1 AS locked");
        $this->db->from("t_coa_saldo_awal a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_coa c", "c.code=a.coa_no", "left");
        $this->db->where("a.store_id", $this->session->userdata('store_id'));
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->result() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->result_array(), 'tanggal' => $tanggal);
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function getList($filter)
    {
        // getList
    }

    public function getDataList($filter)
    {
        // getDataList
    }

    public function confirm($input)
    {
        // confirm
    }

    public function cancel($input)
    {
        // cancel
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function import($data2input, $flag_update)
    {
        $message = "";

        # Mulai begin trans
        $this->db->trans_begin();

        foreach ($data2input as $key => $value) {
            # Cek data
            $cek = get_row_values("id", "t_coa_saldo_awal", ["store_id", "coa_no"], [$value['store_id'], $value['coa_no']]);
            # Jika data kosong maka lakukan insert
            if ($cek == null) {
                $data = [
                    'store_id' => $value['store_id'],
                    'coa_no' => $value['coa_no'],
                    'tanggal' => $value['tanggal'],
                    'jumlah' => $value['jumlah'],
                    'created_by' => $this->session->userdata('user_id'),
                    'creation_date' => date('Y-m-d H:i:s'),
                ];
                $this->db->insert('t_coa_saldo_awal', $data);
                $err_db = $this->db->error(); # Tangkap error
                # Set message
                $message .= "Simpan data : " . $value['coa_no'] . "." . PHP_EOL;
            } else {
                # Jika flag_update == 1 maka update data yang sudah ada
                if ($flag_update == 1) {
                    # Update kondisi
                    $arrWhere = ['store_id' => $value['store_id'], 'coa_no' => $value['coa_no']];
                    # data 2 update
                    $data = [
                        'jumlah' => $value['jumlah'],
                        'modified_by' => $this->session->userdata('user_id'),
                        'modification_date' => date('Y-m-d H:i:s'),
                    ];
                    $this->db->update('t_coa_saldo_awal', $data, $arrWhere);
                    $err_db = $this->db->error(); # Tangkap error
                    # Set message
                    $message .= "Update data : " . $value['coa_no'] . "." . PHP_EOL;
                }
            }

            if (!empty($err_db['message'])) {
                break;
            }
        }

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'message' => $message);
        }
    }
}
