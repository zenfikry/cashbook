<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Coa extends Core_Controller
{

    public function __construct()
    {
        parent::__construct("coa"); # parsing menu_id
        $this->load->model("coa_Model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->data['action'] = 'create';
        $this->load->view('Coa', $this->data);
    }

    public function getList()
    {
        $filter = [];

        $this->output->set_content_type('application/json');
        echo $this->m_app->getList($filter);
    }

    public function list_coa()
    {
        $filter = [
            'a.type' => $this->input->post("filter_coa_tipe", true),
            'a.is_cash_bank' => $this->input->post("filter_coa_cash_bank", true),
        ];

        $this->output->set_content_type('application/json');
        echo $this->m_app->list_coa($filter);
    }

    public function list_coa_setting()
    {
        $filter = [];

        $this->output->set_content_type('application/json');
        echo $this->m_app->list_coa_setting($filter);
    }

    private function validation($method = 'create')
    {
        if ($method == 'create' || $method == 'edit'):
            $this->form_validation->set_rules('code', 'Code', 'required|trim');
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
        endif;

        if ($method == 'edit' || $method == 'delete'):
            $this->form_validation->set_rules('id', 'Id', 'required|trim');
        endif;

        if ($this->form_validation->run()) {
            return ['result' => true, 'message' => 'OK'];
        } else {
            return ['result' => false, 'message' => validation_errors()];
        }
    }

    private function validation_coa($method = 'create')
    {
        if ($method == 'create' || $method == 'edit'):
            $this->form_validation->set_rules('group_code', 'Grup Code', 'required|trim');
            $this->form_validation->set_rules('group_name', 'Grup Name', 'required|trim');
            $this->form_validation->set_rules('code_coa', 'Code', 'required|trim');
            $this->form_validation->set_rules('name_coa', 'Name', 'required|trim');
            $this->form_validation->set_rules('tipe_coa', 'Tipe Name', 'required|trim');
            $this->form_validation->set_rules('kas_bank_coa', 'Kas / Bank', 'required|trim');
        endif;

        if ($method == 'edit' || $method == 'delete'):
            $this->form_validation->set_rules('id', 'Id', 'required|trim');
        endif;

        if ($this->form_validation->run()) {
            return ['result' => true, 'message' => 'OK'];
        } else {
            return ['result' => false, 'message' => validation_errors()];
        }
    }

    private function validation_setting($method = 'create')
    {
        if ($method == 'create' || $method == 'edit'):
            $this->form_validation->set_rules('setting_code', 'Code', 'required|trim');
            $this->form_validation->set_rules('setting_coa_code', 'COA', 'required|trim');
            $this->form_validation->set_rules('setting_coa_name', 'COA', 'required|trim');
        endif;

        if ($method == 'edit' || $method == 'delete'):
            $this->form_validation->set_rules('id', 'Id', 'required|trim');
        endif;

        if ($this->form_validation->run()) {
            return ['result' => true, 'message' => 'OK'];
        } else {
            return ['result' => false, 'message' => validation_errors()];
        }
    }

    public function save($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation('create');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->save($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function save_coa($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation_coa('create');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->save_coa($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function save_setting($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation_setting('create');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->save_setting($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function getData2Edit($format = 'json', $id = null)
    {
        if ($id != null || trim($id) != '' || !empty($id)) {
            $hasil = $this->m_app->getData2Edit($id);
        } else {
            $hasil = array('result' => false, 'message' => 'Data kosong.', 'data' => null);
        }

        echo $this->output($format, $hasil);
    }

    public function getData2Edit_coa($format = 'json', $id = null)
    {
        if ($id != null || trim($id) != '' || !empty($id)) {
            $hasil = $this->m_app->getData2Edit_coa($id);
        } else {
            $hasil = array('result' => false, 'message' => 'Data kosong.', 'data' => null);
        }

        echo $this->output($format, $hasil);
    }

    public function getData2Edit_setting($format = 'json', $id = null)
    {
        if ($id != null || trim($id) != '' || !empty($id)) {
            $hasil = $this->m_app->getData2Edit_setting($id);
        } else {
            $hasil = array('result' => false, 'message' => 'Data kosong.', 'data' => null);
        }

        echo $this->output($format, $hasil);
    }

    public function update($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation('edit');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->update($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function update_coa($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation_coa('edit');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->update_coa($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function update_setting($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation_setting('edit');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->update_setting($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function delete($format = 'json')
    {
        show_404();
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Daftar Group COA";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('L', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "KopSurat";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = [];

        $data = $this->m_app->getDataList($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 14);
        $pdf->Cell(0, 5, $JudulLaporan, 0, 1, 'L');
        $pdf->Ln(10);

        $pdf->SetFont('helvetica', '', 10);
        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
					<th>Code</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>
				' . $this->pdf_render_detail($data->result_array()) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			<tr nobr="true">
				<td>' . $value['code'] . '</td>
				<td>' . $value['name'] . '</td>
			</tr>';
        }

        return $html;
    }

    public function pdf_coa()
    {
        # Judul laporan
        $JudulLaporan = "Daftar COA";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('L', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "KopSurat";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'a.type' => $this->input->post("filter_coa_tipe", true),
            'a.is_cash_bank' => $this->input->post("filter_coa_cash_bank", true),
        );

        $data = $this->m_app->get_data_list_coa($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 14);
        $pdf->Cell(0, 5, $JudulLaporan, 0, 1, 'L');
        $pdf->Ln(10);

        $pdf->SetFont('helvetica', '', 10);
        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
					<th>Grup COA</th>
					<th>Parent COA</th>
					<th>Code</th>
					<th>Name</th>
					<th>Type</th>
					<th>Kas/Bank</th>
				</tr>
			</thead>
			<tbody>
				' . $this->pdf_render_detail_coa($data->result_array()) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail_coa($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			<tr nobr="true">
				<td>' . $value['group_code'] . ' - ' . $value['group_name'] . '</td>
                <td>' . $value['parent_code'] . ' - ' . $value['parent_name'] . '</td>
				<td>' . $value['code'] . '</td>
				<td>' . $value['name'] . '</td>
				<td>' . $value['type'] . '</td>
				<td>' . $value['is_cash_bank_label'] . '</td>
			</tr>';
        }

        return $html;
    }

    public function pdf_setting()
    {
        # Judul laporan
        $JudulLaporan = "Daftar Setting COA";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('L', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "KopSurat";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = [];

        $data = $this->m_app->get_data_list_setting($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 14);
        $pdf->Cell(0, 5, $JudulLaporan, 0, 1, 'L');
        $pdf->Ln(10);

        $pdf->SetFont('helvetica', '', 10);
        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
					<th>Setting</th>
					<th>COA</th>
				</tr>
			</thead>
			<tbody>
				' . $this->pdf_setting_detail($data->result_array()) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_setting_detail($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			<tr nobr="true">
				<td>' . $value['coa_setting'] . '</td>
				<td>' . $value['coa_code'] . ' - ' . $value['coa_name'] . '</td>
			</tr>';
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Daftar Group COA";
        # Ambil data
        $filter = [];
        $rs = $this->m_app->getDataList($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('Code', 'Name');
        $Col['type'] = array('string', 'string');
        $Col['align'] = array('left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    public function xls_coa()
    {
        # Judul laporan
        $JudulLaporan = "Daftar COA";
        # Ambil data
        $filter = array(
            'a.type' => $this->input->post("filter_coa_tipe", true),
            'a.is_cash_bank' => $this->input->post("filter_coa_cash_bank", true),
        );

        $rs = $this->m_app->get_data_list_coa($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('Grup Code', 'Grup Name', 'Parent Code', 'Parent Name', 'Code', 'Name', 'Type', 'Kas/Bank');
        $Col['type'] = array('string', 'string', 'string', 'string', 'string', 'string', 'string', 'string');
        $Col['align'] = array('left', 'left', 'left', 'left', 'left', 'left', 'left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    public function xls_setting()
    {
        # Judul laporan
        $JudulLaporan = "Daftar Setting COA";
        # Ambil data
        $filter = [];
        $rs = $this->m_app->get_data_list_setting($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('Setting', 'COA Code', 'COA Name');
        $Col['type'] = array('string', 'string', 'string');
        $Col['align'] = array('left', 'left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function get_grup_coa_lov($action = "home")
    {
        $data['ColHeader'] = array('Code', 'Name');
        $data['ColShow'] = array(1, 1);
        $data['columns'] = array(
            array('data' => 'code'),
            array('data' => 'name'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                echo $this->m_app->get_grup_coa_lov();
                break;
            case 'home':
                $data['Judul'] = 'Group COA List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '';
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function get_coa_lov($action = "home")
    {
        $data['ColHeader'] = array('Grup', 'Parent', 'Code', 'Name', 'Type', 'Kas/Bank');
        $data['ColShow'] = array(1, 1, 1, 1, 1, 1);
        $data['columns'] = array(
            array('data' => 'group_label'),
            array('data' => 'parent_label'),
            array('data' => 'code'),
            array('data' => 'name'),
            array('data' => 'type'),
            array('data' => 'is_cash_bank_label'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                echo $this->m_app->get_coa_lov();
                break;
            case 'home':
                $data['Judul'] = 'COA List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '';
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function get_coa_detail_lov($action = "home")
    {
        $data['ColHeader'] = array('Code', 'Name');
        $data['ColShow'] = array(1, 1);
        $data['columns'] = array(
            array('data' => 'code'),
            array('data' => 'name'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                echo $this->m_app->get_coa_detail_lov();
                break;
            case 'home':
                $data['Judul'] = 'COA List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '';
                $data['search'] = $this->input->get('search', true);
                $this->load->view('LOV_COA', $data);
                break;
        }
    }

    public function get_coa()
    {
        $keyword = $this->input->get("keyword", true);

        $response = $this->m_app->get_coa($keyword);

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }

}
