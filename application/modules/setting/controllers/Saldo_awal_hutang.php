<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Saldo_awal_hutang extends Core_Controller
{
    public function __construct()
    {
        parent::__construct("saldoawalhutang"); # parsing menu_id
        $this->load->model("saldo_awal_hutang_model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        $this->load->view('Saldo_awal_hutang', $this->data);
    }

    public function create()
    {
        // save
    }

    public function getList()
    {
        // getList
    }

    private function validation($method = 'create')
    {
        if ($method == 'create' || $method == 'edit'):
            $this->form_validation->set_rules('tanggal', 'Tanggal', 'required|trim');
            $this->form_validation->set_rules('detail', 'Detail', 'required|trim');
        endif;

        if ($method == 'edit' || $method == 'delete'):
            $this->form_validation->set_rules('id', 'Item', 'required|trim');
        endif;

        if ($this->form_validation->run()) {
            return ['result' => true, 'message' => 'OK'];
        } else {
            return ['result' => false, 'message' => validation_errors()];
        }
    }

    public function save($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation('create');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->save($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function update($format = 'json')
    {
        show_404();
    }

    public function edit($id = null)
    {
        show_404();
    }

    public function delete($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;

        $verifikasi = $this->validation('delete');
        if ($verifikasi['result']):
            $input = $this->security->xss_clean($this->input->post());
            $hasil = $this->m_app->delete($input);
        else:
            $hasil = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
        endif;

        echo $this->output($format, $hasil);
    }

    public function getData2Edit($format = 'json')
    {
        $input = [
            'supp_id' => $this->input->get("supp_id", true),
        ];

        $hasil = $this->m_app->getData2Edit($input);

        echo $this->output($format, $hasil);
    }

    public function confirm($format = 'json')
    {
        show_404();
    }

    public function cancel($format = 'json')
    {
        show_404();
    }

    public function pdf()
    {
        show_404();
    }

    public function xls()
    {
        show_404();
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function download_template()
    {
        $this->load->helper('download');
        force_download('./assets/data_upload/template/template_import_saldo_awal_hutang.xlsx', null);
    }

    public function import($format = "json")
    {
        # Set rule
        $validation = ['result' => true, 'message' => 'OK'];
        if (empty($_FILES['upload_file_import']['name'])) {
            $this->form_validation->set_rules('upload_file_import', 'File upload', 'required');
            # Cek rule
            if (!$this->form_validation->run()) {
                $validation = ['result' => false, 'message' => validation_errors()];
            }
        }
        # Cek jika $validation sukses
        if ($validation['result']) {
            $data_upload = uploadDataExcel('upload_file_import', sha1(time()) . '_imported', './assets/data_upload');
            $input = $this->security->xss_clean($this->input->post());
            if (!empty($_FILES)) {
                if ($data_upload['upload_status'] == true) {
                    $path_file = $data_upload['upload_data']['full_path'];
                    /**  Identify the type of $inputFileName  **/
                    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path_file);
                    /**  Create a new Reader of the type that has been identified  **/
                    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    /**  Load $inputFileName to a Spreadsheet Object  **/
                    $spreadsheet = $reader->load($path_file);
                    /**  Convert Spreadsheet Object to an Array for ease of use  **/
                    $data_xls = $spreadsheet->getActiveSheet()->toArray();
                    $message = "";
                    $data2input = [];
                    foreach ($data_xls as $key => $row) {
                        # Skip first row
                        if ($key == 0) {
                            continue;
                        }

                        # Check jika store_id, wil_id dan store_name kosong
                        if (check_arr_empty_or_null($row[0]) || check_arr_empty_or_null($row[1]) || check_arr_empty_or_null($row[2]) || check_arr_empty_or_null($row[3])) {
                            $message .= 'Melewati data : ' . $row[1] . "." . PHP_EOL;
                            continue;
                        }

                        # Prepare data untuk disimpan
                        $data2input[] = [
                            'store_id' => $row[0],
                            'kode_supel' => $row[1],
                            'tanggal' => date_convert_format($input['tanggal']),
                            'coa_code' => $row[2],
                            'jumlah' => unformat_numeric($row[3]),
                        ];
                    }
                    # Hapus file yang sudah di upload
                    if (trim($path_file) != '') {
                        if (file_exists($path_file)) {
                            unlink($path_file);
                        }
                    }

                    # Simpan
                    $save = $this->m_app->import($data2input, $input['flag_update']);
                    $message .= $save['message'];
                    $result = ['result' => true, 'message' => $message];
                } else {
                    $result = ['result' => false, 'message' => 'File not found'];
                }
            } else {
                $result = ['result' => false, 'message' => $data_upload['error']];
            }
        } else {
            $result = ['result' => false, 'message' => $validation['message']];
        }

        echo $this->output($format, $result);
    }

}
