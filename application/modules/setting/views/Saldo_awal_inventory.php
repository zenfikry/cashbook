<style type="text/css">
   .disable {
      background: #DDD !Important;
   }
</style>

<div class="main">
   <div class="row">
      <div class="col-md-12">
         <?php if ($priv_arr['create_flag'] || $priv_arr['edit_flag']) {?>
            <button type="button" class="btn btn-outline-success waves-effect waves-light" id="simpan" name="simpan" onclick="Simpan();"><span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span> Simpan</button>
            <button type="button" class="btn btn-outline-info waves-effect waves-light" role="button" id="app_import" name="app_import"> <i class="fas fa-file-import"></i> Import </button>
         <?php }?>
      </div>
   </div>
   <div class="row mt-3">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <form id="form_input" name="form_input">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="card" style="border:solid 1px #3EB7BA;">
                           <div class="card-header bg-primary text-white">
                              <div class="row">
                                 <div class="col-lg-12">
                                    Saldo Awal Inventory
                                 </div>
                              </div>
                           </div>
                           <div class="card-body">
                              <div class="row">
                                 <div class="form-group col-md-3">
                                    <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                                    <div class="input-group date">
                                       <input type="text" class="form-control" placeholder="Tanggal" id="tanggal" name="tanggal" value="<?=date('Y-m-d h:i:s')?>">
                                       <div class="input-group-append">
                                          <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <input type="hidden" name="total_jumlah" id="total_jumlah" value="" readonly="readonly" />
                                    <input type="hidden" name="grand_total" id="grand_total" value="" readonly="readonly" />
                                    <table id="table_detail" class="display compact nowrap table table-striped table-hover" cellspacing="0" width="100%">
                                       <thead>
                                          <tr>
                                             <th>Kode</th>
                                             <th>Nama</th>
                                             <th>Tanggal Expire</th>
                                             <th>Qty</th>
                                             <th>Satuan</th>
                                             <th>Harga</th>
                                             <th>Total</th>
                                             <th>Locked</th>
                                             <th>Actions</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                       </tbody>
                                       <tfoot>
                                          <tr>
                                             <th colspan="3">Sub Total</th>
                                             <th>0</th>
                                             <th></th>
                                             <th></th>
                                             <th>0</th>
                                             <th></th>
                                             <th></th>
                                          </tr>
                                       </tfoot>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

               </form>
            </div>
         </div>
      </div>

   </div>

   <!--begin::Modal-->
   <div class="modal" id="modalFormImport" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalFormImport" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title">Import</small></h5>
            </div>
            <form class="form" id="form_import" name="form_import">
               <div class="modal-body">

                  <div class="row">
                     <div class="col-12">
                        <div class="form-group">
                           <label>File XLS </label>
                           <input type="file" name="upload_file_import" id="upload_file_import" class="form-control mb-3" />
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-6">
                        <div class="form-group">
                           <div class="form-check form-switch mb-3" dir="ltr">
                              <input type="checkbox" class="form-check-input mt-1" id="flag_update" name="flag_update" value="1">
                              <label class="form-check-label" for="flag_update">Update data jika ada</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-6 text-right">
                        <a href="<?=base_url($modules . "/" . $controller . "/download_template")?>" target="_blank" class="btn btn-outline-success font-weight-bold"><span class="btn-label"><i class="fas fa-download mr-2"></i></span> Download Template</a>
                     </div>
                  </div>
                  <div class="row" id="info_import_container">
                     <div class="col-12">
                        <div class="form-group">
                           <label>Info </label>
                           <textarea class="form-control" rows="5" placeholder="Info upload" id="info_upload" name="info_upload" readonly></textarea>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="modal-footer">
                  <div class="col">
                     <button type="button" class="btn btn-outline-success font-weight-bold" id="upload_import" name="upload_import"><span class="btn-label"><i class="fas fa-upload mr-2"></i></span> Upload</button>
                  </div>
                  <div class="col text-right">
                     <button type="button" class="btn btn-outline-danger font-weight-bold" data-dismiss="modal"><span class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!--begin::Modal-->

</div>

<script type="text/javascript">
   // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
   var data2Send = null;
   var action = '<?=$action?>';
   var _id = "<?=$action == 'edit' ? $id : 'null'?>";
   // END VARIABEL WAJIB

   var _dataTable = null;
   var DataTableAction = 'create';
   var DataTableRowIdx = 0;
   var data4DataTable = [];

   $.getScript('<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.js?v=<?=date('YmdHis') . rand()?>', function( data, textStatus, jqxhr ) {
      initPage();
   });
</script>