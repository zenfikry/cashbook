<div class="row">
   <div class="card col">
      <div class="card-body">
         <div class="alert alert-info mb-0" role="alert">
            <h4 class="alert-heading font-18">Master COA</h4>
            <p>Module ini akan digunakan untuk pengaturan Chart Of Account (COA) / Kode Perkiraan.</p>
         </div>
      </div>
   </div>
</div>

<!-- START CONTENT -->

<div class="row">
   <div class="card">
      <div class="card-body">

         <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
            <li class="nav-item active">
               <a class="nav-link" data-bs-toggle="tab" href="#" role="tab" id="coa_grup_link">
                  <span class="d-none d-sm-block">Grup COA</span>
               </a>
            </li>
            <li class="nav-item">
               <a class="nav-link active" data-bs-toggle="tab" href="#" role="tab" id="coa_daftar_link">
                  <span class="d-none d-sm-block">Daftar COA</span>
               </a>
            </li>
            <li class="nav-item">
               <a class="nav-link active" data-bs-toggle="tab" href="#" role="tab" id="coa_setting_link">
                  <span class="d-none d-sm-block">Setting COA</span>
               </a>
            </li>
         </ul>

         <!-- Tab panes -->
         <div class="tab-content">
            <!-- START Group COA -->
            <div class="tab-pane active p-3" id="coa_grup" role="tabpanel">
               <div class="row">
                  <div class="col-md-6">
                     <div class="page-title-box">
                        <div class="button-items">
                           <button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light" id="app_refresh" name="app_refresh" onclick="app_refresh();"><i class="fas fa-sync"></i> Refresh </button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="page-title-box text-right">
                        <div class="button-items">
                           <?php if ($priv_arr['pdf_flag']) {?>
                              <button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" id="app_pdf" name="app_pdf" onclick="app_pdf();"><i class="fas fa-file-pdf"></i> Pdf </button>
                           <?php }?>
                           <?php if ($priv_arr['xls_flag']) {?>
                              <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" id="app_xls" name="app_xls" onclick="app_xls();"><i class="fas fa-file-excel"></i> Excel/CSV </button>
                           <?php }?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <form id="form_input" name="form_input" class="lobi-form">
                        <div class="row">
                           <div class="form-group col-md-12">
                              <label class="control-label">Code <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Code" id="code" name="code" value="" maxlength="2" />
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="" maxlength="50" />
                           </div>
                           <div class="col-md-12">
                              <button type="button" class="btn btn-outline-dark btn-sm waves-effect waves-light" id="reset_form" name="reset_form" onclick="ResetForm();"><span class="btn-label"><i class="glyphicon glyphicon-repeat "></i></span> Reset</button>
                                 <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" style="float: right;" id="simpan" name="simpan" onclick="Simpan();"><span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span> Simpan</button>
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="col-lg-8">
                     <div class="col-lg-12">
                        <div class="card">
                           <div class="card-body">
                              <table id="table_list_data" class="table dt-responsive nowrap text-nowrap">
                                 <thead>
                                    <tr>
                                       <th>Code</th>
                                       <th>Name</th>
                                       <th>Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END Group COA -->

            <!-- START Daftar COA -->
            <div class="tab-pane active p-3" id="coa_daftar" role="tabpanel">
               <div class="row">
                  <div class="col-md-6">
                     <div class="page-title-box">
                        <div class="button-items">
                           <button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light" id="app_refresh_coa" name="app_refresh_coa" onclick="app_refresh_coa();"><i class="fas fa-sync"></i> Refresh </button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="page-title-box text-right">
                        <div class="button-items">
                           <?php if ($priv_arr['pdf_flag']) {?>
                              <button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" id="app_pdf_coa" name="app_pdf_coa" onclick="app_pdf_coa();"><i class="fas fa-file-pdf"></i> Pdf </button>
                           <?php }?>
                           <?php if ($priv_arr['xls_flag']) {?>
                              <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" id="app_xls_coa" name="app_xls_coa" onclick="app_xls_coa();"><i class="fas fa-file-excel"></i> Excel/CSV </button>
                           <?php }?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <form id="form_input_coa" name="form_input_coa" class="lobi-form">
                        <div class="row">
                           <div class="form-group col-md-12">
                              <label class="control-label">Grup COA <span class="text-danger">*</span></label>
                              <div class="input-group">
                                 <input type="hidden" name="group_code" id="group_code" value="" readonly="readonly" />
                                 <input type="text" class="form-control" placeholder="Pilih Grup COA" name="group_name" id="group_name" value="" readonly="readonly" />
                                 <div class="input-group-append">
                                    <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVGroupCOA();"><i class="fas fa-search"></i></button>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">Parent COA </label>
                              <div class="input-group">
                                 <input type="hidden" name="parent_code" id="parent_code" value="" readonly="readonly" />
                                 <input type="text" class="form-control" placeholder="Pilih Parent COA" name="parent_name" id="parent_name" value="" readonly="readonly" />
                                 <div class="input-group-append">
                                    <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVParentCOA();"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-outline-warning btn-sm waves-effect waves-light" type="button" onclick="LOVParentCOAClear();"><i class="mdi mdi-close"></i></button>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">Code <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Code" id="code_coa" name="code_coa" value="" maxlength="12" />
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Name" id="name_coa" name="name_coa" value="" maxlength="50" />
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">Tipe COA <span class="text-danger">*</span></label>
                              <select id="tipe_coa" name="tipe_coa" class="form-control">
                                 <option value="H">Header</option>
                                 <option value="D" selected>Detail</option>
                              </select>
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">Kas / Bank <span class="text-danger">*</span></label>
                              <select id="kas_bank_coa" name="kas_bank_coa" class="form-control">
                                 <option value="1">Ya</option>
                                 <option value="0" selected>Tidak</option>
                              </select>
                           </div>
                           <div class="col-md-12">
                              <button type="button" class="btn btn-outline-dark btn-sm waves-effect waves-light" id="reset_form_coa" name="reset_form_coa" onclick="ResetFormCOA();"><span class="btn-label"><i class="glyphicon glyphicon-repeat "></i></span> Reset</button>
                              <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" style="float: right;" id="simpan_coa" name="simpan_coa" onclick="SimpanCOA();"><span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span> Simpan</button>
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="col-lg-8 pull-right">
                     <div class="col-lg-12">
                        <div class="card">
                           <div class="card-body">
                              <form id="form_filter_coa" name="form_filter_coa">
                                 <div class="row">
                                    <div class="col-md-2">
                                       <div class="form-group">
                                          <label>Tipe</label>
                                          <select id="filter_coa_tipe" name="filter_coa_tipe" class="form-control">
                                             <option value="">SEMUA</option>
                                             <option value="H">Header</option>
                                             <option value="D">Detail</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-md-2">
                                       <div class="form-group">
                                          <label>Kas/Bank</label>
                                          <select id="filter_coa_cash_bank" name="filter_coa_cash_bank" class="form-control">
                                             <option value="">SEMUA</option>
                                             <option value="1">Ya</option>
                                             <option value="0">Tidak</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-md-3 offset-md-5 text-right">
                                       <label>&nbsp;</label>
                                       <div class="button-items">
                                          <a href="#" class="btn btn-outline-info btn-sm waves-effect waves-light" id="search" name="search" onclick="FilterCOA();"><i class="fas fa-filter"></i> Terapkan filter</a>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <table id="table_list_data_coa" class="table dt-responsive nowrap text-nowrap" style="width: 100%;">
                                 <thead>
                                    <tr>
                                       <th>Grup</th>
                                       <th>Parent</th>
                                       <th>Code</th>
                                       <th>Name</th>
                                       <th>Tipe</th>
                                       <th>Kas/Bank</th>
                                       <th>Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END Daftar COA -->

            <!-- START Setting COA -->
            <div class="tab-pane active p-3" id="coa_setting" role="tabpanel">
               <div class="row">
                  <div class="col-md-6">
                     <div class="page-title-box">
                        <div class="button-items">
                           <button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light" id="app_refresh" name="app_refresh" onclick="app_refresh_setting();"><i class="fas fa-sync"></i> Refresh </button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="page-title-box text-right">
                        <div class="button-items">
                           <?php if ($priv_arr['pdf_flag']) {?>
                              <button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" id="app_pdf_setting" name="app_pdf_setting" onclick="app_pdf_setting();"><i class="fas fa-file-pdf"></i> Pdf </button>
                           <?php }?>
                           <?php if ($priv_arr['xls_flag']) {?>
                              <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" id="app_xls_setting" name="app_xls_setting" onclick="app_xls_setting();"><i class="fas fa-file-excel"></i> Excel/CSV </button>
                           <?php }?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <form id="form_input_coa_setting" name="form_input_coa_setting" class="lobi-form">
                        <div class="row">
                           <div class="form-group col-md-12">
                              <label class="control-label">Code <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Code" id="setting_code" name="setting_code" value="" maxlength="50" />
                           </div>
                           <div class="form-group col-md-12">
                              <label class="control-label">COA <span class="text-danger">*</span></label>
                              <div class="input-group">
                                 <input type="hidden" name="setting_coa_code" id="setting_coa_code" value="" readonly="readonly" />
                                 <input type="text" class="form-control" placeholder="Pilih COA" name="setting_coa_name" id="setting_coa_name" value="" readonly="readonly" />
                                 <div class="input-group-append">
                                    <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVSettingCOA();"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-outline-warning btn-sm waves-effect waves-light" type="button" onclick="LOVSettingCOAClear();"><i class="mdi mdi-close"></i></button>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <button type="button" class="btn btn-outline-dark btn-sm waves-effect waves-light" id="reset_form_setting" name="reset_form_setting" onclick="ResetFormSetting();"><span class="btn-label"><i class="glyphicon glyphicon-repeat "></i></span> Reset</button>
                                 <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" style="float: right;" id="simpan_setting" name="simpan_setting" onclick="SimpanSetting();"><span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span> Simpan</button>
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="col-lg-8">
                     <div class="col-lg-12">
                        <div class="card">
                           <div class="card-body">
                              <table id="table_list_setting_coa" class="table dt-responsive nowrap text-nowrap">
                                 <thead>
                                    <tr>
                                       <th>Code</th>
                                       <th>COA</th>
                                       <th>Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END Setting COA -->
         </div>

      </div> <!-- /. card-body -->
   </div> <!-- /. card -->
</div> <!-- /. row -->

<!-- END CONTENT -->

   <script type="text/javascript">
	// START VARIABEL WAJIB
	var Modules = '<?=$modules?>';
	var Controller = '<?=$controller?>';
	var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
	var data2Send = null;
	var dataArr = [];
	var DataTable = null;
	// END VARIABEL WAJIB
   var action = '<?=$action?>';
   var _id = null;

   var _dataTable = null;
   var _dataTableSetting = null;

   $.getScript(['<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.js?v=<?=date('YmdHis') . rand()?>'], function() {
      initPage();
   });
</script>