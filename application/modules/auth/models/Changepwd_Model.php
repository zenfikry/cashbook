<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Changepwd_Model extends Core_Model {
	
	function __construct() {
		  parent::__construct();
    }
	
	function getLogin($input) {
        $dataupd = array(
            'user_pass' => $input['newpass']
        );

        $datalog = array(
            'user_id' => $input['user'],
            'old_pass' => $input['oldpass'],
            'new_pass' => $input['newpass'],
            'created_date' => date('Y-m-d H:i:s')
        );
        
        $user_id = $input['user'];
        # Dapatkan informasi user
        $getData = $this->db->query("SELECT a.user_id, a.user_pass, a.nik FROM app_users a WHERE a.user_id=?", array($input['user']));
        if ($getData->row() == NULL){
            return array('result' => false, 'data' => null, 'msg' => 'Username or nik not found.');
        } else {
            // verify password lama
            if (!password_verify($input['oldpass'], $getData->row()->user_pass) || $input['newpass'] != $input['confirmpass']) {
                return array('result' => false, 'msg' => 'Wrong Password or Confirm Password not same as new password.', 'data' => NULL);
            } else {
                $this->db->where('user_id', $user_id);
                $updData = $this->db->update('app_users', $dataupd);

                // $updData = $this->db->query("UPDATE app_users SET user_pass = ? WHERE user_id = ?", array($input['user_pass'], $input['user']));
                if (!$updData && !empty($this->db->error())) {
                    $msg_err = $this->db->error();
                    $msg = explode(':',$msg_err['message']);
                    return array('result' => false, 'data' => NULL, 'msg' => 'Update User Password failed. Update Cancelled '.$msg[0].': '.$msg[1].', values : '.str_replace('LINE 1','',$msg[2]));
                } else {
                    $InsLog = $this->db->insert('app_log_changepwd', $datalog);
                    if (!$InsLog) {
                        $msg_err = $this->db->error();
                        $msg = explode(':', $msg_err['message']);
                        return array('result' => false, 'data' => NULL, 'msg' => 'Insert Log User Password failed. Insert Cancelled '.$msg[0].': '.$msg[1].', values : '.str_replace('LINE 1','',$msg[2]));
                    } else {

                        return array('result' => true, 'data' => NULL, 'msg' => 'Update Profile Success.');
                    }
                }
            }
        }
	}

	
}
