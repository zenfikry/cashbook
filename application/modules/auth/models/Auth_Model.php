<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Auth_Model extends Core_Model {
	
	function __construct() {
		  parent::__construct();
    }
	
	function getLogin($input) {
		# Dapatkan informasi user
		$getData = $this->db->query("SELECT a.user_id, a.user_pass, a.name, a.nik, a.role_id, c.module_id, b.def_menu_id, c.menu_ctl, c.menu_ctl_def
												FROM app_users a
													JOIN app_roles b ON a.role_id=b.role_id
													JOIN app_menu_lists c ON c.menu_id=b.def_menu_id
												WHERE a.user_id=? AND a.active=1", 
												array($input['user']));
		
		# Cek jika user tidak ditemukan
		if ($getData->row() == NULL){
			return array('result' => false, 'data' => null, 'msg' => 'User name unrecognized.');
		} else {

			$getUser = $this->db->query("SELECT a.user_id, a.user_pass, a.name, a.nik, a.role_id, c.module_id, b.def_menu_id, 
										c.menu_ctl, c.menu_ctl_def, COALESCE(d.email,'0') AS email, 
										COALESCE(d.code_jabatan, '0') AS code_jabatan, COALESCE(e.code_atasan, '0') as code_atasan,
										COALESCE(f.name_dept, '0') AS name_dept, 0 as level_code, COALESCE(e.lvl_expense, '0') AS level_expense,
										COALESCE(e.lvl_settlement, '0') AS level_settlement
												FROM app_users a
													JOIN app_roles b ON a.role_id=b.role_id
													JOIN app_menu_lists c ON c.menu_id=b.def_menu_id
													JOIN m_employee d ON d.nik = a.nik
													JOIN m_jabatan e ON e.code_jabatan = d.code_jabatan
													JOIN m_dept f ON f.code_dept = e.code_dept
												WHERE a.user_id=? AND a.active=1", 
												array($input['user']));
												
												if (!password_verify($input['pass'], $getUser->row()->user_pass)) {
													return array('result' => false, 'msg' => 'Wrong Password', 'data' => NULL);
												   }
										   
												   if ($getUser->row() != NULL){
													   $data = array(
												   'user_id' => $input['user'],
												   'ip_address' => $this->input->ip_address()
													   );
													   $this->db->insert('app_users_log', $data);
													   return array('result' => true, 'msg' => 'User found', 'data' => $getUser->row_array());
												   } else {
													   return array('result' => false, 'msg' => 'User or password failed', 'data' => NULL);
												   }
		}

		#cek jika user non root ditemukan
		// if ($getData->row()->role_id != 'ROOT') {

			# Cek jika user tidak ditemukan
			// if ($getUser->row() == NULL) {
			// 	// $hasil = array('result' => true, 'data' => null, 'msg' => 'Wrong user or password.'); 
			// 	// echo $this->output($format, $hasil);
			// 	// return array('result' => false, 'msg' => 'User atau password anda salah', 'data' => NULL);
			// }

			# Jika password yang diparsing salah, maka kembalikan
		// } else {
		// 	$getUser = $this->db->query("SELECT a.user_id, a.user_pass, a.name, a.nik, a.role_id, c.module_id, c.menu_ctl, c.menu_ctl_def, 0 as level_expense, 0 as level_settlement, 0 AS level_code, '' as divisi_code
		// 										FROM app_users a
		// 											JOIN app_roles b ON a.role_id=b.role_id
		// 											JOIN app_menu_lists c ON c.menu_id=b.def_menu_id													
		// 										WHERE a.user_id=? AND a.active=1", 
		// 										array($input['user']));
		// 	# Cek jika user tidak ditemukan
		// 	if ($getUser->row() == NULL) {
		// 		return array('result' => false, 'msg' => 'User atau password anda salah', 'data' => NULL);
		// 	}

		// 	if (!password_verify($input['pass'], $getData->row()->user_pass)) {
		// 		return array('result' => false, 'msg' => 'User atau password anda salah', 'data' => NULL);
		// 	}
	
		// 	if ($getData->row() != NULL){
		// 		$data = array(
		// 			'user_id' => $input['user'],
		// 			'ip_address' => $this->input->ip_address()
		// 		);
		// 		$this->db->insert('app_users_log', $data);
		// 		return array('result' => true, 'msg' => 'User ditemukan', 'data' => $getData->row_array());
		// 	} else {
		// 		return array('result' => false, 'msg' => 'User atau passord anda salah', 'data' => NULL);
		// 	}
		// }

	}

	
}
