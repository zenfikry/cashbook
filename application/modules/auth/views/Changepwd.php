<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>DSS System</title>
        <link rel="shortcut icon" href="<?=base_url()?>assets/images/mr-diy-menu.png" />

        
        <link href="<?=base_url()?>assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?=base_url()?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?=base_url()?>assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    </head>
    <div class="account-pages my-1 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <!-- <?=$warning?> -->
                    <!-- <div class="text-center">
                    &nbsp;
                    </div> -->
                    <div class="card overflow-hidden">
                        <div class="card-body pt-0">
                        <div class="text-center">
                            <img src="<?=base_url('assets/images/mrdiy_logo.png') ?>" height="100">
                        </div>
                                <h4 class="text-muted font-size-18 mb-1 text-center">DIY SELF SERVICE SYSTEM</h4>
                                <p class="text-muted text-center">Change Password.</p>
                            <div class="p-3">
                                <form class="lobi-form login-form visible" method="post" action="<?=base_url()?>auth/Changepwd/DoLogin" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="username">Username / NIK</label>
                                        <input type="text" name="username" class="form-control" id="username" placeholder="Enter username">                                        
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="oldpassword">Old Password</label>
                                        <input type="password" name="oldpassword" class="form-control" id="oldpassword" placeholder="Old password">
                                    </div>
                                    <div class="form-group">
                                        <label for="newpassword">New Password</label>
                                        <input type="password" name="newpassword" class="form-control" id="newpassword" placeholder="Enter New password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmpassword">Confirm New Password</label>
                                        <input type="password" name="confirmpassword" class="form-control" id="confirmpassword" placeholder="Confirm New password">
                                    </div>
                                    <div class="form-group row mt-4">
                                        <div class="col-6">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="button" onclick="location.href='<?=base_url()?>auth/login';">Back to Login</button>
                                        </div>
                                        <div class="col-6 text-right">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Change It</button>
                                        </div>
                                    </div>                                   
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 text-center">
                        <p style="color:black;">&copy; <?=date('Y')?> <i class="mdi mdi-heart text-danger"></i> by DST </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script src="<?=base_url()?>assets/libs/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?=base_url()?>assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?=base_url()?>assets/libs/node-waves/waves.min.js"></script>
        <script src="<?=base_url()?>assets/libs/jquery-sparkline/jquery.sparkline.min.js"></script>
        <!-- App js -->
        <script src="<?=base_url()?>assets/js/app.js"></script>
        <script src="<?=base_url()?>assets/js/script.js"></script>

    </body>
</html>
