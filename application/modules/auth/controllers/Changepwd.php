<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepwd extends Core_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Changepwd_Model", "m_app");
	}

	function index() {
		$data['warning'] = '';
		$this->load->view('Changepwd', $data);
	}
		
	function DoLogin() {
		# Dapatkan informasi user
		$userid = $this->input->post("username");
		$oldpass = $this->input->post("oldpass");
		$newpass = $this->input->post("newpass");
		$conpass = $this->input->post("confirmpass");

		$datain = array(
			'user_id' => $this->input->post("username"),
			'old_pass' => $this->input->post("oldpassword"),
			'new_pass' => $this->input->post("newpassword"),
			'created_date' => date('Y-m-d H:i:s'),
		);

		$dataupd = array(
			// 'user_id' => $this->input->post("username"),
			'user_pass' => password_hash($this->input->post('newpassword'), PASSWORD_DEFAULT),
		);

        $getData = $this->db->query("SELECT a.user_id, a.user_pass, a.nik FROM app_users a WHERE a.user_id=?", $userid);
        if ($getData->row() == NULL){
            echo '<script>alert("User id not found."); </script>';
			redirect("auth/Login", "refresh");
        } else {
            // verify password lama
            if (trim($newpass) != trim($conpass)) {
				// var_dump('masuk sini');
                // return array('result' => false, 'msg' => 'Wrong Password or New Password and Confirm Password does not match!', 'data' => NULL);
				echo '<script>alert("New Password and Confirm Password does not match!");</script>';
				redirect("#","refresh");
            } else {
                $this->db->where('user_id', $this->input->post('username'));
				$upd = $this->db->update('app_users', $dataupd);
				if ($upd) {
					$hasil = $this->db->insert('app_log_changepwd', $datain);
					if ($hasil) {
						echo '<script>alert("Update Password Success.");</script>';
						redirect("auth/Login","refresh");
					} else {
						echo '<script>alert("Insert Log Error.");</script>';
						redirect("auth/Login", "refresh");
					}
				} else {
					echo '<script>alert("Update Password Failed.");</script>';
					redirect("auth/Login", "refresh");
				}		
            }
        }
	}
	
	function DoLogout() {
		$user_data = $this->session->all_userdata();
		
    	foreach ($user_data as $key => $value) {
			$this->session->unset_userdata($key);
		}
		redirect("auth/Login");
	}
}
