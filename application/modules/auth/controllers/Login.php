<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Core_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Auth_Model", "m_app");
	}

	function index() {
		// $data['warning'] = '';
		// $data['chaptca'] = $this->_get_chaptca(4).' '.$this->_get_chaptca(3).' '.$this->_get_chaptca(2);
		$this->load->helper('captcha');
		$vals = array(
        //'word'          => 'Randdfdom word',
        'img_path'      => './captcha-images/',
        'img_url'       => base_url().'captcha-images/',
        'font_path'     => './path/to/fonts/arial.ttf',
		// 'font_path' 	=> './assets/fonts/ionicons.ttf',
        'img_width'     => '200',
        'img_height'    => 50,
        'expiration'    => 7200,
        'word_length'   => 6,
        'font_size'     => 40,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

        // White background and border, black text and red grid
        'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 204, 204)
		        )
		);

		$cap = create_captcha($vals);
		$image= $cap['image'];
		$captchaword= $cap['word'];
		$this->session->set_userdata('captchaword',$captchaword);
		$this->load->view('Login',['captcha_image'=>$image]);


		// $this->load->view('Login', $data);
	}

	public function registerSubmit()
	{
		
	}
	
	function _get_chaptca($param) // method pembuat chapta
       {
        $alphabet   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $num        = range(0, 35);
        $result     = '';
         shuffle($num);
         for ($x = 0; $x < $param; $x++)
          {
            $result .= substr($alphabet, $num[$x], 1);
          }
          return $result;
      }
	
	function DoLogin() {
		// if (strtolower($this->input->post('txt_chaptca_real')) == strtolower($this->input->post('txt_chaptca'))) 
		// {

			$captcha=$this->input->post('captcha');
			$captcha_answer=$this->session->userdata('captchaword');
			$psw = $this->input->post('password');
			if($psw == '123456') {
				echo '<script>alert("You Password Vulnerable! please Change Your Password.");</script>';
				redirect('auth/Changepwd', 'refresh');
			}
			///check both captcha
			if($captcha==$captcha_answer)
			{
				$data = array(
					'user' => $this->input->post("username"),
					'pass' => $this->input->post("password"),
				);
		
				$hasil = $this->m_app->getLogin($data);
				// dd($hasil['result']);
				
				if ($hasil['result']) {
					
					// $lvl_expense=$hasil['data']['level_expense'];
					$arrSession = array('login' 				=> true,
												'user_id'			=> $hasil['data']['user_id'],
												#'employee_id'		=> $hasil['data']['employee_id'],
												'name'				=> $hasil['data']['name'],
												'nik' 				=> $hasil['data']['nik'],
												'role_id'			=> $hasil['data']['role_id'],
												'def_menu_id' 		=> $hasil['data']['def_menu_id'],
												'def_menu_link' 	=> $hasil['data']['module_id']."/".$hasil['data']['menu_ctl'].'/'.$hasil['data']['menu_ctl_def'],
												'level_code'		=> $hasil['data']['level_code'],
												'email'				=> $hasil['data']['email'],
												'code_jabatan'		=> $hasil['data']['code_jabatan'],
												'code_atasan'		=> $hasil['data']['code_atasan'],
												'level_expense' => $hasil['data']['level_expense'],
												'level_settlement' => $hasil['data']['level_settlement'],
												'apps'				=> 'cashbook_apps', 
					);
					// dd($arrSession);
					#echo "<pre>"; print_r($arrSession); echo "</pre>"; exit;
					
					$this->session->set_userdata($arrSession);
					redirect("app");
				} else {
					$data['warning'] = '<div class="alert alert-warning alert-dismissible" role="alert">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong>Peringatan!</strong> Gagal login, cek username dan password anda.
									</div>';
					// $this->load->view('Login', $data);
					// print_r($data['warning']);
					// $hasil = array('result' => false, 'msg' => $data['warning'], 'data' => null);
		
					// echo $this->output($format, $hasil);
					echo '<script>alert("You are not recognized! please try again.");</script>';
					redirect('#', 'refresh');
				}
			}
			else{
				echo '<script>alert("Captcha does not match.");</script>';
				redirect('#', 'refresh');
				// $this->session->set_flashdata('error','<div class="alert alert-danger">Captcha does not match.</div>');
				// redirect('home');
			}

		

		// }
		// else{
		// 	echo '<script>alert("You have wrong captcha! please try again.");</script>';
		// 	redirect('#', 'refresh');
		// }
	}
	
	function DoLogout() {
		// $user_data = $this->session->all_userdata();
		
    	// foreach ($user_data as $key => $value) {
		// 	$this->session->unset_userdata($key);
		// }

		$this->load->helper("cookie");
		$cookie = array(
			'name'   => 'dss_session',
			'value'  => '',
			'expire' => 0,
			'path' 	 => '/',                                                                                   
			'secure' => TRUE
			);
		$this->input->set_cookie($cookie);
		
		$this->session->sess_destroy();
		redirect("auth/Login");
	}
}
