<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends Core_Controller {

	function __construct(){
		# parsing menu_id
		parent::__construct("finance");
		$this->load->model("finance_model", "m_app");
	}

	function index() {
		$data['warning'] = '';
		/* echo "dashboard";
		echo "<pre>"; print_r($this->data); echo "</pre>"; exit; */
		$this->load->view('Finance', $this->data);
	}

	public function getListPc()
    {
        $filter = array(
            'a.status' => $this->input->post("filter_status", true),
            'a.code_company' => $this->input->post("filter_company", true),
            'a.periode_awal >=' => date_convert_format($this->input->post("filter_start_date", true)),
            'a.periode_akhir <=' => date_convert_format($this->input->post("filter_end_date", true)),
        );

        $this->output->set_content_type('application/json');
        // $this->session->userdata('code_jabatan')
        echo $this->m_app->getListPc($filter);
    }
	
	/* 
	function index2() {
		$txt_pass = "root";
		$hashed_password = password_hash($txt_pass, PASSWORD_DEFAULT);
		echo $hashed_password."<br/>";
		if (password_verify("root", $hashed_password)) {
			 echo "valid"."<br/>";
		} else {
			echo "invalid"."<br/>";
		}
	} */
	
}
