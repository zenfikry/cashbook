<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Payment extends Core_Controller
{
    private $total_qty = 0;
    private $total_gross = 0;
    private $total_price_point = 0;
    private $total_potongan = 0;
    private $total_pajak = 0;
    private $grand_total = 0;

    public function __construct()
    {
        parent::__construct("payment"); # parsing menu_id
        $this->load->model("Payment_Model", "m_app");
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function index()
    {
        // dd($this->session->userdata());
    
        $this->load->view('Payment', $this->data);
    }

    public function create()
    {
       
        $this->load->view('Payment_Form', $this->data);
    }

    public function getEmployeeID(){
        $employee = $this->m_app->getEmployeeID();
        echo json_encode($employee);
    }

    public function getPaymentMethod(){
        $employee = $this->m_app->getPaymentMethood();
        echo json_encode($employee);
    }

    public function getList()
    {
        // $filter = array(
        //     'a.status' => $this->input->post("filter_status", true),
        // );

        if($this->input->post("filter_start_date") || $this->input->post("filter_end_date")) {
            $filter = array(
                'a.status' => $this->input->post("filter_status", true),
                // 'a.code_company' => $this->input->post("filter_company", true),
                'a.periode_awal >=' => date_convert_format($this->input->post("filter_start_date", true)),
                'a.periode_akhir <=' => date_convert_format($this->input->post("filter_end_date", true)),
            );
        } else {
            $filter = array(
                'a.status' => $this->input->post("filter_status", true),
                // 'a.code_company' => $this->input->post("filter_company", true),
            );
        }
        $this->output->set_content_type('application/json');
        // $this->session->userdata('code_jabatan')
        echo $this->m_app->getList($filter);
    }

    private function validation($method = 'create')
    {
        if ($method == 'create' || $method == 'edit'):

            if($this->input->post('update_action') != "AP"){
                $this->form_validation->set_rules('inv_date', 'Invoice Date', 'required|trim');
            
            }
            else {
                $this->form_validation->set_rules('inv_date', 'Invoice Date', 'required|trim');
            }


        endif;

        if ($method == 'edit' || $method == 'delete'):
            $this->form_validation->set_rules('id', 'Id', 'required|trim');
        endif;

        if ($this->form_validation->run()) {
            return ['result' => true, 'message' => 'OK'];
        } else {
            return ['result' => false, 'message' => validation_errors()];
        }
    }

    public function save($format = 'json')
    {
        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;


        $no_doc = $this->m_app->generate_trans_number($this->input->post('inv_date'), $this->input->post('code_creditor'));
        $total_expense = $this->input->post('total_expense');
        // $taxable_amount = $this->input->post('taxable_amount');
        $wht = $this->input->post('wht_amount');
        $ppn = $this->input->post('ppn');
        $net_total = $this->input->post('net_total');
        // $outstanding = $this->input->post('outstanding');

       

        // CEK KODE ATASAN DARI SI PENGAJU
        $code_jabatans = $this->session->userdata('code_jabatan');
        $code_atasans  = $this->m_app->get_atasan($code_jabatans);
        $email = "herman.sulaeman@gmail.com";
        
        // END CEK KODE ATASAN DARI SI PENGAJU

        $validation_detail = json_decode($this->input->post("detail"));
        
        // foreach($validation_detail as $dt){
        //     if($dt->remark == ""){
        //         $hasil = array('result' => false, 'msg' => "Project code is required", 'data' => null);

        //         echo $this->output($format, $hasil);
        //         return;
        //     }
        // }
        $verifikasi = $this->validation('create');
        if ($verifikasi['result']>0){
            

            // email
					ini_set( 'display_errors', 1 );   
					error_reporting( E_ALL );    
					
                   
                    $to = $email;
                    // $to = $this->input->post('email');
					$this->config->load('email');

					$subject = 'Purchase Invoice '.$no_doc;
					
					
					$message = '<!DOCTYPE html>
					<html lang="en">
					
					<head>					
						<meta charset="utf-8" />
						<title>DSS System - Purchase Invoice</title>
						<style>
                        #customers {
                        font-family: Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                        }

                        #customers td, #customers th {
                        border: 1px solid #ddd;
                        padding: 8px;
                        }

                        #customers tr:nth-child(even){background-color: #f2f2f2;}

                        #customers tr:hover {background-color: #ddd;}

                        #customers th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                        }
                        </style>					
					</head>
					
										<!-- end page title -->
					
										<div class="row">
											<div class="col-12">
												<div class="card">
													<div class="card-body">
					
														<div class="row">
															<div class="col-12">
																<div class="invoice-title">
																	<div class="row">
																		<div class="col-md-2">
																			<img src="'.base_url().'/assets/images/mrdiy_logo.png" alt="logo" height="50"/>
																		</div>
																		<div class="col-md-10">
																			<div align="right">
																				<p class=" font-size-16"><strong>DIY Self Service System</strong></p>
																			</div>
																		</div>
																	</div>
																	<h3 class="mt-0">
																		</h3>
																</div>
																<hr>
																<div class="row">
																	<div class="col-6">
																		<address>
																				<strong>Settlement Advance Dari:<br></strong>';
					$message .= '<strong>'.$no_doc.'</strong>
																	</div>
																	<div class="col-6 text-right">
																		<address>';
																			$message .= '<strong>Periode: </strong>'.date('d-m-Y').'<br>';
																			$message .= '<strong>Status: </strong> Baru<br><br><br>
																		</address>
																	</div>
																</div>
															</div>
														</div>
					
														<div class="row">
															<div class="col-12">
																<div>
																	<div class="p-2">
																		<!-- <h3 class="font-size-16"><strong>Detail Summary</strong></h3> -->
																	</div>
																	<div class="">
																		<div class="table-responsive">
																			<table id="customers">
																					<tr>
																						<th scope="col"> Settlement Advance Limit</th>
																						<th scope="col"> Settlement Advance Expense</th>
																						<th scope="col"> Settlement Advance Sisa</th>
																					</tr>
																					<!-- foreach ($order->lineItems as $line) or some such thing here -->';
					$message .= '					
																						<td>Rp. '.number_format(number_format_convert($total_expense)).'</td>
																					</tr>
																			</table>
																		</div>
																	</div>
																</div>
								
					
					</html>';
					
                    // upload file expense
                    $upload_msg = "";
                    $file_expensed = "";
                    $file_name                      = $no_doc.".pdf";
                    $config['upload_path']          = FCPATH.'assets\upload';
                    $config['allowed_types']        = 'pdf';
                    $config['file_name']            = $file_name;
                    $config['max_size']             = 100000;

                    $this->load->library('upload', $config);
                    
                    if (!$this->upload->do_upload('file_expense')) {
                        $upload_msg .= $this->upload->display_errors();
                    } else {
                        $uploaded_data = $this->upload->data();
                        $file_expensed .= $uploaded_data['file_name'];
                
                    }
                    // end upload file expense

                    $data_approval = [];
                    $data_pengajuan = [];
                    $data_h = [
                        'inv_no'        => $no_doc,
                        'code_creditor'       =>  $this->input->post('code_creditor'),
                        'code_company'       =>  $this->input->post('company_code'),
                        'nik'       => $this->session->userdata('nik'),
                        'inv_date'  =>  date_convert_format($this->input->post('inv_date')),
                        'inv_desc'           =>  $this->input->post('inv_desc'),
                        'supplier_iv'  =>  $this->input->post('supplier_iv'),
                        'ref_no'     =>  $this->input->post('ref_no'),
                        // 'expense_period'     =>  $this->input->post('expense_period'),
                        'due_date'     =>  date_convert_format($this->input->post('due_date')),
                        'description'     =>  $this->input->post('description'),
                        'taxable_amount'     =>  unformat_numeric($this->input->post('total_expense')),
                        // 'taxable_amount'     =>  $this->input->post('taxable_amount'),
                        'ppn'     =>  unformat_numeric($this->input->post('ppn')),
                        'wht'       => unformat_numeric($this->input->post('wht_amount')),
                        'net_total'     =>  unformat_numeric($this->input->post('net_total')),
                        // 'outstanding'     =>  0,
                        // 'taxable_amount'     =>  $this->input->post('taxable_amount'),
                        // 'ppn'     =>  $this->input->post('ppn'),
                        // 'net_total'     =>  $this->input->post('net_total'),
                        // 'outstanding'     =>  $this->input->post('outstanding'),
                        'status'        =>  1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'creation_date' => date('Y-m-d H:i:s')
                    ];

                    if(count($code_atasans) > 0){
                        foreach($code_atasans as $atas){
                            $data_approval[] = [
                                'inv_no'    => $no_doc,
                                'code_atasan' => $atas['code_jabatan']
                            ];
                        }
                    }
                    $data_pengajuan = [
                        'inv_no'    => $no_doc,
                        'code_pengaju' => $code_jabatans
                    ];
                    $data_d = ['detail'=>$this->input->post('detail',true)];
                    $input = $this->security->xss_clean($data_h);
                    $input['attachment_file'] = $file_expensed;
                    
                    $hasil = $this->m_app->save($input,$data_d,$data_approval,$data_pengajuan);
					$email_result = $this->sendMailer($to,$subject,$message);
					$hasil = array('result' => true, 'msg' => 'Data Saved'.$email_result, 'data' => $data_h);
            
                    echo $this->output($format, $hasil);
                }
                else{
                    $hasil = array('result' => false, 'msg' => $verifikasi['message'], 'data' => null);

                    echo $this->output($format, $hasil);
                }
    }

    public function update($format = 'json')
    {
        // show_404();

        if (!$this->input->is_ajax_request()):
            show_404();
            exit;
        endif;



        $no_doc = $this->input->post('no_doc');
        $taxable_amount = $this->input->post('total_expense');

        // $m_code = $this->input->post('code_employee');
        // $periode_awal = $this->input->post('periode_awal');
        // $periode_akhir = $this->input->post('periode_akhir');
        $tgl_inv = $this->input->post('inv_date');
        // print_r($no_doc);
        // die();
        $action_ap = false;
        // CHECK YANG EDIT AP 
        if($this->session->userdata('code_jabatan') == "PO001" || $this->session->userdata('code_jabatan') == "PO002"){
            $status_type = "Pengajuan Telah Disetujui AP";
            $action_ap = true;
        }
        else {
            $status_type = "Pengajuan Ulang";
        }
       
        $validation_detail = json_decode($this->input->post("detail"));
        
        foreach($validation_detail as $dt){
            if($dt->remark == ""){
                $hasil = array('result' => false, 'msg' => "Project code is required", 'data' => null);

                echo $this->output($format, $hasil);
                return;
            }
        }

        $verifikasi = $this->validation('create');
        if ($verifikasi['result']>0){
            
           

            // email
					ini_set( 'display_errors', 1 );   
					error_reporting( E_ALL );    
					
                   
                    $to = $this->input->post('email');
					$this->config->load('email');

					$subject = 'Expense Claim '.$no_doc;
					
					
					$message = '<!DOCTYPE html>
					<html lang="en">
					
					<head>					
						<meta charset="utf-8" />
						<title>DSS System - Purchase Invoice</title>
						<style>
                        #customers {
                        font-family: Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                        }

                        #customers td, #customers th {
                        border: 1px solid #ddd;
                        padding: 8px;
                        }

                        #customers tr:nth-child(even){background-color: #f2f2f2;}

                        #customers tr:hover {background-color: #ddd;}

                        #customers th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                        }
                        </style>					
					</head>
					
										<!-- end page title -->
					
										<div class="row">
											<div class="col-12">
												<div class="card">
													<div class="card-body">
					
														<div class="row">
															<div class="col-12">
																<div class="invoice-title">
																	<div class="row">
																		<div class="col-md-2">
																			<img src="https://autocountmrdiy.id/cashbook/assets/images/mrdiy_logo.png" alt="logo" height="50"/>
																		</div>
																		<div class="col-md-10">
																			<div align="right">
																				<p class=" font-size-16"><strong>DIY Self Service System</strong></p>
																			</div>
																		</div>
																	</div>
																	<h3 class="mt-0">
																		</h3>
																</div>
																<hr>
																<div class="row">
																	<div class="col-6">
																		<address>
																				<strong>Expenseclaim Dari:<br></strong>';
					$message .= '<strong>'.$m_code.'</strong>
																	</div>
																	<div class="col-6 text-right">
																		<address>';
																			$message .= '<strong>Tanggal: </strong>'.$tgl_inv.'<br>';
																			$message .= '<strong>Status: </strong> '.$status_type.'<br><br><br>
																		</address>
																	</div>
																</div>
															</div>
														</div>
					
														<div class="row">
															<div class="col-12">
																<div>
																	<div class="p-2">
																		<!-- <h3 class="font-size-16"><strong>Detail Summary</strong></h3> -->
																	</div>
																	<div class="">
																		<div class="table-responsive">
																			<table id="customers">
																					<tr>
																						<th scope="col">Expense Claim Total</th>
																					</tr>
																					<!-- foreach ($order->lineItems as $line) or some such thing here -->';
					$message .= '					
					<tr>
																						<td>Rp. '.number_format(number_format_convert($taxable_amount)).'</td>
																					</tr>
																			</table>
																		</div>
																	</div>
																</div>
								
					
					</html>';
					$data_h = [
                        'inv_no'        => $no_doc,
                        'code_creditor'       =>  $this->input->post('code_creditor'),
                        'code_company'       =>  $this->input->post('company_code'),
                        'nik'       => $this->session->userdata('nik'),
                        'inv_date'  =>  date_convert_format($this->input->post('inv_date')),
                        'inv_desc'           =>  $this->input->post('inv_desc'),
                        'supplier_iv'  =>  $this->input->post('supplier_iv'),
                        'ref_no'     =>  $this->input->post('ref_no'),
                        'due_date'     =>  date_convert_format($this->input->post('due_date')),
                        'description'     =>  $this->input->post('description'),
                        'taxable_amount'     =>  unformat_numeric($this->input->post('total_expense')),
                        'ppn'     =>  unformat_numeric($this->input->post('ppn')),
                        'wht'       => unformat_numeric($this->input->post('wht_amount')),
                        'net_total'     =>  unformat_numeric($this->input->post('net_total')),
                        'status'        =>  0,
                        'modified_by'    => $this->session->userdata('user_id'),
                        'modification_date' => date('Y-m-d H:i:s')
                    ];
                    $data_d = ['detail'=>$this->input->post('detail',true)];

                    $input = $this->security->xss_clean($data_h);
                    
                    if($action_ap){ //jika status ap yang login dan melakukan update
                        $total_amount = unformat_numeric($this->input->post('total_expense'));
                        $hasil = $this->m_app->update_ap($no_doc,$data_d,$total_amount);
                    }
                    else {
                        $hasil = $this->m_app->update($input,$data_d);
                    }
					$email_result = $this->sendMailer($to,$subject,$message);
					$hasil = array('result' => true, 'msg' => 'Data Saved'.$email_result, 'data' => $data_h);
            
                        echo $this->output($format, $hasil);
                } else {
                        $hasil = array('result' => false, 'msg' => $verifikasi['message'], 'data' => null);

                        echo $this->output($format, $hasil);
                }
    }

    public function readyAction($id = null){
        $code_jabatan =  $this->session->userdata('code_jabatan');
        $insert_briding = $this->m_app->insert_bridge($id);
        $msg = "";
        if($insert_briding > 0){            
            $msg .= "Doc : $id success approval payment...";
            echo json_encode($stat=['msg' =>$msg,'status'=>200]);
        } else {
            $msg .= "Doc : $id failed approval payment, try again thanks.";
            echo json_encode($stat=['msg' =>$msg,'status'=>400]);
        }
    }
    public function appAction($id = null){
        $code_jabatan =  $this->session->userdata('code_jabatan');
        $insert_briding = $this->m_app->update_payment($id);
        $msg = "";
        if($insert_briding > 0){            
            $msg .= "Doc : $id success approval payment...";
            echo json_encode($stat=['msg' =>$msg,'status'=>200]);
        } else {
            $msg .= "Doc : $id failed approval payment, try again thanks.";
            echo json_encode($stat=['msg' =>$msg,'status'=>400]);
        }
    }
    public function detail($id = null)
    {
        // show_404();
        $cek_id = $this->m_app->check_no_doc_purchase($id);
        $purchase_h = $this->m_app->get_purchase_h($id);
        $purchase_d = $this->m_app->get_purchase_d($id);
        $purchase_a = $this->m_app->get_purchase_a($id);
        // $cek_id = $this->m_app->check_no_doc_claimexp($id);
        // $claimexp_h = $this->m_app->get_purchase_h($id);
        // $claimexp_d = $this->m_app->get_purchase_d($id);
        // $claimexp_a = $this->m_app->get_purchase_a($id);

    
        $this->data['no_doc']       = $id;
        if($cek_id > 0){
            $this->data['purchase_h'] = $purchase_h;
            $this->data['purchase_d'] = $purchase_d;
            $this->data['purchase_a'] = $purchase_a;
            $this->load->view('Purchase_detail', $this->data);

        }
        else {
            show_404();
        }

        // if ($id == null || trim($id) == '' || empty($id)) {
        //     show_404();
        // } else {
        //     $this->data['id'] = $id;
        //     $this->load->view('Pos_form', $this->data);
        // }
    }

    public function edit($id = null)
    {
        // show_404();
        $cek_id = $this->m_app->check_no_doc_purchase($id);
        $purchase_h = $this->m_app->get_purchase_h($id);
        $purchase_d = $this->m_app->get_purchase_d($id);
        $purchase_a = $this->m_app->get_purchase_a($id);
        if($cek_id > 0){
            $this->data['no_doc']       = $id;
            $this->data['purchase_h'] = $purchase_h;
            $this->data['purchase_d'] = $purchase_d;
            $this->data['purchase_a'] = $purchase_a;
            $this->load->view('Purchaseapp_edit', $this->data);

        }
        else {
            show_404();
        }

        // if ($id == null || trim($id) == '' || empty($id)) {
        //     show_404();
        // } else {
        //     $this->data['id'] = $id;
        //     $this->load->view('Pos_form', $this->data);
        // }
    }

    public function delete($format = 'json')
    {
        show_404();
    }

    public function getData2Edit($format = 'json', $id = null)
    {
        if ($id != null || trim($id) != '' || !empty($id)) {
            $hasil = $this->m_app->getData2Edit($id);
        } else {
            $hasil = array('result' => false, 'message' => 'Data kosong.', 'data' => null);
        }

        echo $this->output($format, $hasil);
    }

    public function confirm($format = 'json')
    {
        show_404();
    }

    public function cancel($format = 'json')
    {
        show_404();
    }

    public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Petty Cash Detail";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'judul_laporan' => $JudulLaporan,
            'periode_awal' => $this->input->post("filter_start_date", true),
            'periode_akhir' => $this->input->post("filter_end_date", true),
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "KopSurat";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 15, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'a.status' => $this->input->post("filter_status", true),
            'a.code_company' => $this->input->post("filter_company", true),
            'a.periode_awal >=' => date_convert_format($this->input->post("filter_start_date", true)),
            'a.periode_akhir <=' => date_convert_format($this->input->post("filter_end_date", true)),
        );

        $data = $this->m_app->getDataList($filter);

        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 8);

        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				' . $this->pdf_render_detail($data->result_array()) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(160, 5, "Total Qty :", 0, 0, 'R');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(40, 5, number_format($this->total_qty, 0, ",", "."), 0, 0, 'R');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(160, 5, "Sub Total :", 0, 0, 'R');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(40, 5, number_format($this->total_gross, 0, ",", "."), 0, 0, 'R');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(160, 5, "Price Point :", 0, 0, 'R');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(40, 5, number_format($this->total_price_point, 0, ",", "."), 0, 0, 'R');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(160, 5, "Potongan :", 0, 0, 'R');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(40, 5, number_format($this->total_potongan, 0, ",", "."), 0, 0, 'R');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(160, 5, "Pajak :", 0, 0, 'R');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(40, 5, number_format($this->total_pajak, 0, ",", "."), 0, 0, 'R');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', 'B', 8);
        $pdf->Cell(160, 5, "Grand Total :", 0, 0, 'R');
        $pdf->SetFont('helvetica', '', 8);
        $pdf->Cell(40, 5, number_format($this->grand_total, 0, ",", "."), 0, 0, 'R');
        $pdf->Ln(5);

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			<tr nobr="true">
				<td width="99">' . $value['no_doc'] . '</td>
				<td width="51" align="center">' . date('d/m/Y', strtotime($value['tgl_trans'])) . '</td>
				<td width="83">' . $value['cust_id'] . '</td>
				<td width="141">' . $value['cust_name'] . '</td>
				<td width="193">' . $value['alamat'] . '</td>
			</tr>
            <br/>';
            $html .= '
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr nobr="true">
                    <td align="center" width="20" style="height:13px"><u><i>No.</i></u></td>
                    <td width="75"><u><i>Kode Item</i></u></td>
                    <td width="140"><u><i>Nama Item</i></u></td>
                    <td align="center" width="50"><u><i>Qty</i></u></td>
                    <td width="30"><u><i>Satuan</i></u></td>
                    <td align="right" width="60"><u><i>Harga</i></u></td>
                    <td align="right" width="60"><u><i>Price Point</i></u></td>
                    <td align="right" width="60"><u><i>Potongan</i></u></td>
                    <td align="right" width="72"><u><i>Jumlah</i></u></td>
                </tr>
                ' . $this->pdf_render_detail_transaksi($value['no_doc']) . '
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="6" style="height:10px">' . str_repeat(".", 149) . '</td>
                </tr>
                <tr>
                    <td align="center" width="20" style="height:15px"></td>
                    <td width="75"></td>
                    <td width="150"></td>
                    <td align="center" width="50">' . number_format($value['total_qty'], 0, ",", ".") . '</td>
                    <td width="50"></td>
                    <td align="right" width="70"></td>
                    <td align="right" width="70"></td>
                    <td align="right" width="82">' . number_format($value['total_gross'] - $value['total_potongan'], 0, ",", ".") . '</td>
                </tr>
                <tr>
                    <td colspan="8" style="height:10px">
                        <table border="0">
                            <tr>
                                <td width="188"><b>Potongan :</b> ' . number_format($value['total_potongan'], 0, ",", ".") . '</td>
                                <td width="188"><b>Pajak (' . number_format($value['ppn_pcn'], 0, ",", ".") . '%) :</b> ' . number_format($value['ppn_value'], 0, ",", ".") . '</td>
                                <td width="188"><b>Grand Total :</b> ' . number_format(($value['total_gross'] - $value['total_potongan']) + $value['ppn_value'], 0, ",", ".") . '</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" style="height:15px">' . str_repeat(".", 254) . '</td>
                </tr>
            ';

            $this->total_qty += $value['total_qty'];
            $this->total_gross += $value['total_gross'];
            $this->total_price_point += $value['total_price_point'];
            $this->total_potongan += $value['total_potongan'];
            $this->total_pajak += $value['ppn_value'];
            $this->grand_total += ($value['total_gross'] - $value['total_potongan']) + $value['ppn_value'];
        }

        $html .= '</table>';

        return $html;
    }

    private function pdf_render_detail_transaksi($no_doc)
    {
        $html = '';
        $detail = $this->m_app->detail($no_doc);
        foreach ($detail as $key => $value) {
            $html .= '
			<tr nobr="true">
                <td align="center">' . ($key + 1) . '</td>
                <td>' . $value['item_id'] . '</td>
                <td>' . $value['item_name'] . '</td>
                <td align="center" width="50">' . number_format($value['qty'], 0, ",", ".") . '</td>
                <td width="30">' . $value['uom_name'] . '</td>
                <td align="right" width="60">' . number_format($value['harga'], 0, ",", ".") . '</td>
                <td align="right" width="60">' . number_format($value['price_point'], 0, ",", ".") . '</td>
				<td align="right" width="60">' . number_format($value['potongan'], 0, ",", ".") . '</td>
                <td align="right" width="72">' . number_format($value['jumlah'], 0, ",", ".") . '</td>
			</tr>';
        }

        return $html;
    }

    public function pdf_dokumen($doc_no = '')
    {
        if (trim($doc_no) == '') {
            show_404();
        }

        # Judul laporan
        $JudulLaporan = $doc_no;
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        // $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        $pdf->_construct('L', 'mm', array(210, 150), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'store_id' => get_row_values("store_id", "t_sales_hdr", "no_doc", $doc_no)['store_id'],
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "FakturPenjualan";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 25, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $data = $this->m_app->getData2Edit($doc_no);
        $header = $data['data'];

        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 12);
        $pdf->Cell(0, 5, "Faktur Penjualan", 0, 1, 'C');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', '', 8);

        $jatuh_tempo = "";
        if (!check_arr_empty_or_null($header['tgl_jatuh_tempo']) && $header['type_payment'] != 'TUNAI') {
            $jatuh_tempo = '<td width="100">Jatuh Tempo</td>
                            <td width="180">: ' . date("d/m/Y", strtotime($header['tgl_jatuh_tempo'])) . '</td>';
        }

        $html_pdf = '
		<table cellpadding="3" border="0">
			<tr>
				<td width="100">No.</td>
				<td width="180">: ' . $header['no_doc'] . '</td>
				<td width="100">Tanggal</td>
				<td width="180">: ' . date("d/m/Y", strtotime($header['tgl_trans'])) . '</td>
			</tr>
            <tr>
				<td width="100">Customer</td>
				<td width="180">: ' . $header['cust_name'] . '</td>
				<td width="100">Sales</td>
				<td width="180">: ' . $header['employee_name'] . '</td>
			</tr>
            <tr>
				<td width="100">Tipe Pembayaran</td>
				<td width="180">: ' . $header['type_payment'] . '</td>
                ' . $jatuh_tempo . '
			</tr>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', '', 8);
        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
					<th width="75">Kode</th>
					<th width="150">Nama</th>
					<th align="center" width="70">Expired Date</th>
					<th align="center" width="50">Qty</th>
					<th align="right" width="70">Harga</th>
					<th align="right" width="70">Potongan</th>
                    <th align="right" width="82">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				' . $this->pdf_render_detail_item($data['detail']) . '
			</tbody>
            <tfoot>
                <tr>
                    <th align="center" colspan="3">Sub Total</th>
                    <th align="center" width="50">' . number_format($header['total_qty'], 0, ",", ".") . '</th>
                    <th align="right" width="70">' . number_format($header['total_gross'] - $header['total_price_point'], 0, ",", ".") . '</th>
                    <th align="right" width="70">' . number_format($header['total_potongan'], 0, ",", ".") . '</th>
                    <th align="right" width="82">' . number_format($header['total_gross'] - $header['total_potongan'], 0, ",", ".") . '</th>
                </tr>
            </tfoot>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');

        // START TOTAL
        $terbilang = preg_replace('/\s+/', ' ', "# " . ucwords(terbilang($header['grand_total'])) . " Rupiah #");
        $html_pdf = '
		<table border="0">
			<tr nobr="true">
				<td>
                    <table cellpadding="1" border="0">
                        <tr>
                            <td width="411"><b>Terbilang :</b></td>
                            ' . ($header['ppn'] != 'N' ? '<td width="70"><b>PPN (' . number_format($header['ppn_pcn'], 0, ",", ".") . '%)</b></td><td width="81" align="right"><b>' . number_format($header['ppn_value'], 0, ",", ".") . '</b></td>' : '') . '
                        </tr>
                        <tr>
                            <td width="411">' . $terbilang . '</td>
                            <td width="70"><b>Grand Total</b></td>
                            <td width="81" align="right"><b>' . number_format($header['grand_total'], 0, ",", ".") . '</b></td>
                        </tr>
                    </table>
                </td>
			</tr>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');
        $pdf->Ln(5);
        // END TOTAL

        // START TANDA TANGAN
        $pdf->SetFont('helvetica', 'B', 8);
        $html_pdf = '
		<table border="0">
			<tr nobr="true">
				<td>
                    <table cellpadding="3" border="1">
                        <tr>
                            <td width="280">Hormat Kami</td>
                            <td width="280">Penerima</td>
                        </tr>
                        <tr>
                            <td width="280" height="40"></td>
                            <td width="280"></td>
                        </tr>
                        <tr>
                            <td width="280">Nama :</td>
                            <td width="280">Nama :</td>
                        </tr>
                        <tr>
                            <td width="280">Tanggal :</td>
                            <td width="280">Tanggal :</td>
                        </tr>
                    </table>
                </td>
			</tr>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');
        // END TANDA TANGAN

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_render_detail_item($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			<tr nobr="true">
				<td width="75">' . $value['item_id'] . '</td>
				<td width="150">' . $value['item_name'] . '</td>
				<td align="center" width="70">' . ($value['use_expire'] == 0 ? "" : date('d/m/Y', strtotime($value['expired_date']))) . '</td>
				<td align="center" width="50">' . number_format($value['qty'], 0, ",", ".") . '</td>
				<td align="right" width="70">' . number_format($value['price_point'] > 0 ? $value['price_point'] : $value['harga'], 0, ",", ".") . '</td>
				<td align="right" width="70">' . number_format($value['potongan'], 0, ",", ".") . '</td>
                <td align="right" width="82">' . number_format($value['jumlah'], 0, ",", ".") . '</td>
			</tr>';
        }

        return $html;
    }

    public function pdf_pesanan($doc_no = '')
    {
        if (trim($doc_no) == '') {
            show_404();
        }

        # Judul laporan
        $JudulLaporan = $doc_no;
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        // $pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
        $pdf->_construct('L', 'mm', array(210, 150), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = [
            'store_id' => get_row_values("store_id", "t_sales_hdr", "no_doc", $doc_no)['store_id'],
        ];
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "FakturPenjualan";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 25, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(30);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $data = $this->m_app->getData2Edit($doc_no);
        $header = $data['data'];

        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 12);
        $pdf->Cell(0, 5, "Surat Pesanan", 0, 1, 'C');
        $pdf->Ln(5);

        $pdf->SetFont('helvetica', '', 8);

        $jatuh_tempo = "";
        if (!check_arr_empty_or_null($header['tgl_jatuh_tempo'])) {
            $jatuh_tempo = '<td width="100">Jatuh Tempo</td>
                            <td width="180">: ' . date("d/m/Y", strtotime($header['tgl_jatuh_tempo'])) . '</td>';
        }

        $html_pdf = '
		<table cellpadding="3" border="0">
			<tr>
				<td width="100">No.</td>
				<td width="180">: ' . $header['no_doc'] . '</td>
				<td width="100">Tanggal</td>
				<td width="180">: ' . date("d/m/Y", strtotime($header['tgl_trans'])) . '</td>
			</tr>
            <tr>
				<td width="100">Customer</td>
				<td width="180">: ' . $header['cust_name'] . '</td>
				<td width="100">Sales</td>
				<td width="180">: ' . $header['employee_name'] . '</td>
			</tr>
            <tr>
				<td width="100">Tipe Pembayaran</td>
				<td width="180">: ' . $header['type_payment'] . '</td>
                ' . $jatuh_tempo . '
			</tr>
		</table>';

        $pdf->writeHTML($html_pdf, true, false, false, false, '');

        $pdf->SetFont('helvetica', '', 8);
        $html_pdf = '
		<style>
			table, th {
				font-weight: bold;
			}

			table, td {
				border-collapse: collapse;
			}
		</style>
		<table cellspacing="0" cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
                    <th align="center" width="30">No.</th>
					<th width="75">Kode</th>
					<th width="250">Nama</th>
					<th align="center" width="70">Expired Date</th>
					<th align="center" width="70">Qty</th>
                    <th align="center" width="70">Satuan</th>
				</tr>
			</thead>
			<tbody>
				' . $this->pdf_pesanan_render_detail_item($data['detail']) . '
			</tbody>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');

        $pdf->SetFont('helvetica', 'B', 8);

        $pdf->Cell(150, 5, "Total Item :", 0, 0, 'R');
        $pdf->Cell(24, 5, number_format($header['total_qty'], 0, ",", "."), 0, 0, 'C');
        $pdf->Ln(10);

        $html_pdf = '
		<table border="0">
			<tr nobr="true">
				<td>
                    <table cellpadding="3" border="1">
                        <tr>
                            <td width="140">Hormat Kami</td>
                            <td width="140">Packer</td>
                            <td width="140">Checker</td>
                            <td width="140">Driver/Customer</td>
                        </tr>
                        <tr>
                            <td width="140" height="40"></td>
                            <td width="140"></td>
                            <td width="140"></td>
                            <td width="140"></td>
                        </tr>
                        <tr>
                            <td width="140">Nama :</td>
                            <td width="140">Nama :</td>
                            <td width="140">Nama :</td>
                            <td width="140">Nama :</td>
                        </tr>
                        <tr>
                            <td width="140">Tanggal :</td>
                            <td width="140">Tanggal :</td>
                            <td width="140">Tanggal :</td>
                            <td width="140">Tanggal :</td>
                        </tr>
                    </table>
                </td>
			</tr>
		</table>';

        $pdf->writeHTML($html_pdf, false, false, false, false, '');

        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }

    private function pdf_pesanan_render_detail_item($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '
			<tr nobr="true">
                <th align="center" width="30">' . ($key + 1) . '</th>
				<td width="75">' . $value['item_id'] . '</td>
				<td width="250">' . $value['item_name'] . '</td>
				<td align="center" width="70">' . ($value['expired_date'] <= '1900:01:01' ? "" : date('d/m/Y', strtotime($value['expired_date']))) . '</td>
				<td align="center" width="70">' . number_format($value['qty'], 0, ",", ".") . '</td>
                <td align="center" width="70">' . $value['uom_name'] . '</td>
			</tr>';
        }

        return $html;
    }

    public function xls()
    {
        # Judul laporan
        $JudulLaporan = "Daftar Penjualan";
        # Ambil data
        $filter = array(
            'a.status_payment' => $this->input->post("filter_status", true),
            'a.tgl_trans >=' => date_convert_format($this->input->post("filter_start_date", true)),
            'a.tgl_trans <=' => date_convert_format($this->input->post("filter_end_date", true)),
        );
        $rs = $this->m_app->get_data4xls($filter);
        # Deklarasi kolom yang akan ditampilkan
        $Col['header'] = array('No. Transaksi', 'Tanggal', 'Store ID', 'Store Name', 'Member ID', 'Member Name', 'Pembayaran', 'Tanggal Jatuh Tempo', 'Tanggal Bayar', 'Qty', 'Gross/Sub Total', 'Potongan', 'PPN', 'Grand Total', 'Status Pembayaran');
        $Col['type'] = array('string', 'date', 'string', 'string', 'string', 'string', 'string', 'date', 'date', 'money', 'money', 'money', 'money', 'money', 'string');
        $Col['align'] = array('left', 'center', 'left', 'left', 'left', 'left', 'left', 'center', 'center', 'center', 'left', 'left', 'left', 'left', 'left');
        # Load library
        $this->load->library('XLSReport');
        $xls = new XLSReport();
        # Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
        if ($rs->num_rows() >= 1000000) {
            $xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
        } else {
            $xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
        }
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/
    public function getPayment(){
        $d = $this->m_app->get_payment_method();
        echo json_encode($d);
    }

    public function get_coa_lov($action = "home", $parameters = null)
    {
        $data['ColHeader'] = array('Kode', 'Nama');
        $data['ColShow'] = array(1, 1);
        $data['columns'] = array(
            array('data' => 'code_coa'),
            array('data' => 'name_coa'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                $parameters = json_decode(urldecode($parameters), true);
                $response = json_decode($this->m_app->get_coa_lov($parameters), true);
                //$response['data'] = array_format_data_item($response['data'], ['use_expire', 'expired_date', 'stok', 'harga'], ['number', 'date', 'number', 'number']);
                $this->output->set_content_type('application/json');
                echo json_encode($response);
                break;
            case 'home':
                $data['Judul'] = 'Coa List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '/' . $parameters;
                $data['search'] = $this->input->get('search', true);
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function getEmployeeList($action = "home") {
		$data['ColHeader'] = array('NIK', 'Name');
		$data['ColShow'] = array(1, 1);
		$data['columns'] = array(
			array('data' => 'nik'),
			array('data' => 'name_employee'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => false, 'searchable' => true),
			array('targets' => 1, 'orderable' => true, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getListEmployee();
				break;
			case 'home':
				$data['Judul'] = 'Employee List';
				$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}

    function getCreditorList($action = "home") {
		$data['ColHeader'] = array('Code Creditor','Desc Creditor');
		$data['ColShow'] = array(1, 1);
		$data['columns'] = array(
			array('data' => 'code_creditor'),
			array('data' => 'desc_creditor'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => false, 'searchable' => true),
			array('targets' => 1, 'orderable' => false, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getCreditorList();
				break;
			case 'home':
				$data['Judul'] = 'Creditor List';
				$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}

    public function getStore($action = "home",$parameters = null)
    {
        $parameters = json_decode(urldecode($parameters), true);
        $data['ColHeader'] = array('Kode', 'Nama', 'PIC', 'Company');
        $data['ColShow'] = array(1, 1, 1, 1);
        $data['columns'] = array(
            array('data' => 'm_code'),
            array('data' => 'm_shortdesc'),
            array('data' => 'm_pic'),
            array('data' => 'name'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
            array('targets' => 2, 'orderable' => true, 'searchable' => true),
            array('targets' => 3, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                $this->output->set_content_type('application/json');
				echo $this->m_app->getStore();
				break;
            case 'home':
                $data['Judul'] = 'Store List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '/' . $parameters;
                $data['search'] = $this->input->get('search', true);
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function getEmail($action = "home", $parameters = null)
    {
        $data['ColHeader'] = array('Kode Jabatan', 'Nama', 'Email');
        $data['ColShow'] = array(1, 1, 1);
        $data['columns'] = array(
            array('data' => 'code_jabatan'),
            array('data' => 'name_employee'),
            array('data' => 'email'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
            array('targets' => 2, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                $parameters = json_decode(urldecode($parameters), true);
                $response = json_decode($this->m_app->getEmail($parameters), true);
                $this->output->set_content_type('application/json');
				echo json_encode($response);
				break;
            case 'home':
                $data['Judul'] = 'Email List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '/' . $parameters;
                $data['search'] = $this->input->get('search', true);
                $this->load->view('LOV', $data);
                break;
        }
    }
    public function getEmailReasign($action = "home", $parameters = null)
    {
        $data['ColHeader'] = array('Kode', 'Nama', 'Email','Code Jabatan');
        $data['ColShow'] = array(1, 1, 1,1);
        $data['columns'] = array(
            array('data' => 'nik'),
            array('data' => 'name_employee'),
            array('data' => 'email'),
            array('data' => 'code_jabatan'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
            array('targets' => 2, 'orderable' => true, 'searchable' => true),
            array('targets' => 3, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                $parameters = json_decode(urldecode($parameters), true);
                $this->output->set_content_type('application/json');
				echo $this->m_app->getEmailReasign($parameters);
				break;
            case 'home':
                $data['Judul'] = 'Email List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '/' . $parameters;
                $data['search'] = $this->input->get('search', true);
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function get_item()
    {
        # Persiapkan input
        $input = [
            'item_id' => $this->input->get("item_id", true),
            'cust_id' => $this->input->get("cust_id", true),
            'cust_type_lvl' => $this->input->get("cust_type_lvl", true),
            'type_member_id' => $this->input->get("type_member_id", true),
            'sub_level' => $this->input->get("sub_level", true) == 'null' ? null : $this->input->get("sub_level", true),
        ];
        # START variabel pengecekan input
        $params = ['item_id', 'member_id', 'cust_type_lvl', 'type_member_id', 'sub_level'];
        $type = ['string', 'string', 'integer', 'string', 'integer'];
        $required = [true, false, false, false, false];
        $check_input = check_arr_empty_or_null_and_type($input, $params, $type, $required);
        # END variabel pengecekan input
        if (!$check_input['result']) {
            $response = $this->m_app->get_item($input);
        } else {
            $response = array('result' => false, 'message' => $check_input['message'], 'data' => []);
        }

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }

    public function get_coa()
    {
        # Persiapkan input
        $input = [
            'code_coa' => $this->input->get("code_coa", true),
            // 'name_coa' => $this->input->get("name_coa", true),
            // 'cust_type_lvl' => $this->input->get("cust_type_lvl", true),
            // 'type_member_id' => $this->input->get("type_member_id", true),
            // 'sub_level' => $this->input->get("sub_level", true) == 'null' ? null : $this->input->get("sub_level", true),
        ];
        # START variabel pengecekan input
        $params = ['code_coa'];
        $type = ['string'];
        $required = [true];
        $check_input = check_arr_empty_or_null_and_type($input, $params, $type, $required);
        # END variabel pengecekan input
        if (!$check_input['result']) {
            $response = $this->m_app->get_coa();
        } else {
            $response = array('result' => false, 'message' => $check_input['message'], 'data' => []);
        }

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }

    public function ganti_harga_jual()
    {
        # Persiapkan input
        $parameters = [
            'cust_id' => $this->input->get("cust_id", true),
            'cust_type_lvl' => $this->input->get("cust_type_lvl", true),
            'type_member_id' => $this->input->get("type_member_id", true),
            'sub_level' => $this->input->get("sub_level", true),
        ];
        # START variabel pengecekan input
        $params = ['member_id', 'cust_type_lvl', 'type_member_id', 'sub_level'];
        $type = ['string', 'integer', 'string', 'integer'];
        $required = [false, false, false, false];
        $check_input = check_arr_empty_or_null_and_type($parameters, $params, $type, $required);
        # END variabel pengecekan input
        if (!$check_input['result']) {
            $raw = $this->security->xss_clean($this->input->raw_input_stream);
            $input = $this->input('json', $raw);

            if (!check_arr_empty_or_null($input)) {
                $data = $this->m_app->ganti_harga_jual($input, $parameters);
                $response = array('result' => true, 'message' => 'Harga berhasil diupdate.', 'data' => $data);
            } else {
                $response = array('result' => false, 'message' => 'Data kosong.', 'data' => []);
            }
        } else {
            $response = array('result' => false, 'message' => $check_input['message'], 'data' => []);
        }

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }

    public function terapkan_promo()
    {
        # Persiapkan input
        $parameters = [
            'cust_id' => $this->input->get("cust_id", true),
            'cust_type_lvl' => $this->input->get("cust_type_lvl", true),
            'type_member_id' => $this->input->get("type_member_id", true),
            'sub_level' => $this->input->get("sub_level", true),
        ];
        # START variabel pengecekan input
        $params = ['member_id', 'cust_type_lvl', 'type_member_id', 'sub_level'];
        $type = ['string', 'integer', 'string', 'integer'];
        $required = [false, false, false, false];
        $check_input = check_arr_empty_or_null_and_type($parameters, $params, $type, $required);
        # END variabel pengecekan input
        if (!$check_input['result']) {
            $verifikasi = $this->validation('create');
            if ($verifikasi['result']):
                $input = $this->security->xss_clean($this->input->post());
                $data = $this->m_app->terapkan_promo($input, $parameters);
                $response = array('result' => true, 'message' => 'Promo sudah diterapkan.', 'data' => $data);
            else:
                $response = array('result' => false, 'message' => $verifikasi['message'], 'data' => null);
            endif;
        } else {
            $response = array('result' => false, 'message' => $check_input['message'], 'data' => []);
        }

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }

    public function get_transaksi_member_lov($action = "home")
    {
        $data['ColHeader'] = array('Outlet', 'No. Transaksi', 'Tanggal', 'Member', 'Tipe Pembayaran', 'Tanggal JT', 'Nilai Transaksi', 'Pembayaran', 'Sisa');
        $data['ColShow'] = array(1, 1, 1, 1, 1, 1, 1, 1, 1);
        $data['columns'] = array(
            array('data' => 'store_name'),
            array('data' => 'no_doc'),
            array('data' => 'tgl_trans', "className" => "text-center"),
            array('data' => 'cust_name'),
            array('data' => 'type_payment'),
            array('data' => 'tgl_jatuh_tempo', "className" => "text-center"),
            array('data' => 'grand_total', "className" => "text-right"),
            array('data' => 'terbayar', "className" => "text-right"),
            array('data' => 'sisa', "className" => "text-right"),
        );
        $data['columnDefs'] = [];

        $parameters = [
            'cust_id' => $this->input->get('cust_id', true),
            'type_payment' => $this->input->get('type_payment', true),
        ];

        switch ($action) {
            case 'nav':
                $response = json_decode($this->m_app->get_transaksi_member_lov($parameters), true);
                $response['data'] = array_format_data($response['data'], ['tgl_trans', 'tgl_jatuh_tempo', 'grand_total', 'terbayar', 'sisa'], ['date', 'date', 'number', 'number', 'number']);
                $this->output->set_content_type('application/json');
                echo json_encode($response);
                break;
            case 'home':
                $data['Judul'] = 'Daftar Transaksi';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '?' . http_build_query($parameters);
                $data['search'] = '';
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function get_transaksi_member()
    {
        $parameters = [
            'no_doc' => $this->input->get('no_doc', true),
        ];

        $response = $this->m_app->get_transaksi_member($parameters);

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }

    public function get_history_harga()
    {
        $filter = array(
            'item_id' => $this->input->post("item_id", true),
            'cust_id' => $this->input->post("cust_id", true),
        );

        $this->output->set_content_type('application/json');
        echo $this->m_app->get_history_harga($filter);
    }

    public function get_transaksi_lov($action = "home")
    {
        $data['ColHeader'] = array('Outlet', 'No. Transaksi', 'Tanggal', 'Member', 'Tipe Pembayaran', 'Nilai Transaksi');
        $data['ColShow'] = array(1, 1, 1, 1, 1, 1);
        $data['columns'] = array(
            array('data' => 'store_name'),
            array('data' => 'no_doc'),
            array('data' => 'tgl_trans', "className" => "text-center"),
            array('data' => 'cust_name'),
            array('data' => 'type_payment'),
            array('data' => 'grand_total', "className" => "text-right"),
        );
        $data['columnDefs'] = [];

        switch ($action) {
            case 'nav':
                $response = json_decode($this->m_app->get_transaksi_lov(), true);
                $response['data'] = array_format_data($response['data'], ['tgl_trans', 'grand_total'], ['date', 'number']);
                $this->output->set_content_type('application/json');
                echo json_encode($response);
                break;
            case 'home':
                $data['Judul'] = 'Daftar Penjualan';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '';
                $data['search'] = '';
                $this->load->view('LOV', $data);
                break;
        }
    }

    public function get_transaksi_detail()
    {
        $doc_no = $this->input->get("doc_no", true);

        $response = $this->m_app->get_transaksi_detail($doc_no);

        $this->output->set_content_type('application/json');
        echo json_encode($response);
    }


    // APPROVAL PETTYCASH 

    public function approval_expclaim(){
      

        $input = $this->security->xss_clean($this->input->post());

        $no_doc = $input['no_doc'];
        $comment = $input['comment'];
        $m_code = $this->input->post('code_employee');
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $code_jabatan =  $this->session->userdata('code_jabatan');
        $email =  $this->input->post('email');

        $claimexp_spv = $this->m_app->get_claimexp_spv_email($no_doc);
        $email_spv = $claimexp_spv[0]['email'];

        $claimexp_d = $this->m_app->get_claimexp_d($no_doc);
        // $code_atasan = $this->m_app->get_code_atasan($code_jabatan);

         // CEK KODE ATASAN DARI SI PENGAJU
        $code_atasans  = $this->m_app->get_atasan($code_jabatan);
        $data_approval =[];
        if(count($code_atasans) > 0){
            foreach($code_atasans as $atas){
                $data_approval[] = [
                    'no_doc'    => $no_doc,
                    'code_atasan' => $atas['code_jabatan']
                ];
            }
        }
        // dd($email_spv);
        if(count($code_atasans) == 0){
            $email = $email_spv;
        }
        // dd($code_jabatan);die;
        $data = [
            'no_doc' => $no_doc,
            'code_jabatan' =>$code_jabatan,
            'email' => $email,
            'comment'   => $comment,
            'status'    => 1, //aprove by hod
            'creation_date' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id')
        ];

        $data_bridge = [
            'docno'=> $no_doc
        ];
        
        // email
        ini_set( 'display_errors', 1 );   
        error_reporting( E_ALL );    
        
    
        
        $to = $email;
        $this->config->load('email');

        $subject = 'Expsense Claim '.$no_doc;
        
        
        $message = '<!DOCTYPE html>
        <html lang="en">
        
        <head>					
            <meta charset="utf-8" />
            <title>DSS System - Expsense Claim</title>
            <style>
            #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            }

            #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
            }
            </style>					
        </head>
        
                            <!-- end page title -->
        
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
        
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="invoice-title">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <img src="https://localhost/cashbook/assets/images/mrdiy_logo.png" alt="logo" height="50"/>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div align="right">
                                                                    <p class=" font-size-16"><strong>DIY Self Service System</strong></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3 class="mt-0">
                                                            </h3>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <address>
                                                                    <strong>Expense claim Dari:<br></strong>';
        $message .= '<strong>'.$m_code.'</strong>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <address>';
                                                                $message .= '<strong>Periode: </strong>'.$periode_awal .'-'. $periode_akhir.'<br>';
                                                                $message .= '<strong>Status: </strong> Approve '.$code_jabatan.'<br><br><br>
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="row">
                                                <div class="col-12">
                                                    <div>
                                                        <div class="p-2">
                                                            <!-- <h3 class="font-size-16"><strong>Detail Summary</strong></h3> -->
                                                        </div>
                                                        <div class="">
                                                            <div class="table-responsive">
                                                               <table id="customers">
                                                                    <tr>
                                                                        <th  scope="col">No</th>
                                                                        <th  scope="col">Deskripsi</th>
                                                                        <th  scope="col">Remark</th>
                                                                        <th  scope="col">Nominal</th>
                                                                    </tr>
                                                                    ';
                                                                    $total = 0;
                                                                    $no = 1;
                                                                    foreach($claimexp_d as $pd){
                                                                        $total = $pd['nominal']+ $total;
                                                                        $message.= '<tr>
                                                                            <td>'.$no++.'</td>
                                                                            <td>'.$pd["name_coa"].'</td>
                                                                            <td>'.$pd["remark"].'</td>
                                                                            <td>'.$pd["nominal"].'</td>
                                                                        </tr>';
                                                                    }
            $message .='<tr>
            <th colspan="3">Total</th>
            <th>Rp . '.number_format($total).'</th> <!-- Nominal -->
         </tr>';
            $message .='
                                                               </table>
                                                            </div>
                                                        </div>
                                                    </div>
                    
        
        </html>';
        

        $approve = $this->m_app->approval_claimexp($data,$data_bridge,$data_approval);
        $email_result = $this->sendMailer($to,$subject,$message);
        $msg ="";
        if($approve >0){
            $msg .= "Update data succesfully, $email_result";
            echo json_encode($stat=['msg' =>$msg,'status'=>200]);
        }
        else {
            $msg .= "Update data error";
            echo json_encode($stat=['msg' =>$msg,'status'=>400]);
        }
        

    }
    // REASIGN SETTLMENT 

    public function reasign_expclaim(){
      

        $input = $this->security->xss_clean($this->input->post());

        $no_doc = $input['no_doc'];
        $comment = $input['comment'];
        $code_employee = $this->input->post('code_employee');
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $code_jabatan =  $this->session->userdata('code_jabatan');

        $claimexp_spv = $this->m_app->get_claimexp_spv_email($no_doc);
        $email_spv = $claimexp_spv[0]['email'];
        $claimexp_d = $this->m_app->get_claimexp_d($no_doc);
        $data = [
            'no_doc'        => $no_doc,
            'code_jabatan'  =>$code_jabatan,
            'email'         => $email_spv,
            'comment'       => $comment,
            'status'        => 0,
            'creation_date' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id')
        ];
        


        
        // email
        ini_set( 'display_errors', 1 );   
        error_reporting( E_ALL );    
        
    
        $to = $email_spv;
        // $to = 'henrykurniawan1996@gmail.com';
        $this->config->load('email');

        $subject = 'Expenseclaim '.$no_doc;
        
        
        $message = '<!DOCTYPE html>
        <html lang="en">
        
        <head>					
            <meta charset="utf-8" />
            <title>DSS System - Expenseclaim</title>
            <style>
            #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            }

            #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
            }
            </style>					
        </head>
        
                            <!-- end page title -->
        
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
        
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="invoice-title">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <img src="'.base_url().'"/assets/images/mrdiy_logo.png" alt="logo" height="50"/>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div align="right">
                                                                    <p class=" font-size-16"><strong>DIY Self Service System</strong></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3 class="mt-0">
                                                            </h3>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <address>
                                                                    <strong>Expenseclaim Dari:<br></strong>';
        $message .= '<strong>'.$code_employee.'</strong>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <address>';
                                                                $message .= '<strong>Periode: </strong>'.$periode_awal .'-'. $periode_akhir.'<br>';
                                                                $message .= '<strong>Status: </strong> Revision '.$code_jabatan.'<br><br><br>';
                                                                $message .= '<strong>Comment: </strong> Revision '.$comment.'<br><br><br>
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="row">
                                                <div class="col-12">
                                                    <div>
                                                        <div class="p-2">
                                                            <!-- <h3 class="font-size-16"><strong>Detail Summary</strong></h3> -->
                                                        </div>
                                                        <div class="">
                                                            <div class="table-responsive">
                                                               <table id="customers">
                                                                    <tr>
                                                                        <th  scope="col">No</th>
                                                                        <th  scope="col">Deskripsi</th>
                                                                        <th  scope="col">Remark</th>
                                                                        <th  scope="col">Nominal</th>
                                                                    </tr>
                                                                    ';
                                                                    $total = 0;
                                                                    $no = 1;
                                                                    foreach($claimexp_d as $pd){
                                                                        $total = $pd['nominal']+ $total;
                                                                        $message.= '<tr>
                                                                            <td>'.$no++.'</td>
                                                                            <td>'.$pd["name_coa"].'</td>
                                                                            <td>'.$pd["remark"].'</td>
                                                                            <td>'.$pd["nominal"].'</td>
                                                                        </tr>';
                                                                    }
            $message .='<tr>
            <th colspan="3">Total</th>
            <th>Rp . '.number_format($total).'</th> <!-- Nominal -->
         </tr>';
            $message .='
                                                               </table>
                                                            </div>
                                                        </div>
                                                    </div>
                    
        
        </html>';
        

        $approve = $this->m_app->reasign_claimexp($data);

        $email_result = $this->sendMailer($email_spv,$subject,$message);
        $msg ="";
        if($approve >0){
            $msg .= "Update data success, $email_result";
            echo json_encode($stat=['msg' =>$msg,'status'=>200]);
        }
        else {
            $msg .= "Update data error";
            echo json_encode($stat=['msg' =>$msg,'status'=>400]);
        }
        

    }

    public function reject_expclaim(){
        $input = $this->security->xss_clean($this->input->post());

        $no_doc = $input['no_doc'];
        $code_jabatan =  $this->session->userdata('code_jabatan');

        $expclaim_spv = $this->m_app->get_claimexp_spv_email($no_doc);
        $email_spv = $expclaim_spv[0]['email'];

        $data = [
            'no_doc'        => $no_doc,
            'code_jabatan' =>$code_jabatan,
            'email'         => $email_spv,
            'status'        => 9,
            'creation_date' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id')
        ];

        // email
        ini_set( 'display_errors', 1 );   
        error_reporting( E_ALL );    
        
    
        $to = $email_spv;
        // $to = 'henrykurniawan1996@gmail.com';
        $this->config->load('email');

        $subject = 'Expenseclaim '.$no_doc;
        
        
        $message = '<!DOCTYPE html>
        <html lang="en">
        
        <head>					
            <meta charset="utf-8" />
            <title>DSS System - Expenseclaim</title>
            <style>
            #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            }

            #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
            }
            </style>					
        </head>
        
                            <!-- end page title -->
        
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
        
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="invoice-title">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <img src="'.base_url().'/assets/images/mrdiy_logo.png" alt="logo" height="50"/>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div align="right">
                                                                    <p class=" font-size-16"><strong>DIY Self Service System</strong></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3 class="mt-0">
                                                            </h3>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <address>
                                                                    <strong>Expenseclaim Dari:<br></strong>';
        $message .= '<strong>'.$no_doc.'</strong>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <address>';
                                                                $message .= '<strong>Status: </strong> Direject '.$code_jabatan.'<br><br><br>;
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
        
        
        </html>';

        $reject = $this->m_app->reject_claimexp($data);
        $email_result = $this->sendMailer($to,$subject,$message);
        $msg ="";
        if($reject >0){
            $msg .= "Rejected data success, $email_result";
            echo json_encode($stat=['msg' =>$msg,'status'=>200]);
        }
        else {
            $msg .= "Rejected data error";
            echo json_encode($stat=['msg' =>$msg,'status'=>400]);
        }
    }


    public function export_pdf($no_doc=null){
        # Judul laporan
        $JudulLaporan = "Penjualan Detail";
        # Load library
        $this->load->library("MyPDF");
        $this->load->library("parser");
        $pdf = new MyPDF('P', 'mm', array(215.9, 330.2), true, 'UTF-8', false);
        
                // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 003');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    
        
        $html = $this->set_header($no_doc);
        // set default header data
        // $pdf->SetHeaderData($headerData['logo'], $headerData['logoWidth'], $headerData['title'],$headerData['pdf_header_string']);
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $headerData, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        // $pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', false);

        // echo $html;
        // die;
        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('example_003.pdf', 'I');
        // $pdf->Output('example_003.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
    }
    public function set_header($no_doc){
        $html="";
            $invoice_no = "Expenseclaim ". $no_doc;
            $date = "";
            $data_h = $this->m_app->get_h_doc_invoice($no_doc);
            $data_d = $this->m_app->get_d_doc_invoice($no_doc);
            $data_a = $this->m_app->get_a_doc_invoice($no_doc);
            $subtotal = 0;
            $name_approve="-";
            if(count($data_a)>0){
                $name_approve = $data_a[0]['name_employee'] ." ".$data_a[0]['code_jabatan']."( ".$data_a[0]['name_jabatan']." )";

            }
            if(count($data_h)>0){
                $invoice_no = $data_h[0]['no_doc'];
                $company_name = $data_h[0]['name'];
                $date = $data_h[0]['periode_awal'] . "  to  " . $data_h[0]['periode_akhir'];
                if($data_h[0]['status'] == 6){
                    $status  = "Done";
                }
                else if($data_h[0]['status'] == 10){
                    $status  = "Ready";
                }
                else {
                    $status  = "Progress";
                }
            }
            
            $html ='
            <div style="text:align:center;">
                <table style="margin:0px auto" border="0" align="left">
                <tr>
                    <td><h1 style="text-align:center;"><u>Expense Claim Document</u></h1></td>
                </tr>
                </table>
               <div><br></div>
                <table style="margin:0px auto; text-align:left; font-size:12px;" border="0" align="left">
                <tr>
                    <td width="20%"><b>Invoice No</b></td>
                    <td width="2%">: </td>
                    <td colspan="2" width="78%">'.$invoice_no.'</td>
                </tr>
                <tr>
                    <td><b>Date</b></td>
                    <td>:</td>
                    <td>'.$date.'</td>
                </tr>
                <tr>
                    <td><b>Company Name</b></td>
                    <td>:</td>
                    <td>'.$company_name.'</td>
                </tr>              
                <tr>
                    <td><b>Status</b></td>
                    <td>:</td>
                    <td>'.$status.'</td>
                </tr>
                <tr>
                    <td><b>Approved By</b></td>
                    <td>:</td>
                    <td>'.$name_approve.'</td>
                </tr>
                </table>
            <br />
            <br />
            <table style="margin:0px auto"  border="1" cellpadding="10">
                <tr style="font-size:12px; font-weight: bold;" align="center">
                   <td style="width:7%">No</td>
                   <td>Doc No</td>
                   <td>Date</td>
                   <td>Code COA</td>
                   <td>Amount</td>
                   <td>Project Code</td>
                </tr>
            ';
            $no=1;
            foreach($data_d as $dt){
                $subtotal = $subtotal + $dt['nominal'];
                $html.='
                    <tr style="font-size:10px;">
                        <td>'.$no++.'</td>
                        <td>'.$dt["no_doc"].'</td>
                        <td>'.datesql_to_datefilter($dt["tanggal"],"d-m-Y").'</td>
                        <td>'.$dt["code_coa"]. "\n" .$dt["name_coa"].'</td>
                        <td align="right">'.number_format($dt["nominal"], 0, '.', ',').'</td>
                        <td>'.$dt["project_code"].'</td>
                    </tr>';
            }
            $html.='
                    <tr >
                        <td colspan="4" style="font-size:12px; font-weight:bold;"><b>Subtotal</b></td>
                        <td colspan="2" align="center" style="font-size:12px; font-weight:bold;"><b>'.number_format($subtotal, 0, '.', ',').'</b></td>
                    </tr>
                </table>
          </div>

               ';
       
        return $html;
    }
    

    public function sendMailer($to="",$subject="",$message=""){

        $from = $this->config->item('smtp_user');
        $this->load->library('email');
       
        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
    
        $email_result = $this->email->send();
        //if(!$email_result) tesx($this->email->print_debugger());
        $email_result = $email_result ? ' Email send' : ', Email failed to send';
        // mail($to, $subject, $message, $headers);
        return $email_result;
    }

    function getCompList($action = "home") {
		$data['ColHeader'] = array('Code Company','Name Company','Categories');
		$data['ColShow'] = array(1, 1, 1);
		$data['columns'] = array(
			array('data' => 'company_code'),
			array('data' => 'company_name'),
            array('data' => 'categories'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => false, 'searchable' => true),
			array('targets' => 1, 'orderable' => false, 'searchable' => true),
			array('targets' => 2, 'orderable' => false, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getCompList();
				break;
			case 'home':
				$data['Judul'] = 'Company List';
				$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}

    public function getWht($action = "home",$parameters = null)
    {
        $parameters = json_decode(urldecode($parameters), true);
        $data['ColHeader'] = array('Code', 'Name');
        $data['ColShow'] = array(1, 1);
        $data['columns'] = array(
            array('data' => 'code_wht'),
            array('data' => 'name_wht'),
        );
        $data['columnDefs'] = array(
            array('targets' => 0, 'orderable' => true, 'searchable' => true),
            array('targets' => 1, 'orderable' => true, 'searchable' => true),
        );
        switch ($action) {
            case 'nav':
                $this->output->set_content_type('application/json');
				echo $this->m_app->getWht();
				break;
            case 'home':
                $data['Judul'] = 'WHT List';
                $data['src_url'] = base_url() . $this->data['modules'] . '/' . $this->data['controller'] . '/' . $this->data['action'];
                $data['end_point'] = '/' . $parameters;
                $data['search'] = $this->input->get('search', true);
                $this->load->view('LOV', $data);
                break;
        }
    }
}