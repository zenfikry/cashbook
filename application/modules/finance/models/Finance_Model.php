<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finance_Model extends Core_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/
    /* Getlist Petty Cash Dashboard  */
    public function getListPc($filter)
    {
        $this->datatables->select("a.no_doc, a.periode_awal, a.periode_akhir, b.m_shortdesc, b.m_odesc, a.pic, 
                                    a.code_company, c.name, a.pettycash_awal, a.pettycash_expense, a.pettycash_akhir, a.status");
        $this->datatables->from("t_pettycash_h a");
        $this->datatables->join("m_store b", "b.m_code=a.m_code", "left");
        $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company", "left");
        $this->datatables->where("a.status", "0");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        $result = $this->datatables->generate();
        $newrs = json_decode($result);
        for($i=0; $i<count($newrs->data); $i++){
            $newrs->data[$i]->code_jabatan =  $this->session->userdata('code_jabatan');
        }

        return json_encode($newrs);
    }

    /* Getlist ExpenseClaim Dashboard  */
    public function getListEx($filter)
    {
        $this->datatables->select("a.id, a.no_doc, a.periode_awal, a.periode_akhir, b.m_shortdesc, a.pic, a.code_company, a.pettycash_awal, a.pettycash_akhir, a.status");
        $this->datatables->from('t_pettycash_h a');
        $this->datatables->join('m_store b', "b.m_code=a.m_code", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        return $this->datatables->generate();
    } 
    


    public function getData2Edit($id)
    {
        $this->db->select("a.no_doc,a.periode_awal,a.periode_akhir,a.total_amount,a.status,b.name_employee,c.name as company_name,c.m_type,b.nik,d.code_dept,d.name_dept,e.code_jabatan,e.name_jabatan,a.bank_to,a.no_rek,a.nama_penerima,a.attachment_file");
        $this->db->from("t_claimexp_h a");
        $this->db->join("m_employee b", "b.nik=a.nik");
        $this->db->join("m_perusahaan c", "c.m_type=a.code_company");
        $this->db->join("m_dept d", "d.code_dept=a.code_dept");
        $this->db->join("m_jabatan e", "e.code_dept=d.code_dept");
        $this->db->where("a.no_doc", $id);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]), 'detail' => $this->detail($id));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row_array(), 'detail' => $this->detail($id));
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null, 'detail' => $this->detail($id));
        }
    }

    public function getList($filter)
    {
        $this->datatables->select("a.no_doc,a.periode_awal,a.total_amount,a.status,b.name_employee,c.name as company_name,c.m_type");
        $this->datatables->from("t_claimexp_h a");
        $this->datatables->join("m_employee b", "b.nik=a.nik");
        $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->datatables->where($key, $val);
            }
        }
        $result = $this->datatables->generate();
        $newrs = json_decode($result);
        for($i=0; $i<count($newrs->data); $i++){
            $newrs->data[$i]->code_jabatan =  $this->session->userdata('code_jabatan');
        }

        return json_encode($newrs);
    } 

    
    public function getDataList($filter)
    {
        $this->db->select("a.store_id, b.store_name, a.no_doc, a.tgl_trans, a.kasir, d.employee_name AS kasir_name, a.type_payment, a.cust_id, e.cust_name, e.alamat, a.employee_id, f.employee_name,
                            a.total_qty, a.total_gross, a.total_price_point, a.total_potongan, a.ppn, a.ppn_pcn, a.ppn_value, a.grand_total,
                            a.cash, a.credit, a.debit, a.transfer, a.digital_wallet, a.kembali, a.credit_no, a.debit_no, a.digital_name,
                            a.status_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
                            a.coa_credit, get_coa_name(a.coa_credit) AS coa_credit_name, a.coa_debit, get_coa_name(a.coa_debit) AS coa_debit_name");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_users c", "c.user_name=a.kasir", "left");
        $this->db->join("m_employee d", "d.employee_id=c.employee_id", "left");
        $this->db->join("m_customer e", "e.cust_id=a.cust_id", "left");
        $this->db->join("m_employee f", "f.employee_id=a.employee_id", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $rs = $this->db->get();

        return $rs;
    }

    public function get_data4xls($filter)
    {
        $this->db->select("a.no_doc, a.tgl_trans, a.store_id, b.store_name, a.cust_id, e.cust_name, a.type_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
                            a.total_qty, a.total_gross, a.total_potongan, a.ppn_pcn, a.grand_total,
                            CASE WHEN a.status_payment=0  THEN 'PENDING' WHEN a.status_payment=1 THEN 'LUNAS' ELSE 'BATAL' END AS status_payment
                            ");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_users c", "c.user_name=a.kasir", "left");
        $this->db->join("m_employee d", "d.employee_id=c.employee_id", "left");
        $this->db->join("m_customer e", "e.cust_id=a.cust_id", "left");
        $this->db->join("m_employee f", "f.employee_id=a.employee_id", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $rs = $this->db->get();

        return $rs;
    }

    public function confirm($input)
    {
        // confirm
    }

    public function cancel($input)
    {
        // cancel
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function detail($doc_no)
    {
        $this->db->select("a.id AS detail_id, a.code_coa, a.tanggal,a.nominal, a.remark, 1 AS locked,b.name_coa");
        $this->db->from("t_claimexp_d a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa");
        $this->db->where("a.no_doc", $doc_no);
        $this->db->order_by("a.tanggal ASC");
        $rs = $this->db->get();

        return $rs->result_array();
    }

    

    public function get_coa_lov($input)
    {
        
        $code_dept = $input["code_dept"];
        $modul = 'Expense Claim';

        $this->datatables->select("a.code_coa, b.name_coa");
        $this->datatables->from("app_coa a");
        $this->datatables->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->datatables->where("a.status",  1);
        $this->datatables->where("a.modul", $modul);
        $this->datatables->where("a.code_dept", $code_dept);

        $result = $this->datatables->generate();

        return $result;
    }

    function getListEmployee() {
		$this->datatables->select('m_employee.nik, m_employee.name_employee,m_jabatan.code_dept,name_dept,m_employee.code_jabatan,m_jabatan.name_jabatan');
		$this->datatables->from('m_employee');
		$this->datatables->join('m_jabatan','m_jabatan.code_jabatan = m_employee.code_jabatan');
		$this->datatables->join('m_dept','m_dept.code_dept = m_jabatan.code_dept');
		return $this->datatables->generate();
	}

    function getCompList() {
		$this->datatables->select('m_type as company_code, name as company_name');
		$this->datatables->from('m_perusahaan');
		return $this->datatables->generate();
	}
    public function getStore()
    {
        // $nik = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        // $modul = 'Petty Cash';
        $nik = $this->session->userdata("nik");
        
        $this->datatables->select("a.m_code, a.m_shortdesc, a.m_pic, a.m_type");
        $this->datatables->from("m_store a");
        $this->datatables->join("m_employee_store b", "b.m_code=a.m_code", "left");
        $this->datatables->where("a.status",  1);
        $this->datatables->where("b.nik", $nik);

        $result = $this->datatables->generate();

        return $result;
    }

    public function getEmail()
    {
        // $modul = 'Petty Cash';
        $jab = $this->session->userdata('code_atasan');
        // $nik = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan",$jab)->get()->result_array();

        $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
        $this->datatables->from("m_employee a");
        $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
        $this->datatables->where("a.status",  1);
        $this->datatables->where("a.code_jabatan", $jab);

        $result = $this->datatables->generate();

        if($this->session->userdata('code_jabatan') == "MGRHRD" ){
            $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
            $this->datatables->from("m_employee a");
            $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
            $this->datatables->where("a.code_jabatan", "HOD");
            $this->datatables->where("a.status",  1);
            

            $result = $this->datatables->generate();
        }
        else if($this->session->userdata('code_jabatan') == "HOD" ){
            $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
            $this->datatables->from("m_employee a");
            $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
            $this->datatables->where("a.code_jabatan", "ARM");
            $this->datatables->where("a.status",  1);

            $result = $this->datatables->generate();
        }
        return $result;

    }

    public function getEmailReasign($parameter)
    {
        $no_doc = $parameter["no_doc"];
        $this->datatables->select('m_employee.nik, m_employee.name_employee, m_employee.email,m_employee.code_jabatan');
        $this->datatables->from('t_claimexp_a');
        $this->datatables->join('app_users','app_users.user_id = t_claimexp_a.created_by');
        $this->datatables->join('m_employee','m_employee.nik = app_users.nik');
        $this->datatables->order_by('t_claimexp_a.id','desc');
        $this->datatables->where('t_claimexp_a.no_doc',$no_doc);
        $this->datatables->where('t_claimexp_a.status',0);

        $result = $this->datatables->generate();

        return $result;
    }

    public function get_coa($input)
    {
        # Ambil Dept
        $code_dept = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        $modul = 'Petty Cash';

        $this->db->select("a.code_coa, b.name_coa");
        $this->db->from("app_coa a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.status",  1);
        $this->db->where("a.modul", $modul);
        $this->db->where("a.code_dept", $code_dept);
        $this->db->group_start();
        $this->db->where("a.code_coa", $input['code_coa']);
        $this->db->group_end();

        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }    

    public function get_transaksi_lov()
    {
        $this->datatables->select("CONCAT(a.store_id, ' - ', c.store_name) AS store_name, a.no_doc, a.tgl_trans, CONCAT(a.cust_id, ' - ', b.cust_name) AS cust_name, a.type_payment, a.grand_total, a.ppn_pcn, a.ppn_value");
        $this->datatables->from("t_sales_hdr a");
        $this->datatables->join("m_customer b", "b.cust_id=a.cust_id", "left");
        $this->datatables->join("m_store c", "c.store_id=a.store_id", "left");
        $this->datatables->where("a.status_payment", 1);
        $result = $this->datatables->generate();

        return $result;
    }

    public function get_transaksi_detail($doc_no)
    {
        $this->db->select("a.code_coa, a.tanggal, b.name_coa, a.nominal, a.remark");
        $this->db->from("t_pettycash_d a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.no_doc", $doc_no);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->result_array());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function check_no_doc_claimexp($id){
        $query = $this->db->query("SELECT * FROM t_claimexp_h WHERE no_doc = '$id'");
        return $query->num_rows();
    }
    public function get_claimexp_h($id){
        $query = $this->db->query("SELECT * FROM t_claimexp_h WHERE no_doc = '$id'");
        return $query->result_array();
    }
    public function get_claimexp_spv_email($id){
        $query = $this->db->query("SELECT m_employee.email FROM t_claimexp_h JOIN app_users ON app_users.user_id = t_claimexp_h.created_by JOIN m_employee ON m_employee.nik = app_users.nik WHERE no_doc = '$id'");
        return $query->result_array();
    }
    public function get_claimexp_d($id){

        $this->db->select("t_claimexp_d.no_doc,t_claimexp_d.tanggal,t_claimexp_d.code_coa,m_coa.name_coa,t_claimexp_d.nominal,t_claimexp_d.remark");
        $this->db->from("t_claimexp_d");
        $this->db->join("m_coa", "m_coa.code_coa=t_claimexp_d.code_coa", "left");
        $this->db->where("t_claimexp_d.no_doc", $id);
        $rs = $this->db->get();
        return $rs->result_array();

       
    }
    public function get_claimexp_a($id){

        $this->db->select("email,code_jabatan,comment,status,creation_date as tanggal");
        $this->db->from("t_claimexp_a");
        $this->db->where("t_claimexp_a.no_doc", $id);
        $this->db->order_by('creation_date DESC');;
        $rs = $this->db->get();
        return $rs->result_array();

       
    }

    public function approval_claimexp($data,$data_bridge=[]){

        if($data['code_jabatan'] == "MGRHRD"){
            $this->db->insert('t_claimexp_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',2);
                $this->db->update('t_claimexp_h');
                return $this->db->affected_rows();
            }
        }
        else if($data['code_jabatan'] == "HOD"){
            $this->db->insert('t_claimexp_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',3);
                $this->db->update('t_claimexp_h');
                return $this->db->affected_rows();
            }
        }
        else if($data['code_jabatan'] == "ARM"){
            $this->db->insert('t_claimexp_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',6);
                $this->db->update('t_claimexp_h');
            }
            $this->db->insert('t_bridging', $data_bridge);
            return $this->db->affected_rows();
        }
    }
    public function reasign_claimexp($data){

        $this->db->insert('t_claimexp_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',5);
            $this->db->update('t_claimexp_h');
            return $this->db->affected_rows();
        }
    }

    public function reject_claimexp($data){
        $this->db->insert('t_claimexp_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',9);
            $this->db->update('t_claimexp_h');
            return $this->db->affected_rows();
        }
    }
}
