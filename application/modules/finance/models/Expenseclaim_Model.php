<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Expenseclaim_Model extends Core_Model
{
    public $trans_code = "EXP";

    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/
    public function generate_trans_number($periode_awal, $code_company)
    {
        /**
         * Format penomoran dokumen
         * Contoh Penomoran :
         * SL.SW0001.2103.00002
         *
         * Penjelasan :
         * SL = Kode transaksi
         * SW0001 = Kode cabang
         * 21 = 2 digit tahun
         * 03 = 2 digit bulan
         * 00002 = 5 digit running number
         * . = Separator
         *
         */
        # Definisikan kebutuhan prefix
        $max_length_pad_string = 3;
        $prefix = $this->trans_code;
        $year = date('y', strtotime($periode_awal));
        $month = date('m', strtotime($periode_awal));
        $prefix_full = $prefix . "." .$code_company .".". $month ."." . $year;
        # Get last number
        $this->db->select("MAX(no_doc) AS code");
        $this->db->from("t_claimexp_h");
        $this->db->where([
            "YEAR(periode_awal)" => date('Y', strtotime($periode_awal)),
            // "YEAR(periode_awal)" => $year,
            "MONTH(periode_awal)" => $month,
            "SUBSTRING(no_doc, 5,3) =" => $code_company,
        ]);
        $get_last_number = $this->db->get()->row_array();
        # Jika nomor sudah ada tambahkan 1, jika tidak ada kembali ke 1
        if ($get_last_number != null) {
            $running_no = (int) substr($get_last_number['code'], strlen($prefix_full), $max_length_pad_string) + 1;
        } else {
            $running_no = 1;
        }
        $doc_no = $prefix_full . str_pad($running_no, $max_length_pad_string, "0", STR_PAD_LEFT);

        return $doc_no;
    }

    public function ceknodoc($nodoc) {
        $this->db->select("no_doc");
        $this->db->from("t_claimexp_h");
        $this->db->where("no_doc", $nodoc);
        $cek = $this->db->get()->row_array();
        if ($cek != null) {
            $result = "ada";
        } else {
            $result = "lanjut";
        }
        return $result;
    }   

    public function save($input,$input_d,$data_approval,$data_pengajuan)
    {
       
        # Mulai begin trans
        $this->db->trans_begin();

        # Generate nomor dokumen baru
        # Transaction step 1
       
        $this->db->insert('t_claimexp_h', $input);
        $err_db = $this->db->error(); # Tangkap error

        if(count($data_approval) > 0){
            $this->db->insert_batch('t_claimexp_approval',$data_approval);
        }
        $this->db->insert('t_claimexp_approval',$data_pengajuan);

        # Transaction step 2
        if (empty($err_db['message'])) {
            $detail = json_decode($input_d['detail'], true);
            # Looping insert ke tabel detail
            foreach ($detail as $key => $value) {
                if (empty($err_db['message'])){
                    if (check_arr_empty_or_null($value['code_coa']) || $value['nominal'] == 0) {
                        continue;
                    }

                    $this->db->insert(
                        't_claimexp_d',
                        [
                            'no_doc' => $input['no_doc'],
                            'code_coa' => $value['code_coa'],
                            'project_code' => $value['remark'],
                            'tanggal' => date('Y-m-d', strtotime($value['tanggal'])),
                            'nominal' => $value['nominal'],
                            'remark' => "",
                        ]
                    );
                    $err_db = $this->db->error(); # Tangkap error
                }
                else {
                    break;
                }
            }
        }

        # Transaction step 3, Potong stok penjualan
        // $this->potong_stok_penjualan($data);
        // $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Save Failed. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => ['no_doc' => $input['no_doc']], 'message' => 'Transaction saved successfull with number ' . $input['no_doc'] . '.');
        }
    }
    public function update_ap($input,$detail){
        
        $no_doc = $input['no_doc'];
        $this->db->trans_begin();
        $this->db->where('no_doc', $no_doc);
        // $this->db->set('total_amount', $total_amount);
        $this->db->update('t_claimexp_h', $input);
        $err_db = $this->db->error(); # Tangkap error

        
        $detail = json_decode($detail['detail'], true);
            # Looping insert ke tabel detail
            $this->db->where('no_doc',$no_doc);
            $this->db->delete('t_claimexp_d');
            
            for($i=0; $i<count($detail); $i++){
                $data_detail[] = [
                                    'no_doc' => $no_doc,
                                    'code_coa' => $detail[$i]['code_coa'],
                                    'tanggal' => date('Y-m-d', strtotime($detail[$i]['tanggal'])),
                                    'nominal' => $detail[$i]['nominal'],
                                    'project_code' => $detail[$i]['remark'],
                ];
            }
            $this->db->insert_batch('t_claimexp_d',$data_detail);
            $err_db = $this->db->error(); # Tangkap error

            if ($this->db->trans_status() === false) {
                # Membatalkan semua perubahan
                $this->db->trans_rollback();
            } else {
                # Commit/Simpan semua perubahan
                $this->db->trans_commit();
            }

            if (!empty($err_db['message'])) {
                $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
                return array('result' => false, 'data' => null, 'message' => 'Save Failed. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
            } else {
                return array('result' => true, 'data' => ['no_doc' => $no_doc], 'message' => 'Transaction saved successfully with number ' . $no_doc . '.');
            }
    }
    public function update($input,$detail)
    {
        $input['status'] = 1;
        
        # Mulai begin trans
        $this->db->trans_begin();

        # Generate nomor dokumen baru
        $no_doc = $input['no_doc'];

        # Transaction step 1
        

        $this->db->where('no_doc', $no_doc);
        // $this->db->set('total_amount', $total_amount);
        $this->db->update('t_claimexp_h', $input);
        $err_db = $this->db->error(); # Tangkap error

        # Transaction step 2
        if (empty($err_db['message'])) {
            $detail = json_decode($detail['detail'], true);
            # Looping insert ke tabel detail
            $this->db->where('no_doc',$no_doc);
            $this->db->delete('t_claimexp_d');
            
            for($i=0; $i<count($detail); $i++){
                $data_detail[] = [
                                    'no_doc' => $no_doc,
                                    'code_coa' => $detail[$i]['code_coa'],
                                    'tanggal' => date('Y-m-d', strtotime($detail[$i]['tanggal'])),
                                    'nominal' => $detail[$i]['nominal'],
                                    'project_code' => $detail[$i]['remark'],
                ];
            }
            $this->db->insert_batch('t_claimexp_d',$data_detail);
            $err_db = $this->db->error(); # Tangkap error
            // foreach ($detail as $key => $value) {
            //     if (empty($err_db['message'])):
            //         if (check_arr_empty_or_null($value['code_coa']) || $value['nominal'] <= 0) {
            //             continue;
            //         }

            //         $this->db->where('no_doc',$no_doc);
            //         $this->db->where('code_coa',$value['code_coa']);
            //         $this->db->update(
            //             't_pettycash_d',
            //             [
            //                 'no_doc' => $no_doc,
            //                 'code_coa' => $value['code_coa'],
            //                 'tanggal' => date('Y-m-d', strtotime($value['tanggal'])),
            //                 'nominal' => $value['nominal'],
            //                 'remark' => $value['remark'],
            //             ]
            //         );
            //         $err_db = $this->db->error(); # Tangkap error
            //     else:
            //         break;
            //     endif;
            // }
        }

        # Transaction step 3, Potong stok penjualan
        // $this->potong_stok_penjualan($data);
        // $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Save Failed. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => ['no_doc' => $no_doc], 'message' => 'Transaction saved successfully with number ' . $no_doc . '.');
        }
    }

    public function readyStatus($no_doc){
        $this->db->set('status',10);
        $this->db->where('no_doc',$no_doc);
        $this->db->update('t_claimexp_h');
        return $this->db->affected_rows();
    }
    
    public function get_expense_h_bridge($id){
        $query = $this->db->query("SELECT a.no_doc,a.creation_date,a.periode_awal,a.periode_akhir,a.code_company,a.total_amount, a.nik, b.name_employee FROM t_claimexp_h as a join m_employee b on b.nik = a.nik WHERE a.no_doc = '$id'");
        return $query->result_array();
    }

    public function get_h_doc_invoice($no_doc){
        $this->db->select('t_claimexp_h.*,m_perusahaan.name');
        $this->db->from('t_claimexp_h');
        $this->db->join('m_perusahaan','m_perusahaan.m_type = t_claimexp_h.code_company');
        $this->db->where('t_claimexp_h.no_doc',$no_doc);
        return $this->db->get()->result_array();
    }

    public function get_d_doc_invoice($no_doc){
        $this->db->select("a.no_doc, a.tanggal, a.code_coa, b.name_coa, a.nominal, a.project_code");
        $this->db->from('t_claimexp_d a');
        $this->db->join('m_coa b', 'b.code_coa=a.code_coa', 'left');
        $this->db->where('a.no_doc',$no_doc);
        return $this->db->get()->result_array();
    }
    public function get_expense_d_bridge($id){
        $this->db->select("a.no_doc, a.tanggal, a.code_coa, b.name_coa, a.nominal, a.project_code");
        $this->db->from('t_claimexp_d a');
        $this->db->join('m_coa b', 'b.code_coa=a.code_coa', 'left');
        $this->db->where('a.no_doc',$id);
        return $this->db->get()->result_array();
    }
    public function get_a_doc_invoice($no_doc){
        // $this->db->select("b.name_employee,a.code_jabatan,d.name_jabatan");
        // $this->db->from('t_claimexp_a a');
        // $this->db->join('app_users c', 'c.user_id=a.created_by');
        // $this->db->join('m_employee b', 'b.nik=c.nik', 'left');
        // $this->db->join('m_jabatan d', 'd.code_jabatan=a.code_jabatan', 'left');
        // $this->db->where('a.no_doc',$no_doc);
        // $this->db->where('a.status',1);
        // $this->db->order_by('b.creation_date','DESC');
        // $this->db->limit(1);
        // return $this->db->get()->result_array();

        $query = $this->db->query("select name_employee, code_jabatan, name_jabatan, creation_date from 
		(select b.name_employee as name_employee,a.code_jabatan as code_jabatan,d.name_jabatan as name_jabatan, a.creation_date as creation_date
        from t_claimexp_a a
        join m_employee b on b.nik=a.created_by
        join  m_jabatan d on d.code_jabatan=a.code_jabatan
        where a.no_doc = '$no_doc'
        and a.status=1
		group by b.name_employee, a.code_jabatan, d.name_jabatan, a.creation_date) x order by creation_date");

        $result = $query->result_array();
        return $result;
    }

    public function get_atasan($code_jabatan){

        // GET DATA CODE JABATAN DARI SI PENGAJUAN
        $this->db->select("code_atasan");
        $this->db->from("m_jabatan");
        $this->db->where("m_jabatan.code_jabatan =",$code_jabatan);
        $code = $this->db->get()->result_array();
        
        // GET CODE JABATAN DARI ATASAN
        $this->db->select("lvl_expense");
        $this->db->from("m_jabatan");
        $this->db->where("m_jabatan.code_jabatan =",$code[0]['code_atasan']);
        $code_atasan = $this->db->get()->result_array();

        if(count($code_atasan) > 0){
            foreach($code_atasan as $ast){
                // cek jiika tidak ada group lvl expense tidak kosong ambil semua grup code_jabatan berdasarkan lvl expense group
                if($ast['lvl_expense'] !=""){
                    $this->db->select("code_jabatan");
                    $this->db->from("m_jabatan");
                    $this->db->where("m_jabatan.lvl_expense =",$ast['lvl_expense']);
                    $code_jabatan_atasan = $this->db->get()->result_array();
                }
                else {
                    $this->db->select("code_jabatan");
                    $this->db->from("m_jabatan");
                    $this->db->where("m_jabatan.code_jabatan =",$code[0]['code_atasan']);
                    $code_jabatan_atasan = $this->db->get()->result_array();
                }
                return $code_jabatan_atasan;

            }
        }
        else {
            return $code_atasan;
        }

    }

    public function get_jabatan_group($code_jabatan){
        $this->db->select("code_jabatan");
        $this->db->from("m_jabatan");
        $this->db->where("m_jabatan.lvl_expense =",$code_jabatan);
        return $this->db->get()->result_array();
    }

    public function delete($id)
    {
        // delete
        $this->db->where('no_doc',$id);
        $this->db->delete(array('t_claimexp_h','t_claimexp_d','t_claimexp_a','t_claimexp_approval'));
        return $this->db->affected_rows();        
    }

    public function delete_detail($id)
    {
        $this->db->where('no_doc', $id);
        $this->db->delete('t_claimexp_d');
        return $this->db->affected_rows();
    }
    
    public function delete_adetail($id)
    {
        $this->db->where('no_doc', $id);
        $this->db->delete('t_claimexp_a');
        return $this->db->affected_rows();
    }
    
    public function delete_approve($id)
    {
        $this->db->where('no_doc', $id);
        $this->db->delete('t_claimexp_approval');
        return $this->db->affected_rows();
    }

    public function getData2Edit($id)
    {
        $this->db->select("a.no_doc,a.periode_awal,a.periode_akhir,a.total_amount,a.status,a.description,b.name_employee,c.name as company_name,c.m_type,b.nik,d.code_dept,d.name_dept,e.code_jabatan,e.name_jabatan,a.bank_to,a.no_rek,a.nama_penerima,a.attachment_file,a.no_pejadin,a.payment_method");
        $this->db->from("t_claimexp_h a");
        $this->db->join("m_employee b", "b.nik=a.nik");
        $this->db->join("m_perusahaan c", "c.m_type=a.code_company");
        $this->db->join("m_dept d", "d.code_dept=a.code_dept");
        $this->db->join("m_jabatan e", "e.code_dept=d.code_dept");
        $this->db->where("a.no_doc", $id);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]), 'detail' => $this->detail($id));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data found.', 'data' => $rs->row_array(), 'detail' => $this->detail($id));
        } else {
            return array('result' => false, 'message' => 'Data not found.', 'data' => null, 'detail' => $this->detail($id));
        }
    }

    public function getList($filter)
    {
        $code_jabatan = $this->session->userdata('code_jabatan');
        $nik = $this->session->userdata('nik');
        // if($code_jabatan == "PO001" || $code_jabatan == "PO002"){
        //     $this->datatables->select("a.no_doc,a.periode_awal,a.total_amount,a.status,b.name_employee,c.name as company_name,c.m_type,a.code_approve,b.email,a.code_approve,b.code_jabatan as created_by");
        //     $this->datatables->from("t_claimexp_h a");
        //     $this->datatables->join("m_employee b", "b.nik=a.nik");
        //     $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company");
        //     // $this->datatables->join("t_claimexp_approval d", "d.no_doc=a.no_doc");

        //     $this->datatables->where_in("a.status",array(6, 10));
        //     foreach ($filter as $key => $val) {
        //         if (trim($val) != "" || !empty($val) || $val != null) {
        //             $this->datatables->where($key, $val);
        //         }
        //     }
        //     $result = $this->datatables->generate();
        //     $newrs = json_decode($result);
        //     for($i=0; $i<count($newrs->data); $i++){
        //         $newrs->data[$i]->login =  $this->session->userdata('email');
        //         $newrs->data[$i]->code_login =  $this->session->userdata('code_jabatan');
        //     }
    
        //     return json_encode($newrs);
        // }
        // else {
            $this->datatables->select("a.no_doc,a.periode_awal,a.total_amount,a.status,b.name_employee,c.name as company_name,c.m_type,a.code_approve,b.email,a.code_approve,b.code_jabatan as created_by");
            $this->datatables->from("t_claimexp_h a");
            $this->datatables->join("m_employee b", "b.nik=a.nik");
            $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company");
            // $this->datatables->join("t_claimexp_approval d", "d.no_doc=a.no_doc");
            // $this->datatables->where("d.code_pengaju",$code_jabatan);
            $this->datatables->where("a.nik", $nik);
            foreach ($filter as $key => $val) {
                if (trim($val) != "" || !empty($val) || $val != null) {
                    $this->datatables->where($key, $val);
                }
            }

            // $this->datatables->or_where("d.code_atasan",$code_jabatan);
            // foreach ($filter as $key => $val) {
            //     if (trim($val) != "" || !empty($val) || $val != null) {
            //         $this->datatables->where($key, $val);
            //     }
            // }
    
            $result = $this->datatables->generate();
            $newrs = json_decode($result);
            for($i=0; $i<count($newrs->data); $i++){
                $newrs->data[$i]->login =  $this->session->userdata('email');
                $newrs->data[$i]->code_login =  $this->session->userdata('code_jabatan');
            }
    
            return json_encode($newrs);

        // }
    } 

    
    public function getDataList($filter)
    {
        $this->db->select("a.store_id, b.store_name, a.no_doc, a.tgl_trans, a.kasir, d.employee_name AS kasir_name, a.type_payment, a.cust_id, e.cust_name, e.alamat, a.employee_id, f.employee_name,
                            a.total_qty, a.total_gross, a.total_price_point, a.total_potongan, a.ppn, a.ppn_pcn, a.ppn_value, a.grand_total,
                            a.cash, a.credit, a.debit, a.transfer, a.digital_wallet, a.kembali, a.credit_no, a.debit_no, a.digital_name,
                            a.status_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
                            a.coa_credit, get_coa_name(a.coa_credit) AS coa_credit_name, a.coa_debit, get_coa_name(a.coa_debit) AS coa_debit_name");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_users c", "c.user_name=a.kasir", "left");
        $this->db->join("m_employee d", "d.employee_id=c.employee_id", "left");
        $this->db->join("m_customer e", "e.cust_id=a.cust_id", "left");
        $this->db->join("m_employee f", "f.employee_id=a.employee_id", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $rs = $this->db->get();

        return $rs;
    }

    public function get_data4xls($filter)
    {
        $this->db->select("a.no_doc, a.tgl_trans, a.store_id, b.store_name, a.cust_id, e.cust_name, a.type_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
                            a.total_qty, a.total_gross, a.total_potongan, a.ppn_pcn, a.grand_total,
                            CASE WHEN a.status_payment=0  THEN 'PENDING' WHEN a.status_payment=1 THEN 'LUNAS' ELSE 'BATAL' END AS status_payment
                            ");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_users c", "c.user_name=a.kasir", "left");
        $this->db->join("m_employee d", "d.employee_id=c.employee_id", "left");
        $this->db->join("m_customer e", "e.cust_id=a.cust_id", "left");
        $this->db->join("m_employee f", "f.employee_id=a.employee_id", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $rs = $this->db->get();

        return $rs;
    }

    public function confirm($input)
    {
        // confirm
    }

    public function cancel($input)
    {
        // cancel
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function detail($doc_no)
    {
        $this->db->select("a.id AS detail_id, a.code_coa, a.tanggal,a.nominal, a.project_code as remark, 1 AS locked,b.name_coa");
        $this->db->from("t_claimexp_d a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa");
        $this->db->where("a.no_doc", $doc_no);
        $this->db->order_by("a.tanggal ASC");
        $rs = $this->db->get();

        return $rs->result_array();
    }

    
    public function get_payment_method(){
        $this->db->select("a.code_coa, b.name_coa");
        $this->db->from("app_coa a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.status",  1);
        $this->db->where("a.modul", "Expense Claim");
        $this->db->where("a.code_dept", "STORE");

        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_coa_lov($input)
    {
        
        $code_dept = $input["code_dept"];
        $modul = 'Expense Claim';

        $this->datatables->select("a.code_coa, b.name_coa");
        $this->datatables->from("app_coa a");
        $this->datatables->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->datatables->where("a.status",  1);
        $this->datatables->where("a.modul", $modul);
        $this->datatables->where("a.code_dept", $code_dept);

        $result = $this->datatables->generate();

        return $result;
    }

    function getPaymentMethood(){
        $this->db->select('code_coa,name_coa');
        $this->db->where('m_coa.code_coa','3010-1001');
        $this->db->from('m_coa');
        return $this->db->get()->result_array();
    }

    function getEmployeeID(){
        $nik = $this->session->userdata('nik');
        $this->db->select('m_employee.nik, m_employee.name_employee,m_jabatan.code_dept,name_dept,m_employee.code_jabatan,m_jabatan.name_jabatan');
		$this->db->from('m_employee');
		$this->db->join('m_jabatan','m_jabatan.code_jabatan = m_employee.code_jabatan');
		$this->db->join('m_dept','m_dept.code_dept = m_jabatan.code_dept');
		$this->db->where('m_employee.nik',$nik);
		return $this->db->get()->result_array();
    }
    function getListEmployee() {
		$this->datatables->select('m_employee.nik, m_employee.name_employee,m_jabatan.code_dept,name_dept,m_employee.code_jabatan,m_jabatan.name_jabatan');
		$this->datatables->from('m_employee');
		$this->datatables->join('m_jabatan','m_jabatan.code_jabatan = m_employee.code_jabatan');
		$this->datatables->join('m_dept','m_dept.code_dept = m_jabatan.code_dept');
		return $this->datatables->generate();
	}

    function getCompList() {
		$this->datatables->select('m_type as company_code, name as company_name, categories');
		$this->datatables->from('m_perusahaan');
		return $this->datatables->generate();
	}
    public function getStore()
    {
        // $nik = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        // $modul = 'Petty Cash';
        $code_jabatan = $this->session->userdata("code_jabatan");
        
        $this->datatables->select("a.m_code, a.m_shortdesc, a.m_pic, b.name");
        $this->datatables->from("m_store a");
        $this->datatables->join("m_perusahaan b", "b.m_type=a.m_type", "left");
        $this->datatables->where("a.status",  1);

        $result = $this->datatables->generate();

        return $result;
    }

    public function getEmail($code_employee)
    {
        // dd($code_employee);
        // $modul = 'Petty Cash';
        // $jab = $this->session->userdata('code_atasan');
        // $nik = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan",$jab)->get()->result_array();
        $code_jabatan =  $this->session->userdata('code_jabatan');
        if($this->session->userdata('code_atasan')!=""){
            // if($code_jabatan != "PO036"){
                
            // }
            $this->db->distinct();
            $this->db->select("b.code_atasan");
            $this->db->from("m_employee a");
            $this->db->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
            $this->db->where("a.status",  1);
            $this->db->where("b.code_atasan", $this->session->userdata('code_atasan'));
            $this->db->group_by("b.code_atasan");
            $code_atasan = $this->db->get()->result_array();  
            if(count($code_atasan) > 0){
                for($i=0; $i<count($code_atasan); $i++){
                    $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
                    $this->datatables->from("m_employee a");
                    $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
                    $this->datatables->where("a.status",  1);
                    $this->datatables->where_in("a.code_jabatan", $code_atasan[$i]);
                }
            }
            // if(count($code_atasan) > 0){
            //     for($i=0; $i<count($code_atasan); $i++){
            //         $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
            //         $this->datatables->from("m_employee a");
            //         $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
            //         $this->datatables->where("a.status",  1);
            //         $this->datatables->where_in("a.code_jabatan", $code_atasan[$i]);
            //     }
            // }
        }
        // if($this->session->userdata('code_atasan')!=""){
        //     $this->db->select("b.code_atasan");
        //     $this->db->from("m_employee a");
        //     $this->db->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
        //     $this->db->where("a.status",  1);
        //     $this->db->where("a.nik", $code_employee['employee_id']);
        //     $code_atasan = $this->db->get()->result_array();  
        //     dd($code_atasan);
            // if(count($code_atasan) > 0){
            //     for($i=0; $i<count($code_atasan); $i++){
            //         $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
            //         $this->datatables->from("m_employee a");
            //         $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
            //         $this->datatables->where("a.status",  1);
            //         $this->datatables->where_in("a.code_jabatan", $code_atasan[$i]);
            //     }
            // }
        // }
        else{
            $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
            $this->datatables->from("m_employee a");
            $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
            $this->datatables->where("a.status",  1);
                    
        }

        $result = $this->datatables->generate();


        // if($this->session->userdata('code_jabatan') == "MGRHRD" ){
        //     $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
        //     $this->datatables->from("m_employee a");
        //     $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
        //     $this->datatables->where("a.code_jabatan", "HOD");
        //     $this->datatables->where("a.status",  1);
            

        //     $result = $this->datatables->generate();
        // }
        // else if($this->session->userdata('code_jabatan') == "HOD" ){
        //     $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
        //     $this->datatables->from("m_employee a");
        //     $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan");
        //     $this->datatables->where("a.code_jabatan", "ARM");
        //     $this->datatables->where("a.status",  1);

        //     $result = $this->datatables->generate();
        // }
        return $result;

    }

    public function getEmailReasign($parameter)
    {
        $no_doc = $parameter["no_doc"];
        $this->datatables->select('m_employee.nik, m_employee.name_employee, m_employee.email,m_employee.code_jabatan');
        $this->datatables->from('t_claimexp_a');
        $this->datatables->join('app_users','app_users.user_id = t_claimexp_a.created_by');
        $this->datatables->join('m_employee','m_employee.nik = app_users.nik');
        $this->datatables->order_by('t_claimexp_a.id','desc');
        $this->datatables->where('t_claimexp_a.no_doc',$no_doc);
        $this->datatables->where('t_claimexp_a.status',0);

        $result = $this->datatables->generate();

        return $result;
    }

    public function get_coa($input)
    {
        # Ambil Dept
        $code_dept = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        $modul = 'Petty Cash';

        $this->db->select("a.code_coa, b.name_coa");
        $this->db->from("app_coa a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.status",  1);
        $this->db->where("a.modul", $modul);
        $this->db->where("a.code_dept", $code_dept);
        $this->db->group_start();
        $this->db->where("a.code_coa", $input['code_coa']);
        $this->db->group_end();

        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Failed to load data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data found.', 'data' => $rs->row());
        } else {
            return array('result' => false, 'message' => 'Data not found.', 'data' => null);
        }
    }    

    public function get_transaksi_lov()
    {
        $this->datatables->select("CONCAT(a.store_id, ' - ', c.store_name) AS store_name, a.no_doc, a.tgl_trans, CONCAT(a.cust_id, ' - ', b.cust_name) AS cust_name, a.type_payment, a.grand_total, a.ppn_pcn, a.ppn_value");
        $this->datatables->from("t_sales_hdr a");
        $this->datatables->join("m_customer b", "b.cust_id=a.cust_id", "left");
        $this->datatables->join("m_store c", "c.store_id=a.store_id", "left");
        $this->datatables->where("a.status_payment", 1);
        $result = $this->datatables->generate();

        return $result;
    }

    public function get_transaksi_detail($doc_no)
    {
        $this->db->select("a.code_coa, a.tanggal, b.name_coa, a.nominal, a.remark");
        $this->db->from("t_pettycash_d a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.no_doc", $doc_no);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Failed to load data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data found.', 'data' => $rs->result_array());
        } else {
            return array('result' => false, 'message' => 'Data not found.', 'data' => null);
        }
    }

    public function check_no_doc_claimexp($id){
        $query = $this->db->query("SELECT * FROM t_claimexp_h WHERE no_doc = '$id'");
        return $query->num_rows();
    }
    public function get_claimexp_h($id){
        $query = $this->db->query("SELECT * FROM t_claimexp_h WHERE no_doc = '$id'");
        return $query->result_array();
    }
    public function get_claimexp_spv_email($id){
        $query = $this->db->query("SELECT m_employee.email FROM t_claimexp_h JOIN app_users ON app_users.user_id = t_claimexp_h.created_by JOIN m_employee ON m_employee.nik = app_users.nik WHERE no_doc = '$id'");
        return $query->result_array();
    }
    public function get_claimexp_d($id){

        $this->db->select("t_claimexp_d.no_doc,t_claimexp_d.tanggal,t_claimexp_d.code_coa,m_coa.name_coa,t_claimexp_d.nominal,t_claimexp_d.remark");
        $this->db->from("t_claimexp_d");
        $this->db->join("m_coa", "m_coa.code_coa=t_claimexp_d.code_coa", "left");
        $this->db->where("t_claimexp_d.no_doc", $id);
        $rs = $this->db->get();
        return $rs->result_array();

       
    }
    public function get_claimexp_a($id){

        $this->db->select("email,code_jabatan,comment,status,creation_date as tanggal");
        $this->db->from("t_claimexp_a");
        $this->db->where("t_claimexp_a.no_doc", $id);
        $this->db->order_by('creation_date DESC');;
        $rs = $this->db->get();
        return $rs->result_array();

       
    }

    public function approval_claimexp($data,$data_bridge=[],$data_approval){
        if(count($data_approval) > 0){
            $this->db->insert_batch('t_claimexp_approval',$data_approval);
            $this->db->insert('t_claimexp_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',3);
                $this->db->set('code_approve',$this->session->userdata('code_jabatan'));
                $this->db->update('t_claimexp_h');
                return $this->db->affected_rows();
            }
        }
        else {
            $this->db->insert('t_claimexp_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',6);
                $this->db->set('code_approve',$this->session->userdata('code_jabatan'));
                $this->db->update('t_claimexp_h');
                return $this->db->affected_rows();
            }
        }

        // $code_jabatan = $this->session->userdata('code_jabatan');
        // $code_lvl = $this->db->query("select lvl_expense from m_jabatan where code_jabatan='$code_jabatan'")->result_array();
        // // dd($code_lvl);
        // if(count($code_lvl) == 0) return 0;
        // foreach ($code_lvl as $lvl){
        //     if($lvl['lvl_expense'] == "HOD_GROUP"){ //HOD
                // $this->db->insert('t_claimexp_a', $data);
                // if($this->db->affected_rows() > 0){
                //     $this->db->where('no_doc',$data['no_doc']);
                //     $this->db->set('status',3);
                //     $this->db->update('t_claimexp_h');
                //     return $this->db->affected_rows();
                // }
        //     }
        //     else if($lvl['lvl_expense'] == "COO_GROUP"){ // COO
        //         $this->db->insert('t_claimexp_a', $data);
        //         if($this->db->affected_rows() > 0){
        //             $this->db->where('no_doc',$data['no_doc']);
        //             $this->db->set('status',6);
        //             $this->db->update('t_claimexp_h');
        //             // return $this->db->affected_rows();
        //             $this->db->insert('t_bridging', $data_bridge);
        //             return $this->db->affected_rows();
        //         }
        //     }
        // }
       
    }
    public function reasign_claimexp($data){

        $this->db->insert('t_claimexp_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',5);
            $this->db->update('t_claimexp_h');
            return $this->db->affected_rows();
        }
    }

    public function reject_claimexp($data){
        $this->db->insert('t_claimexp_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',9);
            $this->db->update('t_claimexp_h');
            return $this->db->affected_rows();
        }
    }

    public function get_code_atasan($code_jabatan)
    {
        return $this->db->query("select b.code_atasan from m_employee a
        join m_jabatan b on b.code_jabatan=a.code_jabatan 
        where a.code_jabatan='$code_jabatan'")->result_array();
        
    }

    public function insert_bridge($no_doc,$data,$data_approve){
       
        # Mulai begin trans
       //  $this->db->trans_begin();
       
       $this->db->where('no_doc',$no_doc);
       $this->db->set('status',10);
       $this->db->update('t_claimexp_h');

       $this->db->where('no_doc',$no_doc);
       $this->db->insert('t_claimexp_a',$data_approve);

       for($i=0; $i<count($data);$i++){
           $this->db->insert('t_bridging_ex',$data[$i]);
       }
       // if ($this->db->trans_status() === false) {
       //     # Membatalkan semua perubahan
       //     $this->db->trans_rollback();
       //     return $this->db->error();
       // } else {
       //     # Commit/Simpan semua perubahan
       //     $this->db->trans_commit();
       // }
       return $this->db->affected_rows();
   }
}
