<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Posapp_model extends Core_Model
{
    public $trans_code = "PC";

    public function __construct()
    {
        parent::__construct();
    }

    /*******************************************************************************
     *                            START DEFAULT FUNCTION                            *
     *******************************************************************************/
    public function generate_trans_number($periode_awal, $m_code)
    {
        /**
         * Format penomoran dokumen
         * Contoh Penomoran :
         * SL.SW0001.2103.00002
         *
         * Penjelasan :
         * SL = Kode transaksi
         * SW0001 = Kode cabang
         * 21 = 2 digit tahun
         * 03 = 2 digit bulan
         * 00002 = 5 digit running number
         * . = Separator
         *
         */
        # Definisikan kebutuhan prefix
        $max_length_pad_string = 5;
        $prefix = $this->trans_code;
        $year = date('y', strtotime($periode_awal));
        $month = date('m', strtotime($periode_awal));
        $prefix_full = $prefix . "." . $m_code . "." . $year . $month . ".";
        # Get last number
        $this->db->select("MAX(no_doc) AS no_doc");
        $this->db->from("t_pettycash_h");
        $this->db->where([
            "m_code" => $m_code,
            "YEAR(periode_awal)" => date('Y', strtotime($periode_awal)),
            "MONTH(periode_awal)" => $month,
        ]);
        $get_last_number = $this->db->get()->row_array();
        # Jika nomor sudah ada tambahkan 1, jika tidak ada kembali ke 1
        if ($get_last_number != null) {
            $running_no = (int) substr($get_last_number['no_doc'], strlen($prefix_full), $max_length_pad_string) + 1;
        } else {
            $running_no = 1;
        }
        $doc_no = $prefix_full . str_pad($running_no, $max_length_pad_string, "0", STR_PAD_LEFT);

        return $doc_no;
    }

    public function save($input, $rename_folder)
    {
        # Cek limit piutang member
        // if (!check_arr_empty_or_null($input['cust_id']) && $input['tipe_pembayaran'] == 'TEMPO') {
        //     # Ambil limit
        //     $limit = get_row_values("limit_piutang", "m_customer", "cust_id", $input['cust_id']);
        //     # Ambil transaksi tempo belum terbayar
        //     $jumlah_tempo = get_row_values("COALESCE(SUM(grand_total), 0) AS grand_total", "t_sales_hdr", ["cust_id", "type_payment", "status_payment"], [$input['cust_id'], 'TEMPO', 0]);
        //     # Totalkan jumlah tempo dan transksi yang akan datang
        //     $total_piutang = unformat_numeric($input['total']) + $jumlah_tempo['grand_total'];
        //     # Jika $total_piutang melebihi piutang maka batalkan
        //     if ($total_piutang >= $limit['limit_piutang']) {
        //         return array('result' => false, 'data' => null, 'message' => 'Transaksi yang akan dilakukan melebihi nilai limit piutang (' . number_format($total_piutang, 0, ",", ".") . '/' . number_format($limit['limit_piutang'], 0, ",", ".") . ').');
        //     }
        // }
        
        # Mulai begin trans
        $this->db->trans_begin();

        # Generate nomor dokumen baru
        $no_doc = $this->generate_trans_number(date_convert_format($input['periode_awal']), $input['m_code']);

        # Transaction step 1
        $data = [
            'no_doc' => $no_doc,
            'periode_awal' => date_convert_format($input['periode_awal']),
            'periode_akhir' => date_convert_format($input['periode_akhir']),
            'm_code' => $input['m_code'],
            'pic' => $input['m_pic'],
            'code_company' => $input['m_type'],
            'pettycash_awal' => $input['pettycash_store'],
            'pettycash_expense' => unformat_numeric($input['total_expense']),
            'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
            'attachment_expense' => $rename_folder.'/'.$input['attachment_expense'],
            'attachment_sisa' => $rename_folder.'/'.$input['attachment_sisa'],
            'status' => 0,
            'email' => $input['email'],
            'created_by' => $this->session->userdata('user_id'),
            'creation_date' => date('Y-m-d H:i:s'),
        ];

        $this->db->insert('t_pettycash_h', $data);
        $err_db = $this->db->error(); # Tangkap error

        # Transaction step 2
        if (empty($err_db['message'])) {
            $detail = json_decode($input['detail'], true);
            # Looping insert ke tabel detail
            foreach ($detail as $key => $value) {
                if (empty($err_db['message'])):
                    if (check_arr_empty_or_null($value['code_coa']) || $value['nominal'] <= 0) {
                        continue;
                    }

                    $this->db->insert(
                        't_pettycash_d',
                        [
                            'no_doc' => $no_doc,
                            'code_coa' => $value['code_coa'],
                            'tanggal' => date('Y-m-d', strtotime($value['tanggal'])),
                            'nominal' => $value['nominal'],
                            'remark' => $value['remark'],
                        ]
                    );
                    $err_db = $this->db->error(); # Tangkap error
                else:
                    break;
                endif;
            }
        }

        # Transaction step 3, Potong stok penjualan
        // $this->potong_stok_penjualan($data);
        // $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => ['no_doc' => $no_doc], 'message' => 'Transaksi tersimpan dengan nomor ' . $no_doc . '.');
        }
    }

    public function update($input)
    {

      
        # Cek limit piutang member
        // if (!check_arr_empty_or_null($input['cust_id']) && $input['tipe_pembayaran'] == 'TEMPO') {
        //     # Ambil limit
        //     $limit = get_row_values("limit_piutang", "m_customer", "cust_id", $input['cust_id']);
        //     # Ambil transaksi tempo belum terbayar
        //     $jumlah_tempo = get_row_values("COALESCE(SUM(grand_total), 0) AS grand_total", "t_sales_hdr", ["cust_id", "type_payment", "status_payment"], [$input['cust_id'], 'TEMPO', 0]);
        //     # Totalkan jumlah tempo dan transksi yang akan datang
        //     $total_piutang = unformat_numeric($input['total']) + $jumlah_tempo['grand_total'];
        //     # Jika $total_piutang melebihi piutang maka batalkan
        //     if ($total_piutang >= $limit['limit_piutang']) {
        //         return array('result' => false, 'data' => null, 'message' => 'Transaksi yang akan dilakukan melebihi nilai limit piutang (' . number_format($total_piutang, 0, ",", ".") . '/' . number_format($limit['limit_piutang'], 0, ",", ".") . ').');
        //     }
        // }
       
        $data = [
            'periode_awal' => date_convert_format($input['periode_awal']),
            'periode_akhir' => date_convert_format($input['periode_akhir']),
            'm_code' => $input['m_code'],
            'pic' => $input['m_pic'],
            'code_company' => $input['m_type'],
            'pettycash_awal' => $input['pettycash_store'],
            'pettycash_expense' => unformat_numeric($input['total_expense']),
            'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
            'status' => 0,
            'email' => $input['email'],
            'modified_by' => $this->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        ];

        if($input['attachment_expense'] !=""){
            $data['attachment_expense'] = $input['attachment_expense'];
        }
        if($input['attachment_sisa'] !=""){
            $data['attachment_sisa'] = $input['attachment_sisa'];
        }

        // end check status approval
        # Mulai begin trans
        $this->db->trans_begin();

        # Generate nomor dokumen baru
        $no_doc = $input['no_doc'];

        # Transaction step 1
        

        $this->db->where('no_doc', $no_doc);
        $this->db->update('t_pettycash_h', $data);
        $err_db = $this->db->error(); # Tangkap error

        # Transaction step 2
        if (empty($err_db['message'])) {
            $detail = json_decode($input['detail'], true);
            
            # Looping insert ke tabel detail
            $this->db->where('no_doc',$no_doc);
            $this->db->delete('t_pettycash_d');
            
            for($i=0; $i<count($detail); $i++){
                $data_detail[] = [
                                    'no_doc' => $no_doc,
                                    'code_coa' => $detail[$i]['code_coa'],
                                    'tanggal' => date('Y-m-d', strtotime($detail[$i]['tanggal'])),
                                    'nominal' => $detail[$i]['nominal'],
                                    'remark' => $detail[$i]['remark'],
                ];
            }
            $this->db->insert_batch('t_pettycash_d',$data_detail);
            $err_db = $this->db->error(); # Tangkap error
        }

        # Transaction step 3, Potong stok penjualan
        // $this->potong_stok_penjualan($data);
        // $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => ['no_doc' => $no_doc], 'message' => 'Transaksi tersimpan dengan nomor ' . $no_doc . '.');
        }
    }
    public function proses($input)
    {

        # Cek limit piutang member
        // if (!check_arr_empty_or_null($input['cust_id']) && $input['tipe_pembayaran'] == 'TEMPO') {
        //     # Ambil limit
        //     $limit = get_row_values("limit_piutang", "m_customer", "cust_id", $input['cust_id']);
        //     # Ambil transaksi tempo belum terbayar
        //     $jumlah_tempo = get_row_values("COALESCE(SUM(grand_total), 0) AS grand_total", "t_sales_hdr", ["cust_id", "type_payment", "status_payment"], [$input['cust_id'], 'TEMPO', 0]);
        //     # Totalkan jumlah tempo dan transksi yang akan datang
        //     $total_piutang = unformat_numeric($input['total']) + $jumlah_tempo['grand_total'];
        //     # Jika $total_piutang melebihi piutang maka batalkan
        //     if ($total_piutang >= $limit['limit_piutang']) {
        //         return array('result' => false, 'data' => null, 'message' => 'Transaksi yang akan dilakukan melebihi nilai limit piutang (' . number_format($total_piutang, 0, ",", ".") . '/' . number_format($limit['limit_piutang'], 0, ",", ".") . ').');
        //     }
        // }

        #Check status approval role
        $this->db->select('code_jabatan,status');
        $this->db->from('t_pettycash_a');
        $this->db->where('no_doc',$input['no_doc']);
        $this->db->where('status',1);
        $this->db->order_by('creation_date','DESC');
        $this->db->limit(1);
        $rs_approval = $this->db->get()->result_array();
        $data = [
            'periode_awal' => date_convert_format($input['periode_awal']),
            'periode_akhir' => date_convert_format($input['periode_akhir']),
            'm_code' => $input['m_code'],
            'pic' => $input['m_pic'],
            'code_company' => $input['m_type'],
            'pettycash_awal' => $input['pettycash_store'],
            'pettycash_expense' => unformat_numeric($input['total_expense']),
            'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
            'status' => 1,
            'email' => $input['email'],
            'modified_by' => $this->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
        ];
        // if(count($rs_approval) > 0){
        //     foreach($rs_approval as $rs){
        //         $data = [
        //             'periode_awal' => date_convert_format($input['periode_awal']),
        //             'periode_akhir' => date_convert_format($input['periode_akhir']),
        //             'm_code' => $input['m_code'],
        //             'pic' => $input['m_pic'],
        //             'code_company' => $input['m_type'],
        //             'pettycash_awal' => $input['pettycash_store'],
        //             'pettycash_expense' => unformat_numeric($input['total_expense']),
        //             'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
        //             'status' => 1,
        //             'email' => $input['email'],
        //             'modified_by' => $this->session->userdata('user_id'),
        //             'modification_date' => date('Y-m-d H:i:s'),
        //         ];
        //         // if($rs['code_jabatan'] == "BM"){

        //             // $data = [
        //             //     'periode_awal' => date_convert_format($input['periode_awal']),
        //             //     'periode_akhir' => date_convert_format($input['periode_akhir']),
        //             //     'm_code' => $input['m_code'],
        //             //     'pic' => $input['m_pic'],
        //             //     'code_company' => $input['m_type'],
        //             //     'pettycash_awal' => $input['pettycash_store'],
        //             //     'pettycash_expense' => unformat_numeric($input['total_expense']),
        //             //     'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
        //             //     'status' => 2,
        //             //     'email' => $input['email'],
        //             //     'modified_by' => $this->session->userdata('user_id'),
        //             //     'modification_date' => date('Y-m-d H:i:s'),
        //             // ];
        //         // }
        //         // else if($rs['code_jabatan'] == "HOD"){

        //         //     $data = [
        //         //         'periode_awal' => date_convert_format($input['periode_awal']),
        //         //         'periode_akhir' => date_convert_format($input['periode_akhir']),
        //         //         'm_code' => $input['m_code'],
        //         //         'pic' => $input['m_pic'],
        //         //         'code_company' => $input['m_type'],
        //         //         'pettycash_awal' => $input['pettycash_store'],
        //         //         'pettycash_expense' => unformat_numeric($input['total_expense']),
        //         //         'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
        //         //         'status' => 3,
        //         //         'email' => $input['email'],
        //         //         'modified_by' => $this->session->userdata('user_id'),
        //         //         'modification_date' => date('Y-m-d H:i:s'),
        //         //     ];
        //         // }
        //         // else if($rs['code_jabatan'] == "FIN"){

        //         //     $data = [
        //         //         'periode_awal' => date_convert_format($input['periode_awal']),
        //         //         'periode_akhir' => date_convert_format($input['periode_akhir']),
        //         //         'm_code' => $input['m_code'],
        //         //         'pic' => $input['m_pic'],
        //         //         'code_company' => $input['m_type'],
        //         //         'pettycash_awal' => $input['pettycash_store'],
        //         //         'pettycash_expense' => unformat_numeric($input['total_expense']),
        //         //         'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
        //         //         'status' => 4,
        //         //         'email' => $input['email'],
        //         //         'modified_by' => $this->session->userdata('user_id'),
        //         //         'modification_date' => date('Y-m-d H:i:s'),
        //         //     ];
        //         // }
                
        //     }
        // }
        // else {
        //     $data = [
        //         'periode_awal' => date_convert_format($input['periode_awal']),
        //         'periode_akhir' => date_convert_format($input['periode_akhir']),
        //         'm_code' => $input['m_code'],
        //         'pic' => $input['m_pic'],
        //         'code_company' => $input['m_type'],
        //         'pettycash_awal' => $input['pettycash_store'],
        //         'pettycash_expense' => unformat_numeric($input['total_expense']),
        //         'pettycash_akhir' => unformat_numeric($input['sisa_pettycash']),
        //         'status' => 1,
        //         'email' => $input['email'],
        //         'modified_by' => $this->session->userdata('user_id'),
        //         'modification_date' => date('Y-m-d H:i:s'),
        //     ];
        // }

        // end check status approval
        # Mulai begin trans
        $this->db->trans_begin();

        # Generate nomor dokumen baru
        $no_doc = $input['no_doc'];

        # Transaction step 1
        

        $this->db->where('no_doc', $no_doc);
        $this->db->update('t_pettycash_h', $data);
        $err_db = $this->db->error(); # Tangkap error

        # Transaction step 2
        if (empty($err_db['message'])) {
            $detail = json_decode($input['detail'], true);
            # Looping insert ke tabel detail
            $this->db->where('no_doc',$no_doc);
            $this->db->delete('t_pettycash_d');
            
            for($i=0; $i<count($detail); $i++){
                $data_detail[] = [
                                    'no_doc' => $no_doc,
                                    'code_coa' => $detail[$i]['code_coa'],
                                    'tanggal' => date('Y-m-d', strtotime($detail[$i]['tanggal'])),
                                    'nominal' => $detail[$i]['nominal'],
                                    'remark' => $detail[$i]['remark'],
                ];
            }
            $this->db->insert_batch('t_pettycash_d',$data_detail);
            $err_db = $this->db->error(); # Tangkap error
            // foreach ($detail as $key => $value) {
            //     if (empty($err_db['message'])):
            //         if (check_arr_empty_or_null($value['code_coa']) || $value['nominal'] <= 0) {
            //             continue;
            //         }

            //         $this->db->where('no_doc',$no_doc);
            //         $this->db->where('code_coa',$value['code_coa']);
            //         $this->db->update(
            //             't_pettycash_d',
            //             [
            //                 'no_doc' => $no_doc,
            //                 'code_coa' => $value['code_coa'],
            //                 'tanggal' => date('Y-m-d', strtotime($value['tanggal'])),
            //                 'nominal' => $value['nominal'],
            //                 'remark' => $value['remark'],
            //             ]
            //         );
            //         $err_db = $this->db->error(); # Tangkap error
            //     else:
            //         break;
            //     endif;
            // }
        }

        # Transaction step 3, Potong stok penjualan
        // $this->potong_stok_penjualan($data);
        // $err_db = $this->db->error(); # Tangkap error

        # Jika terdapat masalah pada proses CRUD
        if ($this->db->trans_status() === false) {
            # Membatalkan semua perubahan
            $this->db->trans_rollback();
        } else {
            # Commit/Simpan semua perubahan
            $this->db->trans_commit();
        }

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal simpan. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => ['no_doc' => $no_doc], 'message' => 'Transaksi tersimpan dengan nomor ' . $no_doc . '.');
        }
    }

    public function get_h_doc_invoice($no_doc){
        $this->db->select('t_pettycash_h.*,m_perusahaan.name,m_store.m_shortdesc,m_store.m_odesc');
        $this->db->from('t_pettycash_h');
        $this->db->join('m_perusahaan','m_perusahaan.m_type = t_pettycash_h.code_company');
        $this->db->join('m_store','m_store.m_code = t_pettycash_h.m_code');
        $this->db->where('t_pettycash_h.no_doc',$no_doc);
        return $this->db->get()->result_array();
    }

    public function get_d_doc_invoice($no_doc){
        $this->db->select("a.no_doc, a.tanggal, a.code_coa, b.name_coa, a.nominal, a.remark");
        $this->db->from('t_pettycash_d a');
        $this->db->join('m_coa b', 'b.code_coa=a.code_coa', 'left');
        $this->db->where('a.no_doc',$no_doc);
        return $this->db->get()->result_array();
    }
    public function get_a_doc_invoice($no_doc){
        // $this->db->select("b.name_employee,a.code_jabatan,d.name_jabatan");
        // $this->db->from('t_pettycash_a a');
        // $this->db->join('m_employee b', 'b.email=a.created_by', 'left');
        // $this->db->join('m_jabatan d', 'd.code_jabatan=a.code_jabatan', 'left');
        // $this->db->where('a.no_doc',$no_doc);
        // $this->db->where('a.status',1);
        // $this->db->group_by("b.name_employee, a.code_jabatan, d.name_jabatan");
        $query = $this->db->query("select name_employee, code_jabatan, name_jabatan, creation_date from 
		(select b.name_employee as name_employee,a.code_jabatan as code_jabatan,d.name_jabatan as name_jabatan, a.creation_date as creation_date
        from t_pettycash_a a
        join m_employee b on b.email=a.created_by
        join  m_jabatan d on d.code_jabatan=a.code_jabatan
        where a.no_doc = '$no_doc'
        and a.status=1
		group by b.name_employee, a.code_jabatan, d.name_jabatan, a.creation_date) x order by creation_date");

        $result = $query->result_array();
        return $result;
        
        // return $this->db->get()->result_array();

    }


    public function delete($id)
    {
        // delete
        $this->db->where('no_doc',$id);
        $this->db->delete(array('t_pettycash_h','t_pettycash_d','t_pettycash_a'));
        return $this->db->affected_rows();
    }

    public function confirmapp($id)
    {
        // confirm
        $this->db->where('no_doc',$id);
        $this->db->update(
            't_pettycash_h',
        [
            'status' => 6
        ]
        );
        return $this->db->affected_rows();
    }

    public function getData2Edit($id)
    {
        $this->db->select("a.no_doc, a.periode_awal, a.periode_akhir, a.m_code, b.m_odesc, a.pic, 
                            a.code_company, c.name, a.pettycash_awal, a.pettycash_akhir, a.status");
        $this->db->from("t_pettycash_h a");
        $this->db->join("m_store b", "b.m_code=a.m_code", "left");
        $this->db->join("m_perusahaan c", "c.m_type=a.code_company", "left");
        $this->db->where("a.no_doc", $id);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]), 'detail' => $this->detail($id));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row_array(), 'detail' => $this->detail($id));
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null, 'detail' => $this->detail($id));
        }
    }

    public function getList($filter)
    {
     
        $AP = array(3,6);
        $FM = array(7);
        $this->db->select("m_code");
        $this->db->from("m_employee_store");
        $this->db->where("nik", $this->session->userdata('nik'));
        $Rol = $this->session->userdata('role_id');

        $res_mcode = $this->db->get()->result_array();

        $res_mcode_new = [];
        foreach ($res_mcode as $rs) {
            $res_mcode_new[] = $rs['m_code'];
        }
        
        // if($this->session->userdata('role_id') != "STAFF"){
        if($this->session->userdata('code_jabatan') != "PO001" && $this->session->userdata('code_jabatan') != "PO002" && $this->session->userdata('code_jabatan') != "PO039") {
            $this->datatables->select("a.no_doc, a.periode_awal, a.periode_akhir, b.m_shortdesc, b.m_odesc, a.pic, 
                                        a.code_company, c.name, a.pettycash_awal, a.pettycash_expense, a.pettycash_akhir, a.status");
            $this->datatables->from("t_pettycash_h a");
            $this->datatables->join("m_store b", "b.m_code=a.m_code", "left");
            $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company", "left");
            if ($Rol == 'STORE') {
                $this->datatables->where_in("a.m_code", $res_mcode_new);
            }  
        } else {
            $this->datatables->select("a.no_doc, a.periode_awal, a.periode_akhir, b.m_shortdesc, b.m_odesc, a.pic, 
                                        a.code_company, c.name, a.pettycash_awal, a.pettycash_expense, a.pettycash_akhir, a.status");
            $this->datatables->from("t_pettycash_h a");
            $this->datatables->join("m_store b", "b.m_code=a.m_code", "left");
            $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company", "left");
            if ($this->session->userdata('code_jabatan') != "PO039") {
                $this->datatables->where_in("a.status", $AP);
            } else {
                $this->datatables->where_in("a.status", $FM);
            }            
        }
        // }
        // else {
        //     $this->datatables->select("a.no_doc, a.periode_awal, a.periode_akhir, b.m_shortdesc, b.m_odesc, a.pic, 
        //                                 a.code_company, c.name, a.pettycash_awal, a.pettycash_expense, a.pettycash_akhir, a.status");
        //     $this->datatables->from("t_pettycash_h a");
        //     $this->datatables->join("m_store b", "b.m_code=a.m_code", "left");
        //     $this->datatables->join("m_perusahaan c", "c.m_type=a.code_company", "left");
        // }

        
        // if ($this->session->userdata('nik') != "11708010") {
        //     foreach ($filter as $key => $val) {
        //         if (trim($val) != "" || !empty($val) || $val != null) {
        //             $this->datatables->where($key, $val);
        //         }
        //     }
        // }
        $result = $this->datatables->generate();
        $newrs = json_decode($result);
        for($i=0; $i<count($newrs->data); $i++){
            $newrs->data[$i]->code_jabatan =  $this->session->userdata('code_jabatan');
        }
        

        return json_encode($newrs);
    } 

    // public function getList($filter)
    // {
    //     $this->datatables->select("a.store_id, b.store_name, a.no_doc, a.tgl_trans, a.kasir, d.employee_name AS kasir_name, a.type_payment, a.cust_id, e.cust_name, a.employee_id, f.employee_name,
    //                                 a.total_qty, a.total_gross, a.total_price_point, a.total_potongan, a.ppn, a.ppn_pcn, a.ppn_value, a.grand_total,
    //                                 a.cash, a.credit, a.debit, a.transfer, a.digital_wallet, a.kembali, a.credit_no, a.debit_no, a.digital_name,
    //                                 a.status_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
    //                                 a.coa_credit, get_coa_name(a.coa_credit) AS coa_credit_name, a.coa_debit, get_coa_name(a.coa_debit) AS coa_debit_name");
    //     $this->datatables->from("t_sales_hdr a");
    //     $this->datatables->join("m_store b", "b.store_id=a.store_id", "left");
    //     $this->datatables->join("m_users c", "c.user_name=a.kasir", "left");
    //     $this->datatables->join("m_employee d", "d.employee_id=c.employee_id", "left");
    //     $this->datatables->join("m_customer e", "e.cust_id=a.cust_id", "left");
    //     $this->datatables->join("m_employee f", "f.employee_id=a.employee_id", "left");
    //     foreach ($filter as $key => $val) {
    //         if (trim($val) != "" || !empty($val) || $val != null) {
    //             $this->datatables->where($key, $val);
    //         }
    //     }
    //     $result = $this->datatables->generate();

    //     return $result;
    // }

    public function getDataList($filter)
    {
        $this->db->select("a.store_id, b.store_name, a.no_doc, a.tgl_trans, a.kasir, d.employee_name AS kasir_name, a.type_payment, a.cust_id, e.cust_name, e.alamat, a.employee_id, f.employee_name,
                            a.total_qty, a.total_gross, a.total_price_point, a.total_potongan, a.ppn, a.ppn_pcn, a.ppn_value, a.grand_total,
                            a.cash, a.credit, a.debit, a.transfer, a.digital_wallet, a.kembali, a.credit_no, a.debit_no, a.digital_name,
                            a.status_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
                            a.coa_credit, get_coa_name(a.coa_credit) AS coa_credit_name, a.coa_debit, get_coa_name(a.coa_debit) AS coa_debit_name");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_users c", "c.user_name=a.kasir", "left");
        $this->db->join("m_employee d", "d.employee_id=c.employee_id", "left");
        $this->db->join("m_customer e", "e.cust_id=a.cust_id", "left");
        $this->db->join("m_employee f", "f.employee_id=a.employee_id", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $rs = $this->db->get();

        return $rs;
    }

    public function get_data4xls($filter)
    {
        $this->db->select("a.no_doc, a.tgl_trans, a.store_id, b.store_name, a.cust_id, e.cust_name, a.type_payment, a.tgl_jatuh_tempo, a.tgl_bayar,
                            a.total_qty, a.total_gross, a.total_potongan, a.ppn_pcn, a.grand_total,
                            CASE WHEN a.status_payment=0  THEN 'PENDING' WHEN a.status_payment=1 THEN 'LUNAS' ELSE 'BATAL' END AS status_payment
                            ");
        $this->db->from("t_sales_hdr a");
        $this->db->join("m_store b", "b.store_id=a.store_id", "left");
        $this->db->join("m_users c", "c.user_name=a.kasir", "left");
        $this->db->join("m_employee d", "d.employee_id=c.employee_id", "left");
        $this->db->join("m_customer e", "e.cust_id=a.cust_id", "left");
        $this->db->join("m_employee f", "f.employee_id=a.employee_id", "left");
        foreach ($filter as $key => $val) {
            if (trim($val) != "" || !empty($val) || $val != null) {
                $this->db->where($key, $val);
            }
        }
        $rs = $this->db->get();

        return $rs;
    }

    public function confirm($input)
    {
        // confirm
    }

    public function cancel($input)
    {
        // cancel
    }

    /*******************************************************************************
     *                              END DEFAULT FUNCTION                            *
     *******************************************************************************/

    public function detail($doc_no)
    {
        $this->db->select("a.row_no AS detail_id, a.code_coa, a.tanggal, b.name_coa, a.nominal, a.remark, 1 AS locked");
        $this->db->from("t_pettycash_d a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.no_doc", $doc_no);
        $this->db->order_by("a.tanggal ASC");
        $rs = $this->db->get();

        return $rs->result_array();
    }

    

    public function get_coa_lov($input)
    {
        $code_dept = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        // $code_dept = "RTM";
        $modul = 'Petty Cash';

        $this->datatables->select("a.code_coa, b.name_coa");
        $this->datatables->from("app_coa a");
        $this->datatables->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->datatables->where("a.status",  1);
        $this->datatables->where("a.modul", $modul);
        $this->datatables->where("a.code_dept", $code_dept);

        $result = $this->datatables->generate();

        return $result;
    }

    public function getStore()
    {
        // $nik = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        // $modul = 'Petty Cash';
        $nik = $this->session->userdata("nik");
        
        $this->datatables->select("a.m_code, a.m_shortdesc, a.m_pic, a.m_type");
        $this->datatables->from("m_store a");
        $this->datatables->join("m_employee_store b", "b.m_code=a.m_code", "left");
        $this->datatables->where("a.status",  1);
        $this->datatables->where("b.nik", $nik);

        $result = $this->datatables->generate();

        return $result;
    }

    public function getEmail()
    {
        // $nik = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        // $modul = 'Petty Cash';
        $code_jabatan = $this->session->userdata('code_jabatan');
        // ROLE SPV

      
        // BACKUP

        $this->datatables->select("b.nik, b.name_employee, b.email");
        $this->datatables->from("m_settings_approval a");
        $this->datatables->join("m_employee b","b.code_jabatan = a.to_jabatan");
        $this->datatables->where("a.code_jabatan", $code_jabatan);
        $result = $this->datatables->generate();

        return $result;
        // $setting_email = $this->db->get()->result_array();
      



        // $this->datatables->select("a.nik, a.name_employee, a.email");
        // $this->datatables->from("m_employee a");
        // $this->datatables->where("a.status",  1);
        // $this->datatables->where("a.code_jabatan", $jab);

        // $result = $this->datatables->generate();

        // return $result;
    }

    public function getEmailALL($mt_code)
    {
        $m_code = $mt_code['m_code'];
        $code_jabatan = $this->session->userdata('code_jabatan');
        
        if ($this->session->userdata('code_jabatan') != "PO001" && $this->session->userdata('code_jabatan') != "PO002") {
            $this->datatables->select("b.nik, b.name_employee, b.email,b.code_jabatan");
            $this->datatables->from("m_settings_approval a");
            $this->datatables->join("m_employee b","b.code_jabatan = a.to_jabatan");
            $this->datatables->join("m_employee_store c","c.nik = b.nik");
            $this->datatables->where("c.m_code", $m_code);
            $this->datatables->where("a.code_jabatan", $code_jabatan);
        } else {
            $this->datatables->select("nik, name_employee, email, code_jabatan");
            $this->datatables->from("m_employee");
            $this->datatables->where("nik", "11708010");
        }
        // $this->datatables->where("c.m_code", "SN2004");
        // $to = $this->db->get()->result_array();
        return $this->datatables->generate();

        // $this->db->select("b.nik, b.name_employee, b.email,b.code_jabatan");
        // $this->db->from("m_settings_approval a");
        // $this->db->join("m_employee b","b.code_jabatan = a.code_jabatan");
        // $this->db->where("a.to_jabatan", $code_jabatan);
        // $before = $this->db->get()->result_array();

        // $result = array_merge($to, $before);
        // $result = $this->datatables->generate();

        // $datatable = [
        //     "draw"=>$this->input->post('draw'),
        //     "recordsTotal"=> count($result),
        //     "recordsFiltered"=> count($result),
        //     "data"  => $result
        // ];
        // return json_encode($datatable);
        
        // $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
        // $this->datatables->from("m_employee a");
        // $this->datatables->where("a.status",  1);
        // $this->datatables->where_in("a.code_jabatan", ["BM","ABM","HOD","ARM"]);

        // $result = $this->datatables->generate();

        // return $result;
    }

    public function get_date_last_pt($m_code){

        if($m_code !=""){
            $this->db->select("periode_awal,periode_akhir");
            // $this->db->select("max(periode_akhir) as date");
            $this->db->from("t_pettycash_h");
            $this->db->where("m_code",$m_code);
            return $this->db->get()->result_array();
        }
        else {

            $nik  = $this->session->userdata('nik');

            $this->db->select("m_code");
            $this->db->from("m_employee_store");
            $this->db->where("nik",$nik);
            $get_m_code = $this->db->get()->result_array();
            if($get_m_code > 0){
                // $this->db->select("max(periode_akhir) as date");
                $this->db->select("periode_awal,periode_akhir");
                $this->db->from("t_pettycash_h");
                $this->db->where("m_code",$get_m_code[0]['m_code']);
                return $this->db->get()->result_array();
            }
            else {
                return [];
            }
        }

    }
    public function getEmailReasign()
    {
        // $m_code = $m_code['m_code'];
        $code_jabatan = $this->session->userdata('code_jabatan');
        $this->db->select("b.code_dept");
        $this->db->from("m_employee a");
        $this->db->join("m_jabatan b","b.code_jabatan = a.code_jabatan");
        $this->datatables->join("m_employee_store c","c.nik = a.nik");
        $this->db->where("a.code_jabatan", $code_jabatan);
        // $this->datatables->where("c.m_code", $m_code);
        $code_dept = $this->db->get()->result_array();

        $this->datatables->select("a.nik, a.name_employee, a.email,a.code_jabatan");
        $this->datatables->from("m_employee a");
        // $to = $this->db->get()->result_array();
        return $this->datatables->generate();


        // $this->datatables->select("b.nik, b.name_employee, b.email,b.code_jabatan");
        // $this->datatables->from("m_settings_approval a");
        // $this->datatables->join("m_employee b","b.code_jabatan = a.to_jabatan");
        // $this->datatables->where("a.code_jabatan", $code_jabatan);
        // // $to = $this->db->get()->result_array();
        // return $this->datatables->generate();

    }

    public function get_coa($input)
    {
        # Ambil Dept
        $code_dept = $this->db->select("code_dept")->from("m_jabatan")->where("code_jabatan", $this->session->userdata("code_jabatan"))->get()->row()->code_dept;
        $modul = 'Petty Cash';

        $this->db->select("a.code_coa, b.name_coa");
        $this->db->from("app_coa a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.status",  1);
        $this->db->where("a.modul", $modul);
        $this->db->where("a.code_dept", $code_dept);
        $this->db->group_start();
        $this->db->where("a.code_coa", $input['code_coa']);
        $this->db->group_end();

        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }    

    public function get_transaksi_lov()
    {
        $this->datatables->select("CONCAT(a.store_id, ' - ', c.store_name) AS store_name, a.no_doc, a.tgl_trans, CONCAT(a.cust_id, ' - ', b.cust_name) AS cust_name, a.type_payment, a.grand_total, a.ppn_pcn, a.ppn_value");
        $this->datatables->from("t_sales_hdr a");
        $this->datatables->join("m_customer b", "b.cust_id=a.cust_id", "left");
        $this->datatables->join("m_store c", "c.store_id=a.store_id", "left");
        $this->datatables->where("a.status_payment", 1);
        $result = $this->datatables->generate();

        return $result;
    }

    public function get_transaksi_detail($doc_no)
    {
        $this->db->select("a.code_coa, a.tanggal, b.name_coa, a.nominal, a.remark");
        $this->db->from("t_pettycash_d a");
        $this->db->join("m_coa b", "b.code_coa=a.code_coa", "left");
        $this->db->where("a.no_doc", $doc_no);
        $rs = $this->db->get();
        $err_db = $this->db->error(); # Tangkap error

        if (!empty($err_db['message'])) {
            $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
            return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } elseif ($rs->row() != null) {
            return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->result_array());
        } else {
            return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
        }
    }

    public function check_no_doc_pettycash($id){
        $query = $this->db->query("SELECT * FROM t_pettycash_h WHERE no_doc = '$id'");
        return $query->num_rows();
    }
    public function get_pettycash_h($id){
        $query = $this->db->query("SELECT * FROM t_pettycash_h WHERE no_doc = '$id'");
        return $query->result_array();
    }
    public function get_pettycash_h_bridge($id){
        $query = $this->db->query("SELECT a.no_doc,a.creation_date,a.periode_awal,a.periode_akhir,a.code_company,a.pettycash_awal,a.pettycash_expense,b.m_shortdesc FROM t_pettycash_h as a JOIN m_store as b ON b.m_code = a.m_code WHERE a.no_doc = '$id'");
        return $query->result_array();
    }

    public function get_pettycash_d_bridge($id){

        $this->db->select("t_pettycash_d.no_doc,t_pettycash_d.tanggal,t_pettycash_d.code_coa,m_coa.name_coa,t_pettycash_d.nominal,t_pettycash_d.remark");
        $this->db->from("t_pettycash_d");
        $this->db->join("m_coa", "m_coa.code_coa=t_pettycash_d.code_coa", "left");
        $this->db->where("t_pettycash_d.no_doc", $id);
        $rs = $this->db->get();
        return $rs->result_array();

       
    }
    public function get_pettycash_spv_email($id){
        $query = $this->db->query("SELECT m_employee.email FROM t_pettycash_h JOIN app_users ON app_users.user_id = t_pettycash_h.created_by JOIN m_employee ON m_employee.nik = app_users.nik WHERE no_doc = '$id'");
        return $query->result_array();
    }
    public function get_pettycash_d($id){

        $this->db->select("t_pettycash_d.no_doc,t_pettycash_d.tanggal,t_pettycash_d.code_coa,m_coa.name_coa,t_pettycash_d.nominal,t_pettycash_d.remark");
        $this->db->from("t_pettycash_d");
        $this->db->join("m_coa", "m_coa.code_coa=t_pettycash_d.code_coa", "left");
        $this->db->where("t_pettycash_d.no_doc", $id);
        $rs = $this->db->get();
        return $rs->result_array();

       
    }
    public function get_pettycash_a($id){

        $this->db->select("a.email,a.code_jabatan, a.comment, a.status, a.creation_date as tanggal, c.name_employee");
        $this->db->from("t_pettycash_a a");
        $this->db->join("app_users b", "b.user_id=a.created_by");
        $this->db->join("m_employee c","c.nik=b.nik");
        $this->db->where("a.no_doc", $id);
        $this->db->order_by('a.creation_date DESC');
        $rs = $this->db->get();
        // dd($this->db->last_query());
        return $rs->result_array();

       
    }

    public function get_dm_data(){
        $this->db->select('email');
        $this->db->from('m_employee');
        $this->db->where('code_jabatan',"PO036");
        return $this->db->get()->result_array();
    }

    public function insert_bridge($no_doc,$data,$data_approve){
       
         # Mulai begin trans
        //  $this->db->trans_begin();
        
        $this->db->where('no_doc',$no_doc);
        $this->db->set('status',6);
        $this->db->update('t_pettycash_h');

        $this->db->where('no_doc',$no_doc);
        $this->db->insert('t_pettycash_a',$data_approve);

        for($i=0; $i<count($data);$i++){
            $this->db->insert('t_bridging',$data[$i]);
        }
        // if ($this->db->trans_status() === false) {
        //     # Membatalkan semua perubahan
        //     $this->db->trans_rollback();
        //     return $this->db->error();
        // } else {
        //     # Commit/Simpan semua perubahan
        //     $this->db->trans_commit();
        // }
        return $this->db->affected_rows();
    }
    public function approval_pettycash($data,$data_bridge=[]){

        if($data['code_jabatan'] == "PO020" || $data['code_jabatan'] == "PO012"){
            $this->db->insert('t_pettycash_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',2);
                $this->db->update('t_pettycash_h');
                return $this->db->affected_rows();
            }
        }
        else if($data['code_jabatan'] == "PO048"){
            $this->db->insert('t_pettycash_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',3);
                $this->db->update('t_pettycash_h');
                return $this->db->affected_rows();
            }
        }
        else if($data['code_jabatan'] == "PO003" || $data['code_jabatan'] == "PO133"){
            $this->db->insert('t_pettycash_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',4);
                $this->db->update('t_pettycash_h');
            }
           
            return $this->db->affected_rows();
        }
        else if($data['code_jabatan'] == "PO002" || $data['code_jabatan'] == "PO001"){
            $this->db->insert('t_pettycash_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',7);
                $this->db->update('t_pettycash_h');
            }
           
            return $this->db->affected_rows();
        }
        else if($data['code_jabatan'] == "PO039"){
            $this->db->insert('t_pettycash_a', $data);
            if($this->db->affected_rows() > 0){
                $this->db->where('no_doc',$data['no_doc']);
                $this->db->set('status',6);
                $this->db->update('t_pettycash_h');
            }
           
            return $this->db->affected_rows();
        }
    }
    public function reasign_pettycash($data){

        $this->db->insert('t_pettycash_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',5);
            $this->db->update('t_pettycash_h');
            return $this->db->affected_rows();
        }
    }
    public function revisi_pettycash($data){
        $this->db->insert('t_pettycash_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',10);
            $this->db->update('t_pettycash_h');
            return $this->db->affected_rows();
        }
    }

    public function reject_pettycash($data){
        $this->db->insert('t_pettycash_a', $data);
        if($this->db->affected_rows() > 0){
            $this->db->where('no_doc',$data['no_doc']);
            $this->db->set('status',9);
            $this->db->update('t_pettycash_h');
            return $this->db->affected_rows();
        }
    }
}
