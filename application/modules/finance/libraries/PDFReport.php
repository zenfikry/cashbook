<?php

require_once APPPATH . '/third_party/tcpdf/tcpdf.php';

class PDFReport extends TCPDF
{

    public $RepFuncHeader;
    public $dataHeader;
    public $RepFuncFooter;
    public $dataFooter;

    public function __construct()
    {
        // parent::__construct();
    }

    public function _construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }

    /*******************************************************************************
     *                               START HEADER                                   *
     *******************************************************************************/

    public function Header()
    {
        $this->SetY(3); // agar header tidak terlalu rapat dengan ujung kertas

        // To be implemented in your own inherited class
        if ($this->RepFuncHeader != "") {
            call_user_func(array($this, $this->RepFuncHeader), $this->dataHeader); # Call function with parameters
        } else {
            call_user_func(array($this, 'DefaultHeader'), $this->dataHeader); # Call function with parameters
        }
    }

    public function DefaultHeader($data)
    {
        // x
    }

    public function KopSurat()
    {
        // Cetak kop surat dari general_helper
        pdfKopSurat($this);
    }

    public function Bank($data)
    {
        $this->SetFont('helvetica', '8', 14);
        $this->Cell(0, 5, 'Daftar Bank', 0, 0, 'L');
        $this->Ln(20);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(50, 5, " ID Bank", 1, 0, 'L');
        $this->Cell(100, 5, " Nama Bank", 1, 0, 'L');
        $this->Cell(25, 5, " Status", 1, 0, 'L');
        $this->Ln(5);
    }

    public function Edc($data)
    {
        $this->SetFont('helvetica', '8', 14);
        $this->Cell(0, 5, 'Daftar EDC Toko', 0, 0, 'L');
        $this->Ln(20);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(50, 5, " Nama Toko", 1, 0, 'L');
        $this->Cell(100, 5, " Nama Bank", 1, 0, 'L');
        $this->Cell(100, 5, " TID", 1, 0, 'L');
        $this->Cell(100, 5, " MID", 1, 0, 'L');
        $this->Cell(100, 5, " Jaringan", 1, 0, 'L');
        $this->Cell(30, 5, " Qty", 1, 0, 'L');
        $this->Cell(25, 5, " Status", 1, 0, 'L');
        $this->Ln(5);
    }

    public function Cardfee($data)
    {
        $this->SetFont('helvetica', '8', 14);
        $this->Cell(0, 5, 'Daftar Cardfee', 0, 0, 'L');
        $this->Ln(20);

        $this->SetFont('helvetica', 'B', 8);
        $this->Cell(50, 5, " Cardfee Code", 1, 0, 'L');
        $this->Cell(100, 5, " Cardfee Name", 1, 0, 'L');
        $this->Cell(50, 5, " Persen", 1, 0, 'L');
        $this->Cell(25, 5, " Status", 1, 0, 'L');
        $this->Ln(5);
    }

    /*******************************************************************************
     *                               END HEADER                                     *
     *******************************************************************************/

    /*******************************************************************************
     *                               START FOOTER                                   *
     *******************************************************************************/

    public function Footer()
    {
        // To be implemented in your own inherited class
        if ($this->RepFuncFooter != "") {
            call_user_func(array($this, $this->RepFuncFooter), $this->dataFooter); # Call function with parameters
        } else {
            call_user_func(array($this, 'DefaultFooter'), $this->dataFooter); # Call function with parameters
        }
    }

    public function DefaultFooter($data)
    {
        $this->SetY(-15);
        // helvetica italic 8
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $width = $this->w / 3;
        #$this->Cell($width,10,'Waktu Cetak : '.date("d M Y H:i:s"),0,0,'L');
        $this->Cell($width, 10, 'Waktu Cetak : ' . date("d M Y H:i:s") . ', Cetak Oleh : ' . $data['printed_by'], 0, 0, 'L');
        $this->Cell(0, 10, 'Halaman ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'R');
    }

    /*******************************************************************************
 *                               END FOOTER                                     *
 *******************************************************************************/
}
