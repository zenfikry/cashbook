<style type="text/css">
    .disable {
        background: #DDD !Important;
    }

    #po-form-selected {
        display: none;
    }
</style>
<div class="main">
    <div class="row margin-bottom-15 padding-left-10">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali" onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
                Back </button>
            <div class="pull-right">
                <button type="button" class="btn btn-success waves-effect waves-light" id="simpan" name="simpan" onclick="Simpan();"> <span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span>
                    Save and Send </button>
            </div>
        </div>
    </div>
    <div class="row padding-horizontal-10">
        <div class="col-md-12 mt-3">
            <form id="form_input" name="form_input">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">PI No. *</label>
                                <input class="form-control disable" placeholder="PI Number" id="inv_no" name="inv_no" value="" readonly />
                            </div>
</div>
<div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Creditor <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control font-size-sm" placeholders="Code Creditor" name="code_creditor" id="code_creditor" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCreditor();"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Creditor Description</label>

                                <input type="text" class="form-control font-size-sm" placeholder="Creditor Description" id="desc_creditor" name="desc_creditor" value="" readonly="readonly" />


                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Company <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="company_code" id="company_code" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Select Company"
                                            name="company_name" id="company_name" value="" readonly="readonly" />
                                        <input type="hidden" name="categories" id="categories" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVCompany();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                                        <input type="hidden" name="name_employee" id="name_employee" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Choose Email" name="email"
                                            id="email" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVEmail();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Invoice Date <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Invoice Date" id="inv_date" name="inv_date" value="<?= date('Y-m-d') ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Due Date <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Due Date" id="due_date" name="due_date" value="<?= date('Y-m-d') ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Invoice Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="inv_desc" id="inv_desc" placeholder="Invoice Description" value="">
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Supplier IV No. <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="supplier_iv" id="supplier_iv" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Ref No. 2 <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="ref_no" id="ref_no" value="" maxlength="20">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Expense Period <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="expense_period" id="expense_period" value="">
                                </div>
                            </div>
                            
                        </div> -->
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" class="form-control font-size-sm" placeholder="Description" id="description" name="description" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <h4>Attachment (*pdf)</h4>
                                <div class="row padding-horizontal-10 mt-3">
                                    <div class="form-group col-md-12" id="">
                                        <label class="control-label">Attachment <span class="text-danger">*</span></label><br>
                                        <input type="file" name="file_expense" id="file_expense">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <h4>Detail Invoice (*.xls)</h4>
                                <div class="row padding-horizontal-10 mt-3">
                                    <div class="form-group col-md-12">
                                        <label>Detail Invoice <span class="text-danger">*</span></label><br>
                                        <input type="file" name="import" id="import">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    // START VARIABEL WAJIB
    var Modules = '<?= $modules ?>';
    var Controller = '<?= $controller ?>';
    var Priv = JSON.parse('<?= json_encode($priv_arr) ?>');
    var data2Send = null;
    var action = '<?= $action ?>';
    var _id = "<?= $action == 'edit' ? $id : 'null' ?>";
    // END VARIABEL WAJIB

    var _dataTable = null;
    var DataTableAction = 'create';
    var DataTableRowIdx = 0;
    var data4DataTable = [];

    var _dataRiwayatHarga = null;
    var data4DataTableRiwayatHarga = [];

    var selectedKaryawanId = null;
    var selectedCustomerId = null;
    var selectedCOACodeDebit = null;
    var selectedCOACodeCredit = null;
    var flagTerapkanPromoDiskon = false;
    var flagItemEndRow = false;

    LobiAdmin.loadScript([
        '<?= base_url() ?>assets/js/lib/autoNumeric/autoNumeric.js',
        '<?= base_url() ?>assets/js/lib/accounting/accounting.min.js',
    ], function() {
        LobiAdmin.loadScript([
            '<?= base_url() ?>assets/js/lib/autoNumeric/init.js',
            '<?= base_url() ?>assets/js/lib/accounting/init.js',
        ], function() {
            LobiAdmin.loadScript([
                '<?= base_url() ?>assets/js/modules/' + Modules + '/' + Controller +
                '.form.js?v=<?= date('YmdHis') . rand() ?>',
            ], function() {
                initPage();
            });
        });
    });
</script>