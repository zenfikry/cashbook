<style type="text/css">
   .disable {
      background: #DDD !Important;
   }

   #po-form-selected {
      display: none;
   }
</style>
<div class="main">
   <div class="row margin-bottom-15 padding-left-10">
      <div class="col-md-12">
         <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali" onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span> Back </button>
         <div class="pull-right">
         </div>
      </div>
   </div>
   <div class="row padding-horizontal-10">
      <div class="col-md-12 mt-3">
         <div class="card">
            <div class="card-body">
               <form id="form_input" name="form_input">
                  <div class="row">
                     <div class="form-group col-md-3">
                        <label class="control-label">Doc No. *</label>
                        <input class="form-control disable" placeholder="No. Document" id="no_doc" name="no_doc" value="<?= $no_doc;?>" readonly />
                     </div>
                     <div class="form-group col-md-3" id="doc_date_container">
                        <label class="control-label">Period <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Periode" id="periode_awal" name="periode_awal" value="<?=date('Y-m-d')?>" disabled>
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>                        
                     </div>
                     <div class="form-group col-md-3" id="doc_date_container">
                        <label class="control-label">up to <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Periode" id="periode_akhir" name="periode_akhir" value="<?=date('Y-m-d')?>" disabled>
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <div class="form-group">
                           <label>Company <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="company_code" id="company_code" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Select Company" name="company_name" id="company_name" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCompany();" disabled><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>    
                  <div class="row">    
                  <div class="form-group col-md-3 " style="display:none;">
                        <div class="form-group" style="display:none;">
                           <label>Employee <span class="text-danger">*</span></label>
                           <div class="input-group">
                           <input type="hidden" name="code_employee" id="code_employee" value="" readonly="readonly" />
                           <input type="text" class="form-control" placeholder="Select Employee" name="name_employee"
                              id="name_employee" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmployee();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>          
                     
                     <div class="form-group col-md-3" style="display:none;">
                        <label class="control-label">Department</label>
                        <input type="hidden" class="form-control" placeholder="Dapartement" name="code_dept" id="code_dept" value="" readonly="readonly" />
                        <input type="text" class="form-control" placeholder="Dapartement" name="dapartement" id="dapartement" value="" readonly="readonly" />
                     </div>
                     <div class="form-group col-md-3" style="display:none;">
                        <label class="control-label">Position <span class="text-danger">*</span></label>
                           <input type="hidden" class="form-control" placeholder="Position" id="code_jabatan" name="code_jabatan" value="" readonly="readonly">
                           <input type="text" class="form-control" placeholder="Position" id="position" name="position" value="" readonly="readonly">
                     </div> 
                     <div class="form-group col-md-3">
                        <div class="form-group">
                           <label>Email <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                              <input type="hidden" name="name_employee" id="name_employee" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Choose Email" name="email" id="email" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmail();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <h4>Transfer To</h4>
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Select Bank <span class="text-danger">*</span></label>
                        <select id="bank" name="bank" class="form-control" disabled>
                           <option>Select Bank</option>
                           <option value="Mandiri">Mandiri</option>
                           <option value="Mandiri Syariah">Mandiri Syariah</option>
                           <option value="BCA">BCA</option>
                           <option value="BRI">BRI</option>
                           <option value="Danamon">Danamon</option>
                        </select>
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Account Number <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Account Number" class="form-control" name="account_number" id="account_number" disabled>
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Account Name <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Account Name" class="form-control" name="account_name" id="account_name" disabled>
                     </div>
                  </div>
                  <h4>Proof Documents</h4>
                  <div class="row padding-horizontal-10 mt-3">
                     <!-- <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Expanse <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_expense" id="file_expense">
                     </div> -->
                     <div class="form-group col-md-3" id="">
                        <label class="control-label">Proof Expanse uploaded <span class="text-danger">*</span></label>
                        <div id="file_uploaded"></div>
                     </div>
                     <!-- <div class="form-group col-md-3" id="">
                        <label class="control-label">Status <span class="text-danger">*</span></label>
                        <span class="badge badge-warning">Open</span>
                     </div> -->
                  </div>
                  <div class="row padding-horizontal-10 mt-3 d-flex align-items-center">
                     <!-- <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Expanse <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_expense" id="file_expense">
                     </div> -->
                     <div class="form-group col-md-3" id="">
                       <textarea name="comment" id="comment" class="form-control" placeholder="Comment"></textarea>
                     </div>
                     <?php if($claimexp_h[0]['status'] != 6 || $claimexp_h[0]['status'] != 9 ):?>
                     <div class="form-group col-md-3" id="">
                        <!-- <button class="btn btn-success" id="btn-approve">Approve</button> -->
                        <button class="btn btn-warning" id="btn-reasign">Revision</button>
                        <button class="btn btn-danger" id="btn-reject">Reject</button>
                     </div>
                     <?php endif; ?>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <form class="form" id="form_summary" name="form_summary">
                  <div class="row">
                     <div class="col-md-12">
                        <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>No</th> <!-- 0 -->
                                 <th>Account</th> <!-- 0 -->
                                 <th>Date</th> <!-- 1 -->
                                 <th>Description</th> <!-- 2 -->
                                 <th>Amount</th> <!-- 3 -->
                                 <th>Project Code</th> <!-- 4  -->
                                 <th>Row No</th> <!-- 5 -->
                                 <th>Locked</th> <!-- 6 -->
                                 <th>Actions</th> <!-- 7 -->
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th colspan="3">Total</th>
                                 <th></th> <!-- Nominal -->
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>                                
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 offset-md-6 text-right">
                        <table class="display compact nowrap table-hover mt-3" cellspacing="0" width="100%">
                        <tfoot>
                                        <!-- <tr>
                                            <td class="text-right">Cash Advance : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right"
                                                        id="pettycash_store" name="pettycash_store"
                                                        placeholder="Petty Cash" value="3500000"
                                                        onfocus="this.select()">
                                                </div>
                                            </td>
                                        </tr> -->
                                        <tr>
                                            <td class="text-right">Total : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable"
                                                        id="total_expense" name="total_expense"
                                                        placeholder="Total Expense" value="0" onfocus="this.select()"
                                                        readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- <tr>
                                            <td class="text-right">Remaining Settlement Advance : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable"
                                                        id="sisa_pettycash" name="sisa_pettycash"
                                                        placeholder="Sisa Pettycash" value="0" onfocus="this.select()"
                                                        readonly>
                                                </div>
                                            </td>
                                        </tr> -->
                                    </tfoot>
                        </table>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>

       <!-- HISTORI -->
		<div class="col-md-12">
         <div class="card">
            <div class="card-body">
                  <div class="row">
                     <div class="col-md-12">
                         <h4>History</h4>
                        <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>No</th> <!-- 0 -->
                                 <th>Date</th> <!-- 2 -->
                                 <th>Code Occupation</th> <!-- 3 -->
                                 <th>Email</th> <!-- 4  -->
                                 <th>Comment</th> <!-- 5 -->
                                 <th>Status</th> <!-- 6 -->
                                 
                              </tr>
                           </thead>
                           <tbody>
                               <?php 
                               $no = 1;
                               foreach($claimexp_a as $pa):?>
                               <tr>
                                   <td><?= $no++;?></td>
                                   <td><?= $pa['tanggal']?></td>
                                   <td><?= $pa['code_jabatan']?></td>
                                   <td><?= $pa['email']?></td>
                                   <td><?= $pa['comment']?></td>
                                   <td>
                                       <?php if($pa['status'] == 0){?>
                                        <span class="badge badge-warning">Revision</span>
                                        <?php }else if($pa['status'] == 9){?>
                                            <span class="badge badge-danger">Reject</span>
                                        <?php }else if($pa['status'] == 1){?>
                                            <span class="badge badge-success">Approve</span>
                                        <?php }?>
                                   </td>
                               </tr>
                               <?php endforeach?>
                           </tbody>
                        </table>
                     </div>
                  </div>
            </div>
         </div>
      </div>
      <!-- END HISTORI -->
   </div>

   <!--begin::Modal Pembayaran-->
   <div class="modal fade" id="modalFormPembayaran" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalFormPembayaran" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title">Pembayaran</small></h5>
            </div>
            <form class="form" id="form_pembayaran" name="form_pembayaran">
               <div class="modal-body">
                  <!-- START RANGKUMAN PEMBAYARAN -->
                  <div class="row mt-3">
                     <div class="col-lg-4">
                        <div class="form-group">
                           <label>Total </label>
                           <input type="text" class="form-control text-right disable" id="total_pembelanjaan" name="total_pembelanjaan" placeholder="Total Pembelanjaan" value="0" onfocus="this.select()" readonly>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                           <label>Bayar </label>
                           <input type="text" class="form-control text-right disable" id="total_pembayaran" name="total_pembayaran" placeholder="Total Pembayaran" value="0" onfocus="this.select()" readonly>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                           <label>Sisa / Kembalian </label>
                           <input type="text" class="form-control text-right disable" id="sisa_kembalian" name="sisa_kembalian" placeholder="Total" value="0" onfocus="this.select()" readonly>
                        </div>
                     </div>
                  </div>
                  <!-- END RANGKUMAN PEMBAYARAN -->

                  <hr />

                  <!-- START PEMBAYARAN CASH -->
                  <div class="row mt-3">
                     <div class="col-lg-6">
                        <div class="form-group mt-2">
                           <label>Cash</label>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <input type="text" class="form-control text-right" id="pembayaran_cash" name="pembayaran_cash" placeholder="Pembayaran Cash" value="0" onfocus="this.select()">
                           <small class="form-text text-muted text-right">Nilai pembayaran</small>
                        </div>
                     </div>
                  </div>
                  <!-- END PEMBAYARAN CASH -->

                  <!-- START PEMBAYARAN DEBIT -->
                  <div class="row mt-3">
                     <div class="col-lg-3">
                        <div class="form-group mt-2">
                           <label>Debit</label>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <div class="input-group">
                              <input type="hidden" name="pembayaran_debit_coa_code" id="pembayaran_debit_coa_code" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="No. COA Debit" name="pembayaran_debit_coa_name" id="pembayaran_debit_coa_name" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" id="LOVCOABankDebitButton" onclick="LOVCOADebit();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                           <small class="form-text text-muted">No. COA Debit</small>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <input type="text" class="form-control" id="pembayaran_debit_no" name="pembayaran_debit_no" placeholder="No. Kartu Debit" value="" onfocus="this.select()">
                           <small class="form-text text-muted">No. Kartu Debit</small>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <input type="text" class="form-control text-right" id="pembayaran_debit_nilai" name="pembayaran_debit_nilai" placeholder="Nilai Debit" value="0" onfocus="this.select()">
                           <small class="form-text text-muted">Nilai Debit</small>
                        </div>
                     </div>
                  </div>
                  <!-- END PEMBAYARAN DEBIT -->

                  <!-- START PEMBAYARAN CREDIT -->
                  <div class="row mt-3">
                     <div class="col-lg-3">
                        <div class="form-group mt-2">
                           <label>Credit</label>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <div class="input-group">
                              <input type="hidden" name="pembayaran_credit_coa_code" id="pembayaran_credit_coa_code" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="No. COA Credit" name="pembayaran_credit_coa_name" id="pembayaran_credit_coa_name" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" id="LOVCOABankCreditButton" onclick="LOVCOACredit();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                           <small class="form-text text-muted">No. COA Credit</small>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <input type="text" class="form-control" id="pembayaran_credit_no" name="pembayaran_credit_no" placeholder="No. Kartu Credit" value="" onfocus="this.select()">
                           <small class="form-text text-muted">No. Kartu Credit</small>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <input type="text" class="form-control text-right" id="pembayaran_credit_nilai" name="pembayaran_credit_nilai" placeholder="Nilai Credit" value="0" onfocus="this.select()">
                           <small class="form-text text-muted">Nilai Credit</small>
                        </div>
                     </div>
                  </div>
                  <!-- END PEMBAYARAN CREDIT -->

               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-outline-info font-weight-bold" data-dismiss="modal"><span class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!--begin::Modal Pembayaran-->

   <!--begin::Modal riwayat harga-->
   <div class="modal fade" id="modalRiwayatHarga" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalRiwayatHarga" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title">Riwayat Harga</small></h5>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <input type="hidden" name="item_id_riwayat_harga" id="item_id_riwayat_harga" value="" readonly="readonly" />
                     <table id="table_riwayat_harga" class="display compact nowrap table" cellspacing="0" width="100%">
                        <thead>
                           <tr>
                              <th>Outlet</th>
                              <th>Tanggal</th>
                              <th>Tanggal Expire</th>
                              <th>Qty</th>
                              <th>Harga Beli</th>
                              <th>Harga</th>
                              <th>Price Point</th>
                              <th>Potongan</th>
                              <th>Jumlah</th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-outline-info font-weight-bold" data-dismiss="modal"><span class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
            </div>
         </div>
      </div>
   </div>
   <!--begin::Modal riwayat harga-->

</div>

<script type="text/javascript">
    var data4DataTable = [];

    // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
   function LOVEmail() {
	let employee_id = $("#code_employee").val();
	let parameters = {
		employee_id: employee_id,
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/Expenseclaim/getEmail/home/' + jsonWhere, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee', 'email']);
		$('#list_controls').val(['#nik', '#name_employee', '#email']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#employee_id').val()) return;
			$('#email').val();
		});
	});
}
</script>

<script>
    (function () {
	initPage()
   
})();


function initPage() {
    
	initDataTable();
	// initDataTableRiwayatHarga();
	initDatePicker();
	initValidation();
	// initOtherElements();
	getData();
	clearForm();
}

function Kembali() {
	window.location.href = '#' + Modules + '/' + Controller;
}

function clearForm() {
	$('#doc_date').datepicker("setDate", moment().format('DD/MM/YYYY'));
	$('#tanggal').datepicker("setDate", moment().format('DD/MM/YYYY'));
	$('#top').val(0);

	selectedKaryawanId = null;
   selectedCustomerId = null;
   selectedCOACode = null;
   selectedCOACodeDebit = null;
   selectedCOACodeCredit = null;
   flagTerapkanPromoDiskon = false;
   flagItemEndRow = false;
}

function initDatePicker() {
	$('#periode_awal').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
	$('#periode_akhir').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());

	// $('#periode_awal').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", new Date());

	// $('#periode_akhir').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", new Date());

	$('#tgl_jatuh_tempo').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			doc_date: {
				DateID: true,
				required: true,
			},
		},
	});
}

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"bPaginate": false,
		"ordering": false,
		"info": false,
		"scrollX": true,
		"scrollY": "250px",
		"data": data4DataTable,
		"columns": [
         {},
			{
				"data": "code_coa", "className": "text-center", width: 200,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
									<input type="hidden" name="detail_id_${meta.row}" id="detail_id_${meta.row}" value="${row.detail_id}" readonly="readonly" />
									<input type="hidden" name="code_coa_${meta.row}" id="code_coa_${meta.row}" value="${row.code_coa}" readonly="readonly" />
									<input type="text" class="form-control ${row.locked == 1 ? 'disable' : ''} input-pencarian" placeholder="Masukkan Code COA" name="pencarian_${meta.row}" id="pencarian_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.code_coa}" autocomplete="off" ${row.locked == 1 ? 'readonly' : ''} />
							${row.locked == 1 ? ''
							:
							`<div class="input-group-append">
									<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" id="LOVItemButton_${meta.row}" onclick="LOVCoa('${meta.row}', '#code_coa_${meta.row}', '#pencarian_${meta.row}');"><i class="fas fa-search"></i></button>
								</div>`
						}
								</div>`;
				}
			},
			{
				"data": "tanggal", "className": "text-center", width: 140,
				"render": function (data, type, row, meta) {
						return `<div class="input-group date">
									<input type="text" class="form-control font-size-sm ${row.locked == 1 ? 'disable' : 'input-tanggal'}" placeholder="Tanggal" id="tanggal_${meta.row}" name="tanggal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${moment(row.tanggal).format('DD/MM/YYYY')}" readonly />
									<div class="input-group-append">
										<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
									</div>
								</div>`;
				},
			},
			{ "data": "name_coa", "Classname": "text-left" },
			{
				"data": "nominal", "className": "text-right", width: 150,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
							 <input type="text" class="form-control text-right input-nominal" id="nominal_${meta.row}" name="nominal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Nominal" value="${row.nominal}" onfocus="this.select()" autocomplete="off" disabled/>
							 </div>`;
				}
			},
			{
				"data": "remark", "className": "text-left", width: 150,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
							<input type="text" class="form-control input-remark" placeholder="Remark" name="remark_${meta.row}" id="remark_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.remark}" onfocus="this.select()" autocomplete="off"/>
							</div>`;
				}
			},
			{ "data": "row_no", "visible": false },
			{ "data": "locked", "visible": false },
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';
						actions += `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}" disabled><i class="glyphicon glyphicon-trash"></i></button>`;
					return actions;
				},
			},
		],
		columnDefs: [ { "defaultContent": "-", "targets": "_all" } 
		],
		"footerCallback": function(row, data, start, end, display) {
            let api = this.api();

            let totalQty = api.column(4).data().reduce(function(prevVal, nextVal) {
                return parseInt(prevVal) + parseInt(nextVal);
            }, 0);
            // $(api.column(4).footer()).html(accounting.formatNumber(totalQty));
            let total = accounting.unformat($('#total_qty').val(), _decimalSeparator);
            let sisa = 1000000 - totalQty;
            $('#total_qty').val(totalQty);
            $('#total_expense').val(accounting.formatNumber(totalQty, 0));
            $('#sisa_pettycash').val(accounting.formatNumber(sisa, 0));

            // hitungTotal();
        },
	}).on('draw.dt', function (e, settings, json, xhr) {
		// === START pencarian item
		$('.input-pencarian').on('keypress', function (e) {
			if (e.charCode == 13) {
				e.preventDefault();
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				if ($(this).val().trim() == "") return;
				if (lockedRow == 1) return;
				let parameters = 'code_coa=' + $(this).val().trim();

				loadingProcess();
				ajaxNew(base_url + 'finance/pos/get_coa?' + parameters, null, 'GET')
					.then((data) => {
						if (data.result) {
							let row = data.data;
							let data2save = {
								detail_id: $('#detail_id_' + indexRow).val(),
								code_coa: row.code_coa,
								tanggal: row.tanggal,
								name_coa: row.name_coa,
								nominal: 0,
								remark: row.remark,
								row_no: moment().format('YYYYMMDDHHmmssSSS'),
								locked: 0
							};
							// Cek kode item duplikat
							let cek = null;
							if (row.tanggal == 1) {
								cek = _dataTable.rows().data().toArray().filter(data => {
									return $('#pencarian_' + indexRow).val() == data.code_coa;
								});
	
								if ((cek.length > 1 && DataTableAction == 'create') ||
									(cek.length > 1 && DataTableAction == 'edit' && $('#pencarian_' + indexRow).val() != _dataTable.cek(indexRow).data().code_coa)) {
									MsgBox.Notification('Code Coa Exist, Pleas change Nominal to add', 'Warning', 'warning', true);
									return;
								}
							} else {
								// Cek kode item duplikat
								cek = _dataTable.rows().data().toArray().filter(data => {
									return $('#pencarian_' + indexRow).val() == data.code_coa;
								});

								if (cek.length > 1) {
									MsgBox.Notification('Code Coa Exist, Pleas change Nominal to add', 'Warning', 'warning', true);
									return;
								}
							}
												
							// Simpan data
							DataTableAction = 'edit';
							SimpanRowDataTable(_dataTable, data2save, indexRow);
							DataTableAction = 'create';
							if (row.tanggal == 1) {
								$('#tanggal_' + indexRow).focus();
							} else {
								$('#nominal_' + indexRow).focus();
							}							
							loadingProcess(false);
						} else {
							// MsgBox.Notification(data.message.toString());
							loadingProcess(false);
							LOVCoa(indexRow, '#code_coa_' + indexRow, '#pencarian_' + indexRow, $(this).val().trim());
						}
					})
					.catch((err) => {
						MsgBox.Notification(err.toString());
					});
			}
		});
		// === END pencarian item

		$('.input-tanggal').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
		});

		$('.input-tanggal').on('changeDate', function (e) {
			let getIndex = e.target.id.split("_");
			let indexRow = getIndex[getIndex.length - 1];
			_dataTable.cell(indexRow, 1).data(moment(e.date).format('YYYY-MM-DD'));
			_dataTable.rows().invalidate().draw();
			$('#tanggal_' + indexRow).datepicker('hide');
			$('#tanggal_' + indexRow).datepicker('destroy');
			$('#nominal_' + indexRow).focus();
		});
		// === END expired date

		// === START Nominal

		$('.input-nominal').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			if (e.charCode == 13 || e.charCode == 9) {
				_dataTable.rows().draw();
				$('#remark_' + indexRow).focus();
			}
		});

		$('.input-nominal').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			// Update Qty
			_dataTable.cell(indexRow, 3).data(accounting.unformat($(this).val(), _decimalSeparator));
			// Jumlah
			let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
			// _dataTable.cell(indexRow, 3).data(jumlah);
			_dataTable.rows().invalidate().draw();
		});

		// === END qty

		// === START Remark

		$('.input-remark').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			
			if (e.charCode == 13 || e.charCode == 9) {
				// Check jika lockedRow = 1
				if (lockedRow == 1) {
					_dataTable.rows().draw();
					flagItemEndRow = true;

					indexRow++;
					if ($('#nominal_' + indexRow).length) {
						if ($('#nominal_' + indexRow).data('locked') == 1) {
							$('#nominal_' + indexRow).focus();
						} else {
							$('#pencarian_' + indexRow).focus();
						}
					}
					indexRow--;
					return; 
				}
				
				let row = null;

				// Buat row baru dan fokus ke pencarian

				// Locked
				_dataTable.cell(indexRow, 6).data(1);
				_dataTable.rows().invalidate().draw();
				flagItemEndRow = true;
				indexRow++;
				$('#pencarian_' + indexRow).focus();
			}	
		});	

		$('.input-remark').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			
			// Price point
			// _dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator));
			// Jumlah
			// let jumlah = (accounting.unformat($('#nominal_' + indexRow).val(), _decimalSeparator), _decimalSeparator);
			// _dataTable.cell(indexRow, 7).data(jumlah);
			_dataTable.rows().invalidate().draw();
			if (flagItemEndRow) {
				_dataTable.rows().invalidate().draw();
			}
			flagItemEndRow = false;
		});
		
		$('.input-nominal').autoNumeric('init', formatNumber);
		$('.input-remark').val();
		// hitungTotal();
	});
   
   _dataTable.on('order.dt search.dt', function () {
        let i = 1;
 
        _dataTable.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
            this.data(i++);
        });
    }).draw();
	setTimeout(() => {
		_dataTable.rows().invalidate().draw();
		
		setTimeout(() => {
			$('#pencarian_' + (_dataTable.data().count() - 1)).focus();
		}, 200);
	}, 500);
}


function LOVCoa(RowIdx, elItemId = null, elPencarian = null, search = '') {
	let parameters = {
		code_coa: $(elPencarian).val().trim(),
		code_dept : $("#code_dept").val().trim()
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/expenseclaim/get_coa_lov/home/' + jsonWhere + '?search=' + search, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code_coa', 'code_coa']);
		$('#list_controls').val([elItemId, elPencarian]);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!_resultFromLOV) return;

			// Cek kode item & tanggal expire duplikat
			let row = null;
			row = _dataTable.rows().data().toArray().filter(data => {
				return $('#code_coa_' + RowIdx).val() == data.code_coa;
			});

			if ((row.length > 0 && DataTableAction == 'create') || (row.length > 0 && DataTableAction == 'edit' && $('#code_coa_' + RowIdx).val() != _dataTable.row(indexRow).data().code_coa)) {
				MsgBox.Notification('Code Coa Exists', 'Warning', 'warning', true);
				$(elItemId).val(null);
				$(elPencarian).val(null);
				return;
			} else {
				let data2save = {
					detail_id: 0,
					code_coa: _resultFromLOV.code_coa,
					name_coa: _resultFromLOV.name_coa,
					nominal: 0,
					remark: '',
					row_no: moment().format('YYYYMMDDHHmmssSSS'),
					locked: 0
				};
				// Simpan data
				DataTableAction = 'edit';
				SimpanRowDataTable(_dataTable, data2save, RowIdx);
				DataTableAction = 'create';
				$('#tanggal_' + RowIdx).focus();
			}

			_resultFromLOV = null;
		});
	});
}

function getData() {
    let _id = $("#no_doc").val()
	let url = base_url + Modules + '/' + Controller + `/getData2Edit/json/${_id}`;
	ajaxNew(url, null, 'GET')
		.then((data) => {
			if (data.result) {
				let row = data.data;
          
				$('#no_doc').val(row.no_doc);
				$('#periode_awal').datepicker("setDate", moment(row.periode_awal).format('DD/MM/YYYY'));
				$('#periode_akhir').datepicker("setDate", moment(row.periode_akhir).format('DD/MM/YYYY'));
				$('#company_name').val(row.company_name);
				$('#company_code').val(row.m_type);
				$('#code_employee').val(row.nik);
				$('#name_employee').val(row.name_employee);
				$('#dapartement').val(row.name_dept);
				$('#code_dept').val(row.code_dept);
				$('#code_jabatan').val(row.code_jabatan);
				$('#position').val(row.name_jabatan);
				$('#bank').val(row.bank_to);
				$('#account_number').val(row.no_rek);
				$('#account_name').val(row.nama_penerima);
                if(row.attachment_file){
                    $("#file_uploaded").html(`<a href="<?= base_url('assets/upload')?>/${row.attachment_file}" target="_BLANK"><span class="text-success">Check File Uploaded</span></a>`)
                }
				
				data4DataTable = data.detail;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.detail).draw();
					setTimeout(() => {
					}, _dataTable.data().count() * 100);
				}, 500);

				// Pembayaran tunai
				if (row.type_payment == 'TUNAI') {
					// cash
					$('#pembayaran_cash').val(accounting.formatNumber(row.cash, 2));
					$('#sisa_kembalian').val(accounting.formatNumber(row.kembali, 2));
					// debit
					$('#pembayaran_debit_coa_code').val(row.coa_debit); $('#pembayaran_debit_coa_name').val(row.coa_debit + ' - ' + row.coa_debit_name);
					$('#pembayaran_debit_no').val(row.debit_no);
					$('#pembayaran_debit_nilai').val(accounting.formatNumber(row.debit, 2));
					// credit
					$('#pembayaran_credit_coa_code').val(row.coa_credit); $('#pembayaran_credit_coa_name').val(row.coa_credit + ' - ' + row.coa_credit_name);
					$('#pembayaran_credit_no').val(row.credit_no);
					$('#pembayaran_credit_nilai').val(accounting.formatNumber(row.credit, 2));
				}
				// Pembayaran transfer
				if (row.type_payment == 'TRANSFER') {
					$('#coa_code').val(row.coa_transfer); $('#coa_name').val(row.coa_transfer + ' - ' + row.coa_transfer_name);
				}
				// Pembayaran TEMPO
				if (row.type_payment == 'TEMPO') {
					$('#tgl_jatuh_tempo').datepicker("setDate", moment(row.tgl_jatuh_tempo).format('DD/MM/YYYY'));
					let selisihHari = moment(row.tgl_jatuh_tempo).diff(row.tgl_trans, 'days');
					$('#top').val(selisihHari);
				}
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

function deleteDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Delete ' + getData.item_id + ' with expire date ' + moment(getData.expired_date).format('DD/MM/YYYY') + ' from detail?', 'Hapus detail').then(result => {
		HapusRowDataTable(_dataTable, RowIdx);
	}).catch(err => {
		if (err) console.log(err);
	});
}

$("#tambah_detail_btn").on('click',function(){
    _dataTable.row.add([1]).draw(false)
})

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {
    
	if (DataTableAction == 'edit') {
		DataTableElement.row(RowIdx).data(data2save);
		DataTableElement.rows().invalidate().draw();
	} else {
		DataTableElement.row.add(data2save).draw();
	}
}

function addDTRow() {
	let data2save = {
		detail_id: 0,
		code_coa: "",
		tanggal: moment().format('YYYY-MM-DD'),
		name_coa: "",
		nominal: 0,
		remark: "",
		row_no: moment().format('YYYYMMDDHHmmssSSS'),
		locked: 0
	};
	// Simpan data
	DataTableAction = 'edit';
	SimpanRowDataTable(_dataTable, data2save);
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}


async function Update() {
	// Check qty
	// if (accounting.unformat($('#total_qty').val(), _decimalSeparator) <= 0) {
	// 	MsgBox.Notification('Periksa kembali inputan anda', 'Peringatan', 'warning');
	// 	return;
	// }
	// Check detail
	if (_dataTable.data().count() <= 0) {
		MsgBox.Notification('Detail not found');
		return;
	}
	
	// Hitung total
	// await hitungTotal();
	

	// save
	MsgBox.Confirm('Save this data?').then(result => {
		if (!result) return;
        let _id = $("no_doc").val()
		let url = base_url + Modules + '/' + Controller + '/update/';
		let detail = JSON.stringify(_dataTable.rows().data().toArray());
		let summary = $('#form_summary').serialize();
		_data2Send = $('#form_input').serialize() + '&' + summary + '&detail=' + detail;
		ajaxNew(url, _data2Send, 'POST')
			.then((data) => {
				if (data.result) {
					$('#no_doc').val(data.data.no_doc);
					_id = data.data.no_doc;
					action = 'edit';
					MsgBox.Notification(data.message.toString(), 'bottom right', 'success');
					Kembali();
					// MsgBox.ConfirmNew(data.message.toString() + ' Tambah transaksi lain?').then(result => {
					// 	if (result) {
					// 		$('#form_input')[0].reset();
					// 		clearForm();
					// 		_dataTable.clear().draw();
					// 		addDTRow();
					// 	} else {
					// 		Kembali();
					// 	}
					// }).catch((err) => {
					// 	MsgBox.Notification(err.toString());
					// });
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

$("#btn-reasign").on('click',function(){
   let no_doc = $("#no_doc").val()
   let comment = $("#comment").val()
   let email = $("#email").val()
   let periode_awal = $("#periode_awal").val()
   let periode_akhir = $("#periode_akhir").val()
   let code_employee = $("#code_employee").val()
   MsgBox.Confirm('Revision this data?').then(result => {
       if(!result)return
       $.ajax({
            url:`${base_url + 'finance/expenseclaimapp/reasign_expclaim'}`,
            method:"POST",
            data : {no_doc:no_doc,comment:comment,email:email,code_employee:code_employee,periode_awal:periode_awal,periode_akhir:periode_akhir},
            success:function(data){
               data = JSON.parse(data)
                if(data.status == 200){
                    MsgBox.Notification(data.msg);
                }
                else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
   })
})

$("#btn-approve").on('click',function(){
   let no_doc = $("#no_doc").val()
    let comment = $("#comment").val()
    let email = $("#email").val()
    let nik = $("#nik").val()
    let periode_awal = $("#periode_awal").val()
    let periode_akhir = $("#periode_akhir").val()
    let code_employee = $("#code_employee").val()

    MsgBox.Confirm('Approve this data?').then(result => {
       if(!result)return
       $.ajax({
            url:`${base_url + 'finance/expenseclaim/approval_expclaim'}`,
            method:"POST",
            data : {no_doc:no_doc,comment:comment,email:email,code_employee:code_employee,periode_awal:periode_awal,periode_akhir:periode_akhir,nik:nik},
            success:function(data){
               data = JSON.parse(data)
                if(data.status == 200){
                    MsgBox.Notification(data.msg);
                    Kembali();
                }
                else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
    })
})

$("#btn-reject").on('click',function(){
   let no_doc = $("#no_doc").val()
    MsgBox.Confirm('Reject this data?').then(result => {
       if(!result)return
       $.ajax({
            url:`${base_url + 'finance/expenseclaim/reject_expclaim'}`,
            method:"POST",
            data : {no_doc:no_doc},
            success:function(data){
               data = JSON.parse(data)
                if(data.status == 200){
                    MsgBox.Notification(data.msg);
                }
                else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
    })
})
</script>