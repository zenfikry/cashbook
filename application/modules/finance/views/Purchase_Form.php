<style type="text/css">
    .disable {
        background: #DDD !Important;
    }

    #po-form-selected {
        display: none;
    }
</style>
<div class="main">
    <div class="row margin-bottom-15 padding-left-10">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali" onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
                Back </button>
            <div class="pull-right">
                <button type="button" class="btn btn-success waves-effect waves-light" id="simpan" name="simpan" onclick="Simpan();"> <span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span>
                    Save and Send </button>
            </div>
        </div>
    </div>
    <div class="row padding-horizontal-10">
        <div class="col-md-12 mt-3">
            <form id="form_input" name="form_input">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">PI No. *</label>
                                <input class="form-control disable" placeholder="PI Number" id="inv_no" name="inv_no" value="" readonly />
                            </div>
</div>
<div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Creditor <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control font-size-sm" placeholders="Code Creditor" name="code_creditor" id="code_creditor" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCreditor();"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Creditor Description</label>

                                <input type="text" class="form-control font-size-sm" placeholder="Creditor Description" id="desc_creditor" name="desc_creditor" value="" readonly="readonly" />


                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Company <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="company_code" id="company_code" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Select Company"
                                            name="company_name" id="company_name" value="" readonly="readonly" />
                                        <input type="hidden" name="categories" id="categories" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVCompany();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                                        <input type="hidden" name="name_employee" id="name_employee" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Choose Email" name="email"
                                            id="email" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVEmail();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Invoice Date <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Invoice Date" id="inv_date" name="inv_date" value="<?= date('Y-m-d') ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Due Date <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Due Date" id="due_date" name="due_date" value="<?= date('Y-m-d') ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Invoice Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="inv_desc" id="inv_desc" placeholder="Invoice Description" value="">
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Supplier INV No. <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="supplier_iv" id="supplier_iv" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Ref No. 2 <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="ref_no" id="ref_no" value="" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="form-group">
                                    <label>Rate <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="rate_value" id="rate_value" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Currency <span class="text-danger">*</span></label>
                                    <select class="form-control" name="code_rate" id="code_rate">
                                        <option value="">- Select Currency -</option>
                                        <?php foreach ($this->db->get('m_currency') ->result() as $key => $value): ?>
                                            <option value="<?php echo $value->code_rate ?>"><?php echo $value->name_rate ?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Expense Period <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="expense_period" id="expense_period" value="">
                                </div>
                            </div>
                            
                        </div> -->
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" class="form-control font-size-sm" placeholder="Description" id="description" name="description" value="" />
                                </div>
                            </div>
                        </div>
                        <h4>Attachment</h4>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-3" id="">
                                <label class="control-label">Attachment 1 <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" name="file_expense" id="file_expense">
                            </div>
                            <div class="form-group col-md-3" id="">
                                <label class="control-label">Attachment 2 </label>
                                <input type="file" class="form-control" name="file_import" id="file_import">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form" id="form_summary" name="form_summary">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th> 
                                            <th>Purchase A/C</th> <!-- 0 -->
                                            <th>Date</th> <!-- 2 -->
                                            <th>Description</th>
                                            <th>Taxable Amount</th> <!-- 3 -->
                                            <th>PPN</th>
                                            <th>WHT Amount</th>
                                            <th>Proj No</th> <!-- 4  -->
                                            <th>WHT</th>
                                            <th>Row No</th> <!-- 5 -->
                                            <th>Locked</th> <!-- 6 -->
                                            <th>Actions</th> <!-- 7 -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4">Total</th>
                                            <th>0</th> <!-- Nominal -->
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th> 
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 offset-md-6 text-right">
                                <table class="display compact nowrap table-hover mt-3" cellspacing="0" width="100%">
                                    <tfoot>

                                        <tr>
                                            <td class="text-right">Taxable Amount : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="total_expense" name="total_expense" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">PPN : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="ppn" name="ppn" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">WHT : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="wht_amount" name="wht_amount" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">Total Payment : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="net_total" name="net_total" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--PAYMENT METHOD -->
    <div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="modalPaymentLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment Method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="table-payment" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Bank</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAYMENT METHOD -->
</div>

<script type="text/javascript">
    // START VARIABEL WAJIB
    var Modules = '<?= $modules ?>';
    var Controller = '<?= $controller ?>';
    var Priv = JSON.parse('<?= json_encode($priv_arr) ?>');
    var data2Send = null;
    var action = '<?= $action ?>';
    var _id = "<?= $action == 'edit' ? $id : 'null' ?>";
    // END VARIABEL WAJIB

    var _dataTable = null;
    var DataTableAction = 'create';
    var DataTableRowIdx = 0;
    var data4DataTable = [];

    var _dataRiwayatHarga = null;
    var data4DataTableRiwayatHarga = [];

    var selectedKaryawanId = null;
    var selectedCustomerId = null;
    var selectedCOACodeDebit = null;
    var selectedCOACodeCredit = null;
    var flagTerapkanPromoDiskon = false;
    var flagItemEndRow = false;

    LobiAdmin.loadScript([
        '<?= base_url() ?>assets/js/lib/autoNumeric/autoNumeric.js',
        '<?= base_url() ?>assets/js/lib/accounting/accounting.min.js',
    ], function() {
        LobiAdmin.loadScript([
            '<?= base_url() ?>assets/js/lib/autoNumeric/init.js',
            '<?= base_url() ?>assets/js/lib/accounting/init.js',
        ], function() {
            LobiAdmin.loadScript([
                '<?= base_url() ?>assets/js/modules/' + Modules + '/' + Controller +
                '.form.js?v=<?= date('YmdHis') . rand() ?>',
            ], function() {
                initPage();
            });
        });
    });
</script>