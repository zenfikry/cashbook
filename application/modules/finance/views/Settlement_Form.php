<style type="text/css">
   .disable {
      background: #DDD !Important;
   }

   #po-form-selected {
      display: none;
   }
</style>
<div class="main">
   <div class="row margin-bottom-15 padding-left-10">
      <div class="col-md-12">
         <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali" onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span> Back </button>
         <div class="pull-right">
            <button type="button" class="btn btn-success waves-effect waves-light" id="simpan" name="simpan" onclick="Simpan();"> <span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span> Save and Send </button>
         </div>
      </div>
   </div>
   <div class="row padding-horizontal-10">
      <div class="col-md-12 mt-3">
         <div class="card">
            <div class="card-body">
               <form id="form_input" name="form_input">
                  <div class="row">
                     <div class="form-group col-md-3">
                        <label class="control-label">Doc No. *</label>
                        <input class="form-control disable" placeholder="No. Document" id="no_doc" name="no_doc" value="" readonly />
                     </div>
                     <div class="form-group col-md-3" id="doc_date_container">
                        <label class="control-label">Period <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Period" id="periode_awal" name="periode_awal" value="<?=date('Y-m-d')?>">
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>                        
                     </div>
                     <div class="form-group col-md-3" id="doc_date_container">
                        <label class="control-label">until <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Periode" id="periode_akhir" name="periode_akhir" value="<?=date('Y-m-d')?>">
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <div class="form-group">
                           <label>Company <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="company_code" id="company_code" value="" readonly="readonly" />
                              <input type="hidden" name="categories" id="categories" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Select Company" name="company_name" id="company_name" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCompany();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>    
                  <div class="row">    
                  <div class="form-group col-md-3" style="display:none;" >
                        <div class="form-group">
                           <label>Employee <span class="text-danger">*</span></label>
                           <div class="input-group">
                           <input type="hidden" name="code_employee" id="code_employee" value="" readonly="readonly" />
                           <input type="text" class="form-control" placeholder="Select Employee" name="name_employee"
                              id="name_employee" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmployee();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>          
                     
                     <div class="form-group col-md-3" style="display:none;">
                        <label class="control-label">Department</label>
                        <input type="hidden" class="form-control" placeholder="Dapartment" name="code_dept" id="code_dept" value="" readonly="readonly" />
                        <input type="text" class="form-control" placeholder="Dapartement" name="dapartement" id="dapartement" value="" readonly="readonly" />
                     </div>
                     <div class="form-group col-md-3" style="display:none;">
                        <label class="control-label">Position <span class="text-danger">*</span></label>
                           <input type="hidden" class="form-control" placeholder="Position" id="code_jabatan" name="code_jabatan" value="" readonly="readonly">
                           <input type="text" class="form-control" placeholder="Position" id="position" name="position" value="" readonly="readonly">
                     </div> 
                     <div class="form-group col-md-3">
                        <div class="form-group">
                           <label>Email <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                              <input type="hidden" name="name_employee" id="name_employee" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Choose Email" name="email" id="email" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmail();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label class="control-label">ID Advanced <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" placeholder="ID Advanced" id="id_advanced" name="id_advanced" value="" required>
                         
                     </div> 
                     <div class="form-group col-md-3">
                        <label class="control-label">Received Date <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Received Date" id="received_date" name="received_date" value="<?=date('Y-m-d')?>">
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>       
                           
                     </div> 
                     <div class="form-group col-md-3">
                        <label class="control-label">Total Advanced <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" placeholder="Total Advanced" id="total_advanced" name="total_advanced" value="" required>
                         
                     </div> 
                  </div>
                  <div class="row">
                     <div class="form-group col-md-12">
                        <label class="control-label">Description</label>
                        <input type="text" class="form-control" placeholder="Description" name="description" id="description" value="" />
                     </div>
                  </div>
                  <h4>Transfer To</h4>
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Select Bank <span class="text-danger">*</span></label>
                        <select id="bank" name="bank" class="form-control">
                           <option>Select Bank</option>
                           <option value="Mandiri">Mandiri</option>
                           <option value="Mandiri Syariah">Mandiri Syariah</option>
                           <option value="BCA">BCA</option>
                           <option value="BRI">BRI</option>
                           <option value="Danamon">Danamon</option>
                        </select>
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Account Number <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Account Number" class="form-control" name="account_number" id="account_number">
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Account Name <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Account Name" class="form-control" name="account_name" id="account_name">
                     </div>
                  </div>
                  <h4>Attachment Documents</h4>
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Expense <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_expense" id="file_expense">
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Perjadin <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_pejading" id="file_pejading">
                     </div>
                  </div>
                 
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Refund <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_refund" id="file_refund">
                     </div>

                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Advance <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_cashbon" id="file_cashbon">
                     </div>
                  </div>
                 
               </form>
            </div>
         </div>
      </div>
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <form class="form" id="form_summary" name="form_summary">
                  <div class="row">
                     <div class="col-md-12">
                        <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>No</th>
                                 <th>Account</th> <!-- 0 -->
                                 <th>Date</th> <!-- 1 -->
                                 <th>Description</th> <!-- 2 -->
                                 <th>Amount</th> <!-- 3 -->
                                 <th>Project Code</th> <!-- 4  -->
                                 <th>Row No</th> <!-- 5 -->
                                 <th>Locked</th> <!-- 6 -->
                                 <th>Actions</th> <!-- 7 -->
                                 <th>Company</th> <!-- 8 -->
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th colspan="3">Total</th>
                                 <th></th> <!-- Nominal -->
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                                 <th></th>                                
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 offset-md-6 text-right">
                        <table class="display compact nowrap table-hover mt-3" cellspacing="0" width="100%">
                           <tfoot>
                              <!-- <tr>
                                 <td class="text-right">Total Advance : </td>
                                 <td>
                                    <div class="input-group text-right">
                                       <input type="text" class="form-control text-right" id="pettycash_store" name="pettycash_store" placeholder="Petty Cash" value="3500000" onfocus="this.select()">
                                    </div>
                                 </td>
                              </tr> -->
                              <tr>
                                 <td class="text-right">Total Expense: </td>
                                 <td>
                                    <div class="input-group text-right">
                                       <input type="text" class="form-control text-right disable" id="total_expense" name="total_expense" placeholder="Total Expense" value="0" onfocus="this.select()" readonly>
                                    </div>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="text-right">Refund Advance : </td>
                                 <td>
                                    <div class="input-group text-right">
                                       <input type="text" class="form-control text-right disable" id="sisa_pettycash" name="sisa_pettycash" placeholder="Sisa Pettycash" value="0" onfocus="this.select()" readonly>
                                    </div>
                                 </td>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
   var data2Send = null;
   var action = '<?=$action?>';
   var _id = "<?=$action == 'edit' ? $id : 'null'?>";
   // END VARIABEL WAJIB

   var _dataTable = null;
   var DataTableAction = 'create';
   var DataTableRowIdx = 0;
   var data4DataTable = [];

   var _dataRiwayatHarga = null;
   var data4DataTableRiwayatHarga = [];

   var selectedKaryawanId = null;
   var selectedCustomerId = null;
   var selectedCOACodeDebit = null;
   var selectedCOACodeCredit = null;
   var flagTerapkanPromoDiskon = false;
   var flagItemEndRow = false;

   LobiAdmin.loadScript([
      '<?=base_url()?>assets/js/lib/autoNumeric/autoNumeric.js',
      '<?=base_url()?>assets/js/lib/accounting/accounting.min.js',
   ], function() {
      LobiAdmin.loadScript([
         '<?=base_url()?>assets/js/lib/autoNumeric/init.js',
         '<?=base_url()?>assets/js/lib/accounting/init.js',
      ], function() {
         LobiAdmin.loadScript([
            '<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.form.js?v=<?=date('YmdHis') . rand()?>',
         ], function() {
            initPage();
         });
      });
   });
</script>