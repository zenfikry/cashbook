<style type="text/css">
.disable {
    background: #DDD !Important;
}

#po-form-selected {
    display: none;
}
</style>
<div class="main">
    <div class="row margin-bottom-15 padding-left-10">
        <div class="col-md-12">
            <a href="<?= base_url('app#finance/pos')?>"><button type="button"
                    class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali"> <span
                        class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span> Back </button></a>
        </div>
    </div>
    <div class="row padding-horizontal-10">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-body">
                    <form id="form_input" name="form_input">
                        <?php 
                    foreach($pettycash_h as $rw):
                   ?>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">No. Doc *</label>
                                <input class="form-control disable" placeholder="No. Document" id="no_doc" name="no_doc"
                                    value="<?= $rw['no_doc']?> " readonly />

                            </div>
                            <input type="hidden" id="code_jabatan_login"
                                value="<?= $this->session->userdata('code_jabatan')?>">
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Periode <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Periode"
                                        id="periode_awal" name="periode_awal" value="<?= $rw['periode_awal']?> ">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">up to <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Periode"
                                        id="periode_akhir" name="periode_akhir" value="<?= $rw['periode_akhir']?> ">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Store <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="m_code" id="m_code" value="" readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Pilih Store"
                                            name="m_shortdesc" id="m_shortdesc" value="<?= $rw['m_code']?> "
                                            readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick=""><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="control-label">PIC</label>
                                <input type="text" class="form-control" placeholder="PIC" name="m_pic" id="m_pic"
                                    value="<?= $rw['pic']?> " readonly="readonly" />
                            </div>
                            <div class="form-group col-md-3">
                                <label class="control-label">Company <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Company" id="m_type" name="m_type"
                                    value="<?= $rw['code_company']?> " readonly="readonly">
                            </div>
                            <?php
                     if ($this->session->userdata('code_jabatan') != 'HOD') {
                     ?>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                                        <input type="hidden" name="name_employee" id="name_employee" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Pilih Email" name="email"
                                            id="email" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVEmail();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                     }
                     ?>
                        </div>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-4" id="appr1_date_container">
                                <label class="control-label">Attachment Expense <span
                                        class="text-danger">*</span></label>
                                <a class="badge badge-success"
                                    href="<?= base_url('assets/upload')?>/<?= $rw['attachment_expense']?>"
                                    target="_blank"><?= $rw['attachment_expense']?></a>
                                <!-- <input type="file" name="file" id="file"> -->
                            </div>
                            <div class="form-group col-md-4" id="appr1_date_container">
                                <label class="control-label">Attachment Refund <span class="text-danger">*</span></label>
                                <a class="badge badge-primary"
                                    href="<?= base_url('assets/upload')?>/<?= $rw['attachment_sisa']?>"
                                    target="_blank"><?= $rw['attachment_sisa']?></a>
                            </div>

                        </div>
                        <?php endforeach;?>
                        <?php if($this->session->userdata('code_jabatan') != 'SPV') { ?>
                        <div class="row padding-horizontal-10 mt-3">

                        </div>

                        <?php if($rw['status'] != 7){?>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-3" id="appr1_date_container">
                                <button class="btn btn-success" type="button" id="approve_btn">Approve</button>


                            </div>
                        </div>
                        <?php }else if($rw['status'] == 7){?>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-3" id="appr1_date_container">
                                <button class="btn btn-success" type="button" id="done_btn">Done</button>
                            </div>
                        </div>
                        <?php }?>
                        <?php }; ?>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <ul class="nav nav-tabs" role="tablist" style="font-size:18px;">
                    <li class="nav-item">
                        <a class="nav-link active" href="#reasign" role="tab" data-toggle="tab">Reasign</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#revisi" role="tab" data-toggle="tab">Revision</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#reject" role="tab" data-toggle="tab">Reject</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active show" id="reasign">
                        <div class="card-body">
                            <h3>Reasign</h3>
                            <div class="input-group col-lg-3">
                                <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                                <input type="hidden" name="name_employee" id="name_employee" value=""
                                    readonly="readonly" />
                                <input type="text" class="form-control" placeholder="Pilih Email" name="email_reasign"
                                    id="email_reasign" value="" readonly="readonly" />
                                <div class="input-group-append">
                                    <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button"
                                        onclick="LOVEmailResign('reasign');"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                            <div class="row padding-horizontal-10 mt-3">
                                <div class="form-group col-md-3" id="appr1_date_container">
                                    <label class="control-label">Comment <span class="text-danger"></span></label>
                                    <textarea name="comment_reasign" id="comment_reasign" class="form-control"></textarea>
                                </div>
                            </div>
                            <button class="btn btn-warning" type="button" id="reasign_btn">Reasign</button>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="revisi">
                        <div class="card-body">
                            <h3>Revisi</h3>
                            <div class="row padding-horizontal-10 mt-3">
                                <div class="form-group col-md-3" id="appr1_date_container">
                                    <label class="control-label">Comment <span class="text-danger"></span></label>
                                    <textarea name="comment_revisi" id="comment_revisi" class="form-control"></textarea>
                                </div>
                            </div>

                            <button class="btn btn-primary" type="button" id="reivisi_btn">Revision</button>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="reject">
                        <div class="card-body">
                            <h3>Reject</h3>
                            <!-- <div class="row padding-horizontal-10 mt-3">
                                <div class="form-group col-md-3" id="appr1_date_container">
                                    <label class="control-label">Comment <span class="text-danger"></span></label>
                                    <textarea name="comment_reject" id="comment_reject" class="form-control"></textarea>
                                </div>
                            </div> -->
                            <button class="btn btn-danger" type="button" id="reject_btn">Reject</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form" id="form_summary" name="form_summary">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_detail" class="display compact nowrap table" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>Code</th> <!-- 0 -->
                                            <th>Date</th> <!-- 1 -->
                                            <th>Description</th> <!-- 2 -->
                                            <th>Amount</th> <!-- 3 -->
                                            <th>Remark</th> <!-- 4  -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                foreach($pettycash_d as $pd):
                               ?>
                                        <tr>
                                            <td><?= $pd['code_coa']?></td>
                                            <td><?= $pd['tanggal']?></td>
                                            <td><?= $pd['name_coa']?></td>
                                            <td>Rp. <?= number_format($pd['nominal'])?></td>
                                            <td><?= $pd['remark']?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <?php 
                                $total = 0;
                                foreach($pettycash_d as $pd){
                                    $total = $pd['nominal']+ $total;
                                }
                               ?>
                                        <tr>
                                            <th colspan="3">Total</th>
                                            <th>Rp . <?=number_format($total);?></th> <!-- Nominal -->
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- HISTORI -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>History</h4>
                            <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th> <!-- 0 -->
                                        <th>Date</th> <!-- 2 -->
                                        <th>Actions</th> <!-- 3 -->
                                        <th>Comment</th> <!-- 5 -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                               $no = 1;
                               foreach($pettycash_a as $pa):?>
                                    <tr>
                                        <td><?= $no++;?></td>
                                        <td><?= $pa['tanggal']?></td>
                                        <td>
                                            <?php if($pa['status'] == 0){?>
                                            <span class="badge badge-warning">Reasign by
                                                <?= $pa['code_jabatan']. ' (' . $pa['name_employee']. ')'; ?></span>
                                            <?php }else if($pa['status'] == 9){?>
                                            <span class="badge badge-danger">Reject by
                                                <?= $pa['code_jabatan']. ' (' . $pa['name_employee']. ')'; ?></span>
                                            <?php }else if($pa['status'] == 10){?>
                                            <span class="badge badge-secondary">Revision
                                            <?= $pa['code_jabatan']. ' (' . $pa['name_employee']. ')'; ?></span>
                                            <?php }else if($pa['status'] == 1){?>
                                            <span class="badge badge-success">Approve by
                                                <?= $pa['code_jabatan']. ' (' . $pa['name_employee']. ')'; ?></span>
                                            <?php }?>
                                        </td>
                                        <td><?= $pa['comment']?></td>

                                    </tr>
                                    <?php endforeach?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HISTORI -->
    </div>


    <!--begin::Modal Pembayaran-->
    <div class="modal fade" id="modalFormPembayaran" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="modalFormPembayaran" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pembayaran</small></h5>
                </div>
                <!-- <input type="hidden" value="<?= $this->uri->segment(3, 0)?>" id="myaction"> -->
                <form class="form" id="form_pembayaran" name="form_pembayaran">
                    <div class="modal-body">
                        <!-- START RANGKUMAN PEMBAYARAN -->
                        <div class="row mt-3">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Total </label>
                                    <input type="text" class="form-control text-right disable" id="total_pembelanjaan"
                                        name="total_pembelanjaan" placeholder="Total Pembelanjaan" value="0"
                                        onfocus="this.select()" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Bayar </label>
                                    <input type="text" class="form-control text-right disable" id="total_pembayaran"
                                        name="total_pembayaran" placeholder="Total Pembayaran" value="0"
                                        onfocus="this.select()" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Sisa / Kembalian </label>
                                    <input type="text" class="form-control text-right disable" id="sisa_kembalian"
                                        name="sisa_kembalian" placeholder="Total" value="0" onfocus="this.select()"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <!-- END RANGKUMAN PEMBAYARAN -->

                        <hr />

                        <!-- START PEMBAYARAN CASH -->
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="form-group mt-2">
                                    <label>Cash</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right" id="pembayaran_cash"
                                        name="pembayaran_cash" placeholder="Pembayaran Cash" value="0"
                                        onfocus="this.select()">
                                    <small class="form-text text-muted text-right">Nilai pembayaran</small>
                                </div>
                            </div>
                        </div>
                        <!-- END PEMBAYARAN CASH -->

                        <!-- START PEMBAYARAN DEBIT -->
                        <div class="row mt-3">
                            <div class="col-lg-3">
                                <div class="form-group mt-2">
                                    <label>Debit</label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="hidden" name="pembayaran_debit_coa_code"
                                            id="pembayaran_debit_coa_code" value="" readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="No. COA Debit"
                                            name="pembayaran_debit_coa_name" id="pembayaran_debit_coa_name" value=""
                                            readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" id="LOVCOABankDebitButton" onclick="LOVCOADebit();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                    <small class="form-text text-muted">No. COA Debit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="pembayaran_debit_no"
                                        name="pembayaran_debit_no" placeholder="No. Kartu Debit" value=""
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">No. Kartu Debit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right" id="pembayaran_debit_nilai"
                                        name="pembayaran_debit_nilai" placeholder="Nilai Debit" value="0"
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">Nilai Debit</small>
                                </div>
                            </div>
                        </div>
                        <!-- END PEMBAYARAN DEBIT -->

                        <!-- START PEMBAYARAN CREDIT -->
                        <div class="row mt-3">
                            <div class="col-lg-3">
                                <div class="form-group mt-2">
                                    <label>Credit</label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="hidden" name="pembayaran_credit_coa_code"
                                            id="pembayaran_credit_coa_code" value="" readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="No. COA Credit"
                                            name="pembayaran_credit_coa_name" id="pembayaran_credit_coa_name" value=""
                                            readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" id="LOVCOABankCreditButton" onclick="LOVCOACredit();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                    <small class="form-text text-muted">No. COA Credit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="pembayaran_credit_no"
                                        name="pembayaran_credit_no" placeholder="No. Kartu Credit" value=""
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">No. Kartu Credit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right" id="pembayaran_credit_nilai"
                                        name="pembayaran_credit_nilai" placeholder="Nilai Credit" value="0"
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">Nilai Credit</small>
                                </div>
                            </div>
                        </div>
                        <!-- END PEMBAYARAN CREDIT -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-info font-weight-bold" data-dismiss="modal"><span
                                class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--begin::Modal Pembayaran-->

    <!--begin::Modal riwayat harga-->
    <div class="modal fade" id="modalRiwayatHarga" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="modalRiwayatHarga" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Riwayat Harga</small></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="item_id_riwayat_harga" id="item_id_riwayat_harga" value=""
                                readonly="readonly" />
                            <table id="table_riwayat_harga" class="display compact nowrap table" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>Outlet</th>
                                        <th>Tanggal</th>
                                        <th>Tanggal Expire</th>
                                        <th>Qty</th>
                                        <th>Harga Beli</th>
                                        <th>Harga</th>
                                        <th>Price Point</th>
                                        <th>Potongan</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info font-weight-bold" data-dismiss="modal"><span
                            class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!--begin::Modal riwayat harga-->

</div>


<script>
// START VARIABEL WAJIB
var Modules = '<?=$modules?>';
var Controller = '<?=$controller?>';

function Kembali() {
    window.location.href = '#' + Modules + '/' + Controller;
}

function LOVEmail() {
    let parameters = {
        m_code: $("#m_shortdesc").val()
    };
    let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
    $('#PopUpModal').load(base_url + 'finance/pos/getEmailHOD/home/' + jsonWhere,
        () => { // Ambil URL untuk membuka modal LOV
            // $(".modal-dialog").css("max-width", "70%");
            $(".modal-dialog").addClass("modal-xl modal-dialog-centered");
            $('#ModalLOV').modal('show'); // Tampilkan modal LOV
            $('#list_cols').val(['nik', 'name_employee', 'email']);
            $('#list_controls').val(['#nik', '#name_employee', '#email']);

            $('#ModalLOV').on('hidden.bs.modal', function() {
                if (!$('#employee_id').val()) return;
                $('#email').val();
            });
        });
}

function LOVEmailResign() {
    $('#PopUpModal').load(base_url + 'finance/pos/getEmailReasign/', () => { // Ambil URL untuk membuka modal LOV
        // $(".modal-dialog").css("max-width", "70%");
        $(".modal-dialog").addClass("modal-xl modal-dialog-centered");
        $('#ModalLOV').modal('show'); // Tampilkan modal LOV
        $('#list_cols').val(['nik', 'name_employee', 'email']);
        $('#list_controls').val(['#nik', '#name_employee', '#email_reasign']);

        $('#ModalLOV').on('hidden.bs.modal', function() {
            if (!$('#employee_id').val()) return;
            $('#email_reasign').val();
        });
    });
}

$("#approve_btn").on('click', function() {
    let no_doc = $("#no_doc").val()
    let comment = $("#comment").val()
    let email = $("#email").val()
    let periode_awal = $("#periode_awal").val()
    let periode_akhir = $("#periode_akhir").val()
    let m_shortdesc = $("#m_shortdesc").val()
    let cb = $("#code_jabatan_login").val()
    if (cb != "HOD") {
        if (email) {
            MsgBox.Confirm('Are you sure to approve?').then(result => {
                if (!result) return
                $.ajax({
                    url: `${base_url + 'finance/pos/approval_pettycash'}`,
                    method: "POST",
                    data: {
                        no_doc: no_doc,
                        comment: comment,
                        email: email,
                        m_shortdesc: m_shortdesc,
                        periode_awal: periode_awal,
                        periode_akhir: periode_akhir
                    },
                    success: function(data) {
                        data = JSON.parse(data)
                        if (data.status == 200) {
                            MsgBox.Notification(data.msg);
                            Kembali()
                        } else {
                            MsgBox.Notification(data.msg);
                        }
                    }
                })
            })
        } else {
            MsgBox.Notification('EMAIL REQUIRED');
        }
    } else {
        email = "";
        MsgBox.Confirm('Are you sure to approve?').then(result => {
            if (!result) return
            $.ajax({
                url: `${base_url + 'finance/pos/approval_pettycash'}`,
                method: "POST",
                data: {
                    no_doc: no_doc,
                    comment: comment,
                    email: email,
                    m_shortdesc: m_shortdesc,
                    periode_awal: periode_awal,
                    periode_akhir: periode_akhir
                },
                success: function(data) {
                    data = JSON.parse(data)
                    if (data.status == 200) {
                        MsgBox.Notification(data.msg);
                    } else {
                        MsgBox.Notification(data.msg);
                    }
                }
            })
        })
    }




})

$("#done_btn").on('click', function() {
    let no_doc = $("#no_doc").val()
    MsgBox.Confirm('Are you sure to Reassign this data?').then(result => {
        if (!result) return
        $.ajax({
            url: `${base_url + 'finance/pos/done_pettycash'}`,
            method: "POST",
            data: {
                no_doc: no_doc
            },
            success: function(data) {
                data = JSON.parse(data)
                if (data.status == 200) {
                    MsgBox.Notification(data.msg);
                    Kembali()
                } else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
    })
})

$("#reivisi_btn").on("click", function() {
    let no_doc = $("#no_doc").val()
    let comment = $("#comment_revisi").val()
    let email = $("#email_reasign").val()
    let periode_awal = $("#periode_awal").val()
    let periode_akhir = $("#periode_akhir").val()
    let m_shortdesc = $("#m_shortdesc").val()
    let cb = $("#code_jabatan_login").val()

    MsgBox.Confirm('Are you sure to revision this data?').then(result => {
        if (!result) return
        $.ajax({
            url: `${base_url + 'finance/pos/revisi_pettycash'}`,
            method: "POST",
            data: {
                no_doc: no_doc,
                comment: comment,
                email: email,
                m_shortdesc: m_shortdesc,
                periode_awal: periode_awal,
                periode_akhir: periode_akhir
            },
            success: function(data) {
                data = JSON.parse(data)
                if (data.status == 200) {
                    MsgBox.Notification(data.msg);
                    Kembali()
                } else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
    })

})
$("#reasign_btn").on('click', function() {
    let no_doc = $("#no_doc").val()
    let comment = $("#comment_reasign").val()
    let email = $("#email_reasign").val()
    let periode_awal = $("#periode_awal").val()
    let periode_akhir = $("#periode_akhir").val()
    let m_shortdesc = $("#m_shortdesc").val()
    let cb = $("#code_jabatan_login").val()

    MsgBox.Confirm('Are you sure to Reassign this data?').then(result => {
        if (!result) return
        $.ajax({
            url: `${base_url + 'finance/pos/reasign_pettycash'}`,
            method: "POST",
            data: {
                no_doc: no_doc,
                comment: comment,
                email: email,
                m_shortdesc: m_shortdesc,
                periode_awal: periode_awal,
                periode_akhir: periode_akhir
            },
            success: function(data) {
                data = JSON.parse(data)
                if (data.status == 200) {
                    MsgBox.Notification(data.msg);
                    Kembali()
                } else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
    })

    //  if(cb !="HOD"){
    //    if (email) {
    //       MsgBox.Confirm('Yakin akan reasign data ini?').then(result => {
    //          if(!result)return
    //          $.ajax({
    //                url:`${base_url + 'finance/pos/reasign_pettycash'}`,
    //                method:"POST",
    //                data : {no_doc:no_doc,comment:comment,email_reasign:email_reasign,m_shortdesc:m_shortdesc,periode_awal:periode_awal,periode_akhir:periode_akhir},
    //                success:function(data){
    //                   data = JSON.parse(data)
    //                   if(data.status == 200){
    //                      MsgBox.Notification(data.msg);
    //                   }
    //                   else {
    //                      MsgBox.Notification(data.msg);
    //                   }
    //                }
    //          })
    //       })
    //    } else {
    //       MsgBox.Notification('EMAIL REQUIRED');
    //    }
    // }
    // else{
    //    MsgBox.Confirm('Yakin akan reasign data ini?').then(result => {
    //          if(!result)return
    //          $.ajax({
    //                url:`${base_url + 'finance/pos/reasign_pettycash'}`,
    //                method:"POST",
    //                data : {no_doc:no_doc,comment:comment,email:email,m_shortdesc:m_shortdesc,periode_awal:periode_awal,periode_akhir:periode_akhir},
    //                success:function(data){
    //                   data = JSON.parse(data)
    //                   if(data.status == 200){
    //                      MsgBox.Notification(data.msg);
    //                   }
    //                   else {
    //                      MsgBox.Notification(data.msg);
    //                   }
    //                }
    //          })
    //       })
    // }


})
$("#reject_btn").on('click', function() {
    let no_doc = $("#no_doc").val()

    MsgBox.Confirm('Are you to Reject this data?').then(result => {
        if (!result) return
        $.ajax({
            url: `${base_url + 'finance/pos/reject_pettycash'}`,
            method: "POST",
            data: {
                no_doc: no_doc
            },
            success: function(data) {
                data = JSON.parse(data)
                if (data.status == 200) {
                    MsgBox.Notification(data.msg);
                    Kembali()
                } else {
                    MsgBox.Notification(data.msg);
                }
            }
        })
    })

})
</script>