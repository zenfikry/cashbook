<style type="text/css">
    .disable {
        background: #DDD !Important;
    }

    #po-form-selected {
        display: none;
    }
</style>
<div class="main">
	<div class="row margin-bottom-15 padding-left-10">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali"
                onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
                Back </button>
            <div class="pull-right">
                <button type="button" class="btn btn-success waves-effect waves-light" id="simpan" name="simpan"
                    onclick="Update();"> <span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span>
                    Save </button>
            </div>
        </div>
    </div>
    <div class="row padding-horizontal-10">
		<?php 
            $code_jabatan_login = $this->session->userdata('code_jabatan');
        ?>
        <?php if($code_jabatan_login == "PO002" || $code_jabatan_login == "PO001"){?>
            <div class="col-md-12 mt-3">
        <?php }else {?>
            <div class="col-md-12 mt-3">
        <?php }?>
            <form id="form_input" name="form_input">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">PI No. *</label>
                                <input class="form-control disable" placeholder="PI Number" id="inv_no" name="inv_no" value="<?= $no_doc;?>" readonly />
                            </div>
</div>
<div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Creditor <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control font-size-sm" placeholders="Code Creditor" name="code_creditor" id="code_creditor" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCreditor();"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Creditor Description</label>

                                <input type="text" class="form-control font-size-sm" placeholder="Creditor Description" id="desc_creditor" name="desc_creditor" value="" readonly="readonly" />


                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Company <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="company_code" id="company_code" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Select Company"
                                            name="company_name" id="company_name" value="" readonly="readonly" />
                                        <input type="hidden" name="categories" id="categories" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVCompany();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                                        <input type="hidden" name="name_employee" id="name_employee" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Choose Email" name="email"
                                            id="email" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVEmail();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Invoice Date <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Invoice Date" id="inv_date" name="inv_date" value="<?= date('Y-m-d') ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Due Date <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Due Date" id="due_date" name="due_date" value="<?= date('Y-m-d') ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Invoice Description <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="inv_desc" id="inv_desc" placeholder="Invoice Description" value="">
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Supplier IV No. <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="supplier_iv" id="supplier_iv" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Ref No. 2 <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="ref_no" id="ref_no" value="">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Expense Period <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="expense_period" id="expense_period" value="">
                                </div>
                            </div>
                            
                        </div> -->
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" class="form-control font-size-sm" placeholder="Description" id="description" name="description" value="" />
                                </div>
                            </div>
                        </div>
                        <h4>Attachment</h4>
                        <div class="row padding-horizontal-10 mt-3">
							<div class="form-group col-md-3" id="">
                                <label class="control-label">Proof uploaded 1<span
                                        class="text-danger">*</span></label>
                                <div id="file_uploaded"></div>
                            </div>
							<div class="form-group col-md-3" id="">
                                <label class="control-label">Proof uploaded 2<span
                                        class="text-danger">*</span></label>
                                <div id="file_import"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form" id="form_summary" name="form_summary">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th> 
                                            <th>Purchase A/C</th> <!-- 0 -->
                                            <th>Date</th> <!-- 2 -->
                                            <th>Description</th>
                                            <th>Taxable Amount</th> <!-- 3 -->
                                            <th>PPN</th>
                                            <th>WHT Amount</th>
                                            <th>Proj No</th> <!-- 4  -->
                                            <th>WHT</th>
                                            <th>Row No</th> <!-- 5 -->
                                            <th>Locked</th> <!-- 6 -->
                                            <th>Actions</th> <!-- 7 -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4">Total</th>
                                            <th>0</th> <!-- Nominal -->
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th> 
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 offset-md-6 text-right">
                                <table class="display compact nowrap table-hover mt-3" cellspacing="0" width="100%">
                                    <tfoot>

                                        <tr>
                                            <td class="text-right">Taxable Amount : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="total_expense" name="total_expense" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">PPN : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="ppn" name="ppn" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">WHT : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="wht_amount" name="wht_amount" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">Total Payment : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable" id="net_total" name="net_total" value="0" onfocus="this.select()" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--PAYMENT METHOD -->
    <div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="modalPaymentLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment Method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="table-payment" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Bank</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAYMENT METHOD -->
</div>
<script type="text/javascript">
var data4DataTable = [];

// START VARIABEL WAJIB
var Modules = '<?=$modules?>';
var Controller = '<?=$controller?>';
var Priv = JSON.parse('<?=json_encode($priv_arr)?>');


function getEmployeeID(){
	$.ajax({
		"url":base_url + `finance/Expenseclaim/getEmployeeID`,
		"dataType":"JSON",
		"method": "GET",
		"success" : function(data){
			$.each(data,function(key,val){
				$("#code_employee").val(val.nik);
				$("#code_dept").val(val.code_dept);
				$("#code_jabatan").val(val.code_jabatan);

			})
		}
	})
}

function LOVEmail() {
	let employee_id = $("#code_employee").val();
	let parameters = {
		employee_id: employee_id,
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/Expenseclaim/getEmail/home/' + jsonWhere, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee', 'email']);
		$('#list_controls').val(['#nik', '#name_employee', '#email']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#employee_id').val()) return;
			$('#email').val();
		});
	});
}
</script>
<script>
(function() {
	initPage()
	getData();
})();


function initPage() {	
	initDataTable();
	// console.log('initpage');
	// initDataTableRiwayatHarga();
	initDatePicker();
	initValidation();
	// initOtherElements();
	// if (action == 'edit') getData();
	clearForm();
	// getEmployeeID();
	// get_payment_method();
}

uang_cash = 3500000;

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"bPaginate": false,
		"ordering": false,
		"info": false,
		"scrollX": true,
		"scrollY": "250px",
		"data": data4DataTable,
		"columns": [
			{width: 10},
			{
				"data": "code_coa", "className": "text-center", width: 200,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
									<input type="hidden" name="detail_id_${meta.row}" id="detail_id_${meta.row}" value="${row.detail_id}" readonly="readonly" />
									<input type="hidden" name="code_coa_${meta.row}" id="code_coa_${meta.row}" value="${row.code_coa}" readonly="readonly" />
									<input type="text" class="form-control ${row.locked == 1 ? 'disable' : ''} input-pencarian" placeholder="Entry Code COA" name="pencarian_${meta.row}" id="pencarian_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.code_coa}" autocomplete="off" ${row.locked == 1 ? 'readonly' : ''} />
							${row.locked == 1 ? ''
							:
							`<div class="input-group-append">
									<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" id="LOVItemButton_${meta.row}" onclick="LOVCoa('${meta.row}', '#code_coa_${meta.row}', '#pencarian_${meta.row}');"><i class="fas fa-search"></i></button>
								</div>`
						}
								</div>`;
				}
			},
			{
				"data": "tanggal", "className": "text-center", width: 140,
				"render": function (data, type, row, meta) {
						return `<div class="input-group date">
									<input type="text" class="form-control font-size-sm ${row.locked == 1 ? 'disable' : 'input-tanggal'}" placeholder="Date" id="tanggal_${meta.row}" name="tanggal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${moment(row.tanggal).format('DD/MM/YYYY')}" readonly />
									<div class="input-group-append">
										<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
									</div>
								</div>`;
				},
			},
			{ "data": "name_coa", "Classname": "text-left" },
			{
				"data": "nominal", "className": "text-right", width: 150,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
							 <input type="text" class="form-control text-right input-nominal" id="nominal_${meta.row}" name="nominal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Nominal" value="${row.nominal}" onfocus="this.select()" autocomplete="off" />
							 </div>`;
				}
			},
			{
				"data": "tax1", "className": "text-right", width: 150,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
					<input type="text" class="form-control text-right input-tax1" id="tax1_${meta.row}" name="tax1_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Tax1" value="${row.tax1}" onfocus="this.select()" autocomplete="off" />
					</div>`;
				}
			},
			{
				"data": "tax3", "className": "text-right", width: 150,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
					<input type="text" class="form-control text-right input-tax3" id="tax3_${meta.row}" name="tax3_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="tax3" value="${row.tax3}" onfocus="this.select()" autocomplete="off" />
					</div>`;
				}
			},
			{
				"data": "remark", "className": "text-left", width: 150,
				"render": function (data, type, row, meta) {
					return `
					<div class="input-group">
					<input type="text" class="form-control input-remark" placeholder="Pick Store" name="remark_${meta.row}" id="remark_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.remark}" onfocus="this.select()" autocomplete="off" readonly />
					<div class="input-group-append">
					<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVStore(${meta.row});"><i class="fas fa-search"></i></button>
					</div></div>`;
				}
			},
			{
				"data": "wht", "className": "text-left", width: 150,
				"render": function (data, type, row, meta) {
					return `
					<div class="input-group">
					<input type="text" class="form-control input-wht" placeholder="Pick WHT" name="wht_${meta.row}" id="wht_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.wht}" onfocus="this.select()" autocomplete="off" readonly />
					<div class="input-group-append">
					<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVWht(${meta.row});"><i class="fas fa-search"></i></button>
					</div></div>`;
				}
			},			
			{ "data": "row_no", "visible": false },
			{ "data": "locked", "visible": false },
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';
					actions += `<button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light add_coa" data-index="${meta.row}" id="delete_${meta.row}" name="add_coa"><i class="glyphicon glyphicon-check"></i></button>`;

					actions += `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
					return actions;
				},
			},
		],
		columnDefs: [ { "defaultContent": "-", "targets": "_all" } 
		],
		"footerCallback": function (row, data, start, end, display) {
			let api = this.api();
			
			let totalQty = api.column(4).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(4).footer()).html(accounting.formatNumber(totalQty));

			let totalPPn = api.column(5).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(5).footer()).html(accounting.formatNumber(totalPPn));
			
			let totalWht = api.column(6).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(6).footer()).html(accounting.formatNumber(totalPPn));

			// let totalWht = api.column(7).data().reduce(function (prevVal, nextVal) {
				
			// })

			// let total = accounting.unformat($('#total_qty').val(), _decimalSeparator);
			let total = totalQty + totalPPn  - totalWht;
			// $('#total_qty').val(totalQty);
			$('#total_expense').val(accounting.formatNumber(totalQty, 0));
			$('#ppn').val(accounting.formatNumber(totalPPn, 0));
			$('#wht_amount').val(accounting.formatNumber(totalWht, 0));
			$('#net_total').val(accounting.formatNumber(total, 0));

			// hitungTotal();
		},
	}).on('draw.dt', function (e, settings, json, xhr) {
		// === START pencarian item
		$('.input-pencarian').on('keypress', function (e) {
			if (e.charCode == 13) {
				e.preventDefault();
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				if ($(this).val().trim() == "") return;
				if (lockedRow == 1) return;
				let parameters = 'code_coa=' + $(this).val().trim();

				loadingProcess();
				ajaxNew(base_url + 'finance/purchase/get_coa?' + parameters, null, 'GET')
					.then((data) => {
						if (data.result) {
							let row = data.data;
							let data2save = {
								detail_id: $('#detail_id_' + indexRow).val(),
								code_coa: row.code_coa,
								tanggal: row.tanggal,
								name_coa: row.name_coa,
								nominal: 0,
								tax1: 0,
								tax3: 0,
								remark: row.remark,
								wht: row.wht,
								row_no: moment().format('YYYYMMDDHHmmssSSS'),
								locked: 0
							};
							// Cek kode item duplikat
							let cek = null;
							if (row.tanggal == 1) {
								cek = _dataTable.rows().data().toArray().filter(data => {
									return $('#pencarian_' + indexRow).val() == data.code_coa;
								});
	
								if ((cek.length > 1 && DataTableAction == 'create') ||
									(cek.length > 1 && DataTableAction == 'edit' && $('#pencarian_' + indexRow).val() != _dataTable.cek(indexRow).data().code_coa)) {
									MsgBox.Notification('Code Coa was exist!', 'Peringatan', 'warning', true);
									return;
								}
							} else {
								// Cek kode item duplikat
								cek = _dataTable.rows().data().toArray().filter(data => {
									return $('#pencarian_' + indexRow).val() == data.code_coa;
								});

								if (cek.length > 1) {
									MsgBox.Notification('Code Coa was exist!', 'Peringatan', 'warning', true);
									return;
								}
							}
												
							// Simpan data
							DataTableAction = 'edit';
							SimpanRowDataTable(_dataTable, data2save, indexRow);
							DataTableAction = 'create';
							if (row.tanggal == 1) {
								$('#tanggal_' + indexRow).focus();
							} else {
								$('#nominal_' + indexRow).focus();
							}							
							loadingProcess(false);
						} else {
							// MsgBox.Notification(data.message.toString());
							loadingProcess(false);
							LOVCoa(indexRow, '#code_coa_' + indexRow, '#pencarian_' + indexRow, $(this).val().trim());
						}
					})
					.catch((err) => {
						MsgBox.Notification(err.toString());
					});
			}
		});
		// === END pencarian item

		// === START expired date
		$('.input-tanggal').datepicker({
			format: 'dd/mm/yyyy',
			startDate: subtractMonths(3),
			autoclose: true,
		});

		$('.input-tanggal').on('changeDate', function (e) {
			let getIndex = e.target.id.split("_");
			let indexRow = getIndex[getIndex.length - 1];
			_dataTable.cell(indexRow, 2).data(moment(e.date).format('YYYY-MM-DD'));
			_dataTable.rows().invalidate().draw();
			$('#tanggal_' + indexRow).datepicker('hide');
			$('#tanggal_' + indexRow).datepicker('destroy');
			$('#nominal_' + indexRow).focus();
		});
		// === END expired date

		// === START Nominal

		$('.input-nominal').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			if (e.charCode == 13 || e.charCode == 9) {
				_dataTable.rows().draw();
				$('#tax1_' + indexRow).focus();
			}
		});

		$('.input-nominal').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			// Update Qty
			_dataTable.cell(indexRow, 4).data(accounting.unformat($(this).val(), _decimalSeparator));
			// Jumlah
			let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
			// _dataTable.cell(indexRow, 3).data(jumlah);
			_dataTable.rows().invalidate().draw();
		});

		$('.input-tax1').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			if (e.charCode == 13 || e.charCode == 9) {
				_dataTable.rows().draw();
				$('#tax3_' + indexRow).focus();
			}
		});

		$('.input-tax1').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			// Update Qty
			_dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator));
			// Jumlah
			let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
			// _dataTable.cell(indexRow, 3).data(jumlah);
			_dataTable.rows().invalidate().draw();
		});
		// $('.input-tax2').on('keypress', function (e) {
		// 	e.preventDefault();
		// 	let indexRow = $(this).data('index');
		// 	let lockedRow = $(this).data('locked');
		// 	if (e.charCode == 13 || e.charCode == 9) {
		// 		_dataTable.rows().draw();
		// 		$('#remark_' + indexRow).focus();
		// 	}
		// });

		// $('.input-tax2').on('blur', function (e) {
		// 	e.preventDefault();
		// 	let indexRow = $(this).data('index');
		// 	let lockedRow = $(this).data('locked');
		// 	// Update Qty
		// 	_dataTable.cell(indexRow, 6).data(accounting.unformat($(this).val(), _decimalSeparator));
		// 	// Jumlah
		// 	let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
		// 	// _dataTable.cell(indexRow, 3).data(jumlah);
		// 	_dataTable.rows().invalidate().draw();
		// });
		// $('.input-tax2').on('change', function (e) {
		// 	e.preventDefault();
		// 	let indexRow = $(this).data('index');
		// 	let lockedRow = $(this).data('locked');
		// 	if (e.charCode == 13 || e.charCode == 9) {
		// 		_dataTable.rows().draw();
		// 		$('#tax3_' + indexRow).focus();
		// 	}
		// });

		// $('.input-tax2').on('blur', function (e) {
		// 	e.preventDefault();
		// 	let indexRow = $(this).data('index');
		// 	let lockedRow = $(this).data('locked');
		// 	// Update Qty
		// 	_dataTable.cell(indexRow, 8).data(accounting.unformat($(this).val(), _decimalSeparator));
		// 	// Jumlah
		// 	let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
		// 	// _dataTable.cell(indexRow, 3).data(jumlah);
		// 	_dataTable.rows().invalidate().draw();
		// });
		
		$('.input-tax3').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			if (e.charCode == 13 || e.charCode == 9) {
				_dataTable.rows().draw();
				$('#remark_' + indexRow).focus();
			}
		});

		$('.input-tax3').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			// Update Qty
			_dataTable.cell(indexRow, 6).data(accounting.unformat($(this).val(), _decimalSeparator));
			// Jumlah
			let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
			// _dataTable.cell(indexRow, 3).data(jumlah);
			_dataTable.rows().invalidate().draw();
		});
		// === END qty

		
		$('.input-nominal').autoNumeric('init', formatNumber);
		$('.input-tax1').autoNumeric('init', formatNumber);
		// $('.input-tax2').autoNumeric('init', formatNumber);
		// $('.input-tax2').val();
		$('.input-tax3').autoNumeric('init', formatNumber);
		$('.input-remark').val();
		// hitungTotal();
	});

	_dataTable.on('order.dt search.dt', function () {
        let i = 1;
 
        _dataTable.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
            this.data(i++);
        });
    }).draw();

	setTimeout(() => {
		_dataTable.rows().invalidate().draw();
		// if (action == 'create') addDTRow();
		setTimeout(() => {
			$('#pencarian_' + (_dataTable.data().count() - 1)).focus();
		}, 200);
	}, 500);
}
// add coa
$(document).on('click',".add_coa",function(){
	let indexRow = $(this).data('index');
	let lockedRow = $(this).data('locked');
	// Check jika lockedRow = 1
	if (lockedRow == 1) {
		_dataTable.rows().draw();
		flagItemEndRow = true;

		indexRow++;
		if ($('#nominal_' + indexRow).length) {
			if ($('#nominal_' + indexRow).data('locked') == 1) {
				$('#nominal_' + indexRow).focus();
			} 
			
			else {
				$('#pencarian_' + indexRow).focus();
			}
		}
		indexRow--;
		return; 
	}
	
	let row = null;
	// Buat row baru dan fokus ke pencarian
	// if (DataTableAction == 'create' || DataTableAction =='edit') {
		// Tambah row baru
		addDTRow();
	// }

	// Locked
	_dataTable.cell(indexRow, 9).data(1);
	_dataTable.rows().invalidate().draw(); //ubah disini
	flagItemEndRow = true;
	indexRow++;
	$('#pencarian_' + indexRow).focus();
})
function subtractMonths(numOfMonths, date = new Date()) {
	date.setMonth(date.getMonth() - numOfMonths);
  
	return date;
}
function initDatePicker() {
	$('#inv_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		
	}).datepicker("setDate", new Date());

	$('#due_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		// startDate: subtractMonths(3),
	}).datepicker("setDate", new Date());

	$('#periode_akhir').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		startDate: subtractMonths(3),
	}).datepicker("setDate", new Date());

	$('#tgl_jatuh_tempo').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			doc_date: {
				DateID: true,
				required: true,
			},
		},
	});
}

function initOtherElements() {
	$('#doc_date').on('changeDate', function (e) {
		$('#tanggal').datepicker("setDate", moment(e.date).add(14, 'days').format('DD/MM/YYYY'));
	});

	// $('#top').on('change', function (e) {
	// 	$('#tanggal').datepicker("setDate", moment($('#doc_date').val(), 'DD/MM/YYYY').add($(this).val(), 'days').format('DD/MM/YYYY'));
	// });

	// $('#ppn').on('change', function (e) {
	// 	if ($(this).val() == 'I' || $(this).val() == 'E') {
	// 		$("#ppn_persen").prop("readonly", false).removeClass("disable");
	// 		$("#ppn_nilai").prop("readonly", false).removeClass("disable");
	// 		$("#ppn_persen").val(10);
	// 		$("#ppn_nilai").val(0);
	// 	} else {
	// 		$("#ppn_persen").prop("readonly", true).addClass("disable");
	// 		$("#ppn_nilai").prop("readonly", true).addClass("disable");
	// 		$("#ppn_persen").val(0);
	// 		$("#ppn_nilai").val(0);
	// 	}
	// 	hitungTotal()
	// });

	// $("#nominal").on('change', function (e) {
	// 	hitungTotal();
	// });
	// $("#remark").on('keyore', function (e) {
	// 	hitungTotal();
	// });
	// $("#ppn_persen").prop("readonly", true).addClass("disable");
	// $("#ppn_nilai").prop("readonly", true).addClass("disable");

	// $('#ppn_persen').on('keyup', function (e) {
	// 	hitungTotal();
	// });

	// $('#ppn_nilai').on('keyup', function (e) {
	// 	hitungTotal(false);
	// });

	// $('#level_harga').on('change', function (e) {
	// 	changeLevelHarga();
	// });

	// $('#pembayaran').on('click', function () {
	// 	$('#modalFormPembayaran').modal('show');
	// });

	// $('#modalFormPembayaran').on('shown.bs.modal', function () {
	// 	$('#total_pembelanjaan').val($('#total').val());
	// 	hitungKembalian();
	// 	hitungTotal();
	// });

	// $('#terapkan_promo').on('click', function () {
	// 	applyPromo();
	// });

	// $('#pembayaran_cash, #pembayaran_debit_nilai, #pembayaran_credit_nilai').on('keyup', function (e) {
	// 	hitungKembalian();
	// });

	$('#total_expense').autoNumeric('init', formatDecimalNumber);
	$('#sisa_pettycash').autoNumeric('init', formatDecimalNumber);
	$('#pettycash_store').autoNumeric('init', formatDecimalNumber);
}

// function hitungKembalian() {
// 	let totalPembelanjaan = accounting.unformat($('#total_pembelanjaan').val(), _decimalSeparator);
// 	let pembayaranCash = accounting.unformat($('#pembayaran_cash').val(), _decimalSeparator);
// 	let pembayaranDebit = accounting.unformat($('#pembayaran_debit_nilai').val(), _decimalSeparator);
// 	let pembayaranCredit = accounting.unformat($('#pembayaran_credit_nilai').val(), _decimalSeparator);
// 	let totalPembayaran = pembayaranCash + pembayaranDebit + pembayaranCredit;
// 	let sisaKembalian = totalPembayaran - totalPembelanjaan;
// 	$('#total_pembayaran').val(accounting.formatNumber(totalPembayaran, 2));
// 	$('#sisa_kembalian').val(accounting.formatNumber(sisaKembalian, 2));
// }

function hitungTotal() {
	// hitngPPN(hitungDariPersen);
	let total = accounting.unformat($('#total_qty').val(), _decimalSeparator);
	let sisa = uang_cash - total;
	// $('#total_expense').val(accounting.formatNumber(total, 0));
	$('#sisa_pettycash').val(accounting.formatNumber(sisa, 0));
	// hitungKembalian();
}

function Kembali() {
	window.location.href = '#' + Modules + '/' + Controller;
}

async function Simpan() {
	// Check qty
	// if (accounting.unformat($('#total_qty').val(), _decimalSeparator) <= 0) {
	// 	MsgBox.Notification('Periksa kembali inputan anda', 'Peringatan', 'warning');
	// 	return;
	// }
	// Check detail
	if (_dataTable.data().count() <= 0) {
		MsgBox.Notification('Detail was not found');
		return;
	}
	
	// Hitung total
	// await hitungTotal();
	

	// save
	console.log('mulai save');
	MsgBox.Confirm('Save data?').then(result => {
		if (!result) return;
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		let detail = JSON.stringify(_dataTable.rows().data().toArray());
		let summary = $('#form_summary').serializeArray();
		let _data2Send = action === 'create' ? $('#form_input').serialize() + + '&' + summary + '&detail=' + detail : $('#form_input').serialize() + '&' + summary + '&detail=' + detail + '&id=' + _id;
		
		// var file_data = $('#file_expense').prop('files')[0];   
		var form_data = new FormData();         
		form_data.append('file', file_data);   
		

		// const fileupload = $('#file_expense').prop('files')[0]

		let formData = new FormData();
		// formData.append('file_expense', fileupload);
		formData.append('code_creditor',$("#code_creditor").val());
		formData.append('desc_creditor',$("#desc_creditor").val());
		formData.append('company_code',$("#company_code").val());
		formData.append('inv_date',$("#inv_date").val());
		formData.append('email',$('#email').val());
		// formData.append('inv_desc',$("#description").val());
		formData.append('supplier_iv',$("#supplier_iv").val());
		formData.append('ref_no',$("#ref_no").val());
		// formData.append('expense_period',$("#expense_period").val());
		formData.append('due_date',$("#due_date").val());
		formData.append('description',$("#description").val());
		for(let i=0; i<summary.length;i++){
			formData.append(`${summary[i].name}`,`${summary[i].value}`)
		}
		formData.append('detail',detail);
		
		
		$.ajax({
			type: 'POST',
			url: url,
			data: formData,
			processData: false,
    		contentType: false,
			success: function (data) {
				// data = JSON.parse(data);
				if (data.result) {
					console.log('success');
					$('#inv_no').val(data.data.inv_no);
					_id = data.data.inv_no;
					action = 'edit';
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					Kembali();
				} else {
					MsgBox.Notification(data.msg.toString());
				}
			},
			error: function (err) {
				MsgBox.Notification("Failed to Save Data. Cek your Internet Access!");
			}
		})



		// ajaxNew(url, _data2Send, 'POST')
		// 	.then((data) => {
				// if (data.result) {
				// 	$('#no_doc').val(data.data.no_doc);
				// 	_id = data.data.no_doc;
				// 	action = 'edit';
				// 	MsgBox.Notification(data.message.toString(), 'bottom right', 'success');
				// 	Kembali();
				// } else {
				// 	MsgBox.Notification(data.message.toString());
				// }
		// 	})
		// 	.catch((err) => {
		// 		MsgBox.Notification(err.toString());
		// 	});
	}).catch(err => {
		console.log(err);
	});
}

function getData() {
    let _id = $("#inv_no").val()
    let url = base_url + Modules + '/' + Controller + `/getData2Edit/json/${_id}`;
    ajaxNew(url, null, 'GET')
        .then((data) => {
           
            if (data.result) {
                let row = data.data;

                $('#inv_no').val(row.inv_no);
                $('#inv_date').datepicker("setDate", moment(row.inv_date).format('DD/MM/YYYY'));
                $('#due_date').datepicker("setDate", moment(row.due_date).format('DD/MM/YYYY'));
				$('#code_creditor').val(row.code_creditor); $('#desc_creditor').val(row.desc_creditor);
                $('#company_name').val(row.company_name);
                $('#company_code').val(row.code_company); $('#company_name').val(row.name); $('#categories').val(row.categories);
				$('#supplier_iv').val(row.supplier_iv);
				$('#ref_no').val(row.ref_no);
				$('#description').val(row.description);
                
                if (row.attachment_file) {
                    $("#file_uploaded").html(
                        `<a href="<?= base_url('assets/upload')?>/${row.attachment_file}" target="_BLANK"><span class="text-success">Check File Uploaded</span></a>`
                        )
                }
                if (row.attachment_file) {
                    $("#file_import").html(
                        `<a href="<?= base_url('assets/upload')?>/${row.attachment_file_2}" target="_BLANK"><span class="text-success">Check File Uploaded</span></a>`
                        )
                }

                if (row.status == 5) {
                    $("#send_email").html(`<div class="form-group">
                           <label>Email <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                              <input type="hidden" name="name_employee" id="name_employee" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Pilih Email" name="email" id="email" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmail();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>`)
                }
                data4DataTable = data.detail;
                _dataTable.clear().draw();
                setTimeout(() => {
                    _dataTable.rows.add(data.detail).draw();
                    setTimeout(() => {}, _dataTable.data().count() * 100);
                }, 500);
            } else {
                MsgBox.Notification(data.message.toString());
            }
        })
        .catch((err) => {
            MsgBox.Notification(err.toString());
        });
}

function clearForm() {
	action = 'create';

	$('#doc_date').datepicker("setDate", moment().format('DD/MM/YYYY'));
	$('#tanggal').datepicker("setDate", moment().format('DD/MM/YYYY'));
	$('#top').val(0);

	selectedKaryawanId = null;
   selectedCustomerId = null;
   selectedCOACode = null;
   selectedCOACodeDebit = null;
   selectedCOACodeCredit = null;
   flagTerapkanPromoDiskon = false;
   flagItemEndRow = false;
}

// ======= START DATATABLE

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {
	if (DataTableAction == 'edit') {
		DataTableElement.row(RowIdx).data(data2save);
		DataTableElement.rows().invalidate().draw();
	} else {
		DataTableElement.row.add(data2save).draw();
	}
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}

// ======= END DATATABLE

// ======= START DATATABLE Detail

function addDTRow() {
	let data2save = {
		detail_id: 0,
		code_coa: "",
		tanggal: moment().format('YYYY-MM-DD'),
		name_coa: "",
		nominal: 0,
		tax1: 0,
		tax3: 0,
		remark: "",
		wht: "",
		row_no: moment().format('YYYYMMDDHHmmssSSS'),
		locked: 0
	};
	// Simpan data
	DataTableAction = 'create';
	SimpanRowDataTable(_dataTable, data2save);
	DataTableAction = 'edit';
}

function deleteDetail(RowIdx) {
	let count_row = _dataTable.rows().count()
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Delete ' + getData.code_coa + ' with expired date ' + moment(getData.expired_date).format('DD/MM/YYYY') + ' from detail?', 'Delete detail').then(result => {
		if(count_row <= 1){
			MsgBox.Notification("Delete row failed, Coa has been not empty ...", 'bottom right', 'error');
			return;
		}
		HapusRowDataTable(_dataTable, RowIdx);
	}).catch(err => {
		if (err) console.log(err);
	});
}

// ======= END DATATABLE Detail
function LOVCreditor(){
	$('#PopUpModal').load(base_url + 'finance/Purchase/getCreditorList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['code_creditor', 'desc_creditor']);
		$('#list_controls').val(['#code_creditor', '#desc_creditor']);
	}); 
}
function getEmployeeID(){
	$.ajax({
		"url":base_url + `finance/Expenseclaim/getEmployeeID`,
		"dataType":"JSON",
		"method": "GET",
		"success" : function(data){
			$.each(data,function(key,val){
				$("#code_employee").val(val.nik);
				$("#code_dept").val(val.code_dept);
				$("#code_jabatan").val(val.code_jabatan);

			})
		}
	})
}

function get_payment_method(){
	$.ajax({
		"url":base_url + `finance/Expenseclaim/getPaymentMethod`,
		"dataType":"JSON",
		"method": "GET",
		"success" : function(data){
			let html="<option>Select Payment Method</option>";
			$.each(data,function(key,val){
				html += `<option value="${val.code_coa}">${val.name_coa}</option>`;
			})

			$("#payment_method").html(html)
		}
	})
}
function LOVEmployee() {
	$('#PopUpModal').load(base_url + 'finance/Expenseclaim/getEmployeeList/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-lg modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee','code_dept','name_dept','code_jabatan','name_jabatan']);
		$('#list_controls').val(['#code_employee', '#name_employee','#code_dept','#dapartement',"#code_jabatan",'#position']);
	});

	
}

function LOVPayment(){
		$.ajax({
			url:base_url + 'finance/Expenseclaim/getPayment',
			dataType:"JSON",
			success:function(data){
				console.log(data)
			}
		})
	
	$("#modalPayment").modal("show");
}

function LOVCoa(RowIdx, elItemId = null, elPencarian = null, search = '') {
	let parameters = {
		code_coa: $(elPencarian).val().trim(),
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/Purchase/get_coa_lov/home/' + jsonWhere + '?search=' + search, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code_coa', 'code_coa']);
		$('#list_controls').val([elItemId, elPencarian]);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!_resultFromLOV) return;

			// Cek kode item & tanggal expire duplikat
			let row = null;
			row = _dataTable.rows().data().toArray().filter(data => {
				return $('#code_coa_' + RowIdx).val() == data.code_coa;
			});

				let data2save = {
					detail_id: 0,
					code_coa: _resultFromLOV.code_coa,
					name_coa: _resultFromLOV.name_coa,
					nominal: 0,
					tax1: 0,
					tax3: 0,
					remark: '',
					wht: '',
					row_no: moment().format('YYYYMMDDHHmmssSSS'),
					locked: 0
				};
				// Simpan data
				DataTableAction = 'edit';
				SimpanRowDataTable(_dataTable, data2save, RowIdx);
				DataTableAction = 'create';
				$('#tanggal_' + RowIdx).focus();

			_resultFromLOV = null;
		});
	});
}

function LOVEmail() {
	let employee_id = $("#code_employee").val();
	let parameters = {
		employee_id: employee_id,
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/Purchase/getEmail/home/' + jsonWhere, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee', 'email']);
		$('#list_controls').val(['#nik', '#name_employee', '#email']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#employee_id').val()) return;
			$('#email').val();
		});
	});
}

function LOVCompany(){
	$('#PopUpModal').load(base_url + 'finance/Purchase/getCompList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['company_code', 'company_name','categories']);
		$('#list_controls').val(['#company_code', '#company_name','#categories']);
	});
}

function LOVStore(row) {
	
	$('#PopUpModal').load(base_url + 'finance/expenseclaim/getStore/', () => { // Ambil URL untuk membuka modal LOV
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['m_code', 'm_shortdesc', 'm_pic', 'name']);
		$('#list_controls').val([`#remark_${row}`, `#remark_${row}`, '#m_pic', '#m_type']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$(`#remark_${row}`).val()) return;
				_dataTable.cell(row, 7).data($(`#remark_${row}`).val());
				_dataTable.rows().invalidate().draw();
			// $(`#remark_${row}`).val();
			// get_lastdate_pt($('#m_code').val());
		});
	});
}

function LOVWht(row) {
	
	$('#PopUpModal').load(base_url + 'finance/purchase/getwht/', () => { // Ambil URL untuk membuka modal LOV
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code_wht', 'name_wht']);
		$('#list_controls').val([`#wht_${row}`, `#wht_${row}`]);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$(`#wht_${row}`).val()) return;
				_dataTable.cell(row, 8).data($(`#wht_${row}`).val());
				_dataTable.rows().invalidate().draw();
			// $(`#remark_${row}`).val();
			// get_lastdate_pt($('#m_code').val());
		});
	});
}
function LOVCustomer() {
	$('#PopUpModal').load(base_url + 'master/member/get_member_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['cust_id', 'cust_name', 'type_member_id', 'type_member_name', 'level']);
		$('#list_controls').val(['#cust_id', '#cust_name', '#type_member_id', '#cust_type', '#cust_type_lvl']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedCustomerId == $('#cust_id').val() || !$('#cust_id').val()) return;
			selectedCustomerId = $('#cust_id').val();
			$('#cust_name').val(selectedCustomerId + ' - ' + $('#cust_name').val());
			$('#type_member_name').text($('#cust_type').val());

			$('#tgl_jatuh_tempo').datepicker("setDate", moment($('#doc_date').val(), "DD/MM/YYYY").add(14, 'days').format('DD/MM/YYYY'));
			getMemberPriceLevel($('#type_member_id').val());
		});
	});
}

function LOVCOADebit() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#pembayaran_debit_coa_code', '#pembayaran_debit_coa_name']);
		$('#modalFormPembayaran').modal('hide');

		$('#ModalLOV').on('hidden.bs.modal', function () {
			$('#modalFormPembayaran').modal('show');

			if (selectedCOACodeDebit == $('#pembayaran_debit_coa_code').val() || !$('#pembayaran_debit_coa_code').val()) return;
			selectedCOACodeDebit = $('#pembayaran_debit_coa_code').val();
			$('#pembayaran_debit_coa_name').val(selectedCOACodeDebit + ' - ' + $('#pembayaran_debit_coa_name').val());
		});
	});
}

function LOVCOACredit() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#pembayaran_credit_coa_code', '#pembayaran_credit_coa_name']);
		$('#modalFormPembayaran').modal('hide');

		$('#ModalLOV').on('hidden.bs.modal', function () {
			$('#modalFormPembayaran').modal('show');

			if (selectedCOACodeCredit == $('#pembayaran_credit_coa_code').val() || !$('#pembayaran_credit_coa_code').val()) return;
			selectedCOACodeCredit = $('#pembayaran_credit_coa_code').val();
			$('#pembayaran_credit_coa_name').val(selectedCOACodeCredit + ' - ' + $('#pembayaran_credit_coa_name').val());
		});
	});
}

async function Update() {
    // Check qty
    // if (accounting.unformat($('#total_qty').val(), _decimalSeparator) <= 0) {
    // 	MsgBox.Notification('Periksa kembali inputan anda', 'Peringatan', 'warning');
    // 	return;
    // }
    // Check detail
    if (_dataTable.data().count() <= 0) {
        MsgBox.Notification('Detail tidak ditemukan');
        return;
    }

    // Hitung total
    // await hitungTotal();


    // save
    MsgBox.Confirm('Save this data?').then(result => {
        if (!result) return;
        let _id = $("inv_no").val()
        let url = base_url + Modules + '/' + Controller + '/update/';
        let data_detail = _dataTable.rows().data().toArray();
        let datas_detail = [];

        $.each(data_detail, function(key, val) {
            datas_detail.push({
                detail_id: val.detail_id,
                code_coa: val.code_coa,
                tanggal: val.tanggal,
                name_coa: val.name_coa.replace(/\\n/g, ''),
                nominal: val.nominal,
				ppn: val.tax1,
				wht_amount: val.tax3,
                remark: val.remark,
				wht: val.wht,
                locked: val.locked
            })
        })
        let detail = JSON.stringify(datas_detail);
        let summary = $('#form_summary').serializeArray();
        _data2Send = $('#form_input').serialize() + '&' + summary + '&detail=' + detail;

        let payment_method = $("#payment_method").find(":selected").val()
        let formData = new FormData();
		// formData.append('file_expense', fileupload);
		formData.append('inv_no',$("#inv_no").val());
		formData.append('code_creditor',$("#code_creditor").val());
		formData.append('desc_creditor',$("#desc_creditor").val());
		formData.append('company_code',$("#company_code").val());
		formData.append('inv_date',$("#inv_date").val());
		formData.append('email',$('#email').val());
		// formData.append('inv_desc',$("#description").val());
		formData.append('supplier_iv',$("#supplier_iv").val());
		formData.append('ref_no',$("#ref_no").val());
		formData.append('code_rate',$("#code_rate").val());
		// formData.append('expense_period',$("#expense_period").val());
		formData.append('due_date',$("#due_date").val());
		formData.append('description',$("#description").val());
		for(let i=0; i<summary.length;i++){
			formData.append(`${summary[i].name}`,`${summary[i].value}`)
		}
		formData.append('detail',detail);

        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                loadingProcess(true);
            },
            success: function(data) {				
                if (data.result) {
					console.log('cek sukses');
                    $('#inv_no').val(data.data.inv_no);
                    _id = data.data.inv_no;
                    action = 'edit';
                    MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
                    Kembali();
                } else {
					console.log('cek gagal');
                    MsgBox.Notification(data.msg.toString());
                }               

            },
            error: function(err) {
                // loadingProcess(false);
                console.log(err);
                MsgBox.Notification("Please check your data");
            }
        })
        // formData.append('m_code',$("#m_code").val());
        // formData.append('m_shortdesc',$("#m_shortdesc").val());
        // formData.append('m_pic',$("#m_pic").val());
        // formData.append('m_type',$("#m_type").val());
        // formData.append('nik',$("#nik").val());
        // formData.append('name_employee',$("#name_employee").val());
        // formData.append('email',$("#email").val());
        // ajaxNew(url, _data2Send, 'POST')
        // 	.then((data) => {
        // if (data.result) {
        // 	$('#no_doc').val(data.data.no_doc);
        // 	_id = data.data.no_doc;
        // 	action = 'edit';
        // 	MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
        // 	Kembali();
        // 	// MsgBox.ConfirmNew(data.message.toString() + ' Tambah transaksi lain?').then(result => {
        // 	// 	if (result) {
        // 	// 		$('#form_input')[0].reset();
        // 	// 		clearForm();
        // 	// 		_dataTable.clear().draw();
        // 	// 		addDTRow();
        // 	// 	} else {
        // 	// 		Kembali();
        // 	// 	}
        // 	// }).catch((err) => {
        // 	// 	MsgBox.Notification(err.toString());
        // 	// });
        // } else {
        // 	MsgBox.Notification(data.msg.toString());
        // }
        // 	})
        // 	.catch((err) => {
        // 		MsgBox.Notification(err.toString());
        // 	});
    }).catch(err => {
        console.log(err);
    });
}
</script>