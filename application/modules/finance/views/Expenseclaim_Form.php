<style type="text/css">
.disable {
    background: #DDD !Important;
}

#po-form-selected {
    display: none;
}
</style>
<div class="main">
    <div class="row margin-bottom-15 padding-left-10">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali"
                onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
                Back </button>
            <div class="pull-right">
                <button type="button" class="btn btn-success waves-effect waves-light" id="simpan" name="simpan"
                    onclick="Simpan();"> <span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span>
                    Save and Send  </button>
            </div>
        </div>
    </div>
    <div class="row padding-horizontal-10">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-body">
                    <form id="form_input" name="form_input">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">Doc No. *</label>
                                <input class="form-control disable" placeholder="No. Document" id="no_doc" name="no_doc"
                                    value="" readonly />
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">Period <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Periode"
                                        id="periode_awal" name="periode_awal" value="<?=date('Y-m-d')?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                                <label class="control-label">up to <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control font-size-sm" placeholder="Periode"
                                        id="periode_akhir" name="periode_akhir" value="<?=date('Y-m-d')?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Company <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="company_code" id="company_code" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Select Company"
                                            name="company_name" id="company_name" value="" readonly="readonly" />
                                        <input type="hidden" name="categories" id="categories" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVCompany();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3" style="display:none;">
                                <div class="form-group">
                                    <label>Employee <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="code_employee" id="code_employee" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Select Employee"
                                            name="name_employee" id="name_employee" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVEmployee();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-3" style="display:none;">
                                <label class="control-label">Department</label>
                                <input type="hidden" class="form-control" placeholder="Dapartement" name="code_dept"
                                    id="code_dept" value="" readonly="readonly" />
                                <input type="text" class="form-control" placeholder="Dapartement" name="dapartement"
                                    id="dapartement" value="" readonly="readonly" />
                            </div>
                            <div class="form-group col-md-3" style="display:none;">
                                <label class="control-label">Position <span class="text-danger">*</span></label>
                                <input type="hidden" class="form-control" placeholder="Position" id="code_jabatan"
                                    name="code_jabatan" value="" readonly="readonly">
                                <input type="text" class="form-control" placeholder="Position" id="position"
                                    name="position" value="" readonly="readonly">
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                                        <input type="hidden" name="name_employee" id="name_employee" value=""
                                            readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="Choose Email" name="email"
                                            id="email" value="" readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" onclick="LOVEmail();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Perjadin No. <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="no_pejading" id="no_pejadin" placeholder="Pejadin Number" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" styel="display:none;">
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" class="form-control" name="description" id="description" placeholder="Description">
                                </div>
                            </div>
                        </div>
                        <h4>Transfer To</h4>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-3" id="">
                                <label class="control-label">Select Bank <span class="text-danger">*</span></label>
                                <select id="bank" name="bank" class="form-control">
                                    <option>Select Bank</option>
                                    <option value="Mandiri">Mandiri</option>
                                    <option value="Mandiri Syariah">Mandiri Syariah</option>
                                    <option value="BCA">BCA</option>
                                    <option value="BRI">BRI</option>
                                    <option value="Danamon">Danamon</option>
                                    <option value="CIMB Niaga">CIMB Niaga</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="">
                                <label class="control-label">Account Number <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Account Number" class="form-control"
                                    name="account_number" id="account_number">
                            </div>
                            <div class="form-group col-md-3" id="">
                                <label class="control-label">Account Name <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Account Name" class="form-control" name="account_name"
                                    id="account_name">
                            </div>
                        </div>
                        <h4>Payment Method</h4>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label>Payment Method <span class="text-danger">*</span></label>
                                    <select class="form-control" name="payment_method" id="payment_method">
                                        
                                    </select>
                                    <!-- <select class="form-control" name="code_atasan" id="code_atasan">
                                        <option value="">- Select Payment Method -</option>
                                        <?php foreach ($this->db->select('code_coa, name_coa')->where('code_coa','3010-1001')->get('m_coa')->result() as $key => $value): ?>
                                            <option value="<?php echo $value->code_coa ?>"><?php echo $value->name_coa ?></option>
                                        <?php endforeach?>
                                    </select> -->
                                </div>
                            </div>
                        </div>
                        <h4>Proof Documents</h4>
                        <div class="row padding-horizontal-10 mt-3">
                            <div class="form-group col-md-3" id="">
                                <label class="control-label">Proof Expense <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" name="file_expense" id="file_expense">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form" id="form_summary" name="form_summary">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_detail" class="display compact nowrap table" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Account</th> <!-- 0 -->
                                            <th>Date</th> <!-- 1 -->
                                            <th>Description</th> <!-- 2 -->
                                            <th>Amount</th> <!-- 3 -->
                                            <th>Project Code</th> <!-- 4  -->
                                            <th>Row No</th> <!-- 5 -->
                                            <th>Locked</th> <!-- 6 -->
                                            <th>Actions</th> <!-- 7 -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">Total</th>
                                            <th>0</th> <!-- Nominal -->
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 offset-md-6 text-right">
                                <table class="display compact nowrap table-hover mt-3" cellspacing="0" width="100%">
                                    <tfoot>

                                        <tr>
                                            <td class="text-right">Total : </td>
                                            <td>
                                                <div class="input-group text-right">
                                                    <input type="text" class="form-control text-right disable"
                                                        id="total_expense" name="total_expense"
                                                        placeholder="Total Expense" value="0" onfocus="this.select()"
                                                        readonly>
                                                </div>
                                            </td>
                                        </tr>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--PAYMENT METHOD -->
    <div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="modalPaymentLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment Method</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <table id="table-payment" class="table">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Nama Bank</th>
                           <th>Select</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAYMENT METHOD -->
    <!--begin::Modal Pembayaran-->
    <div class="modal fade" id="modalFormPembayaran" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="modalFormPembayaran" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pembayaran</small></h5>
                </div>
                <form class="form" id="form_pembayaran" name="form_pembayaran">
                    <div class="modal-body">
                        <!-- START RANGKUMAN PEMBAYARAN -->
                        <div class="row mt-3">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Total </label>
                                    <input type="text" class="form-control text-right disable" id="total_pembelanjaan"
                                        name="total_pembelanjaan" placeholder="Total Pembelanjaan" value="0"
                                        onfocus="this.select()" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Bayar </label>
                                    <input type="text" class="form-control text-right disable" id="total_pembayaran"
                                        name="total_pembayaran" placeholder="Total Pembayaran" value="0"
                                        onfocus="this.select()" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Sisa / Kembalian </label>
                                    <input type="text" class="form-control text-right disable" id="sisa_kembalian"
                                        name="sisa_kembalian" placeholder="Total" value="0" onfocus="this.select()"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <!-- END RANGKUMAN PEMBAYARAN -->

                        <hr />

                        <!-- START PEMBAYARAN CASH -->
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="form-group mt-2">
                                    <label>Cash</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right" id="pembayaran_cash"
                                        name="pembayaran_cash" placeholder="Pembayaran Cash" value="0"
                                        onfocus="this.select()">
                                    <small class="form-text text-muted text-right">Nilai pembayaran</small>
                                </div>
                            </div>
                        </div>
                        <!-- END PEMBAYARAN CASH -->

                        <!-- START PEMBAYARAN DEBIT -->
                        <div class="row mt-3">
                            <div class="col-lg-3">
                                <div class="form-group mt-2">
                                    <label>Debit</label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="hidden" name="pembayaran_debit_coa_code"
                                            id="pembayaran_debit_coa_code" value="" readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="No. COA Debit"
                                            name="pembayaran_debit_coa_name" id="pembayaran_debit_coa_name" value=""
                                            readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" id="LOVCOABankDebitButton" onclick="LOVCOADebit();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                    <small class="form-text text-muted">No. COA Debit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="pembayaran_debit_no"
                                        name="pembayaran_debit_no" placeholder="No. Kartu Debit" value=""
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">No. Kartu Debit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right" id="pembayaran_debit_nilai"
                                        name="pembayaran_debit_nilai" placeholder="Nilai Debit" value="0"
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">Nilai Debit</small>
                                </div>
                            </div>
                        </div>
                        <!-- END PEMBAYARAN DEBIT -->

                        <!-- START PEMBAYARAN CREDIT -->
                        <div class="row mt-3">
                            <div class="col-lg-3">
                                <div class="form-group mt-2">
                                    <label>Credit</label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="hidden" name="pembayaran_credit_coa_code"
                                            id="pembayaran_credit_coa_code" value="" readonly="readonly" />
                                        <input type="text" class="form-control" placeholder="No. COA Credit"
                                            name="pembayaran_credit_coa_name" id="pembayaran_credit_coa_name" value=""
                                            readonly="readonly" />
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-info btn-sm waves-effect waves-light"
                                                type="button" id="LOVCOABankCreditButton" onclick="LOVCOACredit();"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                    <small class="form-text text-muted">No. COA Credit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="pembayaran_credit_no"
                                        name="pembayaran_credit_no" placeholder="No. Kartu Credit" value=""
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">No. Kartu Credit</small>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right" id="pembayaran_credit_nilai"
                                        name="pembayaran_credit_nilai" placeholder="Nilai Credit" value="0"
                                        onfocus="this.select()">
                                    <small class="form-text text-muted">Nilai Credit</small>
                                </div>
                            </div>
                        </div>
                        <!-- END PEMBAYARAN CREDIT -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-info font-weight-bold" data-dismiss="modal"><span
                                class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--begin::Modal Pembayaran-->

    <!--begin::Modal riwayat harga-->
    <div class="modal fade" id="modalRiwayatHarga" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="modalRiwayatHarga" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Riwayat Harga</small></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="item_id_riwayat_harga" id="item_id_riwayat_harga" value=""
                                readonly="readonly" />
                            <table id="table_riwayat_harga" class="display compact nowrap table" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th>Outlet</th>
                                        <th>Tanggal</th>
                                        <th>Tanggal Expire</th>
                                        <th>Qty</th>
                                        <th>Harga Beli</th>
                                        <th>Harga</th>
                                        <th>Price Point</th>
                                        <th>Potongan</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info font-weight-bold" data-dismiss="modal"><span
                            class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!--begin::Modal riwayat harga-->

</div>

<script type="text/javascript">
// START VARIABEL WAJIB
var Modules = '<?=$modules?>';
var Controller = '<?=$controller?>';
var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
var data2Send = null;
var action = '<?=$action?>';
var _id = "<?=$action == 'edit' ? $id : 'null'?>";
// END VARIABEL WAJIB

var _dataTable = null;
var DataTableAction = 'create';
var DataTableRowIdx = 0;
var data4DataTable = [];

// var _dataRiwayatHarga = null;
// var data4DataTableRiwayatHarga = [];

// var selectedKaryawanId = null;
// var selectedCustomerId = null;
// var selectedCOACodeDebit = null;
// var selectedCOACodeCredit = null;
// var flagTerapkanPromoDiskon = false;
// var flagItemEndRow = false;

// LobiAdmin.loadScript([
//     '<?=base_url()?>assets/js/lib/autoNumeric/autoNumeric.js',
//     '<?=base_url()?>assets/js/lib/accounting/accounting.min.js',
// ], function() {
    LobiAdmin.loadScript([
        '<?=base_url()?>assets/js/lib/autoNumeric/init.js',
        '<?=base_url()?>assets/js/lib/accounting/init.js',
    ], function() {
        LobiAdmin.loadScript([
            '<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller +
            '.form.js?v=<?=date('YmdHis') . rand()?>',
        ], function() {
            initPage();
        });
    });
// });
</script>