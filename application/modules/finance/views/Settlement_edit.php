<style type="text/css">
.disable {
    background: #DDD !Important;
}

#po-form-selected {
    display: none;
}
</style>
<?php 
    $code_jabatan_login = $this->session->userdata('code_jabatan');
?>
<div class="main">
    <div class="row margin-bottom-15 padding-left-10">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-dark waves-effect waves-light" id="kembali" name="kembali"
                onclick="Kembali()"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
                Back </button>
				<?php if($code_jabatan_login != "PO001" || $code_jabatan_login != "PO002"){?>
					<div class="pull-right">
                <button type="button" class="btn btn-success waves-effect waves-light" id="simpan" name="simpan"
                    onclick="Update();"> <span class="btn-label"><i class="glyphicon glyphicon-floppy-saved"></i></span>
                    Save <?=$code_jabatan_login?> </button>
            </div>
            <?php } ?>
				
        </div>
    </div>
    <div class="row padding-horizontal-10">
        
        <?php if($code_jabatan_login == "PO002" || $code_jabatan_login == "PO001"){?>
            <div class="col-md-12 mt-3">
        <?php }else {?>
            <div class="col-md-12 mt-3">
            <?php }?>
            <div class="card">
                <div class="card-body">
                    <form id="form_input" name="form_input">
                        <?php 
                            if($code_jabatan_login == "PO002" || $code_jabatan_login == "PO001"){
                        ?>
                        <input type="hidden" name="update_action" id="update_action" value="AP">
                        <?php }else{?>
                            <input type="hidden" name="update_action" id="update_action" value="normal">
                        <?php }?>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">No. Doc *</label>
                                <input class="form-control disable" placeholder="No. Document" id="no_doc" name="no_doc"
                                    value="<?= $no_doc;?>" readonly />
                            </div>
                            <div class="form-group col-md-3" id="doc_date_container">
                        <label class="control-label">Period <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Period" id="periode_awal" name="periode_awal" value="<?=date('Y-m-d')?>">
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>                        
                     </div>
                     <div class="form-group col-md-3" id="doc_date_container">
                        <label class="control-label">until <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Periode" id="periode_akhir" name="periode_akhir" value="<?=date('Y-m-d')?>">
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <div class="form-group">
                           <label>Company <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="company_code" id="company_code" value="" readonly="readonly" />
                              <input type="hidden" name="categories" id="categories" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Select Company" name="company_name" id="company_name" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCompany();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>    
                  <div class="row">    
                  <div class="form-group col-md-3" style="display:none;" >
                        <div class="form-group">
                           <label>Employee <span class="text-danger">*</span></label>
                           <div class="input-group">
                           <input type="hidden" name="code_employee" id="code_employee" value="" readonly="readonly" />
                           <input type="text" class="form-control" placeholder="Select Employee" name="name_employee"
                              id="name_employee" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmployee();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>          
                     
                     <div class="form-group col-md-3" style="display:none;">
                        <label class="control-label">Department</label>
                        <input type="hidden" class="form-control" placeholder="Dapartment" name="code_dept" id="code_dept" value="" readonly="readonly" />
                        <input type="text" class="form-control" placeholder="Dapartement" name="dapartement" id="dapartement" value="" readonly="readonly" />
                     </div>
                     <div class="form-group col-md-3" style="display:none;">
                        <label class="control-label">Position <span class="text-danger">*</span></label>
                           <input type="hidden" class="form-control" placeholder="Position" id="code_jabatan" name="code_jabatan" value="" readonly="readonly">
                           <input type="text" class="form-control" placeholder="Position" id="position" name="position" value="" readonly="readonly">
                     </div> 
                     <div class="form-group col-md-3">
                        <div class="form-group">
                           <label>Email <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                              <input type="hidden" name="name_employee" id="name_employee" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Choose Email" name="email" id="email" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmail();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label class="control-label">ID Advanced <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" placeholder="ID Advanced" id="id_advanced" name="id_advanced" value="" required>
                         
                     </div> 
                     <div class="form-group col-md-3">
                        <label class="control-label">Received Date <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="text" class="form-control font-size-sm" placeholder="Received Date" id="received_date" name="received_date" value="<?=date('Y-m-d')?>">
                           <div class="input-group-append">
                              <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                           </div>
                        </div>       
                           
                     </div> 
                     <div class="form-group col-md-3">
                        <label class="control-label">Total Advanced <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" placeholder="Total Advanced" id="total_advanced" name="total_advanced" value="" required>
                         
                     </div> 
                  </div>
                  <div class="row">
                    <div class="form-group col-md-12">
                        <label class="control-label">Description</label>
                        <input type="text" class="form-control" placeholder="Description" id="description" name="description" value="" />
                    </div>
                  </div>
                  <h4>Transfer To</h4>
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Select Bank <span class="text-danger">*</span></label>
                        <select id="bank" name="bank" class="form-control">
                           <option>Select Bank</option>
                           <option value="Mandiri">Mandiri</option>
                           <option value="Mandiri Syariah">Mandiri Syariah</option>
                           <option value="BCA">BCA</option>
                           <option value="BRI">BRI</option>
                           <option value="Danamon">Danamon</option>
                        </select>
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Account Number <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Account Number" class="form-control" name="account_number" id="account_number">
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Account Name <span class="text-danger">*</span></label>
                        <input type="text" placeholder="Account Name" class="form-control" name="account_name" id="account_name">
                     </div>
                  </div>
                  <h4>Attachment Documents</h4>
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Expense <span class="text-danger">*</span></label>
                        <div id="file_expense"></div>
                     </div>
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Perjadin <span class="text-danger">*</span></label>
                        <div id="file_pejading"></div>
                     </div>
                  </div>
                 
                  <div class="row padding-horizontal-10 mt-3">
                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Refund <span class="text-danger">*</span></label>
                        <div id="file_refund"></div>
                     </div>

                     <div class="form-group col-md-3" id="">
                     <label class="control-label">Proof Advance <span class="text-danger">*</span></label>
                        <div id="file_cashbon"></div>
                     </div>
                  </div>
                 
               </form>
            </div>
         </div>
      </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form" id="form_summary" name="form_summary">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <button class="btn btn-primary" id="tambah_detail_btn">Tambah Detail</button> -->
                                <table id="table_detail" class="display compact nowrap table" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th> <!-- 0 -->
                                            <th>Account</th> <!-- 0 -->
                                            <th>Date</th> <!-- 1 -->
                                            <th>Description</th> <!-- 2 -->
                                            <th>Amount</th> <!-- 3 -->
                                            <th>Project Code</th> <!-- 4  -->
                                            <th>Row No</th> <!-- 5 -->
                                            <th>Locked</th> <!-- 6 -->
                                            <th>Actions</th> <!-- 7 -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">Total</th>
                                            <th></th> <!-- Nominal -->
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 offset-md-6 text-right">
                                <table class="display compact nowrap table-hover mt-3" cellspacing="0" width="100%">
                                    <tfoot>
									<tr>
                                 <td class="text-right">Total Expense: </td>
                                 <td>
                                    <div class="input-group text-right">
                                       <input type="text" class="form-control text-right disable" id="total_expense" name="total_expense" placeholder="Total Expense" value="0" onfocus="this.select()" readonly>
                                    </div>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="text-right">Refund Advance : </td>
                                 <td>
                                    <div class="input-group text-right">
                                       <input type="text" class="form-control text-right disable" id="sisa_pettycash" name="sisa_pettycash" placeholder="Sisa Pettycash" value="0" onfocus="this.select()" readonly>
                                    </div>
                                 </td>
                              </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- HISTORI -->
        <?php if($settlement_a){?>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>History</h4>
                            <table id="table_detail" class="display compact nowrap table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th> <!-- 0 -->
                                        <th>Date</th> <!-- 2 -->
                                        <th>Code Occupation</th> <!-- 3 -->
                                        <th>Email</th> <!-- 4  -->
                                        <th>Comment</th> <!-- 5 -->
                                        <th>Status</th> <!-- 6 -->

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                               $no = 1;
                               foreach($settlement_a as $pa):?>
                                    <tr>
                                        <td><?= $no++;?></td>
                                        <td><?= $pa['tanggal']?></td>
                                        <td><?= $pa['code_jabatan']?></td>
                                        <td><?= $pa['email']?></td>
                                        <td><?= $pa['comment']?></td>
                                        <td>
                                            <?php if($pa['status'] == 0){?>
                                            <span class="badge badge-warning">Revision</span>
                                            <?php }else if($pa['status'] == 9){?>
                                            <span class="badge badge-danger">Reject</span>
                                            <?php }else if($pa['status'] == 1){?>
                                            <span class="badge badge-success">Approve</span>
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php endforeach?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
        <!-- END HISTORI -->
    </div>
</div>

<script type="text/javascript">
var data4DataTable = [];

// START VARIABEL WAJIB
var Modules = '<?=$modules?>';
var Controller = '<?=$controller?>';
var Priv = JSON.parse('<?=json_encode($priv_arr)?>');


function getEmployeeID(){
	$.ajax({
		"url":base_url + `finance/Settlement/getEmployeeID`,
		"dataType":"JSON",
		"method": "GET",
		"success" : function(data){
			$.each(data,function(key,val){
				$("#code_employee").val(val.nik);
				$("#code_dept").val(val.code_dept);
				$("#code_jabatan").val(val.code_jabatan);

			})
		}
	})
}

function LOVEmail() {
	let employee_id = $("#code_employee").val();
	let parameters = {
		employee_id: employee_id,
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/Settlement/getEmail/home/' + jsonWhere, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee', 'email']);
		$('#list_controls').val(['#nik', '#name_employee', '#email']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#employee_id').val()) return;
			$('#email').val();
		});
	});
}
</script>

<script>
(function() {
    initPage()

})();


function initPage() {

    initDataTable();
    // initDataTableRiwayatHarga();
    initDatePicker();
    initValidation();
    // initOtherElements();
    getData();
    clearForm();
    // get_payment_method();
}

// function get_payment_method(){
// 	$.ajax({
// 		"url":base_url + `finance/Expenseclaim/getPaymentMethod`,
// 		"dataType":"JSON",
// 		"method": "GET",
// 		"success" : function(data){
// 			let html="<option>Select Payment Method</option>";
// 			$.each(data,function(key,val){
// 				html += `<option value="${val.code_coa}">${val.name_coa}</option>`;
// 			})

// 			$("#payment_method").html(html)
// 		}
// 	})
// }

function Kembali() {
    window.location.href = '#' + Modules + '/' + Controller;
}

function clearForm() {
    $('#doc_date').datepicker("setDate", moment().format('DD/MM/YYYY'));
    $('#tanggal').datepicker("setDate", moment().format('DD/MM/YYYY'));
    $('#top').val(0);

    selectedKaryawanId = null;
    selectedCustomerId = null;
    selectedCOACode = null;
    selectedCOACodeDebit = null;
    selectedCOACodeCredit = null;
    flagTerapkanPromoDiskon = false;
    flagItemEndRow = false;
}

function initDatePicker() {
    $('#periode_awal').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        startDate: subtractMonths(3),
    }).datepicker("setDate", new Date());
    $('#periode_akhir').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        startDate: subtractMonths(3),
    }).datepicker("setDate", new Date());

    // $('#periode_awal').datepicker({
    // 	format: 'dd/mm/yyyy',
    // 	autoclose: true,
    // }).datepicker("setDate", new Date());

    // $('#periode_akhir').datepicker({
    // 	format: 'dd/mm/yyyy',
    // 	autoclose: true,
    // }).datepicker("setDate", new Date());

    $('#tgl_jatuh_tempo').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    }).datepicker("setDate", new Date());
}

function initValidation() {
    $("#form_input").validate({
        rules: {
            doc_date: {
                DateID: true,
                required: true,
            },
        },
    });
}

function initDataTable() {
    _dataTable = $('#table_detail').DataTable({
        "pagingType": "simple",
        "iDisplayLength": -1,
        "bPaginate": false,
        "ordering": false,
        "info": false,
        "scrollX": true,
        "scrollY": "250px",
        "data": data4DataTable,
        "columns": [
            {},
            {
                "data": "code_coa",
                "className": "text-center",
                width: 200,
                "render": function(data, type, row, meta) {
                    return `<div class="input-group">
									<input type="hidden" name="detail_id_${meta.row}" id="detail_id_${meta.row}" value="${row.detail_id}" readonly="readonly" />
									<input type="hidden" name="code_coa_${meta.row}" id="code_coa_${meta.row}" value="${row.code_coa}" readonly="readonly" />
									<input type="text" class="form-control ${row.locked == 1 ? 'disable' : ''} input-pencarian" placeholder="Masukkan Code COA" name="pencarian_${meta.row}" id="pencarian_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.code_coa}" autocomplete="off" ${row.locked == 1 ? 'readonly' : ''} />
							${row.locked == 1 ? ''
							:
							`<div class="input-group-append">
									<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" id="LOVItemButton_${meta.row}" onclick="LOVCoa('${meta.row}', '#code_coa_${meta.row}', '#pencarian_${meta.row}');"><i class="fas fa-search"></i></button>
								</div>`
						}
								</div>`;
                }
            },
            {
                "data": "tanggal",
                "className": "text-center",
                width: 140,
                "render": function(data, type, row, meta) {
                    return `<div class="input-group date">
									<input type="text" class="form-control font-size-sm ${row.locked == 1 ? 'disable' : 'input-tanggal'}" placeholder="Tanggal" id="tanggal_${meta.row}" name="tanggal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${moment(row.tanggal).format('DD/MM/YYYY')}" readonly />
									<div class="input-group-append">
										<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
									</div>
								</div>`;
                },
            },
            {
                "data": "name_coa",
                "Classname": "text-left"
            },
            {
                "data": "nominal",
                "className": "text-right",
                width: 150,
                "render": function(data, type, row, meta) {
                    return `<div class="input-group">
							 <input type="text" class="form-control text-right input-nominal" id="nominal_${meta.row}" name="nominal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Nominal" value="${row.nominal}" onfocus="this.select()" autocomplete="off" />
							 </div>`;
                }
            },
            {
                "data": "remark",
                "className": "text-left",
                width: 150,
                // "render": function(data, type, row, meta) {
                //     return `<div class="input-group">
				// 			<input type="text" class="form-control input-remark" placeholder="Remark" name="remark_${meta.row}" id="remark_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.remark}" onfocus="this.select()" autocomplete="off"/>
				// 			</div>`;
                // }
                "render": function (data, type, row, meta) {
					return `
					<div class="input-group">
					<input type="text" class="form-control input-remark" placeholder="Pilih Store" name="remark_${meta.row}" id="remark_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.remark}" onfocus="this.select()" autocomplete="off" readonly />
					<div class="input-group-append">
					   <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVStore(${meta.row});"><i class="fas fa-search"></i></button>
					</div></div>`;
				}
            },
            {
                "data": "row_no",
                "visible": false
            },
            {
                "data": "locked",
                "visible": false
            },
            {
                "data": null,
                "className": "text-center",
                "width": 80,
                "render": function(data, type, row, meta) {
                    let actions = '';
                    actions +=
                        `<button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light add_coa" data-index="${meta.row}" id="delete_${meta.row}" name="add_coa"><i class="glyphicon glyphicon-check"></i></button>`;

                    actions +=
                        `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
                    return actions;
                },
            },
        ],
        columnDefs: [{
            "defaultContent": "-",
            "targets": "_all"
        }],
        "footerCallback": function(row, data, start, end, display) {
            let api = this.api();

            let totalQty = api.column(4).data().reduce(function(prevVal, nextVal) {
                return parseInt(prevVal) + parseInt(nextVal);
            }, 0);
            let total = accounting.unformat($('#total_qty').val(), _decimalSeparator);
			uang_cash = $("#total_advanced").val();	
			let sisa = uang_cash  - totalQty;
			$('#total_qty').val(totalQty);
			$('#total_expense').val(accounting.formatNumber(totalQty, 0));
			$('#sisa_pettycash').val(accounting.formatNumber(sisa, 0));

            // hitungTotal();
        },
    }).on('draw.dt', function(e, settings, json, xhr) {
        // === START pencarian item
        $('.input-pencarian').on('keypress', function(e) {
            if (e.charCode == 13) {
                e.preventDefault();
                let indexRow = $(this).data('index');
                let lockedRow = $(this).data('locked');
                if ($(this).val().trim() == "") return;
                if (lockedRow == 1) return;
                let parameters = 'code_coa=' + $(this).val().trim();

                loadingProcess();
                ajaxNew(base_url + 'finance/pos/get_coa?' + parameters, null, 'GET')
                    .then((data) => {
                        if (data.result) {
                            let row = data.data;
                            let data2save = {
                                detail_id: $('#detail_id_' + indexRow).val(),
                                code_coa: row.code_coa,
                                tanggal: row.tanggal,
                                name_coa: row.name_coa,
                                nominal: 0,
                                remark: row.remark,
                                row_no: moment().format('YYYYMMDDHHmmssSSS'),
                                locked: 0
                            };
                            // Cek kode item duplikat
                            let cek = null;
                            if (row.tanggal == 1) {
                                cek = _dataTable.rows().data().toArray().filter(data => {
                                    return $('#pencarian_' + indexRow).val() == data
                                        .code_coa;
                                });

                                if ((cek.length > 1 && DataTableAction == 'create') ||
                                    (cek.length > 1 && DataTableAction == 'edit' && $(
                                        '#pencarian_' + indexRow).val() != _dataTable.cek(
                                        indexRow).data().code_coa)) {
                                    MsgBox.Notification(
                                        'Code Coa sudah ada, silahkan ganti Nominal jika ingin menambahkan',
                                        'Peringatan', 'warning', true);
                                    return;
                                }
                            } else {
                                // Cek kode item duplikat
                                cek = _dataTable.rows().data().toArray().filter(data => {
                                    return $('#pencarian_' + indexRow).val() == data
                                        .code_coa;
                                });

                                if (cek.length > 1) {
                                    MsgBox.Notification(
                                        'Code Coa sudah ada, silahkan ganti Nominal jika ingin menambahkan',
                                        'Peringatan', 'warning', true);
                                    return;
                                }
                            }

                            // Simpan data
                            DataTableAction = 'edit';
                            SimpanRowDataTable(_dataTable, data2save, indexRow);
                            DataTableAction = 'create';
                            if (row.tanggal == 1) {
                                $('#tanggal_' + indexRow).focus();
                            } else {
                                $('#nominal_' + indexRow).focus();
                            }
                            loadingProcess(false);
                        } else {
                            // MsgBox.Notification(data.message.toString());
                            loadingProcess(false);
                            LOVCoa(indexRow, '#code_coa_' + indexRow, '#pencarian_' + indexRow, $(
                                this).val().trim());
                        }
                    })
                    .catch((err) => {
                        MsgBox.Notification(err.toString());
                    });
            }
        });
        // === END pencarian item

        $('.input-tanggal').datepicker({
            format: 'dd/mm/yyyy',
            startDate: subtractMonths(3),
            autoclose: true,
        });

        $('.input-tanggal').on('changeDate', function(e) {
            let getIndex = e.target.id.split("_");
            let indexRow = getIndex[getIndex.length - 1];
            _dataTable.cell(indexRow, 2).data(moment(e.date).format('YYYY-MM-DD'));
            _dataTable.rows().invalidate().draw();
            $('#tanggal_' + indexRow).datepicker('hide');
            $('#tanggal_' + indexRow).datepicker('destroy');
            $('#nominal_' + indexRow).focus();
        });
        // === END expired date

        // === START Nominal

        $('.input-nominal').on('keypress', function(e) {
            e.preventDefault();
            let indexRow = $(this).data('index');
            let lockedRow = $(this).data('locked');
            if (e.charCode == 13 || e.charCode == 9) {
                _dataTable.rows().draw();
                $('#remark_' + indexRow).focus();
            }
        });

        $('.input-nominal').on('blur', function(e) {
            e.preventDefault();
            let indexRow = $(this).data('index');
            let lockedRow = $(this).data('locked');
            // Update Qty
            _dataTable.cell(indexRow, 4).data(accounting.unformat($(this).val(), _decimalSeparator));
            // Jumlah
            let jumlah = (accounting.unformat($(this).val(), _decimalSeparator));
            // _dataTable.cell(indexRow, 3).data(jumlah);
            _dataTable.rows().invalidate().draw();
        });

        $(".input-remark").on('change', function() {
            let indexRow = $(this).data('index');
            let lockedRow = $(this).data('locked');
            console.log($(this).val());
            _dataTable.cell(indexRow, 4).data($(this).val());
            _dataTable.rows().invalidate().draw();
        })

        $('.input-nominal').autoNumeric('init', formatNumber);
        $('.input-remark').val();
        // === END qty

        // === START Remark

        // $('.input-remark').on('keypress', function (e) {
        // 	e.preventDefault();
        // 	let indexRow = $(this).data('index');
        // 	let lockedRow = $(this).data('locked');

        // 	if (e.charCode == 13 || e.charCode == 9) {
        // 		// Check jika lockedRow = 1
        // 		if (lockedRow == 1) {
        // 			_dataTable.rows().draw();
        // 			flagItemEndRow = true;

        // 			indexRow++;
        // 			if ($('#nominal_' + indexRow).length) {
        // 				if ($('#nominal_' + indexRow).data('locked') == 1) {
        // 					$('#nominal_' + indexRow).focus();
        // 				} else {
        // 					$('#pencarian_' + indexRow).focus();
        // 				}
        // 			}
        // 			indexRow--;
        // 			return; 
        // 		}

        // 		let row = null;

        // 		// Buat row baru dan fokus ke pencarian

        // 		// Locked
        // 		_dataTable.cell(indexRow, 6).data(1);
        // 		_dataTable.rows().invalidate().draw();
        // 		flagItemEndRow = true;
        // 		indexRow++;
        // 		$('#pencarian_' + indexRow).focus();
        // 	}	
        // });	

        // $('.input-remark').on('blur', function (e) {
        // 	e.preventDefault();
        // 	let indexRow = $(this).data('index');
        // 	let lockedRow = $(this).data('locked');

        // 	// Price point
        // 	// _dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator));
        // 	// Jumlah
        // 	// let jumlah = (accounting.unformat($('#nominal_' + indexRow).val(), _decimalSeparator), _decimalSeparator);
        // 	// _dataTable.cell(indexRow, 7).data(jumlah);
        // 	_dataTable.rows().invalidate().draw();
        // 	if (flagItemEndRow) {
        // 		_dataTable.rows().invalidate().draw();
        // 	}
        // 	flagItemEndRow = false;
        // });

        $('.input-nominal').autoNumeric('init', formatNumber);
        $('.input-remark').val();
        // hitungTotal();
    });
    _dataTable.on('order.dt search.dt', function () {
        let i = 1;
 
        _dataTable.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
            this.data(i++);
        });
    }).draw();

    setTimeout(() => {
        _dataTable.rows().invalidate().draw();

        setTimeout(() => {
            $('#pencarian_' + (_dataTable.data().count() - 1)).focus();
        }, 200);
    }, 500);
}
function subtractMonths(numOfMonths, date = new Date()) {
	date.setMonth(date.getMonth() - numOfMonths);
  
	return date;
}
$(document).on('click', ".add_coa", function() {
    let data2save = {
        detail_id: 0,
        code_coa: "",
        tanggal: moment().format('YYYY-MM-DD'),
        name_coa: "",
        nominal: 0,
        remark: "",
        row_no: moment().format('YYYYMMDDHHmmssSSS'),
        locked: 0
    };

    _dataTable.row.add([4]).draw(false);
    _dataTable.rows().invalidate().draw();
})

function LOVStore(row) {
	
	$('#PopUpModal').load(base_url + 'finance/expenseclaim/getStore/', () => { // Ambil URL untuk membuka modal LOV
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['m_code', 'm_shortdesc', 'm_pic', 'm_type']);
		$('#list_controls').val([`#remark_${row}`, `#remark_${row}`, '#m_pic', '#m_type']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$(`#remark_${row}`).val()) return;
				_dataTable.cell(row, 5).data($(`#remark_${row}`).val());
				_dataTable.rows().invalidate().draw();
			// $(`#remark_${row}`).val();
			// get_lastdate_pt($('#m_code').val());
		});
	});
}

function LOVCoa(RowIdx, elItemId = null, elPencarian = null, search = '') {
    let parameters = {
        code_coa: $(elPencarian).val().trim(),
        code_dept: $("#code_dept").val().trim()
    };
    let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
    $('#PopUpModal').load(base_url + 'finance/Expenseclaim/get_coa_lov/home/' + jsonWhere + '?search=' + search,
() => { // Ambil URL untuk membuka modal LOV
        // $(".modal-dialog").css("max-width", "70%");
        $(".modal-dialog").addClass("modal-xl modal-dialog-centered");
        $('#ModalLOV').modal('show'); // Tampilkan modal LOV
        $('#list_cols').val(['code_coa', 'code_coa']);
        $('#list_controls').val([elItemId, elPencarian]);

        $('#ModalLOV').on('hidden.bs.modal', function() {
            if (!_resultFromLOV) return;

            // Cek kode item & tanggal expire duplikat
            let row = null;
            row = _dataTable.rows().data().toArray().filter(data => {
                return $('#code_coa_' + RowIdx).val() == data.code_coa;
            });


            let data2save = {
                detail_id: 0,
                code_coa: _resultFromLOV.code_coa,
                name_coa: _resultFromLOV.name_coa,
                nominal: 0,
                remark: '',
                row_no: moment().format('YYYYMMDDHHmmssSSS'),
                locked: 0
            };
            // Simpan data
            DataTableAction = 'edit';
            SimpanRowDataTable(_dataTable, data2save, RowIdx);
            DataTableAction = 'create';
            $('#tanggal_' + RowIdx).focus();

            _resultFromLOV = null;
        });
    });
}

function getData() {
    let _id = $("#no_doc").val()
    let url = base_url + Modules + '/' + Controller + `/getData2Edit/json/${_id}`;
    ajaxNew(url, null, 'GET')
        .then((data) => {
           
            if (data.result) {
                let row = data.data;

                $('#no_doc').val(row.no_doc);
                $('#periode_awal').datepicker("setDate", moment(row.periode_awal).format('DD/MM/YYYY'));
                $('#periode_akhir').datepicker("setDate", moment(row.periode_akhir).format('DD/MM/YYYY'));
                $('#company_name').val(row.company_name);
                $('#company_code').val(row.m_type);
                $('#code_employee').val(row.nik);
                $('#name_employee').val(row.name_employee);
                $('#dapartement').val(row.name_dept);
                $('#code_dept').val(row.code_dept);
                $('#code_jabatan').val(row.code_jabatan);
                $('#position').val(row.name_jabatan);
                $('#bank').val(row.bank_to);
                $('#no_pejadin').val(row.no_pejadin);
                $('#payment_method').val(row.payment_method);
                $('#account_number').val(row.no_rek);
                $('#account_name').val(row.nama_penerima);
				$('#id_advanced').val(row.id_advanced);
				$('#total_advanced').val(row.settlement_cash);
                $('#description').val(row.description);
				$('#sisa_pettycash').val(row.settlement_expense);
                if (row.attachment_file) {
                    $("#file_expense").html(
                        `<a href="<?= base_url('assets/upload')?>/${row.attachment_file}" target="_BLANK"><span class="text-success">Check File Expense</span></a>`
                        )
                }
                if (row.pejadin_file) {
                    $("#file_pejading").html(
                        `<a href="<?= base_url('assets/upload')?>/${row.pejadin_file}" target="_BLANK"><span class="text-success">Check File Pejadin</span></a>`
                        )
                }
                if (row.refund_file) {
                    $("#file_refund").html(
                        `<a href="<?= base_url('assets/upload')?>/${row.refund_file}" target="_BLANK"><span class="text-success">Check File Refund</span></a>`
                        )
                }
                if (row.cashbon_file) {
                    $("#file_cashbon").html(
                        `<a href="<?= base_url('assets/upload')?>/${row.cashbon_file}" target="_BLANK"><span class="text-success">Check File Advance</span></a>`
                        )
                }

                if (row.status == 5) {
                    $("#send_email").html(`<div class="form-group">
                           <label>Email <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <input type="hidden" name="nik" id="nik" value="" readonly="readonly" />
                              <input type="hidden" name="name_employee" id="name_employee" value="" readonly="readonly" />
                              <input type="text" class="form-control" placeholder="Pilih Email" name="email" id="email" value="" readonly="readonly" />
                              <div class="input-group-append">
                                 <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVEmail();"><i class="fas fa-search"></i></button>
                              </div>
                           </div>
                        </div>`)
                }
                data4DataTable = data.detail;
                _dataTable.clear().draw();
                setTimeout(() => {
                    _dataTable.rows.add(data.detail).draw();
                    setTimeout(() => {}, _dataTable.data().count() * 100);
                }, 500);

                // Pembayaran tunai
                if (row.type_payment == 'TUNAI') {
                    // cash
                    $('#pembayaran_cash').val(accounting.formatNumber(row.cash, 2));
                    $('#sisa_kembalian').val(accounting.formatNumber(row.kembali, 2));
                    // debit
                    $('#pembayaran_debit_coa_code').val(row.coa_debit);
                    $('#pembayaran_debit_coa_name').val(row.coa_debit + ' - ' + row.coa_debit_name);
                    $('#pembayaran_debit_no').val(row.debit_no);
                    $('#pembayaran_debit_nilai').val(accounting.formatNumber(row.debit, 2));
                    // credit
                    $('#pembayaran_credit_coa_code').val(row.coa_credit);
                    $('#pembayaran_credit_coa_name').val(row.coa_credit + ' - ' + row.coa_credit_name);
                    $('#pembayaran_credit_no').val(row.credit_no);
                    $('#pembayaran_credit_nilai').val(accounting.formatNumber(row.credit, 2));
                }
                // Pembayaran transfer
                if (row.type_payment == 'TRANSFER') {
                    $('#coa_code').val(row.coa_transfer);
                    $('#coa_name').val(row.coa_transfer + ' - ' + row.coa_transfer_name);
                }
                // Pembayaran TEMPO
                if (row.type_payment == 'TEMPO') {
                    $('#tgl_jatuh_tempo').datepicker("setDate", moment(row.tgl_jatuh_tempo).format('DD/MM/YYYY'));
                    let selisihHari = moment(row.tgl_jatuh_tempo).diff(row.tgl_trans, 'days');
                    $('#top').val(selisihHari);
                }
            } else {
                MsgBox.Notification(data.message.toString());
            }
        })
        .catch((err) => {
            MsgBox.Notification(err.toString());
        });
}

function deleteDetail(RowIdx) {
    let getData = _dataTable.row(RowIdx).data();

    MsgBox.Confirm('Delete ' + getData.item_id + ' with expired date ' + moment(getData.expired_date)
        .format('DD/MM/YYYY') + ' from detail?', 'Delete detail').then(result => {
        HapusRowDataTable(_dataTable, RowIdx);
    }).catch(err => {
        if (err) console.log(err);
    });
}

$("#tambah_detail_btn").on('click', function() {
    _dataTable.row.add([1]).draw(false)
})

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {

    if (DataTableAction == 'edit') {
        DataTableElement.row(RowIdx).data(data2save);
        DataTableElement.rows().invalidate().draw();
    } else {
        DataTableElement.row.add(data2save).draw();
    }
}

function addDTRow() {
    let data2save = {
        detail_id: 0,
        code_coa: "",
        tanggal: moment().format('YYYY-MM-DD'),
        name_coa: "",
        nominal: 0,
        remark: "",
        row_no: moment().format('YYYYMMDDHHmmssSSS'),
        locked: 0
    };
    // Simpan data
    DataTableAction = 'edit';
    SimpanRowDataTable(_dataTable, data2save);
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
    DataTableElement.row(RowIdx).remove().draw();
    DataTableElement.rows().invalidate().draw();
}


async function Update() {
    // Check qty
    // if (accounting.unformat($('#total_qty').val(), _decimalSeparator) <= 0) {
    // 	MsgBox.Notification('Periksa kembali inputan anda', 'Peringatan', 'warning');
    // 	return;
    // }
    // Check detail
    if (_dataTable.data().count() <= 0) {
        MsgBox.Notification('Detail tidak ditemukan');
        return;
    }

    // Hitung total
    // await hitungTotal();


    // save
    MsgBox.Confirm('Save this data?').then(result => {
        if (!result) return;
        let _id = $("no_doc").val()
        let url = base_url + Modules + '/' + Controller + '/update/';
        let data_detail = _dataTable.rows().data().toArray();
        let datas_detail = [];

        $.each(data_detail, function(key, val) {
            datas_detail.push({
                detail_id: val.detail_id,
                code_coa: val.code_coa,
                tanggal: val.tanggal,
                name_coa: val.name_coa.replace(/\\n/g, ''),
                nominal: val.nominal,
                remark: val.remark,
                locked: val.locked
            })
        })
        let detail = JSON.stringify(datas_detail);
        let summary = $('#form_summary').serializeArray();
        _data2Send = $('#form_input').serialize() + '&' + summary + '&detail=' + detail;

        let payment_method = $("#payment_method").find(":selected").val()
        var formData = new FormData();
        formData.append('no_doc', $("#no_doc").val());
		formData.append('periode_awal',$("#periode_awal").val());
		formData.append('periode_akhir',$("#periode_akhir").val());
		formData.append('code_employee',$("#code_employee").val());
		formData.append('name_employee',$("#name_employee").val());
		formData.append('email',$("#email").val());
		formData.append('nik',$("#nik").val());
		formData.append('code_dept',$("#code_dept").val());
		formData.append('code_jabatan',$("#code_jabatan").val());
		formData.append('company_code',$("#company_code").val());
		formData.append('bank',$("#bank").find(":selected").val());
		formData.append('account_number',$("#account_number").val());
		formData.append('account_name',$("#account_name").val());
		formData.append('id_advanced',$("#id_advanced").val());
		formData.append('received_date',$("#received_date").val());
		formData.append('total_advanced',$("#total_advanced").val());
        formData.append('description',$("#description").val());
        // formData.append('file_expense', file_data);
        // formData.append('file_sisa', file_data_sisa);
        // formData.append('file_expense_status', file_data_status);
        // formData.append('file_sisa_status', file_data_sisa_status);
        // formData.append('periode_awal', $("#periode_awal").val());
        // formData.append('periode_akhir', $("#periode_akhir").val());
        // formData.append('company_code', $("#company_code").val());
        // formData.append('code_employee', $("#code_employee").val());
        // formData.append('code_dept', $("#code_dept").val());
        // formData.append('code_jabatan', $("#code_jabatan").val());
        // formData.append('no_pejadin', $("#no_pejadin").val());
        // formData.append('payment_method', payment_method);
        // formData.append('bank', $("#bank").val());
        // formData.append('account_number', $("#account_number").val());
        // formData.append('account_name', $("#account_name").val());
        // formData.append('email', $("#email").val());
        formData.append('update_action', $("#update_action").val());
        for (let i = 0; i < summary.length; i++) {
            formData.append(`${summary[i].name}`, `${summary[i].value}`)
        }
        // formData.append('summary',JSON.stringify(summary));

        formData.append('detail', detail);
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                loadingProcess(true);
            },
            success: function(data) {
                if (data.result) {
                    $('#no_doc').val(data.data.no_doc);
                    _id = data.data.no_doc;
                    action = 'edit';
                    MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
                    Kembali();
                    // MsgBox.ConfirmNew(data.message.toString() + ' Tambah transaksi lain?').then(result => {
                    // 	if (result) {
                    // 		$('#form_input')[0].reset();
                    // 		clearForm();
                    // 		_dataTable.clear().draw();
                    // 		addDTRow();
                    // 	} else {
                    // 		Kembali();
                    // 	}
                    // }).catch((err) => {
                    // 	MsgBox.Notification(err.toString());
                    // });
                } else {
                    MsgBox.Notification(data.msg.toString());
                }
                // if (data.result) {
                // 	$('#no_doc').val(data.data.no_doc);
                // 	_id = data.data.no_doc;
                // 	action = 'edit';
                // 	MsgBox.Notification(data.message.toString(), 'bottom right', 'success');

                // 	setTimeout(() => {
                // 		loadingProcess(false);
                // 		window.location.href ="#finance/pos";
                // 	}, 3000);
                // 	// let timerInterval
                // 	// Swal.fire({
                // 	// title: 'Auto close alert!',
                // 	// html: 'I will close in <b></b> milliseconds.',
                // 	// timer: 2000,
                // 	// timerProgressBar: true,
                // 	// didOpen: () => {
                // 	// 	Swal.showLoading()
                // 	// 	const b = Swal.getHtmlContainer().querySelector('b')
                // 	// 	timerInterval = setInterval(() => {
                // 	// 	b.textContent = Swal.getTimerLeft()
                // 	// 	}, 100)
                // 	// },
                // 	// willClose: () => {
                // 	// 	clearInterval(timerInterval)
                // 	// }
                // 	// }).then((result) => {
                // 	// /* Read more about handling dismissals below */
                // 	// 	if (result.dismiss === Swal.DismissReason.timer) {
                // 	// 		Kembali();
                // 	// 	}	
                // 	// })

                // } else {
                // 	// loadingProcess(false);
                // 	MsgBox.Notification(data.message.toString());
                // }

            },
            error: function(err) {
                // loadingProcess(false);
                console.log(err);
                MsgBox.Notification("Please check your data");
            }
        })
        // formData.append('m_code',$("#m_code").val());
        // formData.append('m_shortdesc',$("#m_shortdesc").val());
        // formData.append('m_pic',$("#m_pic").val());
        // formData.append('m_type',$("#m_type").val());
        // formData.append('nik',$("#nik").val());
        // formData.append('name_employee',$("#name_employee").val());
        // formData.append('email',$("#email").val());
        // ajaxNew(url, _data2Send, 'POST')
        // 	.then((data) => {
        // if (data.result) {
        // 	$('#no_doc').val(data.data.no_doc);
        // 	_id = data.data.no_doc;
        // 	action = 'edit';
        // 	MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
        // 	Kembali();
        // 	// MsgBox.ConfirmNew(data.message.toString() + ' Tambah transaksi lain?').then(result => {
        // 	// 	if (result) {
        // 	// 		$('#form_input')[0].reset();
        // 	// 		clearForm();
        // 	// 		_dataTable.clear().draw();
        // 	// 		addDTRow();
        // 	// 	} else {
        // 	// 		Kembali();
        // 	// 	}
        // 	// }).catch((err) => {
        // 	// 	MsgBox.Notification(err.toString());
        // 	// });
        // } else {
        // 	MsgBox.Notification(data.msg.toString());
        // }
        // 	})
        // 	.catch((err) => {
        // 		MsgBox.Notification(err.toString());
        // 	});
    }).catch(err => {
        console.log(err);
    });
}
</script>