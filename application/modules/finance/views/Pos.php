<div class="row">
   <div class="col-md-6">
      <div class="page-title-box">
         <div class="button-items">
            <?php if ($priv_arr['create_flag']) {?>
               <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" role="button" id="app_create" onclick="app_create();"> <i class="fas fa-plus"></i> Add Data</button>
            <?php }?>
            <button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light" id="app_refresh" name="app_refresh" onclick="app_refresh();"><i class="fas fa-sync"></i> Refresh </button>
         </div>
      </div>
   </div>
   <!-- <div class="col-md-6">
      <div class="page-title-box text-right">
         <div class="button-items">
            <?php if ($priv_arr['pdf_flag']) {?>
               <button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" id="app_pdf" name="app_pdf" onclick="app_pdf();"><i class="fas fa-file-pdf"></i> Pdf </button>
            <?php }?>
            <?php if ($priv_arr['xls_flag']) {?>
               <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" id="app_xls" name="app_xls" onclick="app_xls();"><i class="fas fa-file-excel"></i> Excel/CSV </button>
            <?php }?>
         </div>
      </div>
   </div> -->
</div>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <div class="alert alert-info mb-0" role="alert">
               <h4 class="alert-heading font-18">Petty Cash</h4>
               <p>Page to input Petty Cash. </p>
            </div>
         </div>
      </div>
   </div>
</div>
<?php //dd($this->session->userdata()); ?>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <div id="accordion">
               <div class="card mb-1 shadow-none">
                  <div class="card-header p-3" id="headingOne">
                     <h6 class="m-0 font-size-14">
                        <a href="#collapseOne" class="text-dark collapsed" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">Filter</a>
                     </h6>
                  </div>
                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                     <div class="card-body">
                        <form id="form_filter" name="form_filter">
                           <div class="row">
                              <div class="col-md-2">
                                 <div class="form-group">
                                    <label>Status</label>
                                    <select id="filter_status" name="filter_status" class="form-control">
                                       <option value="">All</option>
                                       <option value="0">New</option>
                                       <option value="1">On Progress</option>
                                       <option value="2">Approved by BM/ABM</option>
                                       <option value="3">Approved by HOD</option>
                                       <option value="4">Approved by AR</option>
                                       <option value="7">Approved by AP</option>
                                       <option value="8">Approved by MGR Fin</option>
                                       <option value="5">Reasign</option>
                                       <option value="10">Revision</option>
                                       <option value="6">Done</option>
                                       <option value="9">Reject</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">
                                    <label>Company</label>
                                    <select id="filter_company" name="filter_company" class="form-control">
                                       <option value="">- Select Company -</option>
                                       <?php foreach ($this->db->get('m_perusahaan')->result() as $key => $value): ?>
                                          <option value="<?php echo $value->m_type ?>"><?php echo $value->name ?></option>
                                       <?php endforeach?>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="form-group">
                                    <label>Periode</label>
                                    <div class="input-daterange input-group" data-date-format="dd M, yyyy"  data-date-autoclose="true"  data-provide="datepicker">
                                       <input type="text" class="form-control" id="filter_start_date" name="filter_start_date" readonly />
                                       <input type="text" class="form-control" id="filter_end_date" name="filter_end_date" readonly />
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-2 offset-md-3 mt-4">
                                 <div class="form-group text-right">
                                    <div class="button-items">
                                       <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="search" name="search" onclick="app_refresh();"><i class="fas fa-filter"></i> Submit</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <table id="table_list_data" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
               <thead>
                  <tr>
                     <th>No. Doc</th>
                     <th>Periode</th>
                     <th>Store Code</th>
                     <th>Store Name</th>
                     <th>PIC</th>
                     <th>Company Code</th>
                     <th>Company Name</th>
                     <th>Petty Cash</th>
                     <th>Petty Cash Expense</th>
                     <th>Remaining</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="modalFilter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalFilter" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="modalFilterTitle">Filter Cetak <br/><small id="modalFilterTitleSmall" class="text-muted font-weight-boldest">titleModal</small></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <i aria-hidden="true" class="ki ki-close"></i>
            </button>
         </div>
         <form class="form" id="form_filter_cetak" name="form_filter_cetak">
            <div class="modal-body">
               <input type="hidden" id="doc_no" name="doc_no" value="" />
               <div class="form-group row">
                  <label class="col-4 col-form-label">Jenis</label>
                  <div class="col-8">
                     <select class="form-control selectpicker" id="filter_jenis" name="filter_jenis">
                        <option value="pdf_dokumen">Faktur Penjualan</option>
                        <option value="pdf_pesanan">Surat Pesanan</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-outline-danger font-weight-bold" data-dismiss="modal"><span class="btn-label"><i class="far fa-window-close mr-2"></i></span> Tutup</button>
               <button type="button" class="btn btn-outline-success font-weight-bold" onclick="cetakInvoice()"><span class="btn-label"><i class="fa fa-print mr-2"></i></span> Cetak</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!--begin::Modal-->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script> -->
<script type="text/javascript">
   // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
   var data2Send = null;
   var dataArr = [];
   var DataTable = null;
   // END VARIABEL WAJIB
   var action = '<?=$action?>';
   var idData = null;

   LobiAdmin.loadScript([
      '<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.js?v=<?=date('YmdHis') . rand()?>'
   ], function() {
      initPage();
   });
</script>

<!-- sini -->
<?php 
   if($this->uri->segment(3, 0) == "create"){
?>

<?php }
?>