<div class="row">
    <div class="col-md-6">
        <div class="page-title-box">
            <h5>Dashboard</h5>
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                <li class="breadcrumb-item active">Selamat Datang</li>&nbsp;<b><?php echo $this->session->userdata('name'); ?></b>
            </ol>
        </div>
    </div>
</div>

<div class="row">
   <div class="col-md-6">
      <div class="card">
         <div class="card-body">
            <h4 class="alert-heading font-18">Petty Cash</h4>
            <div id="accordion">
               <div class="card mb-1 shadow-none">
                  <div class="card-header p-3" id="headingOne">
                     <h6 class="m-0 font-size-14">
                        <a href="#collapseOne" class="text-dark collapsed" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">Filter</a>
                     </h6>
                  </div>
                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                     <div class="card-body">
                        <form id="form_filter" name="form_filter">
                           <div class="row">
                              <div class="col-md-2">
                                 <div class="form-group">
                                    <label>Status</label>
                                    <select id="filter_status" name="filter_status" class="form-control">
                                       <option value="">All</option>
                                       <option value="0">New</option>
                                       <option value="1">On Progress</option>
                                       <option value="2">Approved by BM/ABM</option>
                                       <option value="3">Approved by HOD</option>
                                       <option value="4">Approved by FIN</option>
                                       <option value="5">Reasign</option>
                                       <option value="6">Done</option>
                                       <option value="9">Reject</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">
                                    <label>Company</label>
                                    <select id="filter_company" name="filter_company" class="form-control">
                                       <option value="">- Select Company -</option>
                                       <?php foreach ($this->db->get('m_perusahaan')->result() as $key => $value): ?>
                                          <option value="<?php echo $value->m_type ?>"><?php echo $value->name ?></option>
                                       <?php endforeach?>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="form-group">
                                    <label>Periode</label>
                                    <div class="input-daterange input-group" data-date-format="dd M, yyyy"  data-date-autoclose="true"  data-provide="datepicker">
                                       <input type="text" class="form-control" id="filter_start_date" name="filter_start_date" readonly />
                                       <input type="text" class="form-control" id="filter_end_date" name="filter_end_date" readonly />
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-2 offset-md-3 mt-4">
                                 <div class="form-group text-right">
                                    <div class="button-items">
                                       <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="search" name="search" onclick="app_refresh();"><i class="fas fa-filter"></i> Submit</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>            
         </div>
         <table id="table_list_data" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
               <thead>
                  <tr>
                     <th>No. Doc</th>
                     <th>Periode</th>
                     <th>Store Code</th>
                     <th>Store Name</th>
                     <th>PIC</th>
                     <th>Company Code</th>
                     <th>Company Name</th>
                     <th>Petty Cash</th>
                     <th>Petty Cash Expense</th>
                     <th>Remaining</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
      </div>
   </div>
   <div class="col-md-6">
      <div class="card">
         <div class="card-body">
            <h4 class="alert-heading font-18">Expense Claim</h4>
            <div id="accordion">
               <div class="card mb-1 shadow-none">
                  <div class="card-header p-3" id="headingOne">
                     <h6 class="m-0 font-size-14">
                        <a href="#collapseOne" class="text-dark collapsed" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">Filter</a>
                     </h6>
                  </div>
                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                     <div class="card-body">
                        <form id="form_filter" name="form_filter">
                           <div class="row">
                              <div class="col-md-2">
                                 <div class="form-group">
                                    <label>Status</label>
                                    <select id="filter_status" name="filter_status" class="form-control">
                                       <option value="">All</option>
                                       <option value="0">New</option>
                                       <option value="1">On Progress</option>
                                       <option value="2">Approved by BM/ABM</option>
                                       <option value="3">Approved by HOD</option>
                                       <option value="4">Approved by FIN</option>
                                       <option value="5">Reasign</option>
                                       <option value="6">Done</option>
                                       <option value="9">Reject</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">
                                    <label>Company</label>
                                    <select id="filter_company" name="filter_company" class="form-control">
                                       <option value="">- Select Company -</option>
                                       <?php foreach ($this->db->get('m_perusahaan')->result() as $key => $value): ?>
                                          <option value="<?php echo $value->m_type ?>"><?php echo $value->name ?></option>
                                       <?php endforeach?>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="form-group">
                                    <label>Periode</label>
                                    <div class="input-daterange input-group" data-date-format="dd M, yyyy"  data-date-autoclose="true"  data-provide="datepicker">
                                       <input type="text" class="form-control" id="filter_start_date" name="filter_start_date" readonly />
                                       <input type="text" class="form-control" id="filter_end_date" name="filter_end_date" readonly />
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-2 offset-md-3 mt-4">
                                 <div class="form-group text-right">
                                    <div class="button-items">
                                       <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="search" name="search" onclick="app_refresh();"><i class="fas fa-filter"></i> Submit</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <input type="hidden" id="tbl_id" value="<?=$this->session->userdata('role_id')?>">
            <table id="table_list_data" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
               <thead>
                  <tr>
                     <th>No. Doc</th>
                     <th>Periode</th>
                     <th>PIC</th>
                     <th>Company Code</th>
                     <th>Company Name</th>
                     <th>Total Expense</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
      </div>
   </div>
   <div class="col-md-6">
      <div class="card">
         <div class="card-body">
            
         </div>
      </div>
   </div>
</div>
<div class="row">
   
</div>

<script type="text/javascript">
   // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var Priv = JSON.parse('<?=json_encode($priv_arr)?>');
   var data2Send = null;
   var dataArr = [];
   var DataTable = null;
   var DataExcla = null;
   var DataSettl = null;
   // END VARIABEL WAJIB
   var action = '<?=$action?>';
   var idData = null;

   LobiAdmin.loadScript([
      '<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.js?v=<?=date('YmdHis') . rand()?>'
   ], function() {
      initPage();
   });
</script>