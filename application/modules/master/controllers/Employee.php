<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends Core_Controller {
	
	function __construct(){
		parent::__construct("employee"); # parsing menu_id
		$this->load->model("Employee_Model", "m_app");
	}
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/

	function index() {
		$this->data['action'] = 'create';
		$this->load->view('Employee', $this->data);
	}
	
	function getList() {
		$filter = array(
			'a.status' => $this->input->post("status", TRUE),
		);
		
		$this->output->set_content_type('application/json');
		echo $this->m_app->getList($filter);
	}
	
	function save($format = 'json') {
		# START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
		$raw = $this->security->xss_clean($this->input->raw_input_stream);
		$input = $this->input($format, $raw);
		
		if ($input != NULL || trim($input) == '' || !empty($input)) {
			$hasil = $this->m_app->save($input);
		} else {
			$hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => NULL);
		}
		
		echo $this->output($format, $hasil);
	}
	
	function getData2Edit($format = 'json', $id = NULL) {
		if ($id != NULL || trim($id) != '' || !empty($id)) {
			$hasil = $this->m_app->getData2Edit($id);	
		} else {
			$hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => NULL);
		}
		
		echo $this->output($format, $hasil);
	}
	
	function update($format = 'json') {
		# START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
		$raw = $this->security->xss_clean($this->input->raw_input_stream);
		$input = $this->input($format, $raw);
		
		if ($input != NULL || trim($input) == '' || !empty($input)) {
			$hasil = $this->m_app->update($input);
		} else {
			$hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => NULL);
		}
		
		echo $this->output($format, $hasil);
	}

	function delete($format = 'json') {
		# START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
		$raw = $this->security->xss_clean($this->input->raw_input_stream);
		$input = $this->input($format, $raw);
		
		if ($input != NULL || trim($input) == '' || !empty($input)) {
			$hasil = $this->m_app->delete($input);
		} else {
			$hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => NULL);
		}
		
		echo $this->output($format, $hasil);
	}
	
	function pdf() {
		# Judul laporan
		$JudulLaporan = "Master Employee";
		# Load library
		$this->load->library('PDFReport');
		$pdf = new PDFReport();
		# Ukuran kertas
		$pdf->_construct('P', 'mm', 'A4', true, 'UTF-8', false); # A4
		// $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
		# START deklarasi header
		unset($dataHeader);
		$dataHeader = array();
		$pdf->dataHeader = $dataHeader;
		$pdf->RepFuncHeader = "Employee";
		# END deklarasi header
		# START deklarasi footer
		unset($dataFooter);
		$dataFooter = array('printed_by' => $this->session->userdata('name'));
		$pdf->dataFooter = $dataFooter;
		# END deklarasi footer
		# Set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		# Set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 28, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetTitle($JudulLaporan);
		$pdf->setFooterMargin(30);
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->SetAuthor('Mr Sule');
		$pdf->SetDisplayMode('fullwidth', 'continuous');
		# Ambil data
		$filter = array(
			'a.status' => $this->input->post("status", TRUE),
		);
		$data = $this->m_app->getDataList($filter);
		$pdf->AddPage();
		
		$pdf->SetFont('helvetica','',8);
		if ($data->result() != NULL) {
			foreach($data->result() as $key => $val) {
				$pdf->Cell(50,5,$val->nik,1,0,'L');
				$pdf->Cell(50,5,$val->nama_employee,1,0,'L');
				$pdf->Cell(50,5,$val->name_jabatan,1,0,'L');
			  	$pdf->Cell(80,5,$val->name_dept,1,0,'L');
				$pdf->Cell(50,5,$val->code_atasan,1,0,'L');
			  	$pdf->Cell(80,5,$val->email,1,0,'L');
				$pdf->Cell(50,5,$val->nohp,1,0,'L');
				$pdf->Cell(25,5,$val->status,1,0,'L');
			  	$pdf->Ln(5);
			}
		} else {
			$pdf->Cell(0,5,"Data tidak ditemukan.",1,0,'C');
		}
		# Keluarkan Output
		$pdf->Output($JudulLaporan.'_'.date('YmdHis').'.pdf', 'I');
	}
	
	function xls() {
		# Judul laporan
		$JudulLaporan = "Master Jabatan";
		# Ambil data
		// $filter = array(
		// 	'a.status' => $this->input->post("status", TRUE),
		// );
		$rs = $this->m_app->getDataList();
		# Deklarasi kolom yang akan ditampilkan
		$Col['header'] = array('NIK', 'Employee Name', 'Jabatan', 'Departemen', 'Atasan', 'Email', 'No HP', 'Status');
		$Col['type'] = array('string', 'string', 'string', 'string', 'string', 'string', 'string', 'string');
		$Col['align'] = array('left', 'left', 'left', 'left', 'left', 'left', 'left', 'center');
		# Load library
		$this->load->library('XLSReport');
		$xls = new XLSReport();
		# Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
		if ($rs->num_rows() >= 1000000) {
			$xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
		} else {
			$xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
		}
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	function getEmployeeList($action = "home") {
		$data['ColHeader'] = array('NIK', 'Name');
		$data['ColShow'] = array(1, 1);
		$data['columns'] = array(
			array('data' => 'nik'),
			array('data' => 'name_employee'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => false, 'searchable' => true),
			array('targets' => 1, 'orderable' => true, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getEmployeeList();
				break;
			case 'home':
				$data['Judul'] = 'Employee List';
				$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}
	
	function getEmployeeListExpense($action = "home") {
		$data['ColHeader'] = array('NIK', 'Name');
		$data['ColShow'] = array(1, 1);
		$data['columns'] = array(
			array('data' => 'nik'),
			array('data' => 'name_employee'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => false, 'searchable' => true),
			array('targets' => 1, 'orderable' => true, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getEmployeeListExpense();
				break;
			case 'home':
				$data['Judul'] = 'Employee List';
				$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}
	
	function getEmployeeEmail($action = "home") {
		$data['ColHeader'] = array('NIK', 'Name', 'Email');
		$data['ColShow'] = array(1, 1, 1);
		$data['columns'] = array(
			array('data' => 'nik'),
			array('data' => 'name_employee'),
			array('data' => 'email'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => false, 'searchable' => true),
			array('targets' => 1, 'orderable' => true, 'searchable' => true),
			array('targets' => 2, 'orderable' => false, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getEmployeeEmail();
				break;
			case 'home':
				$data['Judul'] = 'Employee List';
				$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}

	function getRole($action = "home") {
		$data['ColHeader'] = array('Role ID', 'Role Name', 'Deskripsi');
		$data['ColShow'] = array(1, 1, 1);
		$data['columns'] = array(
			array('data' => 'role_id'),
			array('data' => 'role_name'),
			array('data' => 'desc'),
		);
		$data['columnDefs'] = array(
			array('targets' => 0, 'orderable' => true, 'searchable' => true),
			array('targets' => 1, 'orderable' => true, 'searchable' => true),
			array('targets' => 3, 'orderable' => true, 'searchable' => true),
		);
		switch ($action) {
			case 'nav':
				$this->output->set_content_type('application/json');
				echo $this->m_app->getRoleList();
				break;
			case 'home':
				$data['Judul'] = 'Role List';
				$data['src_url'] = base_url().'master/'.$this->data['controller'].'/getRole/';
				$data['end_point'] = '';
				$this->load->view('LOV', $data);
				break;
		}
	}

	// function getKelasList($action = "home") {
	// 	$data['ColHeader'] = array('Kode Paket', 'Nama Paket');
	// 	$data['ColShow'] = array(1, 1);
	// 	$data['columns'] = array(
	// 		array('data' => 'kode_paket'),
	// 		array('data' => 'nama_paket'),
	// 	);
	// 	$data['columnDefs'] = array(
	// 		array('targets' => 0, 'orderable' => false, 'searchable' => true),
	// 		array('targets' => 1, 'orderable' => true, 'searchable' => true),
	// 	);
	// 	switch ($action) {
	// 		case 'nav':
	// 			$this->output->set_content_type('application/json');
	// 			echo $this->m_app->getPaketList();
	// 			break;
	// 		case 'home':
	// 			$data['Judul'] = 'Daftar Kelas Paud';
	// 			$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
	// 			$data['end_point'] = '';
	// 			$this->load->view('LOV', $data);
	// 			break;
	// 	}
	// }
	// function getDivisiList($action = "home") {
	// 	$data['ColHeader'] = array('Divisi Code', 'Divisi Name');
	// 	$data['ColShow'] = array(1, 1);
	// 	$data['columns'] = array(
	// 		array('data' => 'divisi_code'),
	// 		array('data' => 'divisi_name'),
	// 	);
	// 	$data['columnDefs'] = array(
	// 		array('targets' => 0, 'orderable' => false, 'searchable' => true),
	// 		array('targets' => 1, 'orderable' => true, 'searchable' => true),
	// 	);
	// 	switch ($action) {
	// 		case 'nav':
	// 			$this->output->set_content_type('application/json');
	// 			echo $this->m_app->getDivisiList();
	// 			break;
	// 		case 'home':
	// 			$data['Judul'] = 'Divisi List';
	// 			$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
	// 			$data['end_point'] = '';
	// 			$this->load->view('LOV', $data);
	// 			break;
	// 	}
	// }

	// function getDivisiSelect2($format = 'json') {
	// 	# START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
	// 	$raw = $this->security->xss_clean($this->input->raw_input_stream);
	// 	$input = $this->input($format, $raw);
		
	// 	$hasil = $this->m_app->getDivisiSelect2($input);
	// 	echo $this->output($format, $hasil);
	// }

}