<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class Upemployee extends Core_Controller {
	
	function __construct(){
		parent::__construct("upemployee"); # parsing menu_id
		$this->load->model("Upemployee_Model", "m_app");
	}
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/

	public function index() {
		// $this->data['action'] = 'create';
		$this->load->view('Upemployee', $this->data);
	}

    public function create() {
        $this->load->view('Upemployee_form', $this->data);
    }
	
	function getList() {
		$filter = array(
			'a.status' => $this->input->post("status", TRUE),
		);
		
		$this->output->set_content_type('application/json');
		echo $this->m_app->getList($filter);
	}
	
	public function save($format = 'json')
    {
        # START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
        $input = $this->input->post();

        $data_upload_item = uploadDataImg('photo', $this->input->post('barcode') . '_photo', './assets/data_upload/photo');

        if (!empty($_FILES)) {
            if ($data_upload_item['upload_status'] == true) {
                if ($input != null || trim($input) == '' || !empty($input)) {
                    $path_file = $data_upload_item['upload_data']['full_path'];
                    $input['photo'] = $this->base64_encode_html_image($path_file, '100x100');
                    // $input['photo'] = ($data_upload_item['upload_status'] != false) ? $data_upload_item['upload_data']['file_name'] : null;
                    $hasil = $this->m_app->save($input);
                    echo $this->output($format, $hasil);
                    // print_r($input);
                } else {
                    $hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => null);
                    echo $this->output($format, $hasil);
                }
            } else {
                $input['photo'] = null;
                $hasil = $this->m_app->save($input);
                echo $this->output($format, $hasil);
            }
        } else {
            if ($input != null || trim($input) == '' || !empty($input)) {
                $input['photo'] = null;
                $hasil = $this->m_app->save($input);
                echo $this->output($format, $hasil);
            } else {
                $hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => null);
                echo $this->output($format, $hasil);
            }
        }
    }
    public function base64_encode_html_image($img_file, $alt = null, $cache = false, $ext = null)
    {
        if (!is_file($img_file)) {
            return false;
        }

        $b64_file = "{$img_file}.b64";
        if ($cache && is_file($b64_file)) {
            $b64 = file_get_contents($b64_file);
        } else {
            $bin = file_get_contents($img_file);
            $b64 = base64_encode($bin);

            if ($cache) {
                file_put_contents($b64_file, $b64);
            }
        }

        if (!$ext) {
            $ext = pathinfo($img_file, PATHINFO_EXTENSION);
        }

        // return "<img alt='{$alt}' src='data:image/{$ext};base64,{$b64}' />";
        return $b64;
    }
    public function base64_decode_html($img_file, $alt = null, $cache = false, $ext = null)
    {
        if (!is_file($img_file)) {
            return false;
        }

        $b64_file = "{$img_file}.b64";
        if ($cache && is_file($b64_file)) {
            $b64 = file_get_contents($b64_file);
        } else {
            $bin = file_get_contents($img_file);
            $b64 = base64_encode($bin);

            if ($cache) {
                file_put_contents($b64_file, $b64);
            }
        }

        if (!$ext) {
            $ext = pathinfo($img_file, PATHINFO_EXTENSION);
        }

        return "<img src='data:image/{$ext};base64,{$b64}' />";
        // return $b64;
    }
	
	public function getData2Edit($format = 'json', $id = null)
    {
        if ($id != null || trim($id) != '' || !empty($id)) {
            // print_r($this->m_app->getData2Edit($id));
            $data = $this->m_app->getData2Edit($id);
            if ($data['photo'] != null) {
                $bin = base64_decode($data['photo']);
                $size = getImageSizeFromString($bin);
                $ext = substr($size['mime'], 6);
                // print_r("<img src='data:image/{$ext};base64,{$data['photo']}' />");
                $data['photo_base64'] = "data:image/{$ext};base64,{$data['photo']}";
            }
            // print_r($data['photo']);
            $hasil = array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $data);
        } else {
            $hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => null);
        }

        echo $this->output($format, $hasil);
    }
    
    public function edit($id = null)
    {
        if ($id == null || trim($id) == '' || empty($id)) {
            show_404();
        } else {
            $this->data['id'] = $id;
            $this->data['action'] = "edit";
            $this->load->view('Hscode_form', $this->data);
        }
    }
	
	public function update($format = 'json')
    {
        $input = $this->input->post();

        $data_upload_item = uploadDataImg('photo', $this->input->post('barcode') . '_photo', './assets/data_upload/photo');

        if (!empty($_FILES)) {
            if ($data_upload_item['upload_status'] == true) {
                if ($input != null || trim($input) == '' || !empty($input)) {
                    $path_file = $data_upload_item['upload_data']['full_path'];
                    $input['photo'] = $this->base64_encode_html_image($path_file, '100x100');
                    // $input['photo'] = ($data_upload_item['upload_status'] != false) ? $data_upload_item['upload_data']['file_name'] : null;
                    $hasil = $this->m_app->update($input);
                    echo $this->output($format, $hasil);
                    // print_r($input);
                } else {
                    $hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => null);
                    echo $this->output($format, $hasil);
                }
            } else {
                // $input['photo'] = null;
                $hasil = $this->m_app->updatenonphoto($input);
                echo $this->output($format, $hasil);
            }
        } else {
            if ($input != null || trim($input) == '' || !empty($input)) {
                //$input['photo'] = null;
                $hasil = $this->m_app->updatenonphoto($input);
                echo $this->output($format, $hasil);
            } else {
                $hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => null);
                echo $this->output($format, $hasil);
            }
        }
    }

	function delete($format = 'json') {
		# START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
		$raw = $this->security->xss_clean($this->input->raw_input_stream);
		$input = $this->input($format, $raw);
		
		if ($input != NULL || trim($input) == '' || !empty($input)) {
			$hasil = $this->m_app->delete($input);
		} else {
			$hasil = array('result' => false, 'msg' => 'Data kosong.', 'data' => NULL);
		}
		
		echo $this->output($format, $hasil);
	}
	
	public function pdf()
    {
        # Judul laporan
        $JudulLaporan = "Daftar HS Code";
        # Load library
        $this->load->library('PDFReport');
        $pdf = new PDFReport();
        # Ukuran kertas
        $pdf->_construct('L', 'mm', 'A4', true, 'UTF-8', false); # A4
        // $pdf->_construct('L', 'mm', array(210, 350), true, 'UTF-8', false); # Custom
        # START deklarasi header
        unset($dataHeader);
        $dataHeader = array();
        $pdf->dataHeader = $dataHeader;
        $pdf->RepFuncHeader = "DaftarHSCode";
        # END deklarasi header
        # START deklarasi footer
        unset($dataFooter);
        $dataFooter = array('printed_by' => $this->session->userdata('name'));
        $pdf->dataFooter = $dataFooter;
        # END deklarasi footer
        # Set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        # Set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 5, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetTitle($JudulLaporan);
        $pdf->setFooterMargin(25);
        $pdf->SetAutoPageBreak(true, 15);
        $pdf->SetAuthor('The AdaMz');
        $pdf->SetDisplayMode('fullwidth', 'continuous');
        # Ambil data
        $filter = array(
            'a.status' => $this->input->post("active", true),
        );
        $data = $this->m_app->getDataList($filter);
        $pdf->AddPage();

        $pdf->SetFont('helvetica', '', 8);
        if ($data->result() != null) {
            foreach ($data->result() as $key => $val) {
                // Format ulang array
                $values = [
                    'barcode' => $val->barcode,
                    'keterangan' => $val->keterangan,
                    'material' => $val->material,
                    'code_china' => $val->code_china,
                    'code_indo' => $val->code_indo,
                    'sni' => $val->sni,
                    'ls' => $val->ls,
                    'lartas' => $val->lartas,
                    'special_req' => $val->special_req,
                    'duty' => number_format($val->duty, 00),
                    'atiga' => number_format($val->atiga, 00),
                    'acfta' => number_format($val->acfta, 00),
                    'ppn' => number_format($val->ppn, 00),
                    'ppnbm' => number_format($val->ppnbm, 00),
                    'pph' => number_format($val->pph, 00),
                    'bmtp' => number_format($val->bmtp, 00),
                    'status' => $val->status,
                ];
                $arrWidth = [25, 70, 25, 25, 25, 25, 25, 25, 25, 20, 20, 20, 20, 20, 20, 20, 20];
                $arrAlign = ['L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'C'];
                pdfCellWrap($pdf, $arrWidth, 5, $values, 1, $arrAlign);
            }
        } else {
            $pdf->Cell(0, 5, "Data tidak ditemukan.", 1, 0, 'C');
        }
        # Keluarkan Output
        $pdf->Output($JudulLaporan . '_' . date('YmdHis') . '.pdf', 'I');
    }
	
	function xls() {
		# Judul laporan
		$JudulLaporan = "Daftar HS Code";
		# Ambil data
		// $filter = array(
		// 	'a.status' => $this->input->post("status", TRUE),
		// );
		$rs = $this->m_app->getDataList();
		# Deklarasi kolom yang akan ditampilkan
		$Col['header'] = array('Barcode','Desc', 'Material', 'Code China', 'Code Indo', 'SNI', 'LS', 'Lartas by Import Permit', 'Special Requirement', 'DUTY/BM(%)', 'ATIGA-BM(%)', 'ACFTA-BM(%)','PPN(%)','PPNBM(%)', 'PPh(%)', 'BMTP(%)', 'Status');
		$Col['type'] = array('string','string', 'string', 'string', 'string', 'string', 'string', 'string', 'string', 'numeric', 'numeric', 'numeric','numeric','numeric','numeric','numeric','string');
		$Col['align'] = array('left','left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'right','right','right','right','right','right','right','center');
		# Load library
		$this->load->library('XLSReport');
		$xls = new XLSReport();
		# Jika jumlah rows lebih dari 1.000.000 download ke CSV, jika bukan ke XLSX
		if ($rs->num_rows() >= 1000000) {
			$xls->generateCSVByQueryObj($rs, $JudulLaporan, ";");
		} else {
			$xls->generateXlsByQueryObj($rs, $Col, $JudulLaporan);
		}
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
  public function import($format = 'json'){

    // UPLOAD EXCEL TO FOLDER DATA 
    $tmpFilePath = $_FILES['import']['tmp_name'];
    $tempname = pathinfo($_FILES['import']['name'], PATHINFO_EXTENSION);

    $target_import = time() . "." . $tempname;
    $destination = "./upload/temp/$target_import";
    $targetPath = $tmpFilePath;
    if(move_uploaded_file($targetPath,$destination)<0){
        $hasil = array('result' => true, 'data' => null, 'msg' => 'File Gagal di upload.');
        echo $this->output($format, $hasil);
        return;
    }

    // END UPLOAD EXCEL TOO FOLDER DATA
    $path_xlsx = $destination;
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load($path_xlsx);
    $d = $spreadsheet->getSheet(0)->toArray();
    unset($d[0]);
    $datas = array();
    $datau = array();
    $message = "";
    foreach ($d as $t) {
       if($t[0] !=""){
            $nikx = strval($t[0]);
            $data["nik"] = strval($t[0]);
            $data["name_employee"] = $t[1];
            $data["code_jabatan"] = $t[2];
            $data["email"] = $t[3];
            $data["nohp"] = $t[4];
            $data["role_id"] = $t[5];
            $data['status'] = 1;
            $data_avail = $this->db->get_where('m_employee',array('nik =' => strval($data['nik'])));
            if ($data_avail->num_rows() == 0) {
                $datas = array (
                    'nik' => strval($t[0]),
                    'name_employee' => $t[1],
                    'code_jabatan' => $t[2],
                    'email' => $t[3],
                    'nohp' => $t[4],
                    'status' => 1
                );
                $this->db->insert('m_employee', $datas) or die('error');	
                $datau = array (
                    'user_id' => strval($t[0]),
                    'user_pass' => password_hash('123456', PASSWORD_DEFAULT),
                    'name' => $t[1],
                    'role_id' => $t[5],
                    'nik' => $t[0],
                    'active' => 1
                );
                $this->db->insert('app_users', $datau) or die('error');	
                // array_push($datas,$data);
                // $message[] = ['barcode'=>$t[0],'status'=>"Barcode Berhasil ditambahkan"];
                $message .= "<pre>nik :".$nikx."Nik Berhasil ditambahkan"."</pre><br>";
            }
            else {
                // $message[] = ['barcode'=>$t[0],'status'=>"Barcode  Sudah Pernah ditambahkan"];
                $message .= "<pre>nik :".$nikx." Nik  Sudah Pernah ditambahkan"."</pre><br>";
            }
        }
    }
    $hasil = array('result' => true, 'data' => null, 'msg' => $message);
    // dd($datas);
    // $result = $this->Mdl_product->add_data($datas);
    // if (!$datas) {
    //     $hasil = array('result' => true, 'data' => null, 'msg' => $message);
    // } else {

    //     $result = $this->m_app->add_data($datas);	
    //     if($result){
    //         // $hasil = array('result' => true, 'data' => null, 'msg' => 'Data berhasil disimpan.');    
    //         $hasil = array('result' => true, 'data' => null, 'msg' => $message);    
    //         // echo "Data berhasil diimport.";
    //     }else{
    //         // $hasil = array('result' => true, 'data' => null, 'msg' => 'File Gagal di upload.');
    //         $hasil = array('result' => true, 'data' => null, 'msg' => $message);
    //         // echo "Data gagal diimport.";
    //     }
        
    // }
    unlink($destination);
    echo $this->output($format, $hasil);
  }
  
   public function upload_data($format = 'json')
  {
      $this->load->library("excel");
      $object = new PHPExcel();
      // print_r($_FILES);
      $uploaddataexcel = uploadDataExcel('import', 'hscode', './assets/upload_temp/');
      if (!empty($_FILES)) {
          if ($uploaddataexcel['upload_status'] == true){
              $upload_data = $uploaddataexcel['upload_data'];
              $excelreader     = new PHPExcel_Reader_Excel2007();
              $loadexcel         = $excelreader->load('assets/upload_temp/'.$upload_data['file_name']);
              $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
              

              $data = array();

              $numrow = 1;
              foreach($sheet as $row){
                  if($numrow > 1){
                      $input['barcode'] = $row['C'];
                      $input['keterangan'] = $row['E'];
                      $input['material'] = $row['G'];
                      $input['code_china'] = $row['H'];
                      $input['code_indo'] = $row['I'];
                      $input['sni'] = $row['J'];
                      $input['ls'] = $row['K'];
                      $input['lartas'] = $row['L'];
                      $input['special_req'] = $row['M'];
                      $input['duty'] = $row['N'];
                      $input['atiga'] = $row['O'];
                      $input['acfta'] = $row['P'];
                      $input['ppn'] = $row['Q'];
                      $input['ppnbm'] = $row['R'];
                      $input['pph'] = $row['S'];
                      $input['bmtp'] = $row['T'];
                      $data_avail = $this->db->get_where('m_hs_code',array('barcode =' => $input['barcode'], 'code_indo =' => $input['code_indo']));
                      // echo $data_avail;
                      if ($data_avail->num_rows() == 0) {
                            $datas = array (
                                'barcode' => $row['C'],
                                'keterangan' => $row['E'],
                                'material' => $row['G'],
                                'code_china' => $row['H'],
                                'code_indo' => $row['I'],
                                'sni' => $row['J'],
                                'ls' => $row['K'],
                                'lartas' => $row['L'],
                                'special_req' => $row['M'],
                                'duty' => $row['N'],
                                'atiga' => $row['O'],
                                'acfta' => $row['P'],
                                'ppn' => $row['Q'],
                                'ppnbm' => $row['R'],
                                'pph' => $row['S'],
                                'bmtp' => $row['T'],
                                'status' => 1
                            );
                            $this->db->insert('m_hs_code', $datas) or die('error');	
                        }	
                  }
                  $numrow++;
                  $hasil = array('result' => true, 'data' => null, 'msg' => 'Data berhasil disimpan.');
              }
              $path = './assets/upload_temp/'.$upload_data['file_name'];
              unlink($path);
          }else{
              $hasil = array('result' => true, 'data' => null, 'msg' => 'File Gagal di upload.');
          }
      }else{
          $hasil = array('result' => true, 'data' => null, 'msg' => 'File tidak boleh Kosong.');
      }
      
      echo $this->output($format, $hasil);
  }
   
   // function getEmployeeList($action = "home") {
	// 	$data['ColHeader'] = array('NIK', 'Name');
	// 	$data['ColShow'] = array(1, 1);
	// 	$data['columns'] = array(
	// 		array('data' => 'nik'),
	// 		array('data' => 'name_employee'),
	// 	);
	// 	$data['columnDefs'] = array(
	// 		array('targets' => 0, 'orderable' => false, 'searchable' => true),
	// 		array('targets' => 1, 'orderable' => true, 'searchable' => true),
	// 	);
	// 	switch ($action) {
	// 		case 'nav':
	// 			$this->output->set_content_type('application/json');
	// 			echo $this->m_app->getEmployeeList();
	// 			break;
	// 		case 'home':
	// 			$data['Judul'] = 'Employee List';
	// 			$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
	// 			$data['end_point'] = '';
	// 			$this->load->view('LOV', $data);
	// 			break;
	// 	}
	// }
	
	// function getEmployeeEmail($action = "home") {
	// 	$data['ColHeader'] = array('NIK', 'Name', 'Email');
	// 	$data['ColShow'] = array(1, 1, 1);
	// 	$data['columns'] = array(
	// 		array('data' => 'nik'),
	// 		array('data' => 'name_employee'),
	// 		array('data' => 'email'),
	// 	);
	// 	$data['columnDefs'] = array(
	// 		array('targets' => 0, 'orderable' => false, 'searchable' => true),
	// 		array('targets' => 1, 'orderable' => true, 'searchable' => true),
	// 		array('targets' => 2, 'orderable' => false, 'searchable' => true),
	// 	);
	// 	switch ($action) {
	// 		case 'nav':
	// 			$this->output->set_content_type('application/json');
	// 			echo $this->m_app->getEmployeeEmail();
	// 			break;
	// 		case 'home':
	// 			$data['Judul'] = 'Employee List';
	// 			$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
	// 			$data['end_point'] = '';
	// 			$this->load->view('LOV', $data);
	// 			break;
	// 	}
	// }



	// function getKelasList($action = "home") {
	// 	$data['ColHeader'] = array('Kode Paket', 'Nama Paket');
	// 	$data['ColShow'] = array(1, 1);
	// 	$data['columns'] = array(
	// 		array('data' => 'kode_paket'),
	// 		array('data' => 'nama_paket'),
	// 	);
	// 	$data['columnDefs'] = array(
	// 		array('targets' => 0, 'orderable' => false, 'searchable' => true),
	// 		array('targets' => 1, 'orderable' => true, 'searchable' => true),
	// 	);
	// 	switch ($action) {
	// 		case 'nav':
	// 			$this->output->set_content_type('application/json');
	// 			echo $this->m_app->getPaketList();
	// 			break;
	// 		case 'home':
	// 			$data['Judul'] = 'Daftar Kelas Paud';
	// 			$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
	// 			$data['end_point'] = '';
	// 			$this->load->view('LOV', $data);
	// 			break;
	// 	}
	// }
	// function getDivisiList($action = "home") {
	// 	$data['ColHeader'] = array('Divisi Code', 'Divisi Name');
	// 	$data['ColShow'] = array(1, 1);
	// 	$data['columns'] = array(
	// 		array('data' => 'divisi_code'),
	// 		array('data' => 'divisi_name'),
	// 	);
	// 	$data['columnDefs'] = array(
	// 		array('targets' => 0, 'orderable' => false, 'searchable' => true),
	// 		array('targets' => 1, 'orderable' => true, 'searchable' => true),
	// 	);
	// 	switch ($action) {
	// 		case 'nav':
	// 			$this->output->set_content_type('application/json');
	// 			echo $this->m_app->getDivisiList();
	// 			break;
	// 		case 'home':
	// 			$data['Judul'] = 'Divisi List';
	// 			$data['src_url'] = base_url().$this->data['modules'].'/'.$this->data['controller'].'/'.$this->data['action'];
	// 			$data['end_point'] = '';
	// 			$this->load->view('LOV', $data);
	// 			break;
	// 	}
	// }

	// function getDivisiSelect2($format = 'json') {
	// 	# START Check input, jika tidak ada input atau kosong maka tidak di lanjutkan
	// 	$raw = $this->security->xss_clean($this->input->raw_input_stream);
	// 	$input = $this->input($format, $raw);
		
	// 	$hasil = $this->m_app->getDivisiSelect2($input);
	// 	echo $this->output($format, $hasil);
	// }

}