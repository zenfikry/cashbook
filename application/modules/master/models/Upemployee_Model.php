<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Upemployee_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	// 	function buat_kode($input)   {
	
// /*		$this->db->select('max(mclass_id) as kode', FALSE);	
// 			$this->db->where('dept_id=',$input['dept_id']);		
// 			$this->db->order_by('mclass_id','DESC');   			 
// 			$this->db->limit(1);    
// 			$query = $this->db->get('m_mclass_baru'); */     //cek dulu apakah ada sudah ada kode di tabel.   
		  			
// 			$query = $this->db->query("SELECT max(mclass_id) as kode
// 										FROM m_mclass_baru 
// 										WHERE dept_id=?", $input['dept_id']);
					  
			  
// 			if($query->num_rows() <> 0){      
// 				//jika kode ternyata sudah ada.      
// 				$data = $query->row();      
// 				$kode = (int) substr($data->kode, -1, 2) + 1;
// 					}
// 					else {      
// 					//jika kode belum ada      
// 					$kode = 1;
									   
// 					}
					 
// 					   $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); // angka 2 menunjukkan jumlah digit angka 0


// 			  $kodejadi = $kodemax;   
// 			  return $kodejadi;  		
			  
// 			  }


	public function save($input) {
		// $cek = $this->db->query("SELECT * FROM m_hs_code WHERE barcode=?", array($input['barcode']));
        // if ($cek->row() != null) {
        //     return array('result' => false, 'data' => null, 'msg' => 'Barcode sudah ada.');
        // }

        $data_input = (object) $input;
        $data2send = json_decode($data_input->data2Send);
		
		$data = array(
            'barcode' => $data2send->barcode,
            'keterangan' => $data2send->keterangan,
            'material' => $data2send->material,
            'code_china' => $data2send->code_china,
            'code_indo' => $data2send->code_indo,
            'sni' => $data2send->sni,
            'ls' => $data2send->ls,
            'lartas' => $data2send->lartas,
            'special_req' => $data2send->special_req,
            'duty' => $data2send->duty,
            'atiga' => $data2send->atiga,
            'acfta' => $data2send->acfta,
            'ppn' => $data2send->ppn,
            'ppnbm' => $data2send->ppnbm,
            'pph' => $data2send->pph,
            'bmtp' => $data2send->bmtp,
            'photo' => $data_input->photo,
            'status' => $data2send->status,
            'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s'),
      );
		
		$NonQry = $this->db->insert('m_hs_code', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil disimpan.');
		}
	}
	


	public function update($input) {
        $data_input = (object) $input;
        $data2send = json_decode($data_input->data2Send);
		$data = array(
            'barcode' => $data2send->barcode,
            'keterangan' => $data2send->keterangan,
            'material' => $data2send->material,
            'code_china' => $data2send->code_china,
            'code_indo' => $data2send->code_indo,
            'sni' => $data2send->sni,
            'ls' => $data2send->ls,
            'lartas' => $data2send->lartas,
            'special_req' => $data2send->special_req,
            'duty' => $data2send->duty,
            'atiga' => $data2send->atiga,
            'acfta' => $data2send->acfta,
            'ppn' => $data2send->ppn,
            'ppnbm' => $data2send->ppnbm,
            'pph' => $data2send->pph,
            'bmtp' => $data2send->bmtp,
            'photo' => $data_input->photo,
            'status' => $data2send->status,
            'modified_by' => $this->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
		);
		
		// OPEN TRANSACTION
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$this->db->where('barcode', $data['barcode']);
		$NonQry = $this->db->update('m_hs_code', $data);
		
		$this->db->trans_complete(); # Completing transaction
		/*Optional*/
		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil diupdate.');
		}
	}

    public function updatenonphoto($input)
    {
        $data_input = (object) $input;
        $data2send = json_decode($data_input->data2Send);

        $data = array(
            'barcode' => $data2send->barcode,
            'keterangan' => $data2send->keterangan,
            'material' => $data2send->material,
            'code_china' => $data2send->code_china,
            'code_indo' => $data2send->code_indo,
            'sni' => $data2send->sni,
            'ls' => $data2send->ls,
            'lartas' => $data2send->lartas,
            'special_req' => $data2send->special_req,
            'duty' => $data2send->duty,
            'atiga' => $data2send->atiga,
            'acfta' => $data2send->acfta,
            'ppn' => $data2send->ppn,
            'ppnbm' => $data2send->ppnbm,
            'pph' => $data2send->pph,
            'bmtp' => $data2send->bmtp,
            'status' => $data2send->status,
        );
        $this->db->where('barcode', $data2send->barcode);
        $NonQry = $this->db->update('m_hs_code', $data);
        // $QryChi = $this->db->update('m_items_price', $datachild);

        if (!$NonQry && !empty($this->db->error())) {
            $msg_err = $this->db->error();
            $msg = explode(':', $msg_err['message']);
            return array('result' => false, 'data' => null, 'msg' => 'Gagal input. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
        } else {
            return array('result' => true, 'data' => null, 'msg' => 'Data berhasil disimpan.');
        }
    }

	function delete($input) {
		$id = $input['id'];

		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_hs_code");
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		}
	}
	
	function getData2Edit($id) {
		$Qry = $this->db->query("SELECT * FROM m_hs_code a
                                        WHERE a.id=?", array($id));
													
		if ($Qry->result() != NULL){
			// return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
            return $Qry->row_array();
		} else {
			return array('result' => false, 'msg' => 'Data tidak ditemukan.', 'data' => NULL);
		}
	}
	
	function getList($filter) {
		$this->datatables->select("a.id, a.nik, a.name_employee, b.name_jabatan, a.email, a.nohp, a.status ");
      $this->datatables->from("m_employee a");
      $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan", "left");
		foreach($filter as $key => $val) { 
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->datatables->where($key, $val);
			}
		}
		return $this->datatables->generate();
	}
	
	function getDataList($filter) {
		$this->db->select("a.id, a.barcode, a.keterangan, a.material, a.code_indo, a.sni, a.ls,
        a.lartas, a.special_req, a.duty, a.atiga, a.acfta, a.ppn, a.ppnbm, a.pph, a.bmtp, a.status");
      $this->db->from("m_hs_code a");
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->db->where($key, $val);
			}
		}
		return $this->db->get();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	function add_data($datas){
    	return $this->db->insert_batch("m_hs_code",$datas);
    }
  	// function getEmployeeList() {
	// 	$this->datatables->select('nik, name_employee');
	// 	$this->datatables->from('m_employee');
	// 	return $this->datatables->generate();
	// }
  	
	// function getEmployeeEmail() {
	// 	$this->datatables->select('nik, name_employee, email');
	// 	$this->datatables->from('m_employee');
	// 	$this->datatables->where('code_jabatan', 'BM');
	// 	return $this->datatables->generate();
	// }

	// function getEmployeeSelect2($input) {
	// 	$Qry = $this->db->query("SELECT nik, name_employee FROM m_employee WHERE nik LIKE ? OR name_employee LIKE ?", 
	// 								   array('%'.$input['keyword'].'%', '%'.$input['keyword'].'%'));
	// 	return $Qry->result();
	// }

	// function getEmployee() {
	// 	$Qry = $this->db->query("SELECT nik, name_employee FROM m_employee");
	// 	return $Qry->result();
	// }

}