<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Creditor_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	// 	function buat_kode($input)   {
	
// /*		$this->db->select('max(mclass_id) as kode', FALSE);	
// 			$this->db->where('dept_id=',$input['dept_id']);		
// 			$this->db->order_by('mclass_id','DESC');   			 
// 			$this->db->limit(1);    
// 			$query = $this->db->get('m_mclass_baru'); */     //cek dulu apakah ada sudah ada kode di tabel.   
		  			
// 			$query = $this->db->query("SELECT max(mclass_id) as kode
// 										FROM m_mclass_baru 
// 										WHERE dept_id=?", $input['dept_id']);
					  
			  
// 			if($query->num_rows() <> 0){      
// 				//jika kode ternyata sudah ada.      
// 				$data = $query->row();      
// 				$kode = (int) substr($data->kode, -1, 2) + 1;
// 					}
// 					else {      
// 					//jika kode belum ada      
// 					$kode = 1;
									   
// 					}
					 
// 					   $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); // angka 2 menunjukkan jumlah digit angka 0


// 			  $kodejadi = $kodemax;   
// 			  return $kodejadi;  		
			  
// 			  }


	function save($input) {
		$cek = $this->db->query("SELECT * FROM m_creditor WHERE code_creditor=?", array($input['code_creditor']));
        if ($cek->row() != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code Creditor was exist.');
        }
		
		$data = array(
			'code_creditor' => $input['code_creditor'],
			'desc_creditor' => $input['desc_creditor'],
			'status' => $input['status'],
            'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s')
      );
		
		$NonQry = $this->db->insert('m_creditor', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Failed to input. '.$msg[0].': '.$msg[1].', value : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data Saved.');
		}
	}
	
	function update($input) {
		$data = array(
			'code_creditor' => $input['code_creditor'],
			'desc_creditor' => $input['desc_creditor'],
			'status' => $input['status'],
            'modified_by' => $this->session->userdata('user_id'),
			'modification_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->where('id', $input['id']);
		$NonQry = $this->db->update('m_creditor', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Failed to input. '.$msg[0].': '.$msg[1].', value : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data Updated.');
		}
	}

	function delete($input) {
		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_creditor");
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Failed to input. '.$msg[0].': '.$msg[1].', value : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data Deleted.');
		}
	}
	
	function getData2Edit($id) {
		$Qry = $this->db->query("SELECT id, code_creditor, desc_creditor, status
										FROM m_creditor 
										WHERE id=?", array($id));
													
		if ($Qry->result() != NULL){
			return array('result' => true, 'msg' => 'Data found.', 'data' => $Qry->row_array());
		} else {
			return array('result' => false, 'msg' => 'Data not found.', 'data' => NULL);
		}
	}
	
	function getList($filter) {
		$this->datatables->select("id, code_creditor, desc_creditor, status");
      $this->datatables->from('m_creditor');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->datatables->where($key, $val);
			}
		}
		return $this->datatables->generate();
	}
	
	function getDataList($filter) {
		$this->db->select("code_creditor, desc_creditor, CASE WHEN status=1 THEN 'Active' ELSE 'Not Active' END AS status");
      $this->db->from('m_creditor');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->db->where($key, $val);
			}
		}
		return $this->db->get();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	
  	function getDeptList() {
		$this->datatables->select('code_creditor, desc_creditor');
		$this->datatables->from('m_creditor');
		$this->datatables->where('status', '1');
		return $this->datatables->generate();
	}

	// function getStoreSelect2($input) {
	// 	$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store WHERE m_code LIKE ? OR m_shortdesc LIKE ?", 
	// 								   array('%'.$input['keyword'].'%', '%'.$input['keyword'].'%'));
	// 	return $Qry->result();
	// }

	// function getStore() {
	// 	$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store");
	// 	return $Qry->result();
	// }

}