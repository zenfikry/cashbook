<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Store_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	// 	function buat_kode($input)   {
	
// /*		$this->db->select('max(mclass_id) as kode', FALSE);	
// 			$this->db->where('dept_id=',$input['dept_id']);		
// 			$this->db->order_by('mclass_id','DESC');   			 
// 			$this->db->limit(1);    
// 			$query = $this->db->get('m_mclass_baru'); */     //cek dulu apakah ada sudah ada kode di tabel.   
		  			
// 			$query = $this->db->query("SELECT max(mclass_id) as kode
// 										FROM m_mclass_baru 
// 										WHERE dept_id=?", $input['dept_id']);
					  
			  
// 			if($query->num_rows() <> 0){      
// 				//jika kode ternyata sudah ada.      
// 				$data = $query->row();      
// 				$kode = (int) substr($data->kode, -1, 2) + 1;
// 					}
// 					else {      
// 					//jika kode belum ada      
// 					$kode = 1;
									   
// 					}
					 
// 					   $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); // angka 2 menunjukkan jumlah digit angka 0


// 			  $kodejadi = $kodemax;   
// 			  return $kodejadi;  		
			  
// 			  }


	function save($input) {
		$cek = $this->db->query("SELECT * FROM m_store WHERE m_code=?", array($input['m_code']));
        if ($cek->row() != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code Store sudah ada.');
        }
		
		$data = array(
			'm_code' => $input['m_code'],
			'm_shortdesc' => $input['m_shortdesc'],
			'm_odesc' => $input['m_odesc'],
			'm_type' => $input['m_type'],
			'm_pic' => $input['m_pic'],
			'm_email' => $input['m_email'],
			'status' => $input['status'],
            'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s')
      );
		
		$NonQry = $this->db->insert('m_store', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil disimpan.');
		}
	}
	
	function update($input) {
		$data = array(
			'm_code' => $input['m_code'],
			'm_shortdesc' => $input['m_shortdesc'],
			'm_odesc' => $input['m_odesc'],
			'm_type' => $input['m_type'],
			'm_pic' => $input['m_pic'],
			'm_email' => $input['m_email'],
			'status' => $input['status'],
            'modified_by' => $this->session->userdata('user_id'),
			'modification_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->where('id', $input['id']);
		$NonQry = $this->db->update('m_store', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil diupdate.');
		}
	}

	function delete($input) {
		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_store");
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		}
	}
	
	function getData2Edit($id) {
		$Qry = $this->db->query("SELECT id, m_code, m_shortdesc, m_odesc, m_type, status, m_pic, m_email
										FROM m_store 
										WHERE id=?", array($id));
													
		if ($Qry->result() != NULL){
			return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
		} else {
			return array('result' => false, 'msg' => 'Data tidak ditemukan.', 'data' => NULL);
		}
	}
	
	function getList($filter) {
		$this->datatables->select('a.id, a.m_code, a.m_shortdesc, a.m_odesc, a.m_type, a.m_pic, a.m_email, a.status');
      $this->datatables->from('m_store a');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->datatables->where($key, $val);
			}
		}
		return $this->datatables->generate();
	}
	
	function getDataList() {
		$this->db->select("m_code, m_shortdesc, m_odesc, m_type");
      $this->db->from('m_store');
		// foreach($filter as $key => $val) {
		// 	if (trim($val) != "" || !empty($val) || $val != NULL) {
		// 		$this->db->where($key, $val);
		// 	}
		// }
		return $this->db->get();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	
  	function getStoreList() {
		$this->datatables->select('m_code, m_shortdesc, m_pic, m_type');
		$this->datatables->from('m_store');
		return $this->datatables->generate();
	}

	function getStoreSelect2($input) {
		$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store WHERE m_code LIKE ? OR m_shortdesc LIKE ?", 
									   array('%'.$input['keyword'].'%', '%'.$input['keyword'].'%'));
		return $Qry->result();
	}

	function getStore() {
		$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store");
		return $Qry->result();
	}

	function getCompList() {
		$this->datatables->select('id as company_code, name as company_name');
		$this->datatables->from('m_perusahaan');
		return $this->datatables->generate();
	}

}