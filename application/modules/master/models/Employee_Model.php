<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Employee_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	// 	function buat_kode($input)   {
	
// /*		$this->db->select('max(mclass_id) as kode', FALSE);	
// 			$this->db->where('dept_id=',$input['dept_id']);		
// 			$this->db->order_by('mclass_id','DESC');   			 
// 			$this->db->limit(1);    
// 			$query = $this->db->get('m_mclass_baru'); */     //cek dulu apakah ada sudah ada kode di tabel.   
		  			
// 			$query = $this->db->query("SELECT max(mclass_id) as kode
// 										FROM m_mclass_baru 
// 										WHERE dept_id=?", $input['dept_id']);
					  
			  
// 			if($query->num_rows() <> 0){      
// 				//jika kode ternyata sudah ada.      
// 				$data = $query->row();      
// 				$kode = (int) substr($data->kode, -1, 2) + 1;
// 					}
// 					else {      
// 					//jika kode belum ada      
// 					$kode = 1;
									   
// 					}
					 
// 					   $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); // angka 2 menunjukkan jumlah digit angka 0


// 			  $kodejadi = $kodemax;   
// 			  return $kodejadi;  		
			  
// 			  }


	function save($input) {
		$cek = $this->db->query("SELECT * FROM m_employee WHERE nik=?", array($input['nik']));
        if ($cek->row() != null) {
            return array('result' => false, 'data' => null, 'msg' => 'NIK atau Nama sudah ada.');
        }
		
		$data = array(
			'nik' => $input['nik'],
			'name_employee' => $input['name_employee'],
            #'password' => $input['password'],
            'code_jabatan' => $input['code_jabatan'],
			'email' => $input['email'],
			'nohp' => $input['nohp'],
            'status' => $input['status'],
            'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s'),
      );

	//   if ($input['role_id'] == 'STORE') {
		  $logs = array(
				'user_id' => $input['nik'],
				'user_pass' => password_hash($input['user_pass'], PASSWORD_DEFAULT),
				'name' => $input['name_employee'],
				'nik' => $input['nik'],
				'role_id' => $input['role_id'],
				'active' => $input['status'],
				'created_by' => $this->session->userdata('user_id'),
				'creation_date' => date('Y-m-d H:i:s'),
		  );
	// 	} else {
	// 	  $logs = array( 
	// 			'user_id' => $input['nik'],
	// 			'user_pass' => password_hash($input['user_pass'], PASSWORD_DEFAULT),
	// 			'name' => $input['name_employee'],
	// 			'nik' => $input['nik'],
	// 			'role_id' => $input['role_id'],
	// 			'active' => $input['status'],
	// 			'created_by' => $this->session->userdata('user_id'),
	// 			'creation_date' => date('Y-m-d H:i:s'),
	// 	  );
	//   }
		
		$NonQry = $this->db->insert('m_employee', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			$LogQry = $this->db->insert('app_users', $logs);
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil disimpan.');
		}
	}
	
	function update($input) {
		$data = array(
			'nik' => $input['nik'],
			'name_employee' => $input['name_employee'],
            // 'password' => $input['password'],
            'code_jabatan' => $input['code_jabatan'],
			'email' => $input['email'],
			'nohp' => $input['nohp'],
            'status' => $input['status'],
            'modified_by' => $this->session->userdata('user_id'),
            'modification_date' => date('Y-m-d H:i:s'),
		);

		// $data_user = array(
		// 	'user_id' => $input['nik'],
		// 	'name' => $input['name'],
		// 	'user_pass' => $input['password'],
		// 	'employee_id' => $input['nik'],
		// 	'role_id' => "STAFF",
		// 	'def_store_id' => "0000",
		// 	'active' => 1,
		// 	'modified_by' => $this->session->userdata('user_id'),
		// 	'modification_date'	=> date('Y-m-d H:i:s')
		// );
		
		// OPEN TRANSACTION
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$this->db->where('id', $input['id']);
		$NonQry = $this->db->update('m_employee', $data);

		// $this->db->where('user_id', $input['nik']);
		// $NonQry = $this->db->update('app_users', $data_user);
		
		$this->db->trans_complete(); # Completing transaction
		/*Optional*/
		if ($this->db->trans_status() === FALSE) {
			# Something went wrong.
			$this->db->trans_rollback();
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} 
		else {
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil diupdate.');
		}
		

		// $this->db->where('id', $input['id']);
		// $NonQry = $this->db->update('m_employee', $data);
		
		// if (!$NonQry && !empty($this->db->error())) {
		// 	$msg_err = $this->db->error();
		// 	$msg = explode(':',$msg_err['message']);
		// 	return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		// } else {
		// 	return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil diupdate.');
		// }
	}

	function delete($input) {
		$id = $input['id'];
		// $getAppuser = $this->db->query("SELECT app_users.id FROM app_users JOIN m_employee ON m_employee.nik = app_users.user_id WHERE m_employee.id = $id");
		// $result =$getAppuser->result_array();
		
		// // OPEN TRANSACTION
		// $this->db->trans_start(); # Starting Transaction
		// $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		// $this->db->where_in('id', $input['id']);
		// $NonQry = $this->db->delete("m_employee");

		// $this->db->where_in('id', $result[0]['id']);
		// $NonQry = $this->db->delete("app_users");

		// $this->db->trans_complete(); # Completing transaction
		// /*Optional*/
		// if ($this->db->trans_status() === FALSE) {
		// 	# Something went wrong.
		// 	$this->db->trans_rollback();
		// 	return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		// } 
		// else {
		// 	# Everything is Perfect. 
		// 	# Committing data to the database.
		// 	$this->db->trans_commit();
		// 	return  array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		// }

		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_employee");
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		}
	}
	
	function getData2Edit($id) {
		$Qry = $this->db->query("SELECT a.id, a.nik, a.name_employee, a.code_jabatan, b.name_jabatan, a.email, a.nohp, a.status 
										FROM m_employee a
                                        LEFT JOIN m_jabatan b ON b.code_jabatan = a.code_jabatan
										WHERE a.id=?", array($id));
													
		if ($Qry->result() != NULL){
			return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
		} else {
			return array('result' => false, 'msg' => 'Data tidak ditemukan.', 'data' => NULL);
		}
	}
	
	function getList($filter) {
		$this->datatables->select("a.id, a.nik, a.name_employee, b.name_jabatan, c.name_dept, a.email, a.nohp, a.status");
      $this->datatables->from("m_employee a");
      $this->datatables->join("m_jabatan b","b.code_jabatan=a.code_jabatan", "left");
	  $this->datatables->join("m_dept c", "c.code_dept=b.code_dept", "left");
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->datatables->where($key, $val);
			}
		}
		return $this->datatables->generate();
	}
	
	function getDataList($filter) {
		$this->db->select("a.id, a.nik, a.name, b.name_jabatan, c.name_dept, a.email, a.nohp, a.status");
      $this->db->from("m_employee a");
      $this->db->join("m_jabatan b","b.code_jabatan=a.code_jabatan","left");
	  $this->db->join("m_dept c", "c.code_dept=b.code_dept", "left");
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->db->where($key, $val);
			}
		}
		return $this->db->get();
	}

	public function getCodeEmployee($search){
		$this->db->select('id as id,code_adv_employee,name as text');
		$this->db->from("m_code_employee");
		$this->db->like('code_adv_employee', "$search");
		$this->db->or_like('name',"$search");
		return $this->db->get()->result_array();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	
  	function getEmployeeList() {
		$this->datatables->select('nik, name_employee');
		$this->datatables->from('m_employee');
		return $this->datatables->generate();
	}
  	function getEmployeeListExpense() {
		$this->datatables->select('m_employee.nik, m_employee.name_employee,m_jabatan.code_dept,name_dept,m_employee.code_jabatan,m_jabatan.name_jabatan');
		$this->datatables->from('m_employee');
		$this->datatables->join('m_jabatan','m_jabatan.code_jabatan = m_employee.code_jabatan');
		$this->datatables->join('m_dept','m_dept.code_dept = m_jabatan.code_dept');
		return $this->datatables->generate();
	}
  	
	function getEmployeeEmail() {
		$this->datatables->select('nik, name_employee, email');
		$this->datatables->from('m_employee');
		$this->datatables->where('code_jabatan', 'BM');
		return $this->datatables->generate();
	}

	function getEmployeeSelect2($input) {
		$Qry = $this->db->query("SELECT nik, name_employee FROM m_employee WHERE nik LIKE ? OR name_employee LIKE ?", 
									   array('%'.$input['keyword'].'%', '%'.$input['keyword'].'%'));
		return $Qry->result();
	}

	function getEmployee() {
		$Qry = $this->db->query("SELECT nik, name_employee FROM m_employee");
		return $Qry->result();
	}

	public function getRoleList()
    {
        $this->datatables->select('a.role_id, a.role_name, a.desc');
        $this->datatables->from('app_roles a');
        return $this->datatables->generate();
    }

}