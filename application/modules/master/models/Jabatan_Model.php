<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Jabatan_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	// 	function buat_kode($input)   {
	
// /*		$this->db->select('max(mclass_id) as kode', FALSE);	
// 			$this->db->where('dept_id=',$input['dept_id']);		
// 			$this->db->order_by('mclass_id','DESC');   			 
// 			$this->db->limit(1);    
// 			$query = $this->db->get('m_mclass_baru'); */     //cek dulu apakah ada sudah ada kode di tabel.   
		  			
// 			$query = $this->db->query("SELECT max(mclass_id) as kode
// 										FROM m_mclass_baru 
// 										WHERE dept_id=?", $input['dept_id']);
					  
			  
// 			if($query->num_rows() <> 0){      
// 				//jika kode ternyata sudah ada.      
// 				$data = $query->row();      
// 				$kode = (int) substr($data->kode, -1, 2) + 1;
// 					}
// 					else {      
// 					//jika kode belum ada      
// 					$kode = 1;
									   
// 					}
					 
// 					   $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); // angka 2 menunjukkan jumlah digit angka 0


// 			  $kodejadi = $kodemax;   
// 			  return $kodejadi;  		
			  
// 			  }


	function save($input) {
		$cek = $this->db->query("SELECT * FROM m_jabatan WHERE code_jabatan=?", array($input['code_jabatan']));
        if ($cek->row() != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code Jabatan sudah ada.');
        }
		
		$data = array(
			'code_jabatan' => $input['code_jabatan'],
            'code_dept' => $input['code_dept'],
			'name_jabatan' => $input['name_jabatan'],
			'code_atasan' => $input['code_atasan'],
			'status' => $input['status'],
            'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s')
      );
		
		$NonQry = $this->db->insert('m_jabatan', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil disimpan.');
		}
	}
	
	function update($input) {
		$data = array(
            'code_jabatan' => $input['code_jabatan'],
			'code_dept' => $input['code_dept'],
			'name_jabatan' => $input['name_jabatan'],
			'code_atasan' => $input['code_atasan'],
			'status' => $input['status'],
            'modified_by' => $this->session->userdata('user_id'),
			'modification_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->where('id', $input['id']);
		$NonQry = $this->db->update('m_jabatan', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil diupdate.');
		}
	}

	function delete($input) {
		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_jabatan");
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		}
	}
	
	function getData2Edit($id) {
		$Qry = $this->db->query("SELECT a.id, a.code_jabatan, a.code_dept, b.name_dept, a.name_jabatan, a.code_atasan, a.status
										FROM m_jabatan a
                                        LEFT JOIN m_dept b ON b.code_dept = a.code_dept
										WHERE a.id=?", array($id));
													
		if ($Qry->result() != NULL){
			return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
		} else {
			return array('result' => false, 'msg' => 'Data tidak ditemukan.', 'data' => NULL);
		}
	}
	
	function getList($filter) {
		$this->datatables->select("a.id, a.code_jabatan, b.name_dept, a.name_jabatan, a.code_atasan, a.status");
      $this->datatables->from('m_jabatan a');
      $this->datatables->join('m_dept b', 'b.code_dept = a.code_dept', 'left');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->datatables->where($key, $val);
			}
		}
		return $this->datatables->generate();
	}
	
	function getDataList($filter) {
		$this->db->select("a.code_jabatam, a.code_dept, b.name_dept, a.name_jabatan, a.code_atasan, CASE WHEN a.status=1 THEN 'Aktif' ELSE 'Tidak Aktif' END AS status");
      $this->db->from('m_jabatan a');
      $this->db->join('m_dept b', 'b.code_dept = a.code_dept', 'left');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->db->where($key, $val);
			}
		}
		return $this->db->get();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
	
  	function getJabatanList() {
		$this->datatables->select('code_jabatan, name_jabatan');
		$this->datatables->from('m_jabatan');
		return $this->datatables->generate();
	}

	// function getStoreSelect2($input) {
	// 	$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store WHERE m_code LIKE ? OR m_shortdesc LIKE ?", 
	// 								   array('%'.$input['keyword'].'%', '%'.$input['keyword'].'%'));
	// 	return $Qry->result();
	// }

	// function getStore() {
	// 	$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store");
	// 	return $Qry->result();
	// }

}