<?php if (! defined('BASEPATH')) {exit('No direct script access allowed');}

class Coa_Model extends Core_Model {
	
	function __construct(){
        parent::__construct();
   }
	
	/*******************************************************************************
   *                            START DEFAULT FUNCTION                            *
   *******************************************************************************/
	// 	function buat_kode($input)   {
	
// /*		$this->db->select('max(mclass_id) as kode', FALSE);	
// 			$this->db->where('dept_id=',$input['dept_id']);		
// 			$this->db->order_by('mclass_id','DESC');   			 
// 			$this->db->limit(1);    
// 			$query = $this->db->get('m_mclass_baru'); */     //cek dulu apakah ada sudah ada kode di tabel.   
		  			
// 			$query = $this->db->query("SELECT max(mclass_id) as kode
// 										FROM m_mclass_baru 
// 										WHERE dept_id=?", $input['dept_id']);
					  
			  
// 			if($query->num_rows() <> 0){      
// 				//jika kode ternyata sudah ada.      
// 				$data = $query->row();      
// 				$kode = (int) substr($data->kode, -1, 2) + 1;
// 					}
// 					else {      
// 					//jika kode belum ada      
// 					$kode = 1;
									   
// 					}
					 
// 					   $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); // angka 2 menunjukkan jumlah digit angka 0


// 			  $kodejadi = $kodemax;   
// 			  return $kodejadi;  		
			  
// 			  }


	function save($input) {
		$cek = $this->db->query("SELECT * FROM m_coa WHERE code_coa=?", array($input['code_coa']));
        if ($cek->row() != null) {
            return array('result' => false, 'data' => null, 'msg' => 'Code COA sudah ada.');
        }
		
		$data = array(
			'code_coa' => $input['code_coa'],
			'name_coa' => $input['name_coa'],
			'dk' => $input['dk'],
			'status' => $input['status'],
            'created_by' => $this->session->userdata('user_id'),
			'creation_date' => date('Y-m-d H:i:s')
      );
		
		$NonQry = $this->db->insert('m_coa', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil disimpan.');
		}
	}
	
	function update($input) {
		$data = array(
			'code_coa' => $input['code_coa'],
			'name_coa' => $input['name_coa'],
			'dk' => $input['dk'],
			'status' => $input['status'],
            'modified_by' => $this->session->userdata('user_id'),
			'modification_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->where('id', $input['id']);
		$NonQry = $this->db->update('m_coa', $data);
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil diupdate.');
		}
	}

	function delete($input) {
		$this->db->where_in('id', $input['id']);
		$NonQry = $this->db->delete("m_coa");
		
		if (!$NonQry && !empty($this->db->error())) {
			$msg_err = $this->db->error();
			$msg = explode(':',$msg_err['message']);
			return array('result' => false, 'data' => NULL, 'msg' => 'Gagal input. '.$msg[0].': '.$msg[1].', nilai : '.str_replace('LINE 1','',$msg[2]));
		} else {
			return array('result' => true, 'data' => NULL, 'msg' => 'Data berhasil dihapus.');
		}
	}
	
	function getData2Edit($id) {
		$Qry = $this->db->query("SELECT id, code_coa, name_coa, dk, status
										FROM m_coa 
										WHERE id=?", array($id));
													
		if ($Qry->result() != NULL){
			return array('result' => true, 'msg' => 'Data ditemukan.', 'data' => $Qry->row_array());
		} else {
			return array('result' => false, 'msg' => 'Data tidak ditemukan.', 'data' => NULL);
		}
	}
	
	function getList($filter) {
		$this->datatables->select("id, code_coa, name_coa, dk, status");
      $this->datatables->from('m_coa');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->datatables->where($key, $val);
			}
		}
		return $this->datatables->generate();
	}
	
	function getDataList($filter) {
		$this->db->select("code_coa, name_coa, CASE WHEN dk='D' THEN 'DEBET' ELSE 'KREDIT' END AS dk, CASE WHEN status=1 THEN 'Aktif' ELSE 'Tidak Aktif' END AS status");
      $this->db->from('m_coa');
		foreach($filter as $key => $val) {
			if (trim($val) != "" || !empty($val) || $val != NULL) {
				$this->db->where($key, $val);
			}
		}
		return $this->db->get();
	}
	
	/*******************************************************************************
   *                              END DEFAULT FUNCTION                            *
   *******************************************************************************/
  public function get_coa($input)
  {
	  $this->db->select("a.code_coa, a.name_coa");
	  $this->db->from("m_coa a");
	  $this->db->where("a.status", 1);
	  $this->db->group_start();
	  $this->db->where("a.code_coa", $input['code_coa']);
	  $this->db->group_end();

	  $rs = $this->db->get();
	  $err_db = $this->db->error(); # Tangkap error

	  if (!empty($err_db['message'])) {
		  $msg = explode(PHP_EOL, str_replace(str_split('\"'), '', $err_db['message']));
		  return array('result' => false, 'data' => null, 'message' => 'Gagal ambil data. ' . $msg[0] . ': ' . $msg[1] . ', nilai : ' . str_replace('LINE 1', '', $msg[2]));
	  } elseif ($rs->row() != null) {
		  return array('result' => true, 'message' => 'Data ditemukan.', 'data' => $rs->row());
	  } else {
		  return array('result' => false, 'message' => 'Data tidak ditemukan.', 'data' => null);
	  }
  }

  
  	// function getStoreList() {
	// 	$this->datatables->select('m_code, m_shortdesc');
	// 	$this->datatables->from('m_store');
	// 	return $this->datatables->generate();
	// }

	// function getStoreSelect2($input) {
	// 	$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store WHERE m_code LIKE ? OR m_shortdesc LIKE ?", 
	// 								   array('%'.$input['keyword'].'%', '%'.$input['keyword'].'%'));
	// 	return $Qry->result();
	// }

	// function getStore() {
	// 	$Qry = $this->db->query("SELECT m_code, m_shortdesc FROM m_store");
	// 	return $Qry->result();
	// }

}