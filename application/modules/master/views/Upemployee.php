<style type="text/css">
.loader {
  border: 16px solid #2B3A4A;
  border-radius: 50%;
  border-top: 16px solid #349B9D;
  width: 70px;
  height: 70px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  display: none;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="row">
   <div class="col-md-6">
      <div class="page-title-box">
         <div class="button-items">
            <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" role="button" id="app_create" onclick="app_create();"> <i class="fas fa-plus"></i> Tambah </button>
            <button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light" id="app_refresh" name="app_refresh" onclick="app_refresh();"><i class="fas fa-sync"></i> Refresh </button>
         </div>
      </div>
   </div>
   <!-- <div class="col-md-6">
      <div class="page-title-box text-right">
         <div class="button-items">
            <?php if ($priv_arr['pdf_flag']) {?>
               <button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" id="app_pdf" name="app_pdf" onclick="app_pdf();"><i class="fas fa-file-pdf"></i> Pdf </button>
            <?php }?>
            <?php if ($priv_arr['xls_flag']) {?>
               <button type="button" class="btn btn-outline-success btn-sm waves-effect waves-light" id="app_xls" name="app_xls" onclick="app_xls();"><i class="fas fa-file-excel"></i> Excel/CSV </button>
            <?php }?>
         </div>
      </div>
   </div> -->
</div>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <div class="alert alert-primary  fade show" role="alert">
               <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
               <div class="alert-text">
                  <h4 class="alert-heading font-18">Upload Master Employee</h4>
                  <p>Module upload employee data.</p>
               </div>
               <div class="alert-close">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true"><i class="la la-close"></i></span>
                  </button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
<div class="col-md-3">&nbsp;</div>
	<div class="col-md-6 mt-5">
		<div class="card">
			<div class="card-body">
				<div class="row">
						<div class="col-md-9">
							<label>Upload Data Employee</label><br>
							<input type="file" name="import" id="import">
						</div>
						<!-- <div class="col-md-3 text-right">
							<button type="submit" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="download_data()"><i class="fa fa-download"></i> Download Format Template </button>
						</div> -->
						<div class="col-md-12 mt-3">
							<button type="submit" class="btn btn-outline-success btn-sm waves-effect waves-light" onclick="upload_data()"><i class="fa fa-paper-plane"></i> Submit </button>
						</div>
						<div class="col-md-12" align="center">
							<br><br>
							<div class="loader"></div>
						</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="col-md-3">&nbsp;</div>
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 3px;">
            <div class="kt-portlet kt-portlet--mobile">
            <table id="table_list_data" class="table table-striped table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
               <thead>
                  <tr>
                  <th>Id</th>
                  <th>Nik</th>
                  <th>Name Employee</th>
                  <th>Ocupation </th>
                  <th>Email </th>
                  <th>Handphone </th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Actions</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
   // START VARIABEL WAJIB
   var Modules = '<?=$modules?>';
   var Controller = '<?=$controller?>';
   var data2Send = null;
   var dataArr = [];
   var DataTable = null;
   // END VARIABEL WAJIB

   $.getScript('<?=base_url()?>assets/js/modules/' + Modules + '/' + Controller + '.js?v=<?=date('YmdHis') . rand()?>', function( data, textStatus, jqxhr ) {
      initPage();
   });
</script>