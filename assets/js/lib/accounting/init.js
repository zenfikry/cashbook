(function () {
	CurrencyFormat();
})();

function CurrencyFormat() {
   accounting.settings = {
     currency: {
       symbol : "",   // default currency symbol is '$'
       format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
       thousand: _thousandSeparator,  // thousands separator
       decimal : _decimalSeparator,  // decimal point separator
       precision : 0   // decimal places
     },
     number: {
       thousand: _thousandSeparator,
       decimal : _decimalSeparator,
       precision : 0  // default precision on numbers is 0
     }
   }  
 }