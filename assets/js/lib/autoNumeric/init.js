var formatNumber = {
	aSep: _thousandSeparator, 
	aDec: _decimalSeparator,
	lZero: false,
	aPad: false,
	mDec: '0',
	vMin: '-999999999999999', 
	vMax: '999999999999999',
	decimalCharacterAlternative: "."
};

var formatDecimalNumber = {
	aSep: _thousandSeparator, 
	aDec: _decimalSeparator,
	lZero: false,
	aPad: false,
	mDec: '2',
	vMin: '-999999999999999', 
	vMax: '999999999999999'
};