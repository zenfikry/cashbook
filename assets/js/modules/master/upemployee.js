(function () {
	// JS construct
		
})();

function initPage(){
	DataTable = $('#table_list_data').DataTable({ 
		"pageLength": 10,
		"numbers_length": 9,
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"scrollCollapse": true,
		"fixedColumns": true,
		"fixedColumns": {
			left: 4
	  	},
		  dom: 'Bfrtip',
		  buttons: [
			  {
				  extend: 'excel',
				  title: 'Export Excel',
				  filename: function() {
					  
					  return "EMPLOYEE RECAP DATA";
				  },
				  className: 'btn btn-excel',
				  action: newexportaction
			  },
			  {
				  extend:'pdf',
				  text:'Export PDF',
				  title: ' ',
				  filename: function() {
					  
					  return "EMPLOYEE RECAP DATA";
				  },
				  action: newexportaction,
				  
				  orientation:'landscape',
				  exportOptions: {
					  columns: [0,1,2,3,4,5,6]
				  },
				  customize : function(doc){
					  doc.content.splice(1, 0, {
						  text: [{
							  text: `EMPLOYEE RECAP DATA `+"\n",
							  bold: true,
							  fontSize: 16
						  }],
					  })
					  var colCount = new Array();
					  var length = $('#table_list_data tbody tr:first-child td').length;
					  $('#table_list_data').find('tbody tr:first-child td').each(function(){
						  if($(this).attr('colspan')){
							  for(var i=1;i<=$(this).attr('colspan');$i++){
								  colCount.push('*');
							  }
						  }else{ colCount.push(parseFloat(100 / length)+'%'); }
					  });
				  }
			  }
		  ],
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
		},
		"columns": [
			{"data": "id"},
			{"data": "nik"},
			{"data": "name_employee"},
			{"data": "name_jabatan"},
			{"data": "email"},
            {"data": "nohp"},
			// { 
			// 	"data": "duty", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.duty);
			// 	}
			// },
			// { 
			// 	"data": "atiga", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.atiga);
			// 	}
			// },
			// { 
			// 	"data": "acfta", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.acfta);
			// 	}
			// },
			// { 
			// 	"data": "ppn", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.ppn);
			// 	}
			// },
			// { 
			// 	"data": "ppnbm", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.ppnbm);
			// 	}
			// },
			// { 
			// 	"data": "pph", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.pph);
			// 	}
			// },
			// { 
			// 	"data": "bmtp", "seachable": false, "className": "text-right", 
			// 	"render": function (data, type, row, meta) {
			// 		return accounting.formatNumber(row.bmtp);
			// 	}
			// },
			{"data": "status", "width": 40, "className": "text-center"},
			{"data": null, "width": 20, "className": "text-center"}
      ],
		"columnDefs": [
			{
				"targets": 0,
				"orderable": true,
				"searchable": false,
				"visible": false,
			},
			{
				"targets": -2,
				"orderable": true,
				"searchable": false,
				"render": function ( data, type, row ) {
					if (row.status == 1){
						return '<span class="badge badge-success text-center">Aktif</span>';
					} else {
						return '<span class="badge badge-danger text-center">Tidak Aktif</span>';
					}
				},
			},
			{
				"targets": -1,
				"orderable": false,
				"searchable": false,
				"render": function ( data, type, row ) {
					let actions = '';
						actions += '<a href="#" class="btn btn-outline-primary btn-sm waves-effect waves-light" onclick="app_edit(' + row.id + ');"><i class="glyphicon glyphicon-pencil"></i> </a> ';
						// actions += '<a href="#" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="app_delete(0, ' + row.id + ', \'' + row.item_name + '\');"><i class="glyphicon glyphicon-trash"></i> </a>';
					return actions;
				},
			},
		],
	});
}

// START function default
function newexportaction(e, dt, button, config) {
    var self = this;
    var oldStart = dt.settings()[0]._iDisplayStart;
    dt.one('preXhr', function(e, s, data) {
        // Just this once, load all data from the server...
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function(e, settings) {
            // Call the original action function
            if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button,
                    config);
            } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt,
                        button, config) :
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt,
                        button, config);
            } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
            dt.one('preXhr', function(e, s, data) {
                // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                // Set the property to what it was before exporting.
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
            });
            // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            setTimeout(dt.ajax.reload, 0);
            // Prevent rendering of the full data to the DOM
            return false;
        });
    });
    // Requery the server with the new one-time export settings
    dt.ajax.reload();
};
function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

function app_edit(id = null) {
	window.location.href = '#' + Modules + '/' + Controller + '/edit/' + id;
	// FetchWithTimeout(base_url + Modules + '/' + Controller + '/getData2Edit/json/' + id, null, 'GET', 5000)
	// .then((data) => {
	// 	if (data.result) {
	// 		ResetForm();
	// 		action = 'edit';
	// 		let row = data.data;
	// 		idData = row.location_id;
	// 		$('#store_id').val(row.store_id);
	// 		$('#store_name').val(row.store_name);
	// 		$('#loc_id').val(row.loc_id);
	// 		$('#area_id').val(row.area_id);
	// 		$('#area_name').val(row.area_name);
	// 		$('#loc_name').val(row.loc_name);
	// 		$('#active').prop('checked', row.active == 1 ? true : false);
	// 	} else {
	// 		MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
	// 	}
	// })
	// .catch((err) => {
	// 	MsgBox.Notification(err.toString(), 'bottom right', 'warning');
	// });
}

function app_delete(multi = 0, id = null, menu_id = '') {
	if (multi) {
		if (dataArr.length <= 0) {
			MsgBox.Notification('Checklist terlebih dahulu data yang akan dihapus.', 'bottom right', 'info');
			return;
		}
		data2Send = JSON.stringify({
			id: dataArr
		});
		MsgBox.Confirm('Hapus menu checklist ?').then(result => {
			FetchWithTimeout(base_url + Modules + '/' + Controller + '/delete/', data2Send, 'POST', 5000)
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					app_refresh();
				} else {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString(), 'bottom right', 'warning');
			});
		}).catch(err => {
			console.log(err);
		});
	} else {
		data2Send = JSON.stringify({
			id: id
		});
		MsgBox.Confirm('Hapus Data ' + menu_id + ' ?').then(result => {
			FetchWithTimeout(base_url + Modules + '/' + Controller + '/delete/', data2Send, 'POST', 5000)
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					app_refresh();
				} else {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString(), 'bottom right', 'warning');
			});
		}).catch(err => {
			console.log(err);
		});
		
	}
}

function app_pdf() {
	window.open(base_url + Modules + '/' + Controller + '/pdf/', '_blank');
}

function app_xls() {
	window.open(base_url + Modules + '/' + Controller + '/xls/', '_blank');
}

function app_refresh() {
	data2Send = null;
	DataTable.ajax.reload(null,true);
}

function Add2Arr(CheckAll = 0, id = null) {
	if (CheckAll) {
		dataArr = []; 
		let chkboxes = $('input[name=\'chk\']');
		for (let i = 0; i < chkboxes.length; i++) {
			if ($('#CheckAll').is(':checked')) {
				$('#chk_' + i).prop('checked', true);
				dataArr.push(parseInt($('#chk_' + i).val()));
			} else {
				$('#chk_' + i).prop('checked', false);
			}
		}
	} else {
		if (dataArr.indexOf(id) > -1) {
			dataArr.splice(dataArr.indexOf(id), 1);
		} else {
			dataArr.push(id);
		}
		$('#CheckAll').prop('checked', false);
	}
}

function upload_data() {
	var formData = new FormData();
	formData.append('import', $('input[name=import]')[0].files[0]);
	MsgBox.Confirm('Yakin akan Upload data ini?').then(result => {
		let url =  base_url + Modules + '/' + Controller + '/import/';
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: formData,
			timeout: 5000,
			cache: false,
		    processData: false,
	    	contentType: false,
	    	beforeSend: function() {
	           $(".loader").show();
	        },
	        success: successUpload,
	    	error: function(xhr, textStatus, errorThrown) {
	            console.log('error');
	        }
		})
		 app_refresh();
		//  SendAjaxWithUpload(url, formData, 'POST', 'JSON', 5000, successUpload);

	}).catch(err => {
		console.log(err);
	});
}
function successUpload(data) {
	if (data.result == true) {
		// MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
		// MsgBox.Notification(data.msg, 'bottom right', 'success');
		Swal.fire({
			title: "<i>HSCODE IMPORT</i>", 
			html: data.msg,  
			confirmButtonText: "Ok", 
		  });
		$(".loader").hide();
		app_refresh();
	}else{
		Swal.fire({
			title: "<i>HSCODE IMPORT</i>", 
			html: data.msg,  
			confirmButtonText: "Ok", 
		  });
		// MsgBox.Notification(data.msg, 'bottom right', 'info');
		// MsgBox.Notification(data.msg.toString(), 'bottom right', 'info');
		app_refresh();
	}
}

function Filter() {
	DataTable.ajax.reload(null,true);
}

function SearchStore() {
	$('#PopUpModal').load(base_url + 'master/location/getStoreList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['store_id', 'store_name']);
		$('#list_controls').val(['#store_id', '#store_name']);
	});
}

function ClearStore() {
	$('#store_id').val('');
	$('#store_name').val('');
}

function SearchArea() {
	$('#PopUpModal').load(base_url + 'master/location/getAreaList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['area_id', 'area_name']);
		$('#list_controls').val(['#area_id', '#area_name']);
	});
}

function ClearArea() {
	$('#area_id').val('');
	$('#area_name').val('');
}
// END function default

function ResetForm() {
	action = 'create';
	$('#active').prop('checked', true);
	$('#form_input')[0].reset(); // Kosongkan input
	clearValidation('#form_input');
}