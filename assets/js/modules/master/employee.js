(function () {
	// JS construct
})();

function initPage(){
	LobiAdmin.highlightCode();
	initSelect2();
	initValidationDefaults();
	$("#form_input").validate({
		rules: {
			nik: {
				required: true,
				nowhitespace: true,
			},	
			name: {
				required: true,
			},
            password: {
                required: true,
            },
		},
	});
	
	DataTable = $('#table_list_data').DataTable({ 
		"pageLength": 10,
		"numbers_length": 9,
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
			"data": function ( d ) {
				d.status = $('#filter_status').val();
			},
		},
		"columns": [
			{"data": "id"},
			{"data": "nik"},
			{"data": "name_employee"},
            {"data": "name_jabatan"},
			{"data": "name_dept"},
            {"data": "email"},
            {"data": "nohp"},
            {"data": "status", "width": 40, "className": "text-center"},
			{"data": null, "width": 70, "className": "text-center"}
      ],
		"columnDefs": [
			{
				"targets": 0,
				"visible": false,
			},
            {
				"targets": -2,
				"orderable": true,
				"searchable": false,
				"render": function ( data, type, row ) {
					if (row.status == 1){
						return '<span class="badge badge-success text-center">Aktif</span>';
					} else {
						return '<span class="badge badge-danger text-center">Tidak Aktif</span>';
					}
				},
			},
			{
				"targets": -1,
				"orderable": false,
				"searchable": false,
				"render": function ( data, type, row ) {
					let actions = '';
					if (Priv.edit_flag == 1) {
						actions = '<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="app_edit(' + row.id + ');"><i class="fas fa-pencil-alt"></i></button> ';
					}
					if (Priv.delete_flag == 1) {
						actions = actions + '<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="app_delete(0, ' + row.id + ', \'' + row.kd_store + '\');"><i class="fas fa-trash"></i></button>';
					}
					return actions;
				},
			},
		],
	});
}

// START function default

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}


function Simpan() {
	if (!$('#form_input').valid()) {
		MsgBox.Notification('Periksa inputan anda.', 'bottom right', 'warning', 'mini');
		return;
	}
	
	data2Send = JSON.stringify({
		id: idData,
		nik: $('#nik').val(),
		name_employee: $('#name_employee').val(),
        code_jabatan: $('#code_jabatan').val(),
        email: $('#email').val(),
        nohp: $('#nohp').val(),
		user_pass: $('#user_pass').val(),
		role_id: $('#role_id').val(),
        status: $('#module_active').is(':checked') ? 1 : 0
	});
	
	MsgBox.Confirm('Save Data?', 'Simpan data').then(result => {
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		FetchWithTimeout(url, data2Send, 'POST', 5000)
		.then((data) => {
			if (data.result) {
				MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
				ResetForm();
				app_refresh();
				data2Send = null;
				idData = null;
			} else {
				MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString(), 'bottom right', 'warning');
		});
	}).catch(err => {
		console.log(err);
	});
}

function app_edit(id = null) {
	FetchWithTimeout(base_url + Modules + '/' + Controller + '/getData2Edit/json/' + id, null, 'GET', 5000)
	.then((data) => {
		if (data.result) {
			ResetForm();
			action = 'edit';
			let row = data.data;
			idData = row.id
			$('#nik').val(row.nik); $('#nik').prop('readonly', true);
			$('#name_employee').val(row.name_employee);
            $('#code_jabatan').val(row.code_jabatan);
            $('#email').val(row.email);
            $('#nohp').val(row.nohp);
            $('#module_active').prop('checked', row.status == 1 ? true : false);
		} else {
			MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
		}
	})
	.catch((err) => {
		MsgBox.Notification(err.toString(), 'bottom right', 'warning');
	});
}

function app_delete(multi = 0, id = null, menu_id = '') {
	if (multi) {
		if (dataArr.length <= 0) {
			MsgBox.Notification('Checklist terlebih dahulu data yang akan dihapus.', 'bottom right', 'info');
			return;
		}
		data2Send = JSON.stringify({
			id: dataArr
		});
		MsgBox.Confirm('Hapus menu checklist ?').then(result => {
			FetchWithTimeout(base_url + Modules + '/' + Controller + '/delete/', data2Send, 'POST', 5000)
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					app_refresh();
				} else {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString(), 'bottom right', 'warning');
			});
		}).catch(err => {
			console.log(err);
		});
	} else {
		data2Send = JSON.stringify({
			id: id
		});
		MsgBox.Confirm('Hapus menu ' + menu_id + ' ?').then(result => {
			FetchWithTimeout(base_url + Modules + '/' + Controller + '/delete/', data2Send, 'POST', 5000)
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					app_refresh();
				} else {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString(), 'bottom right', 'warning');
			});
		}).catch(err => {
			console.log(err);
		});
		
	}
}

function app_pdf() {
	window.open(base_url + Modules + '/' + Controller + '/pdf/', '_blank');
}

function app_xls() {
	window.open(base_url + Modules + '/' + Controller + '/xls/', '_blank');
}

function app_refresh() {
	data2Send = null;
	DataTable.ajax.reload(null,true);
}

function Add2Arr(CheckAll = 0, id = null) {
	if (CheckAll) {
		dataArr = []; 
		let chkboxes = $('input[name=\'chk\']');
		for (let i = 0; i < chkboxes.length; i++) {
			if ($('#CheckAll').is(':checked')) {
				$('#chk_' + i).prop('checked', true);
				dataArr.push(parseInt($('#chk_' + i).val()));
			} else {
				$('#chk_' + i).prop('checked', false);
			}
		}
	} else {
		if (dataArr.indexOf(id) > -1) {
			dataArr.splice(dataArr.indexOf(id), 1);
		} else {
			dataArr.push(id);
		}
		$('#CheckAll').prop('checked', false);
	}
}

function Filter() {
	DataTable.ajax.reload(null,true);
}

// END function default

function ResetForm() {
	action = 'create';
	$('#nik').prop('readonly', false);
	$('#form_input')[0].reset(); // Kosongkan input
	clearValidation('#form_input');
}

function LOVRole() {
	$('#PopUpModal').load(base_url + Modules + '/' + Controller + '/getRole/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['role_id', 'role_name']);
		$('#list_controls').val(['#role_id', '#role_name']);
	});
}

function LOVRoleClear() {
	$('#role_id').val(''); $('#role_name').val('');
}

function initSelect2(){
		$('.js-example-basic-single').select2({
			minimumInputLength: 3,
			allowClear: true,
			placeholder: 'Entry your code account or name employee',
			ajax: {
			   dataType: 'json',
			   url: base_url + Modules + '/' + Controller + '/getCodeEmployee/',
			   delay: 800,
			   data: function(params) {
				 return {
				   search: params.term
				 }
			   },
			   processResults: function (data, page) {
				   console.log(data)
			   return {
				 results: data
			   };
			 },
		   }
	   }).on('select2:select', function (evt) {
		  var data = $(".select2 option:selected").text();
		  alert("Data yang dipilih adalah "+data);
	   });

}