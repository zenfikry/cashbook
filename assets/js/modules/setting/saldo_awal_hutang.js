(function () {
	// construct
})();

function initPage() {
	initDataTable();
	initValidation();
	initDatePicker();
	initOtherElements();
	// getData();
}

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"bPaginate": false,
		"ordering": false,
		"info": false,
		"scrollX": true,
		"data": data4DataTable,
		"columns": [
			{
				"data": "no_doc", width: 200,
				"render": function (data, type, row, meta) {
					return `<input type="hidden" name="detail_id_${meta.row}" id="detail_id_${meta.row}" value="${row.detail_id}" readonly="readonly" />
							  <input type="text" class="form-control input-no-doc" id="no_doc_${meta.row}" name="no_doc_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="No. Transaksi" value="${row.no_doc}" autocomplete="off" />`;
				}
			},
			{
				"data": "tgl_trans", "className": "text-center", width: 140,
				"render": function (data, type, row, meta) {
					return `<div class="input-group date">
									<input type="text" class="form-control font-size-sm input-tgl-trans" placeholder="Tanggal" id="tgl_trans_${meta.row}" name="tgl_trans_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${moment(row.tgl_trans).format('DD/MM/YYYY')}" readonly />
									<div class="input-group-append">
										<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
									</div>
								</div>`;
				},
			},
			{
				"data": "coa_code", width: 200,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
									<input type="hidden" name="coa_code_${meta.row}" id="coa_code_${meta.row}" value="${row.coa_code}" readonly="readonly" />
									<input type="text" class="form-control ${row.locked == 1 ? 'disable' : ''} input-pencarian-coa" placeholder="Masukka kode COA" name="pencarian_coa_${meta.row}" id="pencarian_coa_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.coa_code}" autocomplete="off" ${row.locked == 1 ? 'readonly' : ''} />
							${row.locked == 1 ? ''
							:
							`<div class="input-group-append">
											<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVCOA('${meta.row}', '#pencarian_coa_${meta.row}');"><i class="fas fa-search"></i></button>
										</div>`
						}
								</div>`;
				}
			},
			{
				"data": "jumlah", "className": "text-right", width: 200,
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-right input-jumlah" id="jumlah_${meta.row}" name="jumlah_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Jumlah" value="${row.jumlah}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{ "data": "locked", "visible": false },
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';
					if (Priv.delete_flag == 1 && row.jumlah > 0 && row.locked == 1) {
						actions += `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
					}
					return actions;
				},
			},
		],
		"footerCallback": function (row, data, start, end, display) {
			let api = this.api();

			let totalJumlah = api.column(3).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(3).footer()).html(accounting.formatNumber(totalJumlah));
			$('#total_jumlah').val(totalJumlah);
		},

	}).on('draw.dt', function (e, settings, json, xhr) {

		// ============= START input-no-doc ===============
		
		$('.input-no-doc').on('keypress', function (e) {
			if (e.charCode == 13 || e.charCode == 9) {
				e.preventDefault();
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				if (!$(this).val() ||$(this).val().trim() == "") return;

				_dataTable.rows().draw();
				
				$('#tgl_trans_' + indexRow).focus();
			}
		});

		$('.input-no-doc').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			if (!$(this).val() || $(this).val().trim() == '') {
				return;
			}
			// value
			_dataTable.cell(indexRow, 0).data($(this).val().trim());
			_dataTable.rows().invalidate().draw();
		});

		// ============= END input-no-doc ===============

		// ============= START input-tgl-trans ===============

		$('.input-tgl-trans').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
		});

		$('.input-tgl-trans').on('changeDate', function (e) {
			let getIndex = e.target.id.split("_");
			let indexRow = getIndex[getIndex.length - 1];
			_dataTable.cell(indexRow, 1).data(moment(e.date).format('YYYY-MM-DD'));
			_dataTable.rows().invalidate().draw();
			// $('#expired_date_' + indexRow).datepicker('destroy');
			$('#pencarian_coa_' + indexRow).focus();
		});

		// =============== END input-tgl-trans ===============

		// === START pencarian COA
		$('.input-pencarian-coa').on('keypress', function (e) {
			if (e.charCode == 13) {
				e.preventDefault();
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				if ($(this).val().trim() == "") return;
				if (lockedRow == 1) return;

				_dataTable.rows().draw();

				loadingProcess();
				ajaxNew(base_url + 'setting/coa/get_coa?keyword=' + $(this).val().trim(), null, 'GET')
					.then((data) => {
						if (data.result) {
							let row = data.data;
							let data2save = {
								detail_id: $('#detail_id_' + indexRow).val(),
								no_doc: $('#no_doc_' + indexRow).val(),
								tgl_trans: moment($('#tgl_trans_' + indexRow).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
								coa_code: row.code,
								jumlah: accounting.unformat($('#jumlah_' + indexRow).val(), _decimalSeparator),
								locked: 0
							};
							// Simpan data
							DataTableAction = 'edit';
							SimpanRowDataTable(_dataTable, data2save, indexRow);
							DataTableAction = 'create';
							$('#jumlah_' + indexRow).focus();
							loadingProcess(false);
						} else {
							loadingProcess(false);
							LOVCOA(indexRow, '#pencarian_coa_' + indexRow, $(this).val().trim())
						}
					})
					.catch((err) => {
						MsgBox.Notification(err.toString());
					});
			}
		});
		// === END pencarian COA

		// ============= START input-jumlah ===============

		$('.input-jumlah').autoNumeric('init', formatNumber);
		
		$('.input-jumlah').on('keypress', function (e) {
			e.preventDefault();
			if (e.charCode === 13) {
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				// Check jika lockedRow = 1
				if (lockedRow == 1) {
					indexRow++;
					if ($('#jumlah_' + indexRow).length) {
						if ($('#jumlah_' + indexRow).data('locked') == 1) {
							$('#jumlah_' + indexRow).focus();
						} else {
							$('#no_doc_' + indexRow).focus();
						}
					}
					indexRow--;
					return; 
				}
				// Check input
				let list_input = ['no_doc_' + indexRow, 'pencarian_coa_' + indexRow, 'jumlah_' + indexRow];
				if (!ValidasiInput(list_input)) {
					MsgBox.Notification('Periksa kembali inputan anda', 'Input tidak lengkap', 'warning');
					return;
				}
				// check jika value <= 0
				if (accounting.unformat($(this).val(), _decimalSeparator) <= 0) {
					MsgBox.Notification("Jumlah harus lebih dari 0");
					return;
				}

				// Cek kode item duplikat
				let row = null;
				row = _dataTable.rows().data().toArray().filter(data => {
					return $('#no_doc_' + indexRow).val() == data.no_doc;
				});
				if ((row.length > 1 && DataTableAction == 'create') || (row.length > 1 && DataTableAction == 'edit' && $('#no_doc_' + indexRow).val() != _dataTable.row(indexRow).data().no_doc)) {
					MsgBox.Notification('No. Transaksi sudah ada', 'Peringatan', 'warning', true);
					return;
				}
				// Cek jika row terakhir memiliki locked != 1
				let lastRowIndex = (_dataTable.data().count() - 1);
				if ($('#jumlah_' + lastRowIndex).data('locked') != 1 && _dataTable.data().count() > 1 && indexRow != lastRowIndex) {
					$('#no_doc_' + lastRowIndex).focus();
					return;
				}
				
				// Locked
				_dataTable.cell(indexRow, 3).data(1);
				// Buat row baru dan fokus ke pencarian
				if (DataTableAction == 'create') {
					addDTRow();
				} else {
					DataTableAction = 'create';
				}
				// Focus next row
				indexRow++;
				$('#no_doc_' + indexRow).focus();
			}
		});

		$('.input-jumlah').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika value <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) <= 0) {
				return;
			}
			// value
			_dataTable.cell(indexRow, 3).data(accounting.unformat($(this).val(), _decimalSeparator));
			_dataTable.rows().invalidate().draw();
		});

		// ============= END input-jumlah ===============

	});

}

function initDatePicker() {
	$('#tanggal').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			tanggal: {
				DateID: true,
				required: true,
			},
		},
	});
}

function initOtherElements() {

	// START IMPORT
	$('#app_import').on('click', function () {
		$('#info_import_container').hide();
		$('#modalFormImport').modal('show');
	});

	$('#modalFormImport').on('hidden.bs.modal', function () {
		$('#info_import_container').hide();
		$('#form_import')[0].reset(); // Kosongkan input
	});

	$('#upload_import').on('click', function () {
		MsgBox.Confirm('Proses import data?', 'Import').then(result => {
			uploadFileImport();
		}).catch(err => {
			if (err) console.log(err);
		});
	});
	// END IMPORT
}

function uploadFileImport() {
	loadingProcess();
	let url =  base_url + Modules + '/' + Controller + '/import/';
	let form = $('#form_import')[0];
	let formData = new FormData(form);
	formData.append("flag_update", $('#flag_update').is(':checked') ? 1 : 0);
	formData.append("tanggal", $('#tanggal').val());
	_data2Send = formData;
	ajaxNew(url, _data2Send, 'POST', null)
		.then((data) => {
			loadingProcess(false);
			if (data.result) {
				loadingProcess(false);
				$('#info_upload').val(data.message.toString());
				$('#info_import_container').show();
				getData();
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			loadingProcess(false);
			MsgBox.Notification(err.toString());
		});
}

function Simpan() {
	// Check detail
	if (_dataTable.data().count() <= 1) {
		MsgBox.Notification('Detail tidak ditemukan');
		return;
	}
	// Check input
	if (!$('#form_input').valid()) {
		MsgBox.Notification('Periksa kembali inputan anda', 'Input tidak lengkap', 'warning');
		return;
	}
	// save
	MsgBox.Confirm('Yakin akan simpan data ini?').then(result => {
		let url = base_url + Modules + '/' + Controller + '/save/';
		let detail = JSON.stringify(_dataTable.rows().data().toArray());
		_data2Send = $('#form_input').serialize() + '&detail=' + detail;
		loadingProcess();
		ajaxNew(url, _data2Send, 'POST')
			.then((data) => {
				loadingProcess(false);
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					getData();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				loadingProcess(false);
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function getData() {
	let url = base_url + Modules + '/' + Controller + '/getData2Edit/json?supp_id=' + selectedSuppId;
	loadingProcess();
	ajaxNew(url, null, 'GET')
		.then((data) => {
			loadingProcess(false);
			if (data.result) {
				$('#tanggal').datepicker('remove');
				$('#tanggal').val(moment(data.tanggal).format('DD/MM/YYYY'));
				$('#tanggal').addClass('disable');
				$('#tanggal').prop("readonly", true);
				data4DataTable = data.data;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.data).draw();
					setTimeout(() => {
						addDTRow();
					}, _dataTable.data().count() * 100);
				}, 500);
			} else {
				loadingProcess(false);
				if (data.message != 'Data tidak ditemukan.') {
					MsgBox.Notification(data.message.toString());
				} else {
					addDTRow();
					setTimeout(() => {
						$('#no_doc_' + (_dataTable.data().count() - 1)).focus();
					}, 200);
				}
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

// ======= START DATATABLE

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {
	if (DataTableAction == 'edit') {
		DataTableElement.row(RowIdx).data(data2save).draw();
		DataTableElement.rows().invalidate().draw();
	} else {
		DataTableElement.row.add(data2save).draw();
	}
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}

// ======= END DATATABLE

// ======= START DATATABLE Detail

function addDTRow() {
	let data2save = {
		detail_id: 0,
		no_doc: "",
		tgl_trans: moment().format('YYYY-MM-DD'),
		coa_code: "",
		jumlah: 0,
		locked: 0
	};
	// Simpan data
	DataTableAction = 'create';
	SimpanRowDataTable(_dataTable, data2save);
}

function deleteDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Yakin akan hapus ' + getData.item_id + ' ini dari detail?', 'Hapus detail').then(result => {
		_data2Send = new URLSearchParams({ id: getData.detail_id });
		loadingProcess();
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
			loadingProcess(false);
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            HapusRowDataTable(_dataTable, RowIdx);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
			loadingProcess(false);
         MsgBox.Notification(err.toString());
      });
	}).catch(err => {
		if (err) console.log(err);
	});
}

// ======= END DATATABLE Detail

function LOVSupplier() {
	$('#PopUpModal').load(base_url + 'master/supplier/get_supplier_lov/home', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['supp_id', 'supp_name']);
		$('#list_controls').val(['#supp_id', '#supp_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#supp_id').val() || $('#supp_id').val().trim() == "" || selectedSuppId == $('#supp_id').val()) return;
			selectedSuppId = $('#supp_id').val();
			getData();
		});
	});
}

function LOVCOA(RowIdx, elPencarian = null, search = '') {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/home?search=' + search, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['coa_code']);
		$('#list_controls').val([elPencarian]);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!_resultFromLOV) {
				$(elPencarian).val($(elCOANo).val());
				return;
			}

			let data2save = {
				detail_id: $('#detail_id_' + RowIdx).val(),
				no_doc: $('#no_doc_' + RowIdx).val(),
				tgl_trans: moment($('#tgl_trans_' + RowIdx).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
				coa_code: _resultFromLOV.code,				
				jumlah: $('#jumlah_' + RowIdx).val(),
				locked: 0
			};
			// Simpan data
			DataTableAction = 'edit';
			SimpanRowDataTable(_dataTable, data2save, RowIdx);
			DataTableAction = 'create';
			_dataTable.rows().invalidate().draw();
			$('#jumlah_' + RowIdx).focus();

			_resultFromLOV = null;
		});
	});
}