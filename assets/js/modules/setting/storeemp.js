(function () {
	// JS construct
})();

function Add2ArrMenu(menu_id = '', priv_flag = '') {
	action = 'edit';
	if (typeof dataArrMenu[menu_id] === 'undefined') {
		dataArrMenu[menu_id] = { [priv_flag]: $('#' + menu_id + '_' + priv_flag).is(':checked') ? 1 : 0 };
	} else {
		dataArrMenu[menu_id][priv_flag] = $('#' + menu_id + '_' + priv_flag).is(':checked') ? 1 : 0;
	}
	// console.log('dataArrMenu', dataArrMenu);
}

function initPage(){
	LobiAdmin.highlightCode();

	// DataTable
	DataTable = $('#table_list_data').DataTable({ 
		"pageLength": 10,
		"numbers_length": 5,
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
			"data": function ( d ) {
				d.nik = $('#nik').val();
			},
		},
		"columns": [
			{"data": "nik"},
			{"data": "name_employee"},
            {"data": "m_code"},
            {"data": "m_shortdesc"},
            {"data": "m_odesc"},
			{"data": null, "width": 20, "className": "text-center"}
      ],
		"columnDefs": [
			{
				"targets": -1,
				"orderable": false,
				"searchable": false,
				"render": function ( data, type, row ) {
					let actions = '';
					if (Priv.delete_flag == 1) {
						actions = actions + '<a href="#" class="btn btn-pretty btn-warning btn-xs no-margin" onclick="app_delete(0, ' + row.id + ', \'' + row.menu_id + '\');"><i class="glyphicon glyphicon-trash"></i> </a>';
					}
					return actions;
				},
			},
		],
	});
}

// START function default

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

function Simpan() {
	if (isObjectEmpty(dataArrMenu)) {
		MsgBox.Notification('Periksa inputan anda.', 'bottom right', 'warning', 'mini');
		return;
	}
	
	data2Send = JSON.stringify({
		role_id: $('#role_id').val(),
		menu_arr: dataArrMenu
	});
	
	MsgBox.Confirm('Yakin akan simpan data ini?', 'Simpan data').then(result => {
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		FetchWithTimeout(url, data2Send, 'POST', 5000)
		.then((data) => {
			if (data.result) {
				action = 'create';
				MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
				app_refresh();
				data2Send = null;
				dataArrMenu = {};
			} else {
				MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString(), 'bottom right', 'warning');
		});
	}).catch(err => {
		console.log(err);
	});
}

function app_delete(multi = 0, id = null, param = '') {
	if (multi) {
		if (dataArr.length <= 0) {
			MsgBox.Notification('Checklist terlebih dahulu data yang akan dihapus.', 'bottom right', 'info');
			return;
		}
		data2Send = JSON.stringify({
			id: dataArr
		});
		MsgBox.Confirm('Hapus menu checklist ?').then(result => {
			FetchWithTimeout(base_url + Modules + '/' + Controller + '/delete/', data2Send, 'POST', 5000)
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					app_refresh();
				} else {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString(), 'bottom right', 'warning');
			});
		}).catch(err => {
			console.log(err);
		});
	} else {
		data2Send = JSON.stringify({
			id: id
		});
		MsgBox.Confirm('Hapus menu ' + param + ' ?').then(result => {
			FetchWithTimeout(base_url + Modules + '/' + Controller + '/delete/', data2Send, 'POST', 5000)
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					app_refresh();
				} else {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString(), 'bottom right', 'warning');
			});
		}).catch(err => {
			console.log(err);
		});
		
	}
}

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_refresh() {
	data2Send = null;
	DataTable.ajax.reload(null,true);
}

function Add2Arr(CheckAll = 0, id = null) {
	if (CheckAll) {
		dataArr = []; 
		let chkboxes = $('input[name=\'chk\']');
		for (let i = 0; i < chkboxes.length; i++) {
			if ($('#CheckAll').is(':checked')) {
				$('#chk_' + i).prop('checked', true);
				dataArr.push(parseInt($('#chk_' + i).val()));
			} else {
				$('#chk_' + i).prop('checked', false);
			}
		}
	} else {
		if (dataArr.indexOf(id) > -1) {
			dataArr.splice(dataArr.indexOf(id), 1);
		} else {
			dataArr.push(id);
		}
		$('#CheckAll').prop('checked', false);
	}
}

// END function default

function LOVRole() {
	$('#PopUpModal').load(base_url + 'setting/approles/getRoleList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['role_id', 'role_name']);
		$('#list_controls').val(['#role_id', '#role_name']);
	});
}

function LOVRoleClear() {
	$('#role_id').val(''); $('#role_name').val('');
	app_refresh();
	proses = false;
}
function LOVEmployee() {
	$('#PopUpModal').load(base_url + 'master/employee/getEmployeeList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['nik', 'name_employee']);
		$('#list_controls').val(['#nik', '#name_employee']);
	});
}

function LOVEmployeeClear() {
	$('#code_dept').val(''); $('#name_dept').val('');
	app_refresh();
	proses = false;
}

function Proses() {
	proses = true;
	DataTable.ajax.reload(null,true);
}

function AddMenuToRole() {
	//print_r( "test");
	// let list_input = ['role_id', 'menu_list'];
	let list_input = ['nik', 'm_code'];
	if (!ValidasiInput(list_input) || !proses) {
		MsgBox.Notification('Periksa inputan anda.', 'bottom right', 'warning', 'mini');
		return;
	}
	
	data2Send = JSON.stringify({
		nik: $('#nik').val(),
		m_code: $('#m_code').val(),
	});
	
	MsgBox.Confirm('Yakin akan simpan data ini?', 'Tambah Store').then(result => {
		let url = base_url + Modules + '/' + Controller + '/AddMenuToRole/';
		FetchWithTimeout(url, data2Send, 'POST', 5000)
		.then((data) => {
			if (data.result) {
				MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
				$("#m_code").select2("val", "");
				app_refresh();
				data2Send = null;
			} else {
				MsgBox.Notification(data.msg.toString(), 'bottom right', 'warning');
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString(), 'bottom right', 'warning');
		});
	}).catch(err => {
		console.log(err);
	});
}