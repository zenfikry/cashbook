(function () {
	// JS construct
})();

function initPage(){
	initDataTable();
	initDataTableCOA();
	initDataTableSettingCOA();
	initValidation();
	initOtherElements();
	setTimeout(() => {
		$("#coa_grup_link").removeClass("active");
		$("#coa_grup").removeClass("active");

		// $("#coa_daftar_link").removeClass("active");
		// $("#coa_daftar").removeClass("active");

		$("#coa_setting_link").removeClass("active");
		$("#coa_setting").removeClass("active");
	}, 500);
}

function initDataTable() {
	DataTable = $('#table_list_data').DataTable({ 
		"pageLength": 10,
		"numbers_length": 4,
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST"
		},
		"columns": [
			{ "data": "code" },
			{ "data": "name" },
			{ 
				"data": null, "width": 40, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';

					if (Priv.edit_flag == 1) {
						actions += '<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="app_edit(\'' + row.code + '\');"><i class="glyphicon glyphicon-pencil"></i> </button> ';
					}

					return actions;
				},
		 	},
      ],
	});
}

function initDataTableCOA() {
	_dataTable = $('#table_list_data_coa').DataTable({ 
		"pageLength": 10,
		"numbers_length": 4,
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ordering": false,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/list_coa',
			"type": "POST",
			"data": function ( d ) {
				d.filter_coa_tipe = $('#filter_coa_tipe').val();
				d.filter_coa_cash_bank = $('#filter_coa_cash_bank').val();
			},
		},
		"columns": [
			{ 
				"data": "group_code",
				"render": function (data, type, row, meta) {
					return row.group_code + ' - ' + row.group_name;
				},
			},
			{ 
				"data": "parent_code",
				"render": function (data, type, row, meta) {
					if (row.parent_code == row.code) {
						return '';
					} else {
						return row.parent_code + ' - ' + row.parent_name;
					}
				},
			},
			{ "data": "code" },
			{ "data": "name" },
			{ "data": "type" },
			{ 
				"data": "is_cash_bank", "className": "text-center",
				"render": function (data, type, row, meta) {
					if (row.is_cash_bank == 1) {
						return '<span class="badge badge-primary text-center">Ya</span>';
					} else {
						return '<span class="badge badge-info text-center">Tidak</span>';
					}
				},
			},
			{ 
				"data": null, "width": 40, "className": "text-center", "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';

					if (Priv.edit_flag == 1) {
						actions += '<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="app_edit_coa(\'' + row.code + '\');"><i class="glyphicon glyphicon-pencil"></i> </button> ';
					}

					return actions;
				},
		 	},
      ],
	});
}

function initDataTableSettingCOA() {
	_dataTableSetting = $('#table_list_setting_coa').DataTable({ 
		"pageLength": 10,
		"numbers_length": 4,
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/list_coa_setting',
			"type": "POST"
		},
		"columns": [
			{ "data": "coa_setting" },
			{ 
				"data": "coa_code", 
				"render": function (data, type, row, meta) {
					return row.coa_code + ' - ' + row.coa_name;
				},
			},
			{ 
				"data": null, "width": 40, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';

					if (Priv.edit_flag == 1) {
						actions += '<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="app_edit_setting(\'' + row.coa_setting + '\');"><i class="glyphicon glyphicon-pencil"></i> </button> ';
					}

					return actions;
				},
		 	},
      ],
	});
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			code: {
				required: true,
				nowhitespace: true
			},
			name: {
				required: true,
			},
		},
	});

	$("#form_input_coa").validate({
		rules: {
			code_coa: {
				required: true,
				nowhitespace: true
			},
			name_coa: {
				required: true,
			},
			tipe_coa: {
				required: true,
			},
			kas_bank_coa: {
				required: true,
			},
		},
	});

	$("#form_input_coa_setting").validate({
		rules: {
			setting_code: {
				required: true,
				nowhitespace: true
			},
		},
	});

	
}

function initOtherElements() {
	$('#coa_grup_link').on('click', function (e) {
		$("#coa_daftar_link").removeClass("active");
		$("#coa_daftar").removeClass("active");
		$("#coa_setting_link").removeClass("active");
		$("#coa_setting").removeClass("active");

		$("#coa_grup_link").addClass("active");
		$("#coa_grup").addClass("active");
		if (!DataTable) initDataTable();
	});
	
	$('#coa_daftar_link').on('click', function (e) {
		$("#coa_grup_link").removeClass("active");
		$("#coa_grup").removeClass("active");
		$("#coa_setting_link").removeClass("active");
		$("#coa_setting").removeClass("active");

		$("#coa_daftar_link").addClass("active");
		$("#coa_daftar").addClass("active");
		if (!_dataTable) initDataTableCOA();
	});

	$('#coa_setting_link').on('click', function (e) {
		$("#coa_grup_link").removeClass("active");
		$("#coa_grup").removeClass("active");
		$("#coa_daftar_link").removeClass("active");
		$("#coa_daftar").removeClass("active");

		$("#coa_setting_link").addClass("active");
		$("#coa_setting").addClass("active");
		if (!_dataTableSetting) initDataTableCOA();
	});
}

// START function default

function Simpan() {
	if (!$('#form_input').valid()) {
		MsgBox.Notification('Periksa inputan anda.', 'bottom right', 'warning', 'mini');
		return;
	}
	
	MsgBox.Confirm('Yakin akan simpan data ini?', 'Simpan data').then(result => {
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		_data2Send = action === 'create' ? $('#form_input').serialize() : $('#form_input').serialize() + '&id=' + _id;
		ajaxNew(url, _data2Send, 'POST')
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					ResetForm();
					app_refresh();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function app_edit(id = null) {
	FetchWithTimeout(base_url + Modules + '/' + Controller + '/getData2Edit/json/' + id, null, 'GET', 5000)
	.then((data) => {
		if (data.result) {
			ResetForm();
			let row = data.data;
			action = 'edit';
			_id = row.code
			$('#code').prop('readonly', true);
			$('#code').val(row.code);
			$('#name').val(row.name);
		} else {
			MsgBox.Notification(data.message.toString(), 'bottom right', 'warning');
		}
	})
	.catch((err) => {
		MsgBox.Notification(err.toString(), 'bottom right', 'warning');
	});
}

function app_pdf() {
	window.open(base_url + Modules + '/' + Controller + '/pdf/', '_blank');
}

function app_xls() {
	window.open(base_url + Modules + '/' + Controller + '/xls/', '_blank');
}

function app_refresh(resetPaging = true) {
	data2Send = null;
	DataTable.ajax.reload(null, resetPaging);
}

function Filter(resetPaging = true) {
	DataTable.ajax.reload(null, resetPaging);
}

function ResetForm() {
	action = 'create';
	_id = null;
	$('#code').prop('readonly', false);
	$('#form_input')[0].reset(); // Kosongkan input
	clearValidation('#form_input');
}

// END function default

// START function default - Daftar COA

function SimpanCOA() {
	if (!$('#form_input_coa').valid()) {
		MsgBox.Notification('Periksa inputan anda.', 'bottom right', 'warning', 'mini');
		return;
	}
	
	MsgBox.Confirm('Yakin akan simpan data ini?', 'Simpan data').then(result => {
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save_coa/' : base_url + Modules + '/' + Controller + '/update_coa/';
		_data2Send = action === 'create' ? $('#form_input_coa').serialize() : $('#form_input_coa').serialize() + '&id=' + _id;
		ajaxNew(url, _data2Send, 'POST')
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					ResetFormCOA();
					app_refresh_coa();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function app_edit_coa(id = null) {
	FetchWithTimeout(base_url + Modules + '/' + Controller + '/getData2Edit_coa/json/' + id, null, 'GET', 5000)
	.then((data) => {
		if (data.result) {
			ResetFormCOA();
			let row = data.data;
			action = 'edit';
			_id = row.code
			$('#code_coa').prop('readonly', true);
			$('#code_coa').val(row.code);
			$('#name_coa').val(row.name);
			$('#group_code').val(row.group_code);
			$('#group_name').val(row.group_code + ' - ' + row.group_name);
			$('#parent_code').val(row.parent_code);
			$('#parent_name').val(row.parent_code + ' - ' + row.parent_name);
			$('#tipe_coa option[value="' + row.type + '"]').prop('selected', true).change();
			$('#kas_bank_coa option[value="' + row.is_cash_bank + '"]').prop('selected', true).change();
		} else {
			MsgBox.Notification(data.message.toString(), 'bottom right', 'warning');
		}
	})
	.catch((err) => {
		MsgBox.Notification(err.toString(), 'bottom right', 'warning');
	});
}

function app_pdf_coa() {
	$('#form_filter_coa').prop('action', base_url + Modules + '/' + Controller + '/pdf_coa/');
  	$('#form_filter_coa').prop('target', '_blank');
  	$('#form_filter_coa').prop('method', 'POST');
  	document.getElementById('form_filter_coa').submit();
}

function app_xls_coa() {
	$('#form_filter_coa').prop('action', base_url + Modules + '/' + Controller + '/xls_coa/');
  	$('#form_filter_coa').prop('target', '_blank');
  	$('#form_filter_coa').prop('method', 'POST');
  	document.getElementById('form_filter_coa').submit();
}

function app_refresh_coa(resetPaging = true) {
	data2Send = null;
	_dataTable.ajax.reload(null, resetPaging);
}

function FilterCOA(resetPaging = true) {
	_dataTable.ajax.reload(null, resetPaging);
}

function ResetFormCOA() {
	action = 'create';
	_id = null;
	$('#code_coa').prop('readonly', false);
	$('#form_input_coa')[0].reset(); // Kosongkan input
	clearValidation('#form_input');
}

// END function default - Daftar COA

// START function default - Setting COA

function SimpanSetting() {
	let list_input = ['setting_coa_code', 'setting_coa_name'];
	if (!$('#form_input_coa_setting').valid() || !ValidasiInput(list_input)) {
		MsgBox.Notification('Periksa inputan anda.', 'bottom right', 'warning', 'mini');
		return;
	}
	
	MsgBox.Confirm('Yakin akan simpan data ini?', 'Simpan data').then(result => {
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save_setting/' : base_url + Modules + '/' + Controller + '/update_setting/';
		_data2Send = action === 'create' ? $('#form_input_coa_setting').serialize() : $('#form_input_coa_setting').serialize() + '&id=' + _id;
		ajaxNew(url, _data2Send, 'POST')
			.then((data) => {
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					ResetFormSetting();
					app_refresh_setting();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function app_edit_setting(id = null) {
	FetchWithTimeout(base_url + Modules + '/' + Controller + '/getData2Edit_setting/json/' + id, null, 'GET', 5000)
	.then((data) => {
		if (data.result) {
			ResetFormSetting();
			let row = data.data;
			action = 'edit';
			_id = row.coa_setting
			$('#setting_code').prop('readonly', true);
			$('#setting_code').val(row.coa_setting);
			$('#setting_coa_code').val(row.coa_code);
			$('#setting_coa_name').val(row.coa_code + ' - ' + row.coa_name);
		} else {
			MsgBox.Notification(data.message.toString(), 'bottom right', 'warning');
		}
	})
	.catch((err) => {
		MsgBox.Notification(err.toString(), 'bottom right', 'warning');
	});
}

function app_pdf_setting() {
	window.open(base_url + Modules + '/' + Controller + '/pdf_setting/', '_blank');
}

function app_xls_setting() {
	window.open(base_url + Modules + '/' + Controller + '/xls_setting/', '_blank');
}

function app_refresh_setting(resetPaging = true) {
	data2Send = null;
	_dataTableSetting.ajax.reload(null, resetPaging);
}

function ResetFormSetting() {
	action = 'create';
	_id = null;
	$('#setting_code').prop('readonly', false);
	$('#form_input_coa_setting')[0].reset(); // Kosongkan input
	clearValidation('#form_input_coa_setting');
}

// END function default - Setting COA


function LOVGroupCOA() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_grup_coa_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#group_code', '#group_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#group_code').val()) return;
			$('#group_name').val($('#group_code').val() + ' - ' + $('#group_name').val());
		});
	});
}

function LOVParentCOA() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#parent_code', '#parent_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#parent_code').val()) return;
			$('#parent_name').val($('#parent_code').val() + ' - ' + $('#parent_name').val());
		});
	});
}

function LOVParentCOAClear() {
	$('#parent_code').val(null);
	$('#parent_name').val(null);
}

function LOVSettingCOA() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#setting_coa_code', '#setting_coa_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#setting_coa_code').val()) return;
			$('#setting_coa_name').val($('#setting_coa_code').val() + ' - ' + $('#setting_coa_name').val());
		});
	});
}

function LOVSettingCOAClear() {
	$('#setting_coa_code').val(null); $('#setting_coa_name').val(null);
}