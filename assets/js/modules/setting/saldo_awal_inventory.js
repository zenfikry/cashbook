(function () {
	// construct
})();

function initPage() {
	initDataTable();
	initValidation();
	initDatePicker();
	initOtherElements();
	getData();
}

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"bPaginate": false,
		"ordering": false,
		"info": false,
		"scrollX": true,
		"data": data4DataTable,
		"columns": [
			{
				"data": "item_id", "className": "text-center", width: 200,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
									<input type="hidden" name="detail_id_${meta.row}" id="detail_id_${meta.row}" value="${row.detail_id}" readonly="readonly" />
									<input type="hidden" name="item_id_${meta.row}" id="item_id_${meta.row}" value="${row.item_id}" readonly="readonly" />
									<input type="text" class="form-control ${row.locked == 1 ? 'disable' : ''} input-pencarian" placeholder="Masukka kode / barcode" name="pencarian_${meta.row}" id="pencarian_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.item_id}" autocomplete="off" ${row.locked == 1 ? 'readonly' : ''} />
							${row.locked == 1 ? ''
							:
							`<div class="input-group-append">
											<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVItem('${meta.row}', '#item_id_${meta.row}', '#pencarian_${meta.row}', '#jumlah_${meta.row}');"><i class="fas fa-search"></i></button>
										</div>`
						}
								</div>`;
				}
			},
			{ "data": "item_name" },
			{
				"data": "tanggal_expired", "className": "text-center", width: 140,
				"render": function (data, type, row, meta) {
					if (row.use_expire == 0) return "";

					return `<div class="input-group date">
									<input type="text" class="form-control font-size-sm input-tanggal-expired" placeholder="Tanggal Expired" id="tanggal_expired_${meta.row}" name="tanggal_expired_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${moment(row.tanggal_expired).format('DD/MM/YYYY')}" readonly />
									<div class="input-group-append">
										<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
									</div>
								</div>`;
				},
			},
			{
				"data": "jumlah", "className": "text-center", width: 100,
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-center input-jumlah" id="jumlah_${meta.row}" name="jumlah_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Jumlah" value="${row.jumlah}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{ "data": "uom_name", width: 100 },
			{
				"data": "harga", "className": "text-right", width: 100,
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-right input-harga" id="harga_${meta.row}" name="harga_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Harga" value="${row.harga}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{
				"data": "total", "className": "text-right", width: 200,
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.total);
				},
			},
			{ "data": "locked", "visible": false },
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';
					if (Priv.delete_flag == 1 && row.jumlah > 0 && row.locked == 1) {
						actions += `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
					}
					return actions;
				},
			},
		],
		"footerCallback": function (row, data, start, end, display) {
			let api = this.api();

			let totalJumlah = api.column(3).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(3).footer()).html(accounting.formatNumber(totalJumlah));
			$('#total_jumlah').val(totalJumlah);

			let grandTotal = api.column(6).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(6).footer()).html(accounting.formatNumber(grandTotal));
			$('#grand_total').val(grandTotal);

		},

	}).on('draw.dt', function (e, settings, json, xhr) {
		// === START pencarian item
		$('.input-pencarian').on('keypress', function (e) {
			if (e.charCode == 13) {
				e.preventDefault();
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				if ($(this).val().trim() == "") return;
				if (lockedRow == 1) return;

				loadingProcess();
				ajaxNew(base_url + 'master/item/get_item?keyword=' + $(this).val().trim(), null, 'GET')
					.then((data) => {
						if (data.result) {
							let row = data.data;
							let data2save = {
								detail_id: $('#detail_id_' + indexRow).val(),
								item_id: row.item_id,
								item_name: row.item_name,
								tanggal_expired: row.use_expire == 0 ? "1900-01-01" : moment().format('YYYY-MM-DD'),
   							use_expire: row.use_expire,
								jumlah: accounting.unformat($('#jumlah_' + indexRow).val(), _decimalSeparator),
								uom_id: row.uom_id,
								uom_name: row.uom_name,
								harga: accounting.unformat(row.harga_beli),
								total: accounting.unformat($('#jumlah_' + indexRow).val(), _decimalSeparator) * accounting.unformat(row.harga_beli),
								locked: 0
							};
							// Simpan data
							DataTableAction = 'edit';
							SimpanRowDataTable(_dataTable, data2save, indexRow);
							DataTableAction = 'create';
							if (row.use_expire == 0) {
								$('#jumlah_' + indexRow).focus();
							} else {
								$('#tanggal_expired_' + indexRow).focus();
							}
							loadingProcess(false);
						} else {
							loadingProcess(false);
							LOVItem(indexRow, '#item_id_' + indexRow, '#pencarian_' + indexRow, '#jumlah_' + indexRow, $(this).val().trim());
						}
					})
					.catch((err) => {
						MsgBox.Notification(err.toString());
					});
			}
		});
		// === END pencarian item

		// ============= START input-jumlah ===============

		$('.input-jumlah').autoNumeric('init', formatNumber);
		
		$('.input-jumlah').on('keypress', function (e) {
			e.preventDefault();
			if (e.charCode === 13) {
				let indexRow = $(this).data('index');
				// check jika value <= 0
				if (accounting.unformat($(this).val(), _decimalSeparator) <= 0) {
					MsgBox.Notification("Jumlah harus lebih dari 0");
					return;
				}
				// focus next input
				_dataTable.rows().draw();
				$('#harga_' + indexRow).focus();
			}
		});

		$('.input-jumlah').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika value <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) <= 0) {
				return;
			}
			// Save
			_dataTable.cell(indexRow, 3).data(accounting.unformat($(this).val(), _decimalSeparator));
			// value total
			let total = accounting.unformat($(this).val(), _decimalSeparator) * accounting.unformat($('#harga_' + indexRow).val(), _decimalSeparator);
			_dataTable.cell(indexRow, 6).data(total);
			_dataTable.rows().invalidate().draw();
		});

		// ============= END input-jumlah ===============

		// ============= START input-harga ===============

		$('.input-harga').autoNumeric('init', formatNumber);
		
		$('.input-harga').on('keypress', function (e) {
			e.preventDefault();
			if (e.charCode === 13) {
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				// Check jika lockedRow = 1
				if (lockedRow == 1) {
					indexRow++;
					if ($('#jumlah_' + indexRow).length) {
						if ($('#jumlah_' + indexRow).data('locked') == 1) {
							$('#jumlah_' + indexRow).focus();
						} else {
							$('#pencarian_' + indexRow).focus();
						}
					}
					indexRow--;
					return; 
				}
				// check jika value <= 0
				if (accounting.unformat($(this).val(), _decimalSeparator) <= 0) {
					MsgBox.Notification("Harga harus lebih dari 0");
					return;
				}
				// Cek kode item duplikat
				let row = null;
				row = _dataTable.rows().data().toArray().filter(data => {
					if (data.use_expire == 1) {
						return $('#item_id_' + indexRow).val() == data.item_id && moment($('#tanggal_expired_' + indexRow).val(), 'DD/MM/YYYY').format('YYYY-MM-DD') == data.tanggal_expired;
					} else {
						return $('#item_id_' + indexRow).val() == data.item_id;
					}
				});
				if ((row.length > 1 && DataTableAction == 'create') || 
					 (row.length > 1 && DataTableAction == 'edit' && $('#item_id_' + indexRow).val() != _dataTable.row(DataTableRowIdx).data().item_id && moment($('#tanggal_expired_' + indexRow).val(), 'DD/MM/YYYY').format('YYYY-MM-DD') != data.tanggal_expired)) {
					MsgBox.Notification('Item sudah ada', 'Peringatan', 'warning', true);
					return;
				}
				// Check input
				let list_input = ['item_id_' + indexRow, 'pencarian_' + indexRow, 'tanggal_expired_' + indexRow, 'jumlah_' + indexRow, 'harga_' + indexRow];
				if (row.use_expire == 1) {
					list_input = ['item_id_' + indexRow, 'pencarian_' + indexRow, 'jumlah_' + indexRow, 'harga_' + indexRow];
				}
				if (!ValidasiInput(list_input)) {
					MsgBox.Notification('Periksa kembali inputan anda', 'Input tidak lengkap', 'warning');
					return;
				}
				// Locked
				_dataTable.cell(indexRow, 7).data(1);
				// Cek jika row terakhir memiliki locked != 1
				let lastRowIndex = (_dataTable.data().count() - 1);
				if ($('#harga_' + lastRowIndex).data('locked') != 1 && _dataTable.data().count() > 1 && indexRow != lastRowIndex) {
					$('#pencarian_' + lastRowIndex).focus();
					return;
				}
				// Buat row baru dan fokus ke pencarian
				if (DataTableAction == 'create') {
					addDTRow();
				} else {
					DataTableAction = 'create';
				}
				// Focus next row
				indexRow++;
				$('#pencarian_' + indexRow).focus();
			}
		});

		$('.input-harga').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika value <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) <= 0) {
				return;
			}
			// value harga
			_dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator));
			// value total
			let total = accounting.unformat($('#jumlah_' + indexRow).val(), _decimalSeparator) * accounting.unformat($(this).val(), _decimalSeparator);
			_dataTable.cell(indexRow, 6).data(total);
			_dataTable.rows().invalidate().draw();
		});

		// ============= END input-harga ===============

		// === START tanggal expired
		$('.input-tanggal-expired').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
		});

		$('.input-tanggal-expired').on('changeDate', function (e) {
			let getIndex = e.target.id.split("_");
			let indexRow = getIndex[getIndex.length - 1];
			_dataTable.cell(indexRow, 2).data(moment(e.date).format('YYYY-MM-DD'));
			_dataTable.rows().invalidate().draw();
			// $('#tanggal_expired_' + indexRow).datepicker('destroy');
			$('#jumlah_' + indexRow).focus();
		});
		// === END tanggal expired

	});

	setTimeout(() => {
		_dataTable.rows().invalidate().draw();
		setTimeout(() => {
			$('#pencarian_' + (_dataTable.data().count() - 1)).focus();
		}, 200);
	}, 500);
}

function initDatePicker() {
	$('#tanggal').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			tanggal: {
				DateID: true,
				required: true,
			},
		},
	});
}

function initOtherElements() {

	// START IMPORT
	$('#app_import').on('click', function () {
		$('#info_import_container').hide();
		$('#modalFormImport').modal('show');
	});

	$('#modalFormImport').on('hidden.bs.modal', function () {
		$('#info_import_container').hide();
		$('#form_import')[0].reset(); // Kosongkan input
	});

	$('#upload_import').on('click', function () {
		MsgBox.Confirm('Proses import data?', 'Import').then(result => {
			uploadFileImport();
		}).catch(err => {
			if (err) console.log(err);
		});
	});
	// END IMPORT
}

function uploadFileImport() {
	loadingProcess();
	let url =  base_url + Modules + '/' + Controller + '/import/';
	let form = $('#form_import')[0];
	let formData = new FormData(form);
	formData.append("flag_update", $('#flag_update').is(':checked') ? 1 : 0);
	formData.append("tanggal", $('#tanggal').val());
	_data2Send = formData;
	ajaxNew(url, _data2Send, 'POST', null)
		.then((data) => {
			loadingProcess(false);
			if (data.result) {
				loadingProcess(false);
				$('#info_upload').val(data.message.toString());
				$('#info_import_container').show();
				getData();
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			loadingProcess(false);
			MsgBox.Notification(err.toString());
		});
}

function Simpan() {
	// Check detail
	if (_dataTable.data().count() <= 1) {
		MsgBox.Notification('Detail tidak ditemukan');
		return;
	}
	// Check input
	if (!$('#form_input').valid()) {
		MsgBox.Notification('Periksa kembali inputan anda', 'Input tidak lengkap', 'warning');
		return;
	}
	// save
	MsgBox.Confirm('Yakin akan simpan data ini?').then(result => {
		let url = base_url + Modules + '/' + Controller + '/save/';
		let detail = JSON.stringify(_dataTable.rows().data().toArray());
		_data2Send = $('#form_input').serialize() + '&detail=' + detail;
		loadingProcess();
		ajaxNew(url, _data2Send, 'POST')
			.then((data) => {
				loadingProcess(false);
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					getData();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				loadingProcess(false);
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function getData() {
	let url = base_url + Modules + '/' + Controller + '/getData2Edit/json/';
	loadingProcess();
	ajaxNew(url, null, 'GET')
		.then((data) => {
			loadingProcess(false);
			if (data.result) {
				$('#tanggal').datepicker('remove');
				$('#tanggal').val(moment(data.tanggal).format('DD/MM/YYYY'));
				$('#tanggal').addClass('disable');
				$('#tanggal').prop("readonly", true);
				data4DataTable = data.data;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.data).draw();
					setTimeout(() => {
						addDTRow();
					}, _dataTable.data().count() * 100);
				}, 500);
			} else {
				loadingProcess(false);
				if (data.message != 'Data tidak ditemukan.') {
					MsgBox.Notification(data.message.toString());
				} else {
					addDTRow();
				}
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

// ======= START DATATABLE

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {
	if (DataTableAction == 'edit') {
		DataTableElement.row(RowIdx).data(data2save).draw();
		DataTableElement.rows().invalidate().draw();
	} else {
		DataTableElement.row.add(data2save).draw();
	}
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}

// ======= END DATATABLE

// ======= START DATATABLE Detail

function addDTRow() {
	let data2save = {
		detail_id: 0,
		item_id: "",
		item_name: "",
		tanggal_expired: moment().format('YYYY-MM-DD'),
		use_expire: 1,
		jumlah: 0,
		uom_id: "",
		uom_name: "",
		harga: 0,
		total: 0,
		locked: 0
	};
	// Simpan data
	DataTableAction = 'create';
	SimpanRowDataTable(_dataTable, data2save);
}

function deleteDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Yakin akan hapus ' + getData.item_id + ' ini dari detail?', 'Hapus detail').then(result => {
		_data2Send = new URLSearchParams({ id: getData.detail_id });
		loadingProcess();
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
			loadingProcess(false);
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            HapusRowDataTable(_dataTable, RowIdx);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
			loadingProcess(false);
         MsgBox.Notification(err.toString());
      });
	}).catch(err => {
		if (err) console.log(err);
	});
}

// ======= END DATATABLE Detail

function LOVItem(RowIdx, elItemId = null, elPencarian = null, elJumlah = null, search = '') {
	$('#PopUpModal').load(base_url + 'master/item/get_item_lov/home?search=' + search, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['item_id', 'item_id']);
		$('#list_controls').val([elItemId, elPencarian]);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!_resultFromLOV) {
				$(elPencarian).val($(elItemId).val());
				return;
			}

			let data2save = {
				detail_id: $('#detail_id_' + RowIdx).val(),
				item_id: $(elItemId).val(),
				item_name: _resultFromLOV.item_name,
				tanggal_expired: _resultFromLOV.use_expire == 0 ? "1900-01-01" : moment().format('YYYY-MM-DD'),
				use_expire: _resultFromLOV.use_expire,
				jumlah: 0,
				uom_id: _resultFromLOV.uom_id,
				uom_name: _resultFromLOV.uom_name,
				harga: accounting.unformat(_resultFromLOV.harga_beli, _decimalSeparator),
				total: 0,
				locked: 0
			};
			// Simpan data
			DataTableAction = 'edit';
			SimpanRowDataTable(_dataTable, data2save, RowIdx);
			DataTableAction = 'create';
			_dataTable.rows().invalidate().draw();
			$(elJumlah).focus();

			_resultFromLOV = null;
		});
	});
}