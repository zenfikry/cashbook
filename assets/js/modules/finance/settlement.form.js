(function () {
	// construct
})();

function initPage() {
	initDataTable();
	// initDataTableRiwayatHarga();
	initDatePicker();
	initValidation();
	// initOtherElements();
	if (action == 'edit') getData();
	clearForm();
	getEmployeeID()
}

uang_cash = 3500000;

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"bPaginate": false,
		"ordering": false,
		"info": false,
		"scrollX": true,
		"scrollY": "250px",
		"data": data4DataTable,
		"columns": [
			{},
			{
				"data": "code_coa", "className": "text-center", width: 200,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
									<input type="hidden" name="detail_id_${meta.row}" id="detail_id_${meta.row}" value="${row.detail_id}" readonly="readonly" />
									<input type="hidden" name="code_coa_${meta.row}" id="code_coa_${meta.row}" value="${row.code_coa}" readonly="readonly" />
									<input type="text" class="form-control ${row.locked == 1 ? 'disable' : ''} input-pencarian" placeholder="Entry Code COA" name="pencarian_${meta.row}" id="pencarian_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.code_coa}" autocomplete="off" ${row.locked == 1 ? 'readonly' : ''} />
							${row.locked == 1 ? ''
							:
							`<div class="input-group-append">
									<button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" id="LOVItemButton_${meta.row}" onclick="LOVCoa('${meta.row}', '#code_coa_${meta.row}', '#pencarian_${meta.row}');"><i class="fas fa-search"></i></button>
								</div>`
						}
								</div>`;
				}
			},
			{
				"data": "tanggal", "className": "text-center", width: 140,
				"render": function (data, type, row, meta) {
						return `<div class="input-group date">
									<input type="text" class="form-control font-size-sm ${row.locked == 1 ? 'disable' : 'input-tanggal'}" placeholder="Tanggal" id="tanggal_${meta.row}" name="tanggal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${moment(row.tanggal).format('DD/MM/YYYY')}" readonly />
									<div class="input-group-append">
										<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
									</div>
								</div>`;
				},
			},
			{ "data": "name_coa", "Classname": "text-left" },
			{
				"data": "nominal", "className": "text-right", width: 150,
				"render": function (data, type, row, meta) {
					return `<div class="input-group">
							 <input type="text" class="form-control text-right input-nominal" id="nominal_${meta.row}" name="nominal_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" placeholder="Nominal" value="${row.nominal}" onfocus="this.select()" autocomplete="off" />
							 </div>`;
				}
			},
			{
				"data": "remark", "className": "text-left", width: 150,
				"render": function (data, type, row, meta) {
					return `
					<div class="input-group">
					<input type="text" class="form-control input-remark" placeholder="Pick Store" name="remark_${meta.row}" id="remark_${meta.row}" data-index="${meta.row}" data-locked="${row.locked}" value="${row.remark}" onfocus="this.select()" autocomplete="off" readonly />
					<div class="input-group-append">
					   <button class="btn btn-outline-info btn-sm waves-effect waves-light" type="button" onclick="LOVStore(${meta.row});"><i class="fas fa-search"></i></button>
					</div></div>`;
				}
			},
			{ "data": "row_no", "visible": false },
			{ "data": "locked", "visible": false },
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';
						actions += `<button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light add_coa" data-index="${meta.row}" id="delete_${meta.row}" name="add_coa"><i class="glyphicon glyphicon-check"></i></button>`;
						actions += `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
					return actions;
				},
			},
			{ "data": "company_id", "visible": false},
		],
		columnDefs: [ { "defaultContent": "-", "targets": "_all" } 
		],
		"footerCallback": function (row, data, start, end, display) {
			let api = this.api();
			
			let totalQty = api.column(4).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			// $(api.column(4).footer()).html(accounting.formatNumber(totalQty));
			let total = accounting.unformat($('#total_qty').val(), _decimalSeparator);
			uang_cash = $("#total_advanced").val();	
			let sisa = uang_cash  - totalQty;
			$('#total_qty').val(totalQty);
			$('#total_expense').val(accounting.formatNumber(totalQty, 0));
			$('#sisa_pettycash').val(accounting.formatNumber(sisa, 0));

			// hitungTotal();
		},
	}).on('draw.dt', function (e, settings, json, xhr) {
		// === START pencarian item
		$('.input-pencarian').on('keypress', function (e) {
			if (e.charCode == 13) {
				e.preventDefault();
				let indexRow = $(this).data('index');
				let lockedRow = $(this).data('locked');
				if ($(this).val().trim() == "") return;
				if (lockedRow == 1) return;
				let parameters = 'code_coa=' + $(this).val().trim();

				loadingProcess();
				ajaxNew(base_url + 'finance/pos/get_coa?' + parameters, null, 'GET')
					.then((data) => {
						if (data.result) {
							let row = data.data;
							let data2save = {
								detail_id: $('#detail_id_' + indexRow).val(),
								code_coa: row.code_coa,
								tanggal: row.tanggal,
								name_coa: row.name_coa,
								nominal: 0,
								remark: row.remark,
								row_no: moment().format('YYYYMMDDHHmmssSSS'),
								locked: 0
							};
							// Cek kode item duplikat
							let cek = null;
							if (row.tanggal == 1) {
								cek = _dataTable.rows().data().toArray().filter(data => {
									return $('#pencarian_' + indexRow).val() == data.code_coa;
								});
	
								if ((cek.length > 1 && DataTableAction == 'create') ||
									(cek.length > 1 && DataTableAction == 'edit' && $('#pencarian_' + indexRow).val() != _dataTable.cek(indexRow).data().code_coa)) {
									MsgBox.Notification('Code Coa sudah ada, silahkan ganti Nominal jika ingin menambahkan', 'Peringatan', 'warning', true);
									return;
								}
							} else {
								// Cek kode item duplikat
								cek = _dataTable.rows().data().toArray().filter(data => {
									return $('#pencarian_' + indexRow).val() == data.code_coa;
								});

								if (cek.length > 1) {
									MsgBox.Notification('Code Coa sudah ada, silahkan ganti Nominal jika ingin menambahkan', 'Peringatan', 'warning', true);
									return;
								}
							}
												
							// Simpan data
							DataTableAction = 'edit';
							SimpanRowDataTable(_dataTable, data2save, indexRow);
							DataTableAction = 'create';
							if (row.tanggal == 1) {
								$('#tanggal_' + indexRow).focus();
							} else {
								$('#nominal_' + indexRow).focus();
							}							
							loadingProcess(false);
						} else {
							// MsgBox.Notification(data.message.toString());
							loadingProcess(false);
							LOVCoa(indexRow, '#code_coa_' + indexRow, '#pencarian_' + indexRow, $(this).val().trim());
						}
					})
					.catch((err) => {
						MsgBox.Notification(err.toString());
					});
			}
		});
		// === END pencarian item

		// === START expired date
		$('.input-tanggal').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
		});

		$('.input-tanggal').on('changeDate', function (e) {
			let getIndex = e.target.id.split("_");
			let indexRow = getIndex[getIndex.length - 1];
			_dataTable.cell(indexRow, 2).data(moment(e.date).format('YYYY-MM-DD'));
			_dataTable.rows().invalidate().draw();
			$('#tanggal_' + indexRow).datepicker('hide');
			$('#tanggal_' + indexRow).datepicker('destroy');
			$('#nominal_' + indexRow).focus();
		});
		// === END expired date

		// === START Nominal

		$('.input-nominal').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			if (e.charCode == 13 || e.charCode == 9) {
				_dataTable.rows().draw();
				$('#remark_' + indexRow).focus();
			}
		});

		$('.input-nominal').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			// Update Qty
			_dataTable.cell(indexRow, 4).data(accounting.unformat($(this).val(), _decimalSeparator));
			// Jumlah
			let jumlah = (accounting.unformat($(this).val(), _decimalSeparator)) ;
			// _dataTable.cell(indexRow, 3).data(jumlah);
			_dataTable.rows().invalidate().draw();
		});

		// === END qty

		// === START Remark

		// $('.input-remark').on('keypress', function (e) {
		// 	e.preventDefault();
		// 	let indexRow = $(this).data('index');
		// 	let lockedRow = $(this).data('locked');
			
		// 	if (e.charCode == 13 || e.charCode == 9) {
		// 		// Check jika lockedRow = 1
		// 		if (lockedRow == 1) {
		// 			_dataTable.rows().draw();
		// 			flagItemEndRow = true;

		// 			indexRow++;
		// 			if ($('#nominal_' + indexRow).length) {
		// 				if ($('#nominal_' + indexRow).data('locked') == 1) {
		// 					$('#nominal_' + indexRow).focus();
		// 				} else {
		// 					$('#pencarian_' + indexRow).focus();
		// 				}
		// 			}
		// 			indexRow--;
		// 			return; 
		// 		}
				
		// 		let row = null;

		// 		// Cek jika (harga jual - price point) <= hbeli
		// 		// let harga = accounting.unformat($('#harga_' + indexRow).val(), _decimalSeparator);
		// 		// if ($(this).val() && accounting.unformat($(this).val(), _decimalSeparator) > 0) {
		// 		// 	harga = accounting.unformat($(this).val(), _decimalSeparator);
		// 		// }
		// 		// if (harga <= parseInt(_dataTable.row(indexRow).data().hbeli)) {
		// 		// 	// MsgBox.Notification('Harga barang tidak bisa dibawah atau sama dengan modal (' + accounting.formatNumber(parseInt(_dataTable.row(indexRow).data().hbeli)) + ')', 'Peringatan', 'warning', true);
		// 		// 	MsgBox.Notification('Harga barang tidak bisa dibawah atau sama dengan modal', 'Peringatan', 'warning', true);
		// 		// 	return;
		// 		// }

		// 		// Buat row baru dan fokus ke pencarian
		// 		if (DataTableAction == 'create') {
		// 			// Tambah row baru
		// 			addDTRow();
		// 		}

		// 		// Locked
		// 		_dataTable.cell(indexRow, 6).data(1);
		// 		_dataTable.rows().invalidate().draw();
		// 		flagItemEndRow = true;
		// 		indexRow++;
		// 		$('#pencarian_' + indexRow).focus();
		// 	}	
		// });	

		// $('.input-remark').on('blur', function (e) {
		// 	e.preventDefault();
		// 	let indexRow = $(this).data('index');
		// 	let lockedRow = $(this).data('locked');
			
		// 	// Price point
		// 	// _dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator));
		// 	// Jumlah
		// 	// let jumlah = (accounting.unformat($('#nominal_' + indexRow).val(), _decimalSeparator), _decimalSeparator);
		// 	// _dataTable.cell(indexRow, 7).data(jumlah);
		// 	_dataTable.rows().invalidate().draw();
		// 	if (flagItemEndRow) {
		// 		_dataTable.rows().invalidate().draw();
		// 	}
		// 	flagItemEndRow = false;
		// });

		$(".input-remark").on('change',function(){
			let indexRow = $(this).data('index');
			let lockedRow = $(this).data('locked');
			_dataTable.cell(indexRow, 4).data($(this).val());
			_dataTable.rows().invalidate().draw();
		})
		
		$('.input-nominal').autoNumeric('init', formatNumber);
		$('.input-remark').val();
		// hitungTotal();
	});

	_dataTable.on('order.dt search.dt', function () {
        let i = 1;
 
        _dataTable.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
            this.data(i++);
        });
    }).draw();

	setTimeout(() => {
		_dataTable.rows().invalidate().draw();
		if (action == 'create') addDTRow();
		setTimeout(() => {
			$('#pencarian_' + (_dataTable.data().count() - 1)).focus();
		}, 200);
	}, 500);
}

function getEmployeeID(){
	$.ajax({
		"url":base_url + `finance/Expenseclaim/getEmployeeID`,
		"dataType":"JSON",
		"method": "GET",
		"success" : function(data){
			$.each(data,function(key,val){
				$("#code_employee").val(val.nik);
				$("#code_dept").val(val.code_dept);
				$("#code_jabatan").val(val.code_jabatan);

			})
		}
	})
}
// add coa
$(document).on('click',".add_coa",function(){
	let indexRow = $(this).data('index');
	let lockedRow = $(this).data('locked');
	// Check jika lockedRow = 1
	if (lockedRow == 1) {
		_dataTable.rows().draw();
		flagItemEndRow = true;

		indexRow++;
		if ($('#nominal_' + indexRow).length) {
			if ($('#nominal_' + indexRow).data('locked') == 1) {
				$('#nominal_' + indexRow).focus();
			} 
			
			else {
				$('#pencarian_' + indexRow).focus();
			}
		}
		indexRow--;
		return; 
	}
	
	let row = null;
	// Buat row baru dan fokus ke pencarian
	if (DataTableAction == 'create') {
		// Tambah row baru
		addDTRow();
	}

	// Locked
	_dataTable.cell(indexRow, 6).data(1);
	_dataTable.rows().invalidate().draw(); //ubah disini
	flagItemEndRow = true;
	indexRow++;
	$('#pencarian_' + indexRow).focus();
})

function initDatePicker() {
	$('#doc_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());

	$('#periode_awal').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());

	$('#received_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());

	$('#periode_akhir').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());

	$('#tgl_jatuh_tempo').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			doc_date: {
				DateID: true,
				required: true,
			},
		},
	});
}

function initOtherElements() {
	$('#doc_date').on('changeDate', function (e) {
		$('#tanggal').datepicker("setDate", moment(e.date).add(14, 'days').format('DD/MM/YYYY'));
	});

	// $('#top').on('change', function (e) {
	// 	$('#tanggal').datepicker("setDate", moment($('#doc_date').val(), 'DD/MM/YYYY').add($(this).val(), 'days').format('DD/MM/YYYY'));
	// });

	// $('#ppn').on('change', function (e) {
	// 	if ($(this).val() == 'I' || $(this).val() == 'E') {
	// 		$("#ppn_persen").prop("readonly", false).removeClass("disable");
	// 		$("#ppn_nilai").prop("readonly", false).removeClass("disable");
	// 		$("#ppn_persen").val(10);
	// 		$("#ppn_nilai").val(0);
	// 	} else {
	// 		$("#ppn_persen").prop("readonly", true).addClass("disable");
	// 		$("#ppn_nilai").prop("readonly", true).addClass("disable");
	// 		$("#ppn_persen").val(0);
	// 		$("#ppn_nilai").val(0);
	// 	}
	// 	hitungTotal()
	// });

	// $("#nominal").on('change', function (e) {
	// 	hitungTotal();
	// });
	// $("#remark").on('keyore', function (e) {
	// 	hitungTotal();
	// });
	// $("#ppn_persen").prop("readonly", true).addClass("disable");
	// $("#ppn_nilai").prop("readonly", true).addClass("disable");

	// $('#ppn_persen').on('keyup', function (e) {
	// 	hitungTotal();
	// });

	// $('#ppn_nilai').on('keyup', function (e) {
	// 	hitungTotal(false);
	// });

	// $('#level_harga').on('change', function (e) {
	// 	changeLevelHarga();
	// });

	// $('#pembayaran').on('click', function () {
	// 	$('#modalFormPembayaran').modal('show');
	// });

	// $('#modalFormPembayaran').on('shown.bs.modal', function () {
	// 	$('#total_pembelanjaan').val($('#total').val());
	// 	hitungKembalian();
	// 	hitungTotal();
	// });

	// $('#terapkan_promo').on('click', function () {
	// 	applyPromo();
	// });

	// $('#pembayaran_cash, #pembayaran_debit_nilai, #pembayaran_credit_nilai').on('keyup', function (e) {
	// 	hitungKembalian();
	// });

	$('#total_expense').autoNumeric('init', formatDecimalNumber);
	$('#sisa_pettycash').autoNumeric('init', formatDecimalNumber);
	$('#pettycash_store').autoNumeric('init', formatDecimalNumber);
}

// function hitungKembalian() {
// 	let totalPembelanjaan = accounting.unformat($('#total_pembelanjaan').val(), _decimalSeparator);
// 	let pembayaranCash = accounting.unformat($('#pembayaran_cash').val(), _decimalSeparator);
// 	let pembayaranDebit = accounting.unformat($('#pembayaran_debit_nilai').val(), _decimalSeparator);
// 	let pembayaranCredit = accounting.unformat($('#pembayaran_credit_nilai').val(), _decimalSeparator);
// 	let totalPembayaran = pembayaranCash + pembayaranDebit + pembayaranCredit;
// 	let sisaKembalian = totalPembayaran - totalPembelanjaan;
// 	$('#total_pembayaran').val(accounting.formatNumber(totalPembayaran, 2));
// 	$('#sisa_kembalian').val(accounting.formatNumber(sisaKembalian, 2));
// }

function hitungTotal() {
	// hitngPPN(hitungDariPersen);
	let total = accounting.unformat($('#total_qty').val(), _decimalSeparator);
	let sisa = uang_cash - total;
	// $('#total_expense').val(accounting.formatNumber(total, 0));
	$('#sisa_pettycash').val(accounting.formatNumber(sisa, 0));
	// hitungKembalian();
}

function Kembali() {
	window.location.href = '#' + Modules + '/' + Controller;
}

async function Simpan() {
	// Check qty
	// if (accounting.unformat($('#total_qty').val(), _decimalSeparator) <= 0) {
	// 	MsgBox.Notification('Periksa kembali inputan anda', 'Peringatan', 'warning');
	// 	return;
	// }
	// Check detail
	if (_dataTable.data().count() <= 0) {
		MsgBox.Notification('Detail not found');
		return;
	}
	
	// Hitung total
	// await hitungTotal();
	

	// save
	MsgBox.Confirm('Save Data?').then(result => {
		if (!result) return;
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		let data_detail = _dataTable.rows().data().toArray();
		let datas_detail = [];
		$.each(data_detail,function(key,val){
			datas_detail.push({
				detail_id:val.detail_id,
				code_coa:val.code_coa,
				tanggal:val.tanggal,
				name_coa:val.name_coa.replace(/\\n/g, ''),
				nominal:val.nominal,
				remark:val.remark,
				locked:val.locked
			})
		})
		let detail = JSON.stringify(datas_detail);
		let summary = $('#form_summary').serializeArray();
		let _data2Send = action === 'create' ? $('#form_input').serialize() + + '&' + summary + '&detail=' + detail : $('#form_input').serialize() + '&' + summary + '&detail=' + detail + '&id=' + _id;
		
		var file_data = $('#file_expense').prop('files')[0];   
		var file_data_pejadin = $('#file_pejading').prop('files')[0];   
		var file_data_refund = $('#file_refund').prop('files')[0];   
		var file_data_cashbon = $('#file_cashbon').prop('files')[0];   
		var form_data = new FormData();         
		form_data.append('file', file_data);   
		form_data.append('file_pejadin', file_data_pejadin);   
		form_data.append('file_refund', file_data_refund);   
		form_data.append('file_cashbon', file_data_cashbon);   
		

		const fileupload = $('#file_expense').prop('files')[0]

		// var file_data = $('#file_expense').prop('files')[0];   
		// var form_data = new FormData();         
		// form_data.append('file', file_data);   
		

		// const fileupload = $('#file_expense').prop('files')[0]


		let formData = new FormData();
		formData.append('file_expense', fileupload);
		formData.append('file_pejadin', file_data_pejadin);
		formData.append('file_refund', file_data_refund);
		formData.append('file_cashbon', file_data_cashbon);
		formData.append('periode_awal',$("#periode_awal").val());
		formData.append('periode_akhir',$("#periode_akhir").val());
		formData.append('code_employee',$("#code_employee").val());
		formData.append('name_employee',$("#name_employee").val());
		formData.append('email',$("#email").val());
		formData.append('nik',$("#nik").val());
		formData.append('code_dept',$("#code_dept").val());
		formData.append('code_jabatan',$("#code_jabatan").val());
		formData.append('company_code',$("#company_code").val());
		formData.append('bank',$("#bank").find(":selected").val());
		formData.append('account_number',$("#account_number").val());
		formData.append('account_name',$("#account_name").val());
		formData.append('id_advanced',$("#id_advanced").val());
		formData.append('received_date',$("#received_date").val());
		formData.append('total_advanced',$("#total_advanced").val());
		formData.append('description',$("#description").val());
		for(let i=0; i<summary.length;i++){
			formData.append(`${summary[i].name}`,`${summary[i].value}`)
		}
		formData.append('detail',detail);
		
		
		$.ajax({
			type: 'POST',
			url: url,
			data: formData,
			processData: false,
    		contentType: false,
			success: function (data) {
				// data = JSON.parse(data);
				if (data.result) {
					$('#no_doc').val(data.data.no_doc);
					_id = data.data.no_doc;
					action = 'edit';
					MsgBox.Notification(data.message.toString(), 'bottom right', 'success');
					Kembali();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			},
			error: function (err) {
				console.log(err);
				MsgBox.Notification(err.toString());
			}
		})



		// ajaxNew(url, _data2Send, 'POST')
		// 	.then((data) => {
				// if (data.result) {
				// 	$('#no_doc').val(data.data.no_doc);
				// 	_id = data.data.no_doc;
				// 	action = 'edit';
				// 	MsgBox.Notification(data.message.toString(), 'bottom right', 'success');
				// 	Kembali();
				// } else {
				// 	MsgBox.Notification(data.message.toString());
				// }
		// 	})
		// 	.catch((err) => {
		// 		MsgBox.Notification(err.toString());
		// 	});
	}).catch(err => {
		console.log(err);
	});
}

function getData() {
	let url = base_url + Modules + '/' + Controller + '/getData2Edit/json/' + _id;
	ajaxNew(url, null, 'GET')
		.then((data) => {
			if (data.result) {
				let row = data.data;
				$('#no_doc').val(row.no_doc);
				$('#doc_date').datepicker("setDate", moment(row.tgl_trans).format('DD/MM/YYYY'));
				if (row.employee_id) {
					$('#employee_id').val(row.employee_id); $('#employee_name').val(row.employee_id + ' - ' + row.employee_name);
				}
				$('#tipe_pembayaran option[value="' + row.type_payment + '"]').prop('selected', true).change();
				if (row.cust_id) {
					$('#cust_id').val(row.cust_id); $('#cust_name').val(row.cust_id + ' - ' + row.cust_name);
					$('#cust_type').val(row.type_member_name); $('#cust_type_lvl').val(row.level); 
					$('#type_member_id').val(row.type_member_id);
					getMemberPriceLevel(row.type_member_id, false, row.cust_price_sub_level);
				}
				$('#ppn option[value="' + row.ppn + '"]').prop('selected', true).change();
				if (parseFloat(row.total_potongan) > 0) {
					flagTerapkanPromoDiskon = true;
				}
				data4DataTable = data.detail;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.detail).draw();
					setTimeout(() => {
						addDTRow();
					}, _dataTable.data().count() * 100);
				}, 500);

				// Pembayaran tunai
				if (row.type_payment == 'TUNAI') {
					// cash
					$('#pembayaran_cash').val(accounting.formatNumber(row.cash, 2));
					$('#sisa_kembalian').val(accounting.formatNumber(row.kembali, 2));
					// debit
					$('#pembayaran_debit_coa_code').val(row.coa_debit); $('#pembayaran_debit_coa_name').val(row.coa_debit + ' - ' + row.coa_debit_name);
					$('#pembayaran_debit_no').val(row.debit_no);
					$('#pembayaran_debit_nilai').val(accounting.formatNumber(row.debit, 2));
					// credit
					$('#pembayaran_credit_coa_code').val(row.coa_credit); $('#pembayaran_credit_coa_name').val(row.coa_credit + ' - ' + row.coa_credit_name);
					$('#pembayaran_credit_no').val(row.credit_no);
					$('#pembayaran_credit_nilai').val(accounting.formatNumber(row.credit, 2));
				}
				// Pembayaran transfer
				if (row.type_payment == 'TRANSFER') {
					$('#coa_code').val(row.coa_transfer); $('#coa_name').val(row.coa_transfer + ' - ' + row.coa_transfer_name);
				}
				// Pembayaran TEMPO
				if (row.type_payment == 'TEMPO') {
					$('#tgl_jatuh_tempo').datepicker("setDate", moment(row.tgl_jatuh_tempo).format('DD/MM/YYYY'));
					let selisihHari = moment(row.tgl_jatuh_tempo).diff(row.tgl_trans, 'days');
					$('#top').val(selisihHari);
				}
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

function clearForm() {
	action = 'create';

	$('#doc_date').datepicker("setDate", moment().format('DD/MM/YYYY'));
	$('#tanggal').datepicker("setDate", moment().format('DD/MM/YYYY'));
	$('#top').val(0);

	selectedKaryawanId = null;
   selectedCustomerId = null;
   selectedCOACode = null;
   selectedCOACodeDebit = null;
   selectedCOACodeCredit = null;
   flagTerapkanPromoDiskon = false;
   flagItemEndRow = false;
}

// ======= START DATATABLE

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {
	if (DataTableAction == 'edit') {
		DataTableElement.row(RowIdx).data(data2save);
		DataTableElement.rows().invalidate().draw();
	} else {
		DataTableElement.row.add(data2save).draw();
	}
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}

// ======= END DATATABLE

// ======= START DATATABLE Detail

function addDTRow() {
	let data2save = {
		detail_id: 0,
		code_coa: "",
		tanggal: moment().format('YYYY-MM-DD'),
		name_coa: "",
		nominal: 0,
		remark: "",
		row_no: moment().format('YYYYMMDDHHmmssSSS'),
		locked: 0
	};
	// Simpan data
	DataTableAction = 'create';
	SimpanRowDataTable(_dataTable, data2save);
	DataTableAction = 'edit';
}

function deleteDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Delete ' + getData.item_id + ' with expired date ' + moment(getData.expired_date).format('DD/MM/YYYY') + ' from detail?', 'Delete detail').then(result => {
		HapusRowDataTable(_dataTable, RowIdx);
	}).catch(err => {
		if (err) console.log(err);
	});
}

// ======= END DATATABLE Detail
function LOVCompany(){
	$('#PopUpModal').load(base_url + 'finance/settlement/getCompList/', () => { // Ambil URL untuk membuka modal LOV
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$(".modal-dialog").css({width: "600px"}); // Lebar modal LOV
		$('#list_cols').val(['company_code', 'company_name','categories']);
		$('#list_controls').val(['#company_code', '#company_name', '#categories']);
	});
}

function LOVEmployee() {
	$('#PopUpModal').load(base_url + 'finance/settlement/getEmployeeList/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-lg modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee','code_dept','name_dept','code_jabatan','name_jabatan']);
		$('#list_controls').val(['#code_employee', '#name_employee','#code_dept','#dapartement',"#code_jabatan",'#position']);
	});

	
}

function LOVCoa(RowIdx, elItemId = null, elPencarian = null, search = '') {
	
	let parameters = {
		code_coa: $(elPencarian).val().trim(),
		code_dept : $("#code_dept").val().trim()
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/settlement/get_coa_lov/home/' + jsonWhere + '?search=' + search, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code_coa', 'code_coa']);
		$('#list_controls').val([elItemId, elPencarian]);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!_resultFromLOV) return;

			// Cek kode item & tanggal expire duplikat
			let row = null;
			row = _dataTable.rows().data().toArray().filter(data => {
				return $('#code_coa_' + RowIdx).val() == data.code_coa;
			});

				let data2save = {
					detail_id: 0,
					code_coa: _resultFromLOV.code_coa,
					name_coa: _resultFromLOV.name_coa,
					nominal: 0,
					remark: '',
					row_no: moment().format('YYYYMMDDHHmmssSSS'),
					locked: 0
				};
				// Simpan data
				DataTableAction = 'edit';
				SimpanRowDataTable(_dataTable, data2save, RowIdx);
				DataTableAction = 'create';
				$('#tanggal_' + RowIdx).focus();

			_resultFromLOV = null;
		});
	});
}

function LOVEmail() {
	let employee_id = $("#code_employee").val();
	let parameters = {
		employee_id: employee_id,
	};
	let jsonWhere = encodeURIComponent(JSON.stringify(parameters));
	$('#PopUpModal').load(base_url + 'finance/settlement/getEmail/home/' + jsonWhere, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['nik', 'name_employee', 'email']);
		$('#list_controls').val(['#nik', '#name_employee', '#email']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#employee_id').val()) return;
			$('#email').val();
		});
	});
}

function LOVStore(row) {
	
	$('#PopUpModal').load(base_url + 'finance/expenseclaim/getStore/', () => { // Ambil URL untuk membuka modal LOV
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['m_code', 'm_shortdesc', 'm_pic', 'm_type']);
		$('#list_controls').val([`#remark_${row}`, `#remark_${row}`, '#m_pic', '#m_type']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$(`#remark_${row}`).val()) return;
				_dataTable.cell(row, 5).data($(`#remark_${row}`).val());
				_dataTable.rows().invalidate().draw();
			// $(`#remark_${row}`).val();
			// get_lastdate_pt($('#m_code').val());
		});
	});
}

function LOVCustomer() {
	$('#PopUpModal').load(base_url + 'master/member/get_member_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['cust_id', 'cust_name', 'type_member_id', 'type_member_name', 'level']);
		$('#list_controls').val(['#cust_id', '#cust_name', '#type_member_id', '#cust_type', '#cust_type_lvl']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedCustomerId == $('#cust_id').val() || !$('#cust_id').val()) return;
			selectedCustomerId = $('#cust_id').val();
			$('#cust_name').val(selectedCustomerId + ' - ' + $('#cust_name').val());
			$('#type_member_name').text($('#cust_type').val());

			$('#tgl_jatuh_tempo').datepicker("setDate", moment($('#doc_date').val(), "DD/MM/YYYY").add(14, 'days').format('DD/MM/YYYY'));
			getMemberPriceLevel($('#type_member_id').val());
		});
	});
}

function LOVCOADebit() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#pembayaran_debit_coa_code', '#pembayaran_debit_coa_name']);
		$('#modalFormPembayaran').modal('hide');

		$('#ModalLOV').on('hidden.bs.modal', function () {
			$('#modalFormPembayaran').modal('show');

			if (selectedCOACodeDebit == $('#pembayaran_debit_coa_code').val() || !$('#pembayaran_debit_coa_code').val()) return;
			selectedCOACodeDebit = $('#pembayaran_debit_coa_code').val();
			$('#pembayaran_debit_coa_name').val(selectedCOACodeDebit + ' - ' + $('#pembayaran_debit_coa_name').val());
		});
	});
}

function LOVCOACredit() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#pembayaran_credit_coa_code', '#pembayaran_credit_coa_name']);
		$('#modalFormPembayaran').modal('hide');

		$('#ModalLOV').on('hidden.bs.modal', function () {
			$('#modalFormPembayaran').modal('show');

			if (selectedCOACodeCredit == $('#pembayaran_credit_coa_code').val() || !$('#pembayaran_credit_coa_code').val()) return;
			selectedCOACodeCredit = $('#pembayaran_credit_coa_code').val();
			$('#pembayaran_credit_coa_name').val(selectedCOACodeCredit + ' - ' + $('#pembayaran_credit_coa_name').val());
		});
	});
}

function getMemberPriceLevel(type_member_id = '', gantiHarga = true, subLevel = null) {
	loadingProcess();
	ajaxNew(base_url + 'master/member/get_member_price_level?type_member_id=' + type_member_id, null, 'GET')
		.then((data) => {
			loadingProcess(false);
			$('#level_harga').find('option').remove().end();
			$('#level_harga_containter').hide();
			if (data.result) {
				let row = data.data;
				let html = '';
				row.forEach((value, index) => {
					$('#level_harga').append(`<option value="${value.sub_level}" ${subLevel && value.sub_level == subLevel ? 'selected' : ''}>${value.name}</option>`);
				});
				$('#level_harga_containter').show();
			} else {
				$('#level_harga').find('option').remove().end();
			}
			if (_dataTable.data().count() > 0 && gantiHarga) {
				changeLevelHarga(false);
			}
		})
		.catch((err) => {
			loadingProcess(false);
			MsgBox.Notification(err.toString());
		});
}

function changeLevelHarga(cekDetail = true) {
	if (_dataTable.data().count() <= 1 && cekDetail) {
		return;
	}

	loadingProcess();
	let parameters = 'cust_id=' + $('#cust_id').val().trim() + '&cust_type_lvl=' + $('#cust_type_lvl').val().trim() + '&type_member_id=' + $('#type_member_id').val().trim() + '&sub_level=' + $('#level_harga').val();
	data2Send = JSON.stringify(_dataTable.rows().data().toArray());
	ajaxNew(base_url + 'marketplace/pos/ganti_harga_jual?' + parameters, data2Send, 'POST', 'application/json')
		.then((data) => {
			if (data.result) {
				data4DataTable = data.data;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.data).draw();
					data2Send = null;
					loadingProcess(false);
					if (flagTerapkanPromoDiskon) {
						applyPromoActions();
					} else {
						setTimeout(() => {
							addDTRow();
						}, _dataTable.data().count() * 100);
					}
				}, 500);
			} else {
				MsgBox.Notification(data.message.toString());
				loadingProcess(false);
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
			loadingProcess(false);
		});
}

function applyPromo() {
	// Check detail
	if (_dataTable.data().count() <= 1) {
		MsgBox.Notification('Periksa kembali detail transaksi');
		return;
	}

	MsgBox.Confirm('Terapkan promo pada transaksi ini?').then(result => {
		applyPromoActions();
	}).catch(err => {
		console.log(err);
	});
}

function applyPromoActions(closeLoadingProcess = true) {
	loadingProcess();
	let parameters = 'cust_id=' + $('#cust_id').val().trim() + '&cust_type_lvl=' + $('#cust_type_lvl').val().trim() + '&type_member_id=' + $('#type_member_id').val().trim() + '&sub_level=' + $('#level_harga').val();
	let url = base_url + Modules + '/' + Controller + '/terapkan_promo?' + parameters;
	let detail = JSON.stringify(_dataTable.rows().data().toArray());
	let summary = $('#form_summary').serialize();
	_data2Send = $('#form_input').serialize() + summary + '&detail=' + detail;
	ajaxNew(url, _data2Send, 'POST')
		.then((data) => {
			if (data.result) {
				data4DataTable = data.data;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.data).draw();
					setTimeout(() => {
						addDTRow();
					}, _dataTable.data().count() * 100);
				}, 500);
				data2Send = null;
				if (closeLoadingProcess) loadingProcess(false);
				flagTerapkanPromoDiskon = true;
			} else {
				MsgBox.Notification(data.message.toString());
				loadingProcess(false);
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
			loadingProcess(false);
		});
}

function priceHistory(itemId = '') {
	$('#item_id_riwayat_harga').val(itemId);
	$('#modalRiwayatHarga').modal('show');
	$('#modalRiwayatHarga').on('shown.bs.modal', function () {
		_dataRiwayatHarga.ajax.reload(null, true);
	});
}


