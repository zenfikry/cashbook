(function () {
	// Construct
})();

function initPage(){
	initDatePicker();

	DataTable = $('#table_list_data').DataTable({ 
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getListPc',
			"type": "POST",
			"data": function (d) {
				d.filter_status = $('#filter_status').val();
				d.filter_company = $('#filter_company').val();
				d.filter_start_date = $('#filter_start_date').val();
				d.filter_end_date = $('#filter_end_date').val();
         },
		},
		"columns": [
			{ "data": "no_doc" },
			{
				"data": "periode_awal", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.periode_awal).format('DD/MM/YYYY') + '-' + moment(row.periode_akhir).format('DD/MM/YYYY');
				},
			},
			{ "data": "m_shortdesc" },
			{ "data": "m_odesc"},
			{ "data": "pic" },
			{ "data": "code_company", "className": "text-center" },
			{ "data": "name"},
			{
				"data": "pettycash_awal", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_awal);
				},
			},
			{
				"data": "pettycash_expense", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_expense);
				},
			},
			{
				"data": "pettycash_akhir", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_akhir);
				},
			},
			{
				"data": "status", "className": "text-center",
				"render": function (data, type, row, meta) {
					if (row.status == 0) {
						return '<span class="badge badge-primary text-center">New</span>';
					} else if(row.status == 1) {
						return '<span class="badge badge-secondary text-center">On Progress</span>';
					} else if(row.status == 2) {
						return '<span class="badge badge-success text-center">Approved by BM/ABM</span>';
					} else if(row.status == 3) {
						return '<span class="badge badge-success text-center">Approved by HOD</span>';
					} else if(row.status == 4) {
						return '<span class="badge badge-success text-center">Approved by FIN</span>';
					} else if(row.status == 5) {
						return '<span class="badge badge-warning text-center">Reasign</span>';
					} 
					else if(row.status == 6) {
						return '<span class="badge badge-success text-center">Done</span>';
					} else {
						return '<span class="badge badge-danger text-center">Reject</span>';
					}
				},
			},
			{ 
				"data": null, "width": 160, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';
					// APPROVAL
					if(row.code_jabatan == "BM" && row.status == 1 || row.code_jabatan == "ABM" && row.status == 1 || row.code_jabatan == "BM" && row.status == 0 || row.code_jabatan == "ABM" && row.status == 0){
						actions += '<a href="#' + Modules + '/pos/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "HOD" && row.status == 2){
						actions += '<a href="#' + Modules + '/pos/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "ARM" && row.status == 3){
						actions += '<a href="#' + Modules + '/pos/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}

					if(row.code_jabatan == "SPV" && row.status == 0 || row.code_jabatan == "SPV" && row.status == 5){
						actions += '<a href="#' + Modules + '/pos/edit/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </button></a> ';
					}
					// EDIT

					/* if (Priv.cancel_flag == 1 && data.status_payment == 0) {
						actions += '<button class="btn btn-outline-warning btn-sm waves-effect waves-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Batal" onclick="app_cancel(\'' + row.no_doc + '\')"><i class="fas fa-times-circle"></i> </button> ';
					} */
					/* if (Priv.pdf_flag == 1 && data.status_payment < 9) {
						actions += '<a href="' + base_url + Modules + '/' + Controller + '/pdf_dokumen/' + row.no_doc + '" target="_blank" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="fa fa-file-pdf-o"></i> </a> ';
					} */
					/* if (Priv.edit_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </a> ';
					} */
					/* if (Priv.delete_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="app_delete(\'' + row.no_doc + '\', \'' + row.no_doc + '\');"><i class="glyphicon glyphicon-trash"></i> </a>';
					} */
					
					if (Priv.pdf_flag == 1) {
						actions += '<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="invoiceCetak(\'' + row.no_doc + '\')"><i class="fa fa-print"></i> </button> ';
					}

					return actions;
				},
			}
      ],
	 
	});

	DataExcla = $('#table_list_excla').DataTable({ 
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getListEx',
			"type": "POST",
			"data": function (d) {
				d.filter_status = $('#filter_status').val();
				d.filter_company = $('#filter_company').val();
				d.filter_start_date = $('#filter_start_date').val();
				d.filter_end_date = $('#filter_end_date').val();
         },
		},
		"columns": [
			{ "data": "no_doc" },
			{
				"data": "periode_awal", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.periode_awal).format('DD/MM/YYYY') + '-' + moment(row.periode_akhir).format('DD/MM/YYYY');
				},
			},
			{ "data": "m_shortdesc" },
			{ "data": "m_odesc"},
			{ "data": "pic" },
			{ "data": "code_company", "className": "text-center" },
			{ "data": "name"},
			{
				"data": "pettycash_awal", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_awal);
				},
			},
			{
				"data": "pettycash_expense", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_expense);
				},
			},
			{
				"data": "pettycash_akhir", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_akhir);
				},
			},
			{
				"data": "status", "className": "text-center",
				"render": function (data, type, row, meta) {
					if (row.status == 0) {
						return '<span class="badge badge-primary text-center">New</span>';
					} else if(row.status == 1) {
						return '<span class="badge badge-secondary text-center">On Progress</span>';
					} else if(row.status == 2) {
						return '<span class="badge badge-success text-center">Approved by BM/ABM</span>';
					} else if(row.status == 3) {
						return '<span class="badge badge-success text-center">Approved by HOD</span>';
					} else if(row.status == 4) {
						return '<span class="badge badge-success text-center">Approved by FIN</span>';
					} else if(row.status == 5) {
						return '<span class="badge badge-warning text-center">Reasign</span>';
					} 
					else if(row.status == 6) {
						return '<span class="badge badge-success text-center">Done</span>';
					} else {
						return '<span class="badge badge-danger text-center">Reject</span>';
					}
				},
			},
			{ 
				"data": null, "width": 160, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';
					// APPROVAL
					if(row.code_jabatan == "BM" && row.status == 1 || row.code_jabatan == "ABM" && row.status == 1 || row.code_jabatan == "BM" && row.status == 0 || row.code_jabatan == "ABM" && row.status == 0){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "HOD" && row.status == 2){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "ARM" && row.status == 3){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}

					if(row.code_jabatan == "SPV" && row.status == 0 || row.code_jabatan == "SPV" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </button></a> ';
					}
					// EDIT

					/* if (Priv.cancel_flag == 1 && data.status_payment == 0) {
						actions += '<button class="btn btn-outline-warning btn-sm waves-effect waves-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Batal" onclick="app_cancel(\'' + row.no_doc + '\')"><i class="fas fa-times-circle"></i> </button> ';
					} */
					/* if (Priv.pdf_flag == 1 && data.status_payment < 9) {
						actions += '<a href="' + base_url + Modules + '/' + Controller + '/pdf_dokumen/' + row.no_doc + '" target="_blank" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="fa fa-file-pdf-o"></i> </a> ';
					} */
					/* if (Priv.edit_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </a> ';
					} */
					/* if (Priv.delete_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="app_delete(\'' + row.no_doc + '\', \'' + row.no_doc + '\');"><i class="glyphicon glyphicon-trash"></i> </a>';
					} */
					
					if (Priv.pdf_flag == 1) {
						actions += '<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="invoiceCetak(\'' + row.no_doc + '\')"><i class="fa fa-print"></i> </button> ';
					}

					return actions;
				},
			}
      ],
	 
	});
}

function initDatePicker() {
	$('#filter_start_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", _jsDateStart.format('DD/MM/YYYY'));
	
	$('#filter_end_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", _jsDateEnd.format('DD/MM/YYYY'));

	// $('#filter_end_date').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", _jsDateEnd.format('DD/MM/YYYY'));
}

// START function defaul

/* function app_cancel(no_doc = '') {
	MsgBox.Confirm('Yakin akan batalkan dokumen ' + no_doc + ' ini?', 'Batal').then(result => {
      _data2Send = new URLSearchParams({ no_doc: no_doc });
      ajaxNew(base_url + Modules + '/' + Controller + '/cancel/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh(false);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

/* function app_edit(toolbar = 0, id = null) {
	if (toolbar == 1 && dataArr.length <= 0) {
		MsgBox.Notification('Checklist terlebih dahulu data yang akan diedit.', 'bottom right', 'info');
		return;
	}
	
	if (toolbar) {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + dataArr[0];
	} else {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + id;
	}
} */

/* function app_delete(id = 0, messageParam = '') {
	MsgBox.Confirm('Yakin akan hapus data ' + messageParam + ' ini?', 'Hapus data').then(result => {
      _data2Send = new URLSearchParams({ id: id });
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh();
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_refresh(refreshFromStart = true) {
	data2Send = null;
	DataTable.ajax.reload(null, refreshFromStart);
}

// END function default

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function invoiceCetak(doc_no = '') {
   $('#doc_no').val(doc_no);
   $('#modalFilterTitleSmall').text(doc_no);
   $('#modalFilter').modal('show');
}

function cetakInvoice() {
   let url = base_url + Modules + '/' + Controller + '/' + $('#filter_jenis').val() + '/' + $('#doc_no').val();
   window.open(url, '_blank');
}