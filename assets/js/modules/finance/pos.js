(function () {
	// Construct
	initPage()
})();

function get_date_print(){
	var jsDate = $('#filter_start_date').datepicker('getDate');
	if(jsDate){
		month = '' + (jsDate.getMonth() + 1),
		day = '' + jsDate.getDate(),
		year = jsDate.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		if(day && month && year){
			return [day,month,year].join('-');
		}
	}
	return "";
	// let periodex_akhir = $("#filter_endt_date").val();
}

function newexportaction(e, dt, button, config) {
    var self = this;
    var oldStart = dt.settings()[0]._iDisplayStart;
    dt.one('preXhr', function(e, s, data) {
        // Just this once, load all data from the server...
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function(e, settings) {
            // Call the original action function
            if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button,
                    config);
            } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt,
                        button, config) :
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt,
                        button, config);
            } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
            dt.one('preXhr', function(e, s, data) {
                // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                // Set the property to what it was before exporting.
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
            });
            // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            setTimeout(dt.ajax.reload, 0);
            // Prevent rendering of the full data to the DOM
            return false;
        });
    });
    // Requery the server with the new one-time export settings
    dt.ajax.reload();
};
function initPage(){
	initDatePicker();
	
	let date_awal = get_date_print()
	DataTable = $('#table_list_data').DataTable({ 
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"destroy":true,
		dom: 'Bfrtip',
        buttons: [
			{
                extend: 'excel',
                title: 'Export Excel',
                filename: function() {
					
                    return "PETTYCASH RECAP DATA";
                },
                className: 'btn btn-excel',
                action: newexportaction
            },
            {
				extend:'pdf',
				text:'Export PDF',
				title: ' ',
				filename: function() {
					
                    return "PETTYCASH RECAP DATA";
                },
				action: newexportaction,
				
				orientation:'landscape',
				exportOptions: {
					columns: [0,1,2,3,4,5,6,7,8,9,10]
				},
				customize : function(doc){
					doc.content.splice(1, 0, {
						text: [{
							text: `PETTYCASH RECAP DATA ` + date_awal+"\n",
							bold: true,
							fontSize: 16
						}],
					})
					var colCount = new Array();
					var length = $('#table_list_data tbody tr:first-child td').length;
					$('#table_list_data').find('tbody tr:first-child td').each(function(){
						if($(this).attr('colspan')){
							for(var i=1;i<=$(this).attr('colspan');$i++){
								colCount.push('*');
							}
						}else{ colCount.push(parseFloat(100 / length)+'%'); }
					});
				}
			}
        ],
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
			"data": function (d) {
				d.filter_status = $('#filter_status').val();
				d.filter_company = $('#filter_company').val();
				d.filter_start_date = $('#filter_start_date').val();
				d.filter_end_date = $('#filter_end_date').val();
         },
		},
		"columns": [
			{ "data": "no_doc" },
			{
				"data": "periode_awal", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.periode_awal).format('DD/MM/YYYY') + '-' + moment(row.periode_akhir).format('DD/MM/YYYY');
				},
			},
			{ "data": "m_shortdesc" },
			{ "data": "m_odesc"},
			{ "data": "pic" },
			{ "data": "code_company", "className": "text-center" },
			{ "data": "name"},
			{
				"data": "pettycash_awal", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_awal);
				},
			},
			{
				"data": "pettycash_expense", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_expense);
				},
			},
			{
				"data": "pettycash_akhir", "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.pettycash_akhir);
				},
			},
			{
				"data": "status", "className": "text-center",
				"render": function (data, type, row, meta) {
					if (row.status == 0) {
						return '<span class="badge badge-primary text-center">New</span>';
					} else if(row.status == 1) {
						return '<span class="badge badge-secondary text-center">On Progress</span>';
					} else if(row.status == 2) {
						return '<span class="badge badge-success text-center">Approved by BM/ABM</span>';
					} else if(row.status == 4) {
						return '<span class="badge badge-success text-center">Approved by AR</span>';
					} else if(row.status == 3) {
						return '<span class="badge badge-success text-center">Approved by HOD Store</span>';
					} else if(row.status == 7) {
						return '<span class="badge badge-success text-center">Approved by AP</span>';					
					} else if(row.status == 8) {
						return '<span class="badge badge-success text-center">Approved by MGR Finance</span>';					
					} else if(row.status == 5) {
						return '<span class="badge badge-warning text-center">Reasign</span>';
					} 
					else if(row.status == 6) {
						return '<span class="badge badge-success text-center">Done</span>';
					}
					else if(row.status == 10) {
						return '<span class="badge badge-warning text-center">Revision</span>';
					} else {
						return '<span class="badge badge-danger text-center">Reject</span>';
					}
				},
			},
			{ 
				"data": null, "width": 160, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';
					// APPROVAL
					// BM
					if(row.code_jabatan == "PO020" && row.status == 1 || row.code_jabatan == "PO020" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					//DM / AM
					if(row.code_jabatan == "PO036" && row.status == 4 || row.code_jabatan == "PO036" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					// AM
					if(row.code_jabatan == "PO011" && row.status == 4 || row.code_jabatan == "PO011" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					// OR INI ABM
					else if(row.code_jabatan == "PO012" && row.status == 1 || row.code_jabatan == "PO012" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "PO133" && row.status == 2 || row.code_jabatan == "PO133" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "PO048" && row.status == 4 || row.code_jabatan == "PO048" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "PO003" && row.status == 2 || row.code_jabatan == "PO003" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
				
					else if(row.code_jabatan == "PO002" && row.status == 3 || row.code_jabatan == "PO001" && row.status == 3 || row.code_jabatan == "PO002" && row.status == 5 || row.code_jabatan == "PO001" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.code_jabatan == "PO039" && row.status == 7 || row.code_jabatan == "PO039" && row.status == 5){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
				
					// else if(row.code_jabatan == "HOD" && row.status == 3){
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }
					// else if(row.code_jabatan == "SPV"){
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }

					if(row.code_jabatan == "PO107" && row.status == 0 || row.code_jabatan == "PO107" && row.status == 10){
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </button></a> ';
					}

					if(row.code_jabatan == "PO107" && row.status == 0 || row.code_jabatan == "PO107" && row.status != 10){
						actions += `<a href="javascript:void(0)" id="btn_delete" data-id="${row.no_doc}"><button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-trash"></i> </button></a> `;
					}
					
					// EDIT

					/* if (Priv.cancel_flag == 1 && data.status_payment == 0) {
						actions += '<button class="btn btn-outline-warning btn-sm waves-effect waves-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Batal" onclick="app_cancel(\'' + row.no_doc + '\')"><i class="fas fa-times-circle"></i> </button> ';
					} */
					/* if (Priv.pdf_flag == 1 && data.status_payment < 9) {
						actions += '<a href="' + base_url + Modules + '/' + Controller + '/pdf_dokumen/' + row.no_doc + '" target="_blank" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="fa fa-file-pdf-o"></i> </a> ';
					} */
					/* if (Priv.edit_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </a> ';
					} */
					/* if (Priv.delete_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="app_delete(\'' + row.no_doc + '\', \'' + row.no_doc + '\');"><i class="glyphicon glyphicon-trash"></i> </a>';
					} */
					if (row.code_jabatan == "PO039" && row.status == 7){
						actions += `<a href="javascript:void(0)" id="btn_confirm" data-id="${row.no_doc}"><button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="mdi mdi-checkbox-marked-circle-outline"></i> </button></a> `;
					}
					
					if (Priv.pdf_flag == 1) {
						actions += `<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="cetakInvoice('${row.no_doc}')"><i class="fa fa-print"></i> </button> `;
					}

					return actions;
				},
			}
      ],
	 
	});
}

function initDatePicker() {
	// $('#filter_start_date').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", _jsDateStart.format('DD/MM/YYYY'));
	
	$('#filter_start_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	});
	
	$('#filter_end_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	});

	// $('#filter_end_date').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", _jsDateEnd.format('DD/MM/YYYY'));
}

// START function defaul

/* function app_cancel(no_doc = '') {
	MsgBox.Confirm('Yakin akan batalkan dokumen ' + no_doc + ' ini?', 'Batal').then(result => {
      _data2Send = new URLSearchParams({ no_doc: no_doc });
      ajaxNew(base_url + Modules + '/' + Controller + '/cancel/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh(false);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

/* function app_edit(toolbar = 0, id = null) {
	if (toolbar == 1 && dataArr.length <= 0) {
		MsgBox.Notification('Checklist terlebih dahulu data yang akan diedit.', 'bottom right', 'info');
		return;
	}
	
	if (toolbar) {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + dataArr[0];
	} else {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + id;
	}
} */

/* function app_delete(id = 0, messageParam = '') {
	MsgBox.Confirm('Yakin akan hapus data ' + messageParam + ' ini?', 'Hapus data').then(result => {
      _data2Send = new URLSearchParams({ id: id });
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh();
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_refresh(refreshFromStart = true) {
	data2Send = null;
	DataTable.ajax.reload(null, refreshFromStart);
}

// END function default

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function invoiceCetak(doc_no = '') {
   $('#doc_no').val(doc_no);
   $('#modalFilterTitleSmall').text(doc_no);
   $('#modalFilter').modal('show');
}

function cetakInvoice() {
   let url = base_url + Modules + '/' + Controller + '/' + $('#filter_jenis').val() + '/' + $('#doc_no').val();
   window.open(url, '_blank');
}

$(document).on('click',"#btn_delete",function(){
	let id = $(this).data('id');
	MsgBox.Confirm('Are your sure delete this data ?').then(result => {
		if(result){
			$.ajax({
				"url": base_url + Modules + '/' + Controller + '/delete/'+id,
				"data":{no_doc:id},
				"success":function(data){
					data = JSON.parse(data);
					if(data.status == 200){
						MsgBox.Notification(data.msg, 'bottom right', 'success');
						app_refresh();
					}
					else{
						MsgBox.Notification(data.msg);
						app_refresh();
					}
				}
			})
		}
	}).catch(err => {
		console.log(err);
	});
	
})

$(document).on('click',"#btn_confirm",function(){
	let id = $(this).data('id');
	MsgBox.Confirm('Are your sure Approved this data ?').then(result => {
		if(result){
			$.ajax({
				// "url": base_url + Modules + '/' + Controller + '/confirmapp/'+id,
				"url": base_url + Modules + '/' + Controller + '/done_pettycash/'+id,
				"data":{no_doc:id},
				"success":function(data){
					data = JSON.parse(data);
					if(data.status == 200){
						MsgBox.Notification(data.msg, 'bottom right', 'success');
						app_refresh();
					}
					else{
						MsgBox.Notification(data.msg);
						app_refresh();
					}
				}
			})
		}
	}).catch(err => {
		console.log(err);
	});
	
})


 
 function cetakInvoice(no_doc) {
	let url = base_url + Modules + '/' + Controller + '/export_pdf/'+no_doc;
	
	window.open(url, '_blank');
 }