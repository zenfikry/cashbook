(function () {
	// Construct
})();

function initPage(){
	initDatePicker();

	DataTable = $('#table_list_data').DataTable({ 
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
			"data": function (d) {
				d.filter_status = $('#filter_status').val();
				d.filter_start_date = $('#filter_start_date').val();
				d.filter_end_date = $('#filter_end_date').val();
         },
		},
		"columns": [
			{ "data": "no_doc", "width": 100 },
			{
				"data": "tgl_trans", "width": 50,
				"render": function (data, type, row, meta) {
					return moment(row.tgl_trans).format('DD/MM/YYYY');
				},
			},
			{ "data": "store_name", "width": 100 },
			{
				"data": "supp_name", "width": 150,
				"render": function (data, type, row, meta) {
					return row.supp_id + ' - ' + row.supp_name;
				},
			},
			{
				"data": "total_potongan", "width": 150, "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.total_potongan);
				},
			},
			{
				"data": "total_bayar", "width": 150, "className": "text-right",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.total_bayar);
				},
			},
			{
				"data": "coa_name", "width": 150,
				"render": function (data, type, row, meta) {
					return row.coa_no + ' - ' + row.coa_name;
				},
			},
			{ "data": "keterangan", "width": 150 },
			{
				"data": "status", "width": 50, "className": "text-center", "searchable": false,
				"render": function (data, type, row, meta) {
					if (row.status == 0) {
						return '<span class="badge badge-info text-center">Baru</span>';
					} else if (row.status == 1) {
						return '<span class="badge badge-success text-center">Selesai</span>';
					} else {
						return '<span class="badge badge-danger text-center">Batal</span>';
					}
				},
			},
			{ 
				"data": null, "width": 160, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';
					
					if (Priv.confirm_flag == 1 && row.status == 0) {
						actions += '<button class="btn btn-outline-success btn-sm waves-effect waves-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Selesai" onclick="app_confirm(\'' + row.no_doc + '\')"><i class="fas fa-check"></i> </button> ';
					}
					if (Priv.cancel_flag == 1 && data.status == 0) {
						actions += '<button class="btn btn-outline-warning btn-sm waves-effect waves-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Batal" onclick="app_cancel(\'' + row.no_doc + '\')"><i class="fas fa-times-circle"></i> </button> ';
					}
					/* if (Priv.pdf_flag == 1) {
						actions += '<a href="' + base_url + Modules + '/' + Controller + '/pdf_dokumen/' + row.no_doc + '" target="_blank" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="fa fa-file-pdf-o"></i> </a> ';
					} */
					if (Priv.edit_flag == 1 && row.status == 0) {
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </a> ';
					}
					if (Priv.delete_flag == 1 && row.status == 0) {
						actions += '<a href="#" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="app_delete(\'' + row.no_doc + '\', \'' + row.no_doc + '\');"><i class="glyphicon glyphicon-trash"></i> </a>';
					}
					return actions;
				},
			}
      ],
	});
}

function initDatePicker() {
	$('#filter_start_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", _jsDateStart.format('DD/MM/YYYY'));
	
	$('#filter_end_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", _jsDateEnd.format('DD/MM/YYYY'));
}

// START function defaul

function app_confirm(no_doc = '') {
	MsgBox.Confirm('Yakin akan konfirmasi dokumen ' + no_doc + ' ini?', 'Konfirmasi').then(result => {
      _data2Send = new URLSearchParams({ no_doc: no_doc });
		loadingProcess();
      ajaxNew(base_url + Modules + '/' + Controller + '/confirm/', _data2Send, 'POST')
      .then((data) => {
			loadingProcess(false);
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh(false);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
			loadingProcess(false);
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
}

function app_cancel(no_doc = '') {
	MsgBox.Confirm('Yakin akan batalkan dokumen ' + no_doc + ' ini?', 'Batal').then(result => {
      _data2Send = new URLSearchParams({ no_doc: no_doc });
		loadingProcess();
      ajaxNew(base_url + Modules + '/' + Controller + '/cancel/', _data2Send, 'POST')
      .then((data) => {
			loadingProcess(false);
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh(false);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
			loadingProcess(false);
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
}

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

function app_edit(toolbar = 0, id = null) {
	if (toolbar == 1 && dataArr.length <= 0) {
		MsgBox.Notification('Checklist terlebih dahulu data yang akan diedit.', 'bottom right', 'info');
		return;
	}
	
	if (toolbar) {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + dataArr[0];
	} else {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + id;
	}
}

function app_delete(id = 0, messageParam = '') {
	MsgBox.Confirm('Yakin akan hapus data ' + messageParam + ' ini?', 'Hapus data').then(result => {
      _data2Send = new URLSearchParams({ id: id });
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh();
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
}

function app_refresh(refreshFromStart = true) {
	data2Send = null;
	DataTable.ajax.reload(null, refreshFromStart);
}

// END function default

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}