(function () {
	// Construct
})();

function initPage(){
	initDatePicker();

	DataTable = $('#table_list_data').DataTable({ 
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"destroy":true,
		dom: 'Bfrtip',
        buttons: [
			{
                extend: 'excel',
                title: 'Export Excel',
                filename: function() {
					
                    return "PAYMENT RECAP DATA";
                },
                className: 'btn btn-excel',
                action: newexportaction
            },
            {
				extend:'pdf',
				text:'Export PDF',
				title: ' ',
				filename: function() {
					
                    return "PAYMENT RECAP DATA";
                },
				action: newexportaction,
				
				orientation:'landscape',
				exportOptions: {
					columns: [0,1,2,3,4,5,6]
				},
				customize : function(doc){
					doc.content.splice(1, 0, {
						text: [{
							text: `PAYMENT RECAP DATA `+"\n",
							bold: true,
							fontSize: 16
						}],
					})
					var colCount = new Array();
					var length = $('#table_list_data tbody tr:first-child td').length;
					$('#table_list_data').find('tbody tr:first-child td').each(function(){
						if($(this).attr('colspan')){
							for(var i=1;i<=$(this).attr('colspan');$i++){
								colCount.push('*');
							}
						}else{ colCount.push(parseFloat(100 / length)+'%'); }
					});
				}
			}
        ],
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
			"data": function (d) {
				d.filter_status = $('#filter_status').val();
         },
		},
		"columns": [
			{ "data": "inv_no" },
			{
				"data": "inv_date", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.inv_date).format('DD/MM/YYYY');
				},
			},
			{ "data": "code_creditor" },
			{ "data": "desc_creditor" },
            { "data": "supplier_iv"},
            { "data": "ref_no"},
            {
				"data": "due_date", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.due_date).format('DD/MM/YYYY');
				},
			},
			{
				"data": "net_total", "className": "text-centerright",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.net_total);
				},
			},
			{
				"data": "status", "className": "text-center",
				"render": function (data, type, row, meta) {
					if (row.status == 6){
						return '<span class="badge badge-danger text-center">Paid</span>';
					} else if (row.status == 10) {
						// console.log(row.status);
						return '<span class="badge badge-danger text-center">Paid</span>';
					} else if (row.status == 11) {
						return '<span class="badge badge-warning text-center">Finance Manager Approved</span>';
					} else {
						// console.log(row.status);
						return '<span class="badge badge-primary text-center">Approved Complete</span>';
					}
				},
			},
			{ 
				"data": null, "width": 160, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';
					console.log($("#role_group").val());
					// console.log(row.code_login);
					// APPROVAL PAYMENT BTN
					if(row.status == 11){
						actions += `<a href="javas	cript:void(0)"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="readyStatus" data-id="${row.inv_no}"><i class="fas fa-check"></i> </button></a> `;
					} 
					if (row.status == 10) {
						actions += `<a href="javascript:void(0)"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="appStatus" data-id="${row.inv_no}"><i class="fas fa-check"></i> </button></a> `;
					} 
					
					if (row.status == 6 || row.status ==10 || row.status == 11 || row.status == 12) {
						actions += `<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="cetakInvoice('${row.inv_no}')"><i class="fa fa-print"></i> </button> `;
					}

					return actions;
				},
			}
      ],
	 
	});
}
function newexportaction(e, dt, button, config) {
    var self = this;
    var oldStart = dt.settings()[0]._iDisplayStart;
    dt.one('preXhr', function(e, s, data) {
        // Just this once, load all data from the server...
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function(e, settings) {
            // Call the original action function
            if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button,
                    config);
            } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt,
                        button, config) :
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt,
                        button, config);
            } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
            dt.one('preXhr', function(e, s, data) {
                // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                // Set the property to what it was before exporting.
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
            });
            // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            setTimeout(dt.ajax.reload, 0);
            // Prevent rendering of the full data to the DOM
            return false;
        });
    });
    // Requery the server with the new one-time export settings
    dt.ajax.reload();
};
function initDatePicker() {
	$('#filter_start_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	});
	
	$('#filter_end_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	});

	// $('#filter_end_date').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", _jsDateEnd.format('DD/MM/YYYY'));
}

// START function defaul

/* function app_cancel(no_doc = '') {
	MsgBox.Confirm('Yakin akan batalkan dokumen ' + no_doc + ' ini?', 'Batal').then(result => {
      _data2Send = new URLSearchParams({ no_doc: no_doc });
      ajaxNew(base_url + Modules + '/' + Controller + '/cancel/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh(false);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

/* function app_edit(toolbar = 0, id = null) {
	if (toolbar == 1 && dataArr.length <= 0) {
		MsgBox.Notification('Checklist terlebih dahulu data yang akan diedit.', 'bottom right', 'info');
		return;
	}
	
	if (toolbar) {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + dataArr[0];
	} else {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + id;
	}
} */

/* function app_delete(id = 0, messageParam = '') {
	MsgBox.Confirm('Yakin akan hapus data ' + messageParam + ' ini?', 'Hapus data').then(result => {
      _data2Send = new URLSearchParams({ id: id });
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh();
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_refresh(refreshFromStart = true) {
	data2Send = null;
	DataTable.ajax.reload(null, refreshFromStart);
}

// END function default

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function invoiceCetak(doc_no = '') {
   $('#doc_no').val(doc_no);
   $('#modalFilterTitleSmall').text(doc_no);
   $('#modalFilter').modal('show');
}

function cetakInvoice(no_doc) {
	let url = base_url + Modules + '/purchase/export_pdf/'+no_doc;
	
	window.open(url, '_blank');
 }

 $(document).on('click',"#appStatus",function(){
	 id= $(this).data('id');
	 MsgBox.Confirm('Are you sure ready this data?').then(result => {
		if (!result) return;
		$.ajax({
			url: base_url + Modules + '/' + Controller + '/appAction/' +id,
			dataType:"json",
			method:"POST",
			success:function(data){
				
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					// location.reload()
					app_refresh();
				}
				else {
					MsgBox.Notification(data.msg.toString());
					app_refresh();
				}
			}
		})
	})
 })
 $(document).on('click',"#readyStatus",function(){
	 id= $(this).data('id');
	 MsgBox.Confirm('Are you sure ready this data?').then(result => {
		if (!result) return;
		$.ajax({
			url: base_url + Modules + '/' + Controller + '/readyAction/' +id,
			dataType:"json",
			method:"POST",
			success:function(data){
				
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					// location.reload()
					app_refresh();
				}
				else {
					MsgBox.Notification(data.msg.toString());
					app_refresh();
				}
			}
		})
	})
 })