(function () {
	// Construct
})();

function initPage(){
	initDatePicker();

	DataTable = $('#table_list_data').DataTable({ 
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"destroy":true,
		dom: 'Bfrtip',
        buttons: [
			{
                extend: 'excel',
                title: 'Export Excel',
                filename: function() {
					
                    return "PURCHASE RECAP DATA";
                },
                className: 'btn btn-excel',
                action: newexportaction
            },
            {
				extend:'pdf',
				text:'Export PDF',
				title: ' ',
				filename: function() {
					
                    return "PURCHASE RECAP DATA";
                },
				action: newexportaction,
				
				orientation:'landscape',
				exportOptions: {
					columns: [0,1,2,3,4,5,6]
				},
				customize : function(doc){
					doc.content.splice(1, 0, {
						text: [{
							text: `PURCHASE RECAP DATA `+"\n",
							bold: true,
							fontSize: 16
						}],
					})
					var colCount = new Array();
					var length = $('#table_list_data tbody tr:first-child td').length;
					$('#table_list_data').find('tbody tr:first-child td').each(function(){
						if($(this).attr('colspan')){
							for(var i=1;i<=$(this).attr('colspan');$i++){
								colCount.push('*');
							}
						}else{ colCount.push(parseFloat(100 / length)+'%'); }
					});
				}
			}
        ],
		"ajax": {
			"url": base_url + Modules + '/' + Controller + '/getList',
			"type": "POST",
			"data": function (d) {
				d.filter_status = $('#filter_status').val();
         },
		},
		"columns": [
			{ "data": "inv_no" },
			{
				"data": "inv_date", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.inv_date).format('DD/MM/YYYY');
				},
			},
			{ "data": "code_creditor" },
			{ "data": "desc_creditor" },
            { "data": "supplier_iv"},
            { "data": "ref_no"},
			{ "data": "code_rate"},
            {
				"data": "due_date", "className": "text-center",
				"render": function (data, type, row, meta) {
					return moment(row.due_date).format('DD/MM/YYYY');
				},
			},
			{
				"data": "net_total", "className": "text-centerright",
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.net_total);
				},
			},
			{
				"data": "status", "className": "text-center",
				"render": function (data, type, row, meta) {
					if (row.status == 0) {
						return '<span class="badge badge-primary text-center">New</span>';
					} else if(row.status == 1) {
						return '<span class="badge badge-secondary text-center">On Progress</span>';
					} else if(row.status == 2) {
						return '<span class="badge badge-success text-center">Approved by MGRD</span>';
					} else if(row.status == 3) {
						return `<span class="badge badge-success text-center">Approved by ${row.code_approve}</span>`;
					} else if(row.status == 4) {
						return '<span class="badge badge-success text-center">Approved by FIN</span>';
					} else if(row.status == 5) {
						return '<span class="badge badge-warning text-center">Revition</span>';
					} 
					else if(row.status == 6) {
						return '<span class="badge badge-success text-center">Done</span>';
					}else if(row.status == 10) {
						return '<span class="badge badge-success text-center">Ready</span>';
					} else {
						return '<span class="badge badge-danger text-center">Reject</span>';
					}
				},
			},
			{ 
				"data": null, "width": 160, "className": "text-center", "orderable": false, "searchable": false,
				"render": function (data, type, row, meta) {
					let actions = '';
					// console.log($("#role_group").val());

					if(row.status == 1 && row.login != row.email){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.inv_no + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					else if(row.status == 3 && row.login != row.email && row.code_approve !=row.code_login){
						actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.inv_no + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					}
					// if($("#role_group").val() == "HOD_GROUP" && row.status == 1){						
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }
					// else if($("#role_group").val() == "COO_GROUP" && row.status == 3){						
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }

					// APPROVAL
					// if($("#tbl_id").val() == "ROOT"){
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }
					// if(row.code_jabatan == "MGRHRD" && row.status == 1 ){
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }
					// else if(row.code_jabatan == "HOD" && row.status == 2){
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }
					// else if(row.code_jabatan == "ARM" && row.status == 3){
					// 	actions += '<a href="#' + Modules + '/' + Controller + '/detail/' + row.no_doc + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="fa fa-check"></i> </button></a> ';
					// }

					if($("#tbl_id").val() == "ROOT"){
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.inv_no + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </button></a> ';
					}
					else if(row.status == 1 && row.created_by == row.code_login || row.status == 5 && row.created_by == row.code_login || row.status == 6 && row.code_login == "PO002" || row.status == 6 && row.code_login == "PO001"){
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.inv_no + '"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </button></a> ';
						
					}

					// READY BTN
					if(row.status == 6 && row.code_login == "PO002" || row.status == 6 && row.code_login == "PO001"){
						actions += `<a href="javascript:void(0)"><button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="readyStatus" data-id="${row.inv_no}"><i class="glyphicon glyphicon-check"></i> </button></a> `;
					}
					
					// EDIT

					/* if (Priv.cancel_flag == 1 && data.status_payment == 0) {
						actions += '<button class="btn btn-outline-warning btn-sm waves-effect waves-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Batal" onclick="app_cancel(\'' + row.no_doc + '\')"><i class="fas fa-times-circle"></i> </button> ';
					} */
					/* if (Priv.pdf_flag == 1 && data.status_payment < 9) {
						actions += '<a href="' + base_url + Modules + '/' + Controller + '/pdf_dokumen/' + row.no_doc + '" target="_blank" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="fa fa-file-pdf-o"></i> </a> ';
					} */
					/* if (Priv.edit_flag == 1 && row.status_payment == 0) {
						actions += '<a href="#' + Modules + '/' + Controller + '/edit/' + row.no_doc + '" class="btn btn-outline-info btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-pencil"></i> </a> ';
					} */
					if (Priv.delete_flag == 1 && row.status == 1) {
						actions += `<a href="javascript:void(0)" id="btn_delete" data-id="${row.inv_no}"><button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light"><i class="glyphicon glyphicon-trash"></i> </button></a> `;
					}
					
					if (row.status == 6 || row.status ==10) {
						actions += `<button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" onclick="cetakInvoice('${row.inv_no}')"><i class="fa fa-print"></i> </button> `;
					}

					return actions;
				},
			}
      ],
	 
	});
}
function newexportaction(e, dt, button, config) {
    var self = this;
    var oldStart = dt.settings()[0]._iDisplayStart;
    dt.one('preXhr', function(e, s, data) {
        // Just this once, load all data from the server...
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function(e, settings) {
            // Call the original action function
            if (button[0].className.indexOf('buttons-copy') >= 0) {
                $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button,
                    config);
            } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt,
                        button, config) :
                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt,
                        button, config);
            } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button,
                        config) :
                    $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button,
                        config);
            } else if (button[0].className.indexOf('buttons-print') >= 0) {
                $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
            }
            dt.one('preXhr', function(e, s, data) {
                // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                // Set the property to what it was before exporting.
                settings._iDisplayStart = oldStart;
                data.start = oldStart;
            });
            // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
            setTimeout(dt.ajax.reload, 0);
            // Prevent rendering of the full data to the DOM
            return false;
        });
    });
    // Requery the server with the new one-time export settings
    dt.ajax.reload();
};
function initDatePicker() {
	$('#filter_start_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	});
	
	$('#filter_end_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	});

	// $('#filter_end_date').datepicker({
	// 	format: 'dd/mm/yyyy',
	// 	autoclose: true,
	// }).datepicker("setDate", _jsDateEnd.format('DD/MM/YYYY'));
}

$(document).on('click',"#btn_delete",function(){
	let id = $(this).data('id');
	MsgBox.Confirm('Are your sure delete this data ?').then(result => {
		if(result){
			$.ajax({
				"url": base_url + Modules + '/' + Controller + '/delete/'+id,
				"data":{inv_no:id},
				"success":function(data){
					data = JSON.parse(data);
					if(data.status == 200){
						MsgBox.Notification(data.msg, 'bottom right', 'success');
						app_refresh();
					}
					else{
						MsgBox.Notification(data.msg);
						app_refresh();
					}
				}
			})
		}
	}).catch(err => {
		console.log(err);
	});
	
})

// START function defaul

/* function app_cancel(no_doc = '') {
	MsgBox.Confirm('Yakin akan batalkan dokumen ' + no_doc + ' ini?', 'Batal').then(result => {
      _data2Send = new URLSearchParams({ no_doc: no_doc });
      ajaxNew(base_url + Modules + '/' + Controller + '/cancel/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh(false);
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_create() { 
	window.location.href = '#' + Modules + '/' + Controller + '/create';
}

/* function app_edit(toolbar = 0, id = null) {
	if (toolbar == 1 && dataArr.length <= 0) {
		MsgBox.Notification('Checklist terlebih dahulu data yang akan diedit.', 'bottom right', 'info');
		return;
	}
	
	if (toolbar) {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + dataArr[0];
	} else {
		window.location.href = '#' + Modules + '/' + Controller + '/edit/' + id;
	}
} */

/* function app_delete(id = 0, messageParam = '') {
	MsgBox.Confirm('Yakin akan hapus data ' + messageParam + ' ini?', 'Hapus data').then(result => {
      _data2Send = new URLSearchParams({ id: id });
      ajaxNew(base_url + Modules + '/' + Controller + '/delete/', _data2Send, 'POST')
      .then((data) => {
         if (data.result) {
            MsgBox.Notification(data.message.toString());
            app_refresh();
         } else {
            MsgBox.Notification(data.message.toString());
         }
      })
      .catch((err) => {
         MsgBox.Notification(err.toString());
      });
   }).catch(err => {
      if (err) console.log(err);
   });
} */

function app_refresh(refreshFromStart = true) {
	data2Send = null;
	DataTable.ajax.reload(null, refreshFromStart);
}

// END function default

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function invoiceCetak(doc_no = '') {
   $('#doc_no').val(doc_no);
   $('#modalFilterTitleSmall').text(doc_no);
   $('#modalFilter').modal('show');
}

function cetakInvoice(no_doc) {
	let url = base_url + Modules + '/' + Controller + '/export_pdf/'+no_doc;
	
	window.open(url, '_blank');
 }

 $(document).on('click',"#readyStatus",function(){
	 id= $(this).data('id');
	 MsgBox.Confirm('Are you sure ready this data?').then(result => {
		if (!result) return;
		$.ajax({
			url: base_url + Modules + '/' + Controller + '/readyAction/' +id,
			dataType:"json",
			method:"POST",
			success:function(data){
				
				if (data.result) {
					MsgBox.Notification(data.msg.toString(), 'bottom right', 'success');
					location.reload()
				}
				else {
					MsgBox.Notification(data.msg.toString());
				}
			}
		})
	})
 })