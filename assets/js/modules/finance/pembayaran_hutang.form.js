(function () {
	// construct
	$("#lampiran").change(function () {
		if (this.files && this.files[0]) {
			$("#lampiran_preview_container").show();
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#lampiran_preview').attr('src', e.target.result);
			}
			reader.readAsDataURL(this.files[0]);
		}
	});

})();

function initPage() {
	initDataTable();
	initDatePicker();
	initValidation();
	initOtherElements();
	if (action == 'edit') getData();
}

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"oLanguage": {
			"sLengthMenu": `<button type="button" class="btn btn-outline-success waves-effect waves-light" id="addPenerimaan" name="addPenerimaan" onclick="LOVPickDetailPenerimaan();"><i class="fas fa-plus"></i> Penerimaan</button>`,
			"oPaginate": {
				"sFirst": "", // This is the link to the first page
				"sPrevious": "", // This is the link to the previous page
				"sNext": "", // This is the link to the next page
				"sLast": "" // This is the link to the last page
			}
		},
		"ordering": false,
		"info": false,
		"scrollX": true,
		"data": data4DataTable,
		"columns": [
			{ "data": "po_no" },
			{ "data": "po_rcv_no" },
			{
				"data": "nilai_penerimaan", "className": "text-right", 
				"render": function (data, type, row) {
					return accounting.formatNumber(row.nilai_penerimaan);
				},
			},
			{
				"data": "terbayar", "className": "text-right", 
				"render": function (data, type, row) {
					return accounting.formatNumber(row.terbayar);
				},
			},
			{
				"data": "potongan", "className": "text-right",
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-right input-potongan" id="potongan_${meta.row}" name="potongan_${meta.row}" data-index="${meta.row}" placeholder="Potongan" value="${row.potongan}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{
				"data": "bayar", "className": "text-right",
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-right input-bayar" id="bayar_${meta.row}" name="bayar_${meta.row}" data-index="${meta.row}" placeholder="Bayar" value="${row.bayar}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';
					
					actions = actions + `<button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light mr-2" onclick="matchPaymentDetail('${meta.row}');" data-index="${meta.row}" id="match_payment_${meta.row}" name="match_payment_${meta.row}"><i class="fas fa-coins"></i></button>`;

					if (Priv.delete_flag == 1) {
						actions = actions + `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetailPenerimaan('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
					}
					return actions;
				},
			},
		],
		"footerCallback": function (row, data, start, end, display) {
			let api = this.api();

			let totalPenerimaan = api.column(2).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(2).footer()).html(accounting.formatNumber(totalPenerimaan));
			$('#total_penerimaan').val(totalPenerimaan);

			let totalPenerimaanTerbayar = api.column(3).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(3).footer()).html(accounting.formatNumber(totalPenerimaanTerbayar));
			$('#total_penerimaan_terbayar').val(totalPenerimaanTerbayar);

			let totalPenerimaanPotongan = api.column(4).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(4).footer()).html(accounting.formatNumber(totalPenerimaanPotongan));
			$('#total_penerimaan_potongan').val(totalPenerimaanPotongan);

			let totalPenerimaanBayar = api.column(5).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(5).footer()).html(accounting.formatNumber(totalPenerimaanBayar));
			$('#total_penerimaan_bayar').val(totalPenerimaanBayar);
		},
	}).on('draw.dt', function (e, settings, json, xhr) {
		// ============= START input-potongan ===============

		$('.input-potongan').autoNumeric('init', formatNumber);
		
		$('.input-potongan').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			
			if (e.charCode === 13) {
				// Fokus ke bayar input
				if ($('#bayar_' + indexRow).length) {
					$('#bayar_' + indexRow).focus();
				}
			}
		});

		$('.input-potongan').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika bila input <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) < 0) {
				return;
			}

			_dataTable.cell(indexRow, 4).data(accounting.unformat($(this).val(), _decimalSeparator)).draw();
		});

		// ============= END input-potongan ===============

		// ============= START input-bayar ===============
		$('.input-bayar').autoNumeric('init', formatNumber);

		$('.input-bayar').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			
			if (e.charCode === 13) {
				_dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator)).draw();
				// Fokus ke potongan berikutnya
				indexRow++;
				if ($('#potongan_' + indexRow).length) {
					$('#potongan_' + indexRow).focus();
				} else {
					$('#simpan').focus();
				}
			}

		});

		$('.input-bayar').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika bila input <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) < 0) {
				return;
			}
			
			_dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator)).draw();
		});
		// ============= END input-bayar ===============
	});

}

function initDatePicker() {
	$('#doc_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			doc_date: {
				DateID: true,
				required: true,
			},
			supp_id: {
				required: true,
			},
			total_bayar: {
				required: true
			},
			coa_no: {
				required: true,
			},
			lampiran: {
            fileType: {
               types: ["jpg", "jpeg", "png", "bmp"]
            },
            maxFileSize: {
               "unit": "MB",
               "size": 2
            },
         },
		},
	});
}

function initOtherElements() {
	$('#total_bayar').autoNumeric('init', formatNumber);
}

function Kembali() {
	window.location.href = '#' + Modules + '/' + Controller;
} 

function Simpan() {
	// Check detail
	if (_dataTable.data().count() <= 0) {
		MsgBox.Notification('Detail tidak ditemukan');
		return;
	}
	// Check input
	if (!$('#form_input').valid()) {
		MsgBox.Notification('Periksa kembali inputan anda', 'Input tidak lengkap', 'warning');
		return;
	}
	// Check balance
	if (accounting.unformat($('#total_bayar').val(), _decimalSeparator) <= 0 || accounting.unformat($('#total_bayar').val(), _decimalSeparator) !== accounting.unformat($('#total_penerimaan_bayar').val(), _decimalSeparator)) {
		MsgBox.Notification('Periksa kembali total bayar dan alokasi pembayaran', 'Peringatan', 'warning');
		return;
	}
	// Cek detail
	let rowLength = _dataTable.rows().data().toArray().length - 1;
	let flagBlock = false;
	_dataTable.rows().data().toArray().filter((value, index) => {
		if ((parseFloat(value.terbayar) + parseFloat(value.potongan) + parseFloat(value.bayar)) > parseFloat(value.nilai_penerimaan) || (parseFloat(value.terbayar) + parseFloat(value.potongan) + parseFloat(value.bayar)) <= 0) {
			flagBlock = true;
			MsgBox.Notification('Periksa kembali pembayaran dokumen nomor : ' + value.po_rcv_no, 'Peringatan', 'warning', true);
			return;
		}
		
		if (index === rowLength && !flagBlock) {
			saveData();
			flagBlock = false;
		}
	});
}

function saveData() {
	// save
	MsgBox.Confirm('Yakin akan simpan data ini?').then(result => {
		if (!result) return;
		loadingProcess();
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		let form = $('#form_input')[0];
		let formData = new FormData(form);
		formData.append("id", _id);
		formData.append("detail", JSON.stringify(_dataTable.rows().data().toArray()));
		contentType = null;
		_data2Send = formData;
		ajaxNew(url, _data2Send, 'POST', null)
			.then((data) => {
				loadingProcess(false);
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					$('#doc_no').val(data.data.no_doc);
					_id = data.data.no_doc;
					action = 'edit';
					getData();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				loadingProcess(false);
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function getData() {
	let url = base_url + Modules + '/' + Controller + '/getData2Edit/json/' + _id;
	loadingProcess();
	ajaxNew(url, null, 'GET')
		.then((data) => {
			loadingProcess(false);
			if (data.result) {
				let row = data.data;
				$('#doc_no').val(row.no_doc);
				$('#doc_date').datepicker("setDate", moment(row.tgl_trans).format('DD/MM/YYYY'));
				$('#supp_id').val(row.supp_id); $('#supp_name').val(row.supp_id + ' - ' + row.supp_name);
				selectedSupplierId = supp_id;
				$('#LOVSupplierButton').hide(); $('#supp_name').addClass('disable');
				$('#statusDocumentContainer').html(getStatus(row.status));
				$('#total_bayar').val(accounting.formatNumber(row.total_bayar));
				$('#coa_no').val(row.coa_no); $('#coa_name').val(row.coa_no + ' - ' + row.coa_name);
				selectedCOACode = row.coa_no;
				$('#keterangan').val(row.keterangan);
				if (row.lampiran_base64) {
					$("#lampiran_preview_container").show();
					$('#lampiran_preview').attr('src', row.lampiran_base64);
				}
				data4DataTable = data.detail;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.detail).draw();
				}, 500);
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

function getStatus(status) {
	if (status == 0) {
		return '<span class="badge badge-info text-center">Baru</span>';
	} else if (status == 1) {
		return '<span class="badge badge-success text-center">Selesai</span>';
	} else {
		return '<span class="badge badge-danger text-center">Batal</span>';
	}
}

function LOVCOA() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#coa_no', '#coa_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedCOACode == $('#coa_no').val() || !$('#coa_no').val()) return;
			selectedCOACode = $('#coa_no').val();
			$('#coa_name').val(selectedCOACode + ' - ' + $('#coa_name').val());
		});
	});
}

function LOVSupplier() {
	$('#PopUpModal').load(base_url + 'inventory/purchaseorder/get_supplier_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['supp_id', 'supp_name']);
		$('#list_controls').val(['#supp_id', '#supp_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedSupplierId == $('#supp_id').val() || !$('#supp_id').val()) return;
			if (selectedSupplierId != $('#supp_id').val()) {
				_dataTable.clear().draw();
			}
			selectedSupplierId = $('#supp_id').val();
		});
	});
}

function LOVPickDetailPenerimaan() {
	if (!$('#supp_id').val() || !selectedSupplierId) {
		MsgBox.Notification('Pilih supplier terlebih dahulu', 'Peringatan', 'warning');
		return;
	}

	let jsonWhere = encodeURIComponent(JSON.stringify({ supp_id: $('#supp_id').val() }));
	$('#PopUpModal').load(base_url + 'inventory/penerimaan_po/get_penerimaan_pembayaran_lov/home/' + jsonWhere, () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['po_rcv_no']);
		$('#list_controls').val(['#no_doc_ref_penerimaan']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#no_doc_ref_penerimaan').val()) return;
			getDetailPenerimaan($('#no_doc_ref_penerimaan').val());
			$('#no_doc_ref_penerimaan').val(null)
		});
	});
}

function getDetailPenerimaan(no_doc = null) {
	ajaxNew(base_url + 'inventory/penerimaan_po/get_penerimaan?supp_id=' + $('#supp_id').val() + '&no_doc=' + no_doc, null, 'GET')
		.then((data) => {
			if (data.result) {
				let row = null;
				row = _dataTable.rows().data().toArray().filter(value => {
					return no_doc == value.po_rcv_no;
				});

				if (row.length > 0) {
					MsgBox.Notification('No. Penerimaan ' + no_doc + ' sudah ada', 'Peringatan', 'warning', true);
					return;
				}

				data4DataTable.push(data.data);
				setTimeout(() => {	
					_dataTable.rows.add(data.data).draw();
					setTimeout(() => {
						_dataTable.rows().invalidate().draw();
					}, 300)
				}, 500);
				$('#no_doc_ref').val(null);
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

// ======= START DATATABLE

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}

// ======= END DATATABLE

function deleteDetailPenerimaan(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Yakin akan hapus ' + getData.no_doc + ' ini dari detail?', 'Hapus detail').then(result => {
		HapusRowDataTable(_dataTable, RowIdx);
	}).catch(err => {
		if (err) console.log(err);
	});
}

function matchPaymentDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();
	let alokasiPembayaran = parseFloat(getData.nilai_penerimaan) - (parseFloat(getData.terbayar) + parseFloat(getData.potongan));
	_dataTable.cell(RowIdx, 5).data(alokasiPembayaran);
	_dataTable.rows().invalidate().draw();
}