(function () {
	// construct
	$("#lampiran").change(function () {
		if (this.files && this.files[0]) {
			$("#lampiran_preview_container").show();
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#lampiran_preview').attr('src', e.target.result);
			}
			reader.readAsDataURL(this.files[0]);
		}
	});

})();

function initPage() {
	initDataTable();
	initDatePicker();
	initValidation();
	initOtherElements();
	if (action == 'edit') getData();
}

function initDataTable() {
	_dataTable = $('#table_detail').DataTable({
		"pagingType": "simple",
		"iDisplayLength": -1,
		"oLanguage": {
			"sLengthMenu": `<button type="button" class="btn btn-outline-success waves-effect waves-light" id="addInvoice" name="addInvoice" onclick="LOVPickDetail();"><i class="fas fa-plus"></i> Transaksi</button>`,
			"oPaginate": {
				"sFirst": "", // This is the link to the first page
				"sPrevious": "", // This is the link to the previous page
				"sNext": "", // This is the link to the next page
				"sLast": "" // This is the link to the last page
			}
		},
		"ordering": false,
		"info": false,
		"scrollX": true,
		"data": data4DataTable,
		"columns": [
			{ "data": "no_doc" },
			{
				"data": "tgl_jatuh_tempo", "className": "text-center", "width": 50,
				"render": function (data, type, row) {
					return moment(row.tgl_jatuh_tempo).format('DD/MM/YYYY');
				},
			},
			{ "data": "type_payment" },
			{
				"data": "grand_total", "className": "text-right", 
				"render": function (data, type, row) {
					return accounting.formatNumber(row.grand_total);
				},
			},
			{
				"data": "terbayar", "className": "text-right", 
				"render": function (data, type, row, meta) {
					return accounting.formatNumber(row.terbayar);
				},
			},
			{
				"data": "potongan", "className": "text-right", 
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-right input-potongan" id="potongan_${meta.row}" name="potongan_${meta.row}" data-index="${meta.row}" placeholder="Potongan" value="${row.potongan}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{
				"data": "pembayaran", "className": "text-right", 
				"render": function (data, type, row, meta) {
					return `<input type="text" class="form-control text-right input-pembayaran" id="pembayaran_${meta.row}" name="pembayaran_${meta.row}" data-index="${meta.row}" placeholder="Pembayaran" value="${row.pembayaran}" onfocus="this.select()" autocomplete="off" />`;
				},
			},
			{
				"data": null, "className": "text-center", "width": 80,
				"render": function (data, type, row, meta) {
					let actions = '';

					actions = actions + `<button type="button" class="btn btn-outline-primary btn-sm waves-effect waves-light mr-2" onclick="matchPaymentDetail('${meta.row}');" data-index="${meta.row}" id="match_payment_${meta.row}" name="match_payment_${meta.row}"><i class="fas fa-coins"></i></button>`;
					
					if (Priv.delete_flag == 1) {
						actions = actions + `<button type="button" class="btn btn-outline-danger btn-sm waves-effect waves-light" onclick="deleteDetail('${meta.row}');" data-index="${meta.row}" id="delete_${meta.row}" name="delete_${meta.row}"><i class="glyphicon glyphicon-trash"></i></button>`;
					}
					return actions;
				},
			},
		],
		"footerCallback": function (row, data, start, end, display) {
			let api = this.api();

			let totalTransaksi = api.column(3).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(3).footer()).html(accounting.formatNumber(totalTransaksi));
			$('#total_transaksi').val(totalTransaksi);

			let totalTerbayar = api.column(4).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(4).footer()).html(accounting.formatNumber(totalTerbayar));
			$('#total_terbayar').val(totalTerbayar);

			let totalPotongan = api.column(5).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(5).footer()).html(accounting.formatNumber(totalPotongan));
			$('#total_potongan').val(totalPotongan);

			let totalAlokasiPembayaran = api.column(6).data().reduce(function (prevVal, nextVal) {
				return parseInt(prevVal) + parseInt(nextVal);
			}, 0);
			$(api.column(6).footer()).html(accounting.formatNumber(totalAlokasiPembayaran));
			$('#total_alokasi_pembayaran').val(totalAlokasiPembayaran);
		},

	}).on('draw.dt', function (e, settings, json, xhr) {
		// ============= START input-potongan ===============

		$('.input-potongan').autoNumeric('init', formatNumber);
		
		$('.input-potongan').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			
			if (e.charCode === 13) {
				_dataTable.rows().draw();
				// Fokus ke pembayaran input
				if ($('#pembayaran_' + indexRow).length) {
					$('#pembayaran_' + indexRow).focus();
				}
			}
		});

		$('.input-potongan').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika bila input <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) < 0) {
				return;
			}

			_dataTable.cell(indexRow, 5).data(accounting.unformat($(this).val(), _decimalSeparator));
			_dataTable.rows().invalidate().draw();
		});

		// ============= END input-potongan ===============

		// ============= START input-pembayaran ===============
		$('.input-pembayaran').autoNumeric('init', formatNumber);

		$('.input-pembayaran').on('keypress', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			
			if (e.charCode === 13) {
				_dataTable.rows().draw();
				// Fokus ke potongan berikutnya
				indexRow++;
				if ($('#potongan_' + indexRow).length) {
					$('#potongan_' + indexRow).focus();
				} else {
					$('#simpan').focus();
				}
			}

		});

		$('.input-pembayaran').on('blur', function (e) {
			e.preventDefault();
			let indexRow = $(this).data('index');
			// check jika bila input <= 0
			if (accounting.unformat($(this).val(), _decimalSeparator) < 0) {
				return;
			}
			
			_dataTable.cell(indexRow, 6).data(accounting.unformat($(this).val(), _decimalSeparator));
			_dataTable.rows().invalidate().draw();
		});
		// ============= END input-pembayaran ===============
	});
}

function initDatePicker() {
	$('#doc_date').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initValidation() {
	$("#form_input").validate({
		rules: {
			doc_date: {
				DateID: true,
				required: true,
			},
			coa_no: {
				required: true,
			},
			cust_id: {
				required: true,
			},
			total_pembayaran: {
				required: true
			},
			lampiran: {
            fileType: {
               types: ["jpg", "jpeg", "png", "bmp"]
            },
            maxFileSize: {
               "unit": "MB",
               "size": 2
            },
         },
		},
	});
}

function initOtherElements() {
	$('#total_pembayaran').autoNumeric('init', formatNumber);
}

function Kembali() {
	window.location.href = '#' + Modules + '/' + Controller;
} 

function Simpan() {
	// Check detail
	if (_dataTable.data().count() <= 0) {
		MsgBox.Notification('Detail tidak ditemukan');
		return;
	}
	// Check input
	if (!$('#form_input').valid()) {
		MsgBox.Notification('Periksa kembali inputan anda', 'Input tidak lengkap', 'warning');
		return;
	}
	// Check balance
	if (accounting.unformat($('#total_alokasi_pembayaran').val(), _decimalSeparator) != accounting.unformat($('#total_pembayaran').val(), _decimalSeparator)) {
		MsgBox.Notification('Periksa kembali total pembayaran dan alokasi pembayaran', 'Peringatan', 'warning');
		return;
	}
	// Cek detail
	let rowLength = _dataTable.rows().data().toArray().length - 1;
	let flagBlock = false;
	_dataTable.rows().data().toArray().filter((value, index) => {
		if ((parseFloat(value.terbayar) + parseFloat(value.potongan) + parseFloat(value.pembayaran)) > parseFloat(value.grand_total) || (parseFloat(value.terbayar) + parseFloat(value.potongan) + parseFloat(value.pembayaran)) <= 0) {
			flagBlock = true;
			MsgBox.Notification('Periksa kembali pembayaran transaksi : ' + value.no_doc, 'Peringatan', 'warning', true);
			return;
		}
		
		if (index === rowLength && !flagBlock) {
			saveData();
			flagBlock = false;
		}
	});
}

function saveData() {
	// save
	MsgBox.Confirm('Yakin akan simpan data ini?').then(result => {
		if (!result) return;
		let url = action === 'create' ? base_url + Modules + '/' + Controller + '/save/' : base_url + Modules + '/' + Controller + '/update/';
		let form = $('#form_input')[0];
		let formData = new FormData(form);
		formData.append("id", _id);
		formData.append("detail", JSON.stringify(_dataTable.rows().data().toArray()));
		contentType = null;
		_data2Send = formData;
		loadingProcess();
		ajaxNew(url, _data2Send, 'POST', null)
			.then((data) => {
				loadingProcess(false);
				if (data.result) {
					MsgBox.Notification(data.message.toString());
					$('#doc_no').val(data.data.no_doc);
					_id = data.data.no_doc;
					action = 'edit';
					getData();
				} else {
					MsgBox.Notification(data.message.toString());
				}
			})
			.catch((err) => {
				loadingProcess(false);
				MsgBox.Notification(err.toString());
			});
	}).catch(err => {
		console.log(err);
	});
}

function getData() {
	let url = base_url + Modules + '/' + Controller + '/getData2Edit/json/' + _id;
	loadingProcess();
	ajaxNew(url, null, 'GET')
		.then((data) => {
			loadingProcess(false);
			if (data.result) {
				let row = data.data;
				$('#doc_no').val(row.no_doc);
				$('#doc_date').datepicker("setDate", moment(row.tgl_trans).format('DD/MM/YYYY'));
				$('#cust_id').val(row.cust_id); $('#cust_name').val(row.cust_id + ' - ' + row.cust_name);
				selectedCustomerId = row.cust_id;
				$('#LOVCustomerButton').hide(); $('#cust_name').addClass('disable');
				$('#statusDocumentContainer').html(getStatus(row.status));
				$('#total_pembayaran').val(accounting.formatNumber(row.total_pembayaran));
				$('#coa_no').val(row.coa_no); $('#coa_name').val(row.coa_no + ' - ' + row.coa_name);
				selectedCOACode = row.coa_no;
				$('#keterangan').val(row.keterangan);
				if (row.lampiran_base64) {
					$("#lampiran_preview_container").show();
					$('#lampiran_preview').attr('src', row.lampiran_base64);
				}
				data4DataTable = data.detail;
				_dataTable.clear().draw();
				setTimeout(() => {
					_dataTable.rows.add(data.detail).draw();
				}, 500);
			} else {
				loadingProcess(false);
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

function getStatus(status) {
	if (status == 0) {
		return '<span class="badge badge-info text-center">Baru</span>';
	} else if (status == 1) {
		return '<span class="badge badge-success text-center">Selesai</span>';
	} else {
		return '<span class="badge badge-danger text-center">Batal</span>';
	}
}

function LOVCOA() {
	$('#PopUpModal').load(base_url + 'setting/coa/get_coa_detail_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['code', 'name']);
		$('#list_controls').val(['#coa_no', '#coa_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#coa_no').val()) return;
			if (selectedCOACode == $('#coa_no').val() || !$('#coa_no').val()) return;
			selectedCOACode = $('#coa_no').val();
			$('#coa_name').val(selectedCOACode + ' - ' + $('#coa_name').val());
		});
	});
}

function LOVCustomer() {
	$('#PopUpModal').load(base_url + 'master/member/get_member_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['cust_id', 'cust_name']);
		$('#list_controls').val(['#cust_id', '#cust_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedCustomerId == $('#cust_id').val() || !$('#cust_id').val()) return;
			if (selectedCustomerId != $('#cust_id').val()) {
				_dataTable.clear().draw();
			}
			selectedCustomerId = $('#cust_id').val();
			$('#cust_name').val(selectedCustomerId + ' - ' + $('#cust_name').val());
		});
	});
}

function LOVPickDetail() {
	if (!$('#cust_id').val() || !selectedCustomerId) {
		MsgBox.Notification('Pilih member terlebih dahulu', 'Peringatan', 'warning');
		return;
	}

	$('#PopUpModal').load(base_url + 'marketplace/pos/get_transaksi_member_lov/home?cust_id=' + $('#cust_id').val(), () => { // Ambil URL untuk membuka modal LOV
		$(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['no_doc']);
		$('#list_controls').val(['#no_doc_ref']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#no_doc_ref').val()) return;
			getDetailTransaksi($('#no_doc_ref').val());
		});
	});
}

function getDetailTransaksi(no_doc = null) {
	ajaxNew(base_url + 'marketplace/pos/get_transaksi_member?no_doc=' + no_doc, null, 'GET')
		.then((data) => {
			if (data.result) {
				let row = null;
				row = _dataTable.rows().data().toArray().filter(value => {
					return no_doc == value.no_doc;
				});

				if (row.length > 0) {
					MsgBox.Notification('No. Transaksi ' + no_doc + ' sudah ada', 'Peringatan', 'warning', true);
					return;
				}

				data4DataTable.push(data.data);
				setTimeout(() => {
					_dataTable.rows.add(data.data).draw();
					setTimeout(() => {
						_dataTable.rows().invalidate().draw();
					}, 300)
				}, 500);
				$('#no_doc_ref').val(null);
			} else {
				MsgBox.Notification(data.message.toString());
			}
		})
		.catch((err) => {
			MsgBox.Notification(err.toString());
		});
}

// ======= START DATATABLE

function SimpanRowDataTable(DataTableElement, data2save, RowIdx = null) {
	if (DataTableAction == 'edit') {
		DataTableElement.row(RowIdx).data(data2save);
		DataTableElement.rows().invalidate().draw();
	} else {
		DataTableElement.row.add(data2save).draw();
	}
}

function HapusRowDataTable(DataTableElement, RowIdx = null) {
	DataTableElement.row(RowIdx).remove().draw();
	DataTableElement.rows().invalidate().draw();
}

// ======= END DATATABLE

function deleteDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();

	MsgBox.Confirm('Yakin akan hapus ' + getData.no_doc + ' ini dari detail?', 'Hapus detail').then(result => {
		HapusRowDataTable(_dataTable, RowIdx);
	}).catch(err => {
		if (err) console.log(err);
	});
}

function matchPaymentDetail(RowIdx) {
	let getData = _dataTable.row(RowIdx).data();
	let alokasiPembayaran = parseFloat(getData.grand_total) - (parseFloat(getData.terbayar) + parseFloat(getData.potongan));
	_dataTable.cell(RowIdx, 6).data(alokasiPembayaran);
	_dataTable.rows().invalidate().draw();
}