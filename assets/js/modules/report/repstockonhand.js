(function () {
	// Construct
})();

function initPage(){
	initOtherElements();
}

function initOtherElements() {
	$('#btnRepPdf').on('click', function () {
		app_pdf();
	});

	$('#btnRepXls').on('click', function () {
		app_xls();
	});
}

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function LOVItemStart() {
	$('#PopUpModal').load(base_url + 'inventory/purchaseorder/get_item_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-lg modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['item_id', 'item_name']);
		$('#list_controls').val(['#item_id_start', '#item_name_start']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#item_id_start').val()) return;
			$('#item_name_start').val($('#item_id_start').val() + ' - ' + $('#item_name_start').val());
      });
	});
}

function LOVItemEnd() {
	$('#PopUpModal').load(base_url + 'inventory/purchaseorder/get_item_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-lg modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['item_id', 'item_name']);
		$('#list_controls').val(['#item_id_end', '#item_name_end']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (!$('#item_id_end').val()) return;
			$('#item_name_end').val($('#item_id_end').val() + ' - ' + $('#item_name_end').val());
      });
	});
}