(function () {
	// Construct
})();

function initPage(){
	initOtherElements();

	$('#tanggal_periode').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
	}).datepicker("setDate", new Date());
}

function initOtherElements() {
	$('#btnRepPdf').on('click', function () {
		app_pdf();
	});

	$('#btnRepXls').on('click', function () {
		app_xls();
	});
}

function app_pdf() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/pdf/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function app_xls() {
	$('#form_filter').prop('action', base_url + Modules + '/' + Controller + '/xls/');
  	$('#form_filter').prop('target', '_blank');
  	$('#form_filter').prop('method', 'POST');
  	document.getElementById('form_filter').submit();
}

function LOVEmployee() {
	$('#PopUpModal').load(base_url + 'master/karyawan/get_karyawan_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['employee_id', 'employee_name']);
		$('#list_controls').val(['#employee_id', '#employee_name']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedKaryawanId == $('#employee_id').val() || !$('#employee_id').val()) return;
			selectedKaryawanId = $('#employee_id').val();
			$('#employee_name').val(selectedKaryawanId + ' - ' + $('#employee_name').val());
		});
	});
}

function LOVEmployeeClear() {
	$('#employee_id').val(null); $('#employee_name').val(null);
	selectedKaryawanId = null;
}

function LOVMember() {
	$('#PopUpModal').load(base_url + 'master/member/get_member_lov/', () => { // Ambil URL untuk membuka modal LOV
		// $(".modal-dialog").css("max-width", "70%");
		$(".modal-dialog").addClass("modal-xl modal-dialog-centered");
		$('#ModalLOV').modal('show'); // Tampilkan modal LOV
		$('#list_cols').val(['cust_id', 'cust_name', 'type_member_id', 'type_member_name', 'level']);
		$('#list_controls').val(['#cust_id', '#cust_name', '#type_member_id', '#cust_type', '#cust_type_lvl']);

		$('#ModalLOV').on('hidden.bs.modal', function () {
			if (selectedCustomerId == $('#cust_id').val() || !$('#cust_id').val()) return;
			selectedCustomerId = $('#cust_id').val();
			$('#cust_name').val(selectedCustomerId + ' - ' + $('#cust_name').val());
		});
	});
}

function LOVMemberClear() {
	$('#cust_id').val(null); $('#cust_name').val(null);
	selectedCustomerId = null;
}