(function () {
	// JS construct
	var $ul = $("#side-menu");
	var $links = $ul.find('a');
    $links.each(function(ind, a){
        var $a = $(a);
//                If this menu item has nested menu and it does not have toggle icon yet we add toggle icon
        if ($a.next('ul').length > 0 && $a.find('.menu-item-toggle-icon').length === 0){
//                    If this menu item is parent menu item
            if ($a.closest('ul').parent().closest('ul').length === 0){
                $a.append($('<i class="has-arrow waves-effect" style="float:right;"></i>'));
            }else{
                $a.append($('<i class="has-arrow waves-effect" style="float:right;"></i>'));
            }
        }
    });
	// Tambahkan Jquery validator untuk larangan spasi/whitespace
	$.validator.addMethod("nowhitespace", function (value, element) {
		return this.optional(element) || /^\S+$/i.test(value);
	}, "Spasi tidak diperbolehkan");
	// Huruf kecil dan simbol saja
	$.validator.addMethod('lowercasesymbols', function (value) {
		return value.match(/^[a-z\[!@# $%&*\/?=_.,:;\\\]"-]+$/);
	}, "Hanya huruf kecil dan simbol diperbolehkan");
	// Waktu 24 jam hh:mm
	$.validator.addMethod("time24hhmm", function(value, element) { 
		if (!/^\d{2}:\d{2}$/.test(value)) return false;
		var parts = value.split(':');
		if (parts[0] > 23 || parts[1] > 59) return false;
		return true;
	}, "Harap masukkan waktu yang dengan benar.");
	// Waktu 24 jam hh:mm:ss
	$.validator.addMethod("time24hhmmss", function(value, element) { 
		if (!/^\d{2}:\d{2}:\d{2}$/.test(value)) return false;
		var parts = value.split(':');
		if (parts[0] > 23 || parts[1] > 59 || parts[2] > 59) return false;
		return true;
	}, "Harap masukkan waktu yang dengan benar.");
	// Format waktu indonesia
	$.validator.addMethod("DateID", function(value, element) {
		return this.optional(element) || moment(value,"DD/MM/YYYY").isValid();
	}, "Format tanggal yang benar adalah DD/MM/YYYY");
	// Tambahkan Jquery validator untuk tidak mengecek input yang memiliki class ignore
	$.validator.setDefaults({ ignore: ".ignore" });
	$.validator.setDefaults({
		ignore: ".ignore",
		errorElement: 'em',
      errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
      }
	});
	// Ganti default message jquery validator
	$.extend($.validator.messages, {
		lowercasesymbols: "Hanya huruf kecil dan simbol diperbolehkan",
		nowhitespace: "Spasi tidak diperbolehkan.",
		required: "Field ini wajib terisi.",
		remote: "Harap perbaiki field ini.",
		email: "Harap masukkan email dengan benar.",
		url: "Harap masukkan alamat dengan benar.",
		date: "Harap masukkan tanggal dengan benar.",
		dateISO: "Harap masukkan tanggal dengan benar (ISO).",
		number: "Harap masukkan angka yang dengan benar.",
		digits: "Harap masukkan hanya digit.",
		creditcard: "Harap masukkan nomor kartu kredit yang dengan benar.",
		equalTo: "Masukkan kembali inputan yang sama.",
		accept: "Masukkan value dengan ekstensi yang benar.",
		maxlength: jQuery.validator.format("Harap masukkan tidak lebih dari {0} karakter."),
		minlength: jQuery.validator.format("Masukkan setidaknya {0} karakter."),
		rangelength: jQuery.validator.format("Masukkan panjang karakter antara {0} dan {1}."),
		range: jQuery.validator.format("Masukkan nilai antara {0} dan {1}."),
		max: jQuery.validator.format("Masukkan nilai kurang atau sama dengan {0}."),
		min: jQuery.validator.format("Masukkan nilai lebih atau sama dengan {0}."),
		fileType: function({ types }) {
			let ekstensi = "";
			$.each(types, function (key, value) {
				ekstensi = key == 0 ? value : ekstensi + ", " + value;
			});
			return jQuery.validator.format("Extensi file yang diperbolehkan: " + ekstensi + ".");
		},
		maxFileSize: function ({ size, unit }) {
			return jQuery.validator.format(`File tidak boleh lebih besar dari ${size}${unit}`);
		}
	});
	// Ajax pace
	$(document).ajaxStart(function () {
		Pace.restart();
	});
	initSelect2Ajax("#select_mata_pelajaran", "- Ubah Mata Pelajaran -", base_url + 'administrasi/info/getListMataPelajaranSelect2/', ['role_id', 'role_name'], null ,false, "select_store");
	// $("#select_mata_pelajaran").select2("container").addClass("select_store");

	$("#select_mata_pelajaran").on("change", function ()  {
		// console.log("change");
		let mata_id = $(this).val();
		SendAjax(base_url + 'app/App/changeMataPelajaran/json/'+mata_id, null, 'POST', 'JSON', 200000000, changeMataPelajaran);
	});
})();
function changeMataPelajaran(data) {
	location.reload();
}
var MsgBox = {
	Notification: function(notifMsg = '', notifPosition = 'bottom right', notifType = 'info', notifSize = 'normal', notifDelay = 5000) {
		Swal.fire({
            title: notifMsg,
            text: "",
            type: "info",
            confirmButtonColor: "#3eb7ba"
        })
	},
	Confirm: function(MsgConfirm = '', confirmTitle = 'Confirmation') {
		return new Promise(function(resolve, reject) {
			Swal.fire({
			  title: confirmTitle,
			  text: MsgConfirm.toString(),
			  type: 'info',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  cancelButtonText: 'Cancel'
			}).then((result) => {
			  if (result.value) {
			    resolve(true);
			  }
			})
		});
	},
	ConfirmNew: function(MsgConfirm = '', confirmTitle = 'Confirmation') {
		return new Promise(function(resolve, reject) {
			Swal.fire({
			  title: confirmTitle,
			  text: MsgConfirm.toString(),
			  type: 'info',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes',
			  cancelButtonText: 'Cancel'
			}).then((result) => {
			  if (result.value) {
			    resolve(true);
			  }
			  if (result.dismiss == 'cancel') {
				resolve(false);
			  }
			})
		});
	},
};

function loadingProcess(showProcess = true, titleMessage = "Mohon menunggu", messageHtml = "Memproses data...", messageTimer = null) {
	if (showProcess) {
		Swal.fire({
				title: titleMessage,
				html: messageHtml,
				timer: messageTimer,
				confirmButtonColor: "#7a6fbe",
				allowOutsideClick: false,
				onBeforeOpen: function() {
					Swal.showLoading();
				},
		});
	} else {
		Swal.close();
	}
}

(function () {
	StartUp();
})();

function StartUp() {
	CurrencyFormat();
}

function setCurrMenuID(MenuID) {
	CurrMenuID = MenuID;
}

function FetchWithTimeout(ReqURL, dataToSend = null, FetchMethod = 'POST', ms = 30000) {
  return new Promise((resolve, reject) => {

    const timeoutId = setTimeout(() => {
      reject(new Error("Waktu permintaan habis, cek koneksi internet anda."));
    }, ms);
		
		fetch(ReqURL, {
      method: FetchMethod,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: dataToSend,
    })
		.then(response => response.json())
		.then((res) => {
        clearTimeout(timeoutId);
        resolve(res);
    }).catch((err) => {
      clearTimeout(timeoutId);
      reject(err);
    });

  });
}
function SendAjax(SendUrl, DataSend, AjaxMethod, DataType, SetTimeout = 30000, successCallback){
	$.ajax({
		url: SendUrl,
		type: AjaxMethod,
		dataType: DataType,
		data: DataSend,
		timeout: SetTimeout,
    	success: successCallback,
    	error: function(req, errorThrown) {
            console.log(errorThrown);
        }
	});
}

function SendAjaxWithUpload(SendUrl, DataSend, AjaxMethod, DataType, SetTimeout = 30000, successCallback){
	$.ajax({
		url: SendUrl,
		type: AjaxMethod,
		dataType: DataType,
		data: DataSend,
		timeout: SetTimeout,
		cache: false,
	    processData: false,
    	contentType: false,
    	success: successCallback,
    	error: function(xhr, textStatus, errorThrown) {
            console.log('error');
        }
	})
}

function ajaxNew(ReqURL, dataToSend = null, method = 'POST', contentType = 'application/x-www-form-urlencoded', dataType = 'json', timeout = 30000, cache = true) {
   return new Promise(function (resolve, reject) {
      let headers = {
         "X-Requested-With": "XMLHttpRequest"
      };

      let options = {
         url: ReqURL,
         type: method,
         contentType: contentType, // application/x-www-form-urlencoded, multipart/form-data, text/plain
         dataType: dataType,
         data: dataToSend,
         cache: cache,
         crossDomain: true,
         processData: false,
         timeout: timeout,
         headers: headers
      }

      if (contentType != null) {
         options["contentType"] = contentType; // application/x-www-form-urlencoded, multipart/form-data, text/plain
      } else {
         options["contentType"] = false;
      }

      $.ajax(options)
         .done(data => {
            resolve(data);
         }).fail(err => {
            reject(err.status + ' ' + err.statusText)
         });
   });
}

function FetchWithTimeoutNew(ReqURL, dataToSend = null, FetchMethod = 'POST', ms = 30000) {
	return new Promise((resolve, reject) => {
 
	  const timeoutId = setTimeout(() => {
		 reject(new Error("Waktu permintaan habis, cek koneksi internet anda."));
	  }, ms);
		 
		 fetch(ReqURL, {
		 method: FetchMethod,
		 body: dataToSend,
	  })
		 .then(response => response.json())
		 .then((res) => {
			clearTimeout(timeoutId);
			resolve(res);
	  }).catch((err) => {
		 clearTimeout(timeoutId);
		 reject(err);
	  });
 
	});
 }

function CurrencyFormat() {
  accounting.settings = {
    currency: {
      symbol : "",   // default currency symbol is '$'
      format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
      decimal : ",",  // decimal point separator
      thousand: ".",  // thousands separator
      precision : 0   // decimal places
    },
    number: {
      precision : 0,  // default precision on numbers is 0
      thousand: ".",
      decimal : ","
    }
  }  
}

function CvtForm2JSON(frm) {
  var jsonData = {};
   
  var formData = $(frm).serializeArray();
   
  $.each(formData, function() {
    if (jsonData[this.name]) {
      if (!jsonData[this.name].push) {
           jsonData[this.name] = [jsonData[this.name] || ""];
      }
      jsonData[this.name].push(this.value || "");

    } else {
       jsonData[this.name] = this.value || "";
    }
  });

  return JSON.stringify(jsonData);
}

function ValidasiInput(ListInputByID) {
	// Jika ada input yang kosong maka kembali false
  let hasil;
  for(var i = 0; i < ListInputByID.length; i++) {
    if ($("#"+ListInputByID[i]).val() != "") {
      hasil = true;
    } else {
      hasil = false;
      break;
    }
  }
  return hasil;
}

function ValidasiInput2(ListInputByID) {
	var hasil;
	for(var i = 0; i < ListInputByID.length; i++) {
	  if ($("#"+ListInputByID[i]).val().trim() != "") {
		 hasil = true;
	  } else {
		 hasil = $("#"+ListInputByID[i]).attr("placeholder");
		 break;
	  }
	}
	return hasil;
 }

function ValidasiInputReverse(ListInputByID) {
  // Jika ada input yang tidak kosong maka kembali true
  var hasil;
  for(var i = 0; i < ListInputByID.length; i++) {
    if ($("#"+ListInputByID[i]).val().trim() != "") {
      hasil = true;
      break;
    }
  }
  return hasil;
}

function isObjectEmpty(obj) {
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
}

function clearValidation(element = '') {
	$(element).validate().resetForm(); // Clear jquery validation
	// Start remove class validation
	$('.form-group').each(function () { $(this).removeClass('has-success'); });
	$('.form-group').each(function () { $(this).removeClass('has-error'); });
	$('.form-group').each(function () { $(this).removeClass('has-feedback'); });
	$('.form-group').each(function () { $('input').removeClass('is-invalid'); });
	$('.help-block').each(function () { $(this).remove(); });
	$('.form-control-feedback').each(function () { $(this).remove(); });
	// End remove class validation
}

var formatNumber = {
	aSep: '.', 
	aDec: ',',
	lZero: false,
	aPad: false,
	mDec: '0',
	vMin: '-999999999999999', 
	vMax: '999999999999999'
};

var formatDecimalNumber = {
	aSep: '.', 
	aDec: ',',
	lZero: false,
	aPad: false,
	mDec: '2',
	vMin: '-999999999999999', 
	vMax: '999999999999999'
};

function kFormatter(num) {
	return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(1)) + 'k' : Math.sign(num) * Math.abs(num)
}

const xah_format_number = ((n, m = 1) => {
	/* [
	format number with metric prefix, example: 1.2 k
	n is integer. The number to be converted
	m is integer. The number of decimal places to show. Default to 1.
	returns a string, with possibly one of k M G T ... suffix.
	
	http://xahlee.info/js/javascript_format_number.html
	version 2019-04-15
	 ] */
	const prefix = ["", " k", " M", " G", " T", " P", " E", " Z", " Y", " * 10^27", " * 10^30", " * 10^33"]; // should be enough. Number.MAX_VALUE is about 10^308
	let ii = 0;
	while ((n = n / 1000) >= 1) { ii++; }
	return (n * 1000).toFixed(m) + prefix[ii];
});

function initSelect2Ajax(element, placeholder = "", url, valueArr = ['id', 'text'], minimumInputLength = 3) {
	$(element).select2({
		width:'100%',
		// placeholder: placeholder,
		// // minimumInputLength: 3,
		// allowClear: true,
		// ajax: { 
		// 	url: base_url + 'sales/pricegrade/getPriceGradeSelect2',
		// 	type: "POST",
		// 	dataType: 'json',
		// 	delay: 500,
		// 	data: function (term, page) {
		// 		return JSON.stringify({
		// 			keyword: term
		// 		});
		// },
		// 	processResults: function (data, page) { // parse the results into the format expected by Select2.
		// 		return { 
		// 			results: $.map(data, function (item) {
		// 				return {
		// 					margin: item.margin,
		// 					text: item.price_grade_name,
		// 					id: item.price_grade_code
		// 				}
		// 		  }) 
		// 		};
		// 	},
		// 	cache: true

		placeholder: placeholder,
		minimumInputLength: minimumInputLength,
		allowClear: true,
		ajax: { 
			url: url,
			type: "POST",
			dataType: 'json',
			delay: 500,
			data: function (term, page) {
				return JSON.stringify({
					keyword: term
				});
			},
			processResults: function (data, page) { // parse the results into the format expected by Select2.
				return { 
					results: $.map(data, function (item) {
						console.log(data);
						return {
							id: item[valueArr[0]],
							text: item[valueArr[1]]
						}
				  }) 
				};
			},
			cache: true
		},
	});
}